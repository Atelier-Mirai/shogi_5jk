# 五級位上昇を目指す会 Webサイト

## 参考書籍
* Ruby on Rails チュートリアル
* パーフェクト Ruby on Rails
* Ruby on Rails 6 実践ガイド

## 機能
* 掲示板投稿
* 棋譜投稿
* 画像投稿
* 会員名簿

## 開発環境
* Ruby on Rails 6.0.3.2
* Ruby 2.7.1
* Atom 1.48.0
* Bootstrap 4.5.0
