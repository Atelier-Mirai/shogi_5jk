require 'opal'
require 'opal_ujs'
require 'opal-parser'

# require 'popper' # Bootstrapのtooltipsやpopoverの為
# require 'bootstrap'

require 'opal_shogi' # 将棋盤
require 'kifu_play'  # 自動再生用

require 'select2-full'
require 'custom_file_input'
# # R から 段級位への変換
require 'rating_to_dankyu'

require 'semantic-ui'
require 'semantic-ui-customize'

# 未使用
# require_tree '.'
