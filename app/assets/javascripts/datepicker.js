(($ => {
  $.fn.datepicker = function (options) {
    // 設定事項
    const settings = $.extend({
      dateFormat: "YYYY-MM-DD",
      locale: 'ja',
      positionShift: { top: 40, left: 0 },
    }, options);
    moment.locale(settings.locale);

    // 関数本体
    return this.each((i, elem) => {
      elem = $(elem);

      let selectDate = "";
      let field_value = elem.val();
      if (field_value == ""){
        selectDate = moment();
      } else {
        selectDate = moment(field_value);
      }
      let lastSelected = copyDate(selectDate);

      // 手動でinputに入力したときに日付が反映される為に
      elem.on('change', () => {
        field_value  = elem.val();
        selectDate   = moment(field_value);
        lastSelected = copyDate(selectDate);
      });

      elem.on('click', () => {
        const $body    = $('body');
        const $win     = $('<div>').addClass("dp_modal-win");
        const $content = createContent();
        const offset   = elem.offset();
        $body.append($win).append($content);

        // 右端にdatepickerがあったときに、
        // 右端が欠けないように適宜左に寄った位置から表示する
        width_total = offset.left + settings.positionShift.left + $content.width();
        difference  = $win.width() - width_total
        if (difference >= 0){
          $content.css({
            top:  `${offset.top  + settings.positionShift.top}px`,
            left: `${offset.left + settings.positionShift.left}px`
          });
        } else {
          $content.css({
            top:  `${offset.top  + settings.positionShift.top}px`,
            left: `${offset.left + settings.positionShift.left + difference - 20}px`
          });
        }

        feelDates(selectDate);
        $win.on('click', () => {
          $content.remove();
          $win.remove();
        });

        function feelDates(selectDate) {
          $content.find('#field-data')
                  .empty()
                  .append(createMonthPanel(selectDate))
                  .append(createCalendar(selectDate));
        }

        function createCalendar(selectDate) {
          const $c = $('<div>').addClass('dp_modal-calendar');
          for (let i = 0; i < 7; i++) {
            let $e = $('<div>').addClass('dp_modal-calendar-cell dp_modal-colored');
            $e = addWeekdayClass(i, $e);
            $e.text(moment().weekday(i).format('ddd'));
            $c.append($e);
          }
          let m             = copyDate(selectDate);
          // 月初からcalendar表示できるように
          m.date(1);
          // その月の日数
          const daysInMonth = m.daysInMonth();

          // 1日までの空欄を作成
          for (let i = 0; i < m.weekday(); i++) {
            let $b = $('<div>').addClass('dp_modal-calendar-cell');
            $c.append($b);
          }
          // 当月分の日付を作成
          for (let i = 1; i <= daysInMonth; i++){
            let $b = $('<div>').addClass('dp_modal-calendar-cell').text(m.date());
            $b = addWeekdayClass(m.weekday(), $b);
            $b = addHolidayClass(m, $b);

            // 本日を選択しているなら
            if (m.isSame(lastSelected, 'day')) {
              $b.addClass('dp_modal-cell-selected');
            } else {
              $b.addClass('cursorily');
              $b.on('click', changeDate);
            }
            $c.append($b);
            // 翌日へ進める
            m.add(1, 'days');
          }
          return $c;
        }

        function changeDate() {
          const $div = $(this);
          selectDate.date($div.text()); // calendarのクリックした日がセットされる
          lastSelected = copyDate(selectDate);
          updateDate();
          const old = $content.find('#field-data .dp_modal-cell-selected');
          old
            .removeClass('dp_modal-cell-selected')
            .addClass('cursorily')
            .on('click', changeDate);
          $div
            .addClass('dp_modal-cell-selected')
            .removeClass('cursorily')
            .off('click');
        }

        function createMonthPanel(selectMonth) {
          const $d = $('<div>').addClass('dp_modal-months');
          let   $s = $('<i></i>').addClass('fa fa-angle-left cursorily ico-size-month hov').on('click', prevMonth);
          $d.append($s);

          let year  = selectMonth.year();
          let month = selectMonth.month() + 1;
          if (year >= 2020) {
            $s = $('<span>').text(`令和${year-2018}年 ${month}月`);
          } else if (year == 2019 && month >= 5) {
            $s = $('<span>').text(`令和元年 ${month}月`);
          } else {
            $s = $('<span>').text(`平成${year-1988}年 ${month}月`);
          }
          $d.append($s);

          $s = $('<i></i>').addClass('fa fa-angle-right cursorily ico-size-month hov').on('click', nextMonth);
          $d.append($s);
          return $d;
        }

        function nextMonth() {
          selectDate.add(1, 'month');
          feelDates(selectDate);
        }

        function prevMonth() {
          selectDate.subtract(1, 'month');
          feelDates(selectDate);
        }

        function createContent() {
          const $c  = $('<div>').addClass("dp_modal-content");
          let   $el = $('<div>').addClass('dp_modal-cell-date').attr('id', 'field-data');
          $c.append($el);
          return $c;
        }

        function updateDate() {
          elem.val(lastSelected.format(settings.dateFormat));
        }
      });
    });
  };

  function copyDate(d) {
    return moment(d.toDate());
  }

  function addWeekdayClass(w, $e){
    weekday = ['dp_sunday',
               'dp_monday',
               'dp_tuesday',
               'dp_wednesday',
               'dp_thursday',
               'dp_friday',
               'dp_saturday'];
    return $e.addClass(weekday[w]);
  }

  function addHolidayClass(m, $e) {
    const holidays = [
      moment("2018-01-01"),
      moment("2018-01-08"),
      moment("2018-02-11"),
      moment("2018-02-12"),
      moment("2018-03-21"),
      moment("2018-04-29"),
      moment("2018-04-30"),
      moment("2018-05-03"),
      moment("2018-05-04"),
      moment("2018-05-05"),
      moment("2018-07-16"),
      moment("2018-08-11"),
      moment("2018-09-17"),
      moment("2018-09-23"),
      moment("2018-09-24"),
      moment("2018-10-08"),
      moment("2018-11-03"),
      moment("2018-11-23"),
      moment("2018-12-23"),
      moment("2018-12-24"),
      moment("2019-01-01"),
      moment("2019-01-14"),
      moment("2019-02-11"),
      moment("2019-03-21"),
      moment("2019-04-29"),
      moment("2019-04-30"),
      moment("2019-05-01"),
      moment("2019-05-02"),
      moment("2019-05-03"),
      moment("2019-05-04"),
      moment("2019-05-05"),
      moment("2019-05-06"),
      moment("2019-07-15"),
      moment("2019-08-11"),
      moment("2019-08-12"),
      moment("2019-09-16"),
      moment("2019-09-23"),
      moment("2019-10-14"),
      moment("2019-11-03"),
      moment("2019-11-04"),
      moment("2019-11-23")
    ];
    for(let d of holidays){
      if(m.isSame(d, 'day')){
        return $e.addClass('dp_holiday');
      }
    }
    return $e;
  }
})(jQuery));
