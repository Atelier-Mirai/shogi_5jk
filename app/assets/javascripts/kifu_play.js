// 棋譜の自動再生用
// setInterval の記述が、opalでは複雑だったのでjsで記述
$(function() {
  var timer_id;
  var count;
  var remain_times;
  $(".kifu_play").on("click", function() {
    var kifu_number = $(this).attr("data-kifu-number");
    var kifu_max    = $(this).attr("data-kifu-max");

    // 先頭の 's' 一文字を取り除く
    kifu_speed = $("#current_speed_"+kifu_number).val().substr(1);
    if ($("#kifu_play_"+kifu_number).val() == "▶") {
      $("#kifu_play_"+kifu_number).val("■");
      count        = 0;
      remain_times = kifu_max - $("#current_te_"+kifu_number).val();
      timer_id = setInterval(function(){
        $("#kifu_next_"+kifu_number).click();
        count++;
        if (count >= remain_times) {
          $("#kifu_play_"+kifu_number).val("▶");
          clearInterval(timer_id);
        }
      }, kifu_speed);
    } else if ($("#kifu_play_"+kifu_number).val() == "■"){
      $("#kifu_play_"+kifu_number).val("▶");
      clearInterval(timer_id);
    }
  });
});
