(function(jQuery) {
	jQuery.fn.local_storage_tab_name = function(keyName) {
    //localStorage にどのタブを選択しているか保存する //http://ponk.jp/jquery/basic/sticky_save　参考
    $("ul.nav.nav-tabs li.nav-item a.nav-link").click(function() {
      // ローカルストレージに保存
      item = $(this).attr('href');
      localStorage.setItem(keyName, item);
      // タブからactiveを取り除く
      $("ul.nav.nav-tabs li.nav-item a.nav-link").removeClass("active");
      // タブコンテンツからactiveを取り除く
      $(".tab-content .tab-pane.fade").removeClass("active show");
    });

    function load() {
      // ローカルストレージからタブ名を読み込む
      var tab_name = localStorage.getItem(keyName);
      // タブ名が保存されていないときは、先頭をactiveにする。
      if (!tab_name) {
        $("ul.nav.nav-tabs li.nav-item a.nav-link:first").click();
        $(".tab-content .tab-pane.fade:first").addClass("active show");
        return;
      }
      // タブ名が保存されているときは、そのタブをクリックし、表示する。
      $('ul.nav.nav-tabs li.nav-item a.nav-link[href='+"'"+tab_name+"'"+']').click();
    }
    load();
  };
})(jQuery);
