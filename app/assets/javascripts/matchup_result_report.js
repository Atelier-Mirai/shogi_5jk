$(function(){
  // 対局表の設定いろいろ
  $('label[for=game_winner_id]').hide();
  $('label[for=game_reason]').hide();
  $('label[for=game_teai]').hide();

  $(".game_date").on("click", function(){
    date = $(this).attr("data-game_date");
    number = $(this).attr("data-game_number");
    player1_id = $(this).attr("data-player1_id");
    player2_id = $(this).attr("data-player2_id");
    player1_name = $(this).attr("data-player1_name");
    player2_name = $(this).attr("data-player2_name");

    // if ($(this).attr("data-target") == "#matchup_schedule_and_result") {
    //   admin_user      = "#{admin_user?}";
    //   current_user_id = "#{current_user&.id}";
    //   if (admin_user != "true"){
    //     if (current_user_id == 0){
    //       alert("日程変更や結果報告の為に、「ログイン」して下さい。");
    //       return false;
    //     }
    //     if (current_user_id != player1_id && current_user_id != player2_id){
    //       alert(current_user_id);
    //       alert("ご自身の対局の日程変更や結果報告が可能です。");
    //       return false;
    //     }
    //   }
    // }

    $("#game_date").val(date);
    $("#game_desired_date").val(date);
    $("#game_desired_date").text(date);

    $("#game_number_schedule").val(number);
    $("#game_number_result").val(number);
    // select option に対局者名を追加する
    $('#game_winner_id option').remove();
    $option = $('<option>', { value: 0, text: "-- 勝者 選択--" });
    $('#game_winner_id').append($option);
    $option = $('<option>', { value: player1_id, text: player1_name });
    $('#game_winner_id').append($option);
    $option = $('<option>', { value: player2_id, text: player2_name });
    $('#game_winner_id').append($option);
  });

  // ローカルストレージに選択しているタブ名を保存
  $().local_storage_tab_name("matchup");
});
