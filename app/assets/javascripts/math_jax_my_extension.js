MathJax.Hub.Config({
  extensions: ["tex2jax.js","TeX/AMSmath.js","TeX/AMSsymbols.js",
       "TeX/noErrors.js","TeX/noUndefined.js"],
  tex2jax: {
  inlineMath: [ ['$','$'], ["\\(","\\)"] ],
  displayMath: [ ['$$','$$'], ["\\[","\\]"] ]
  },
  TeX: {
  Macros: {
      unit: ['{\\,[\\mathrm{#1}]\\,}', 1],
      diff: '{\\mathrm{d}}',
      vm: ['{\\boldsymbol{#1}}', 1],
      grad: ['{\\nabla #1}', 1],
      div: ['{\\nabla\\cdot #1}', 1],
      rot: ['{\\nabla\\times #1}', 1],
      pdiffA: ['{\\frac{\\partial #1}{\\partial #2}}', 2],
      pdiffB:  ['{\\frac{\\partial^{#1} #2}{\\partial #3^{#1}}}', 3],
      ddiffA: ['{\\frac{\\diff #1}{\\diff #2}}', 2],
      ddiffB:  ['{\\frac{\\diff^{#1} #2}{\\diff #3^{#1}}}', 3]
  }
  }
});
