Document.ready? do
  # puts 0b0001 << 0  #=> 1
  # puts 0b0001 << 10 #=> 1024
  # puts 0b0001 << 20 #=> 1048576
  # puts 0b0001 << 31 #=> -2147483648
  # puts 0b0001 << 32 #=> 1
  # puts 0b0001 << 40 #=> 256
  # puts 0b0001 << 80 #=> 65536
  init_speed_selectbox
  init_list_selectbox
  draw_first
  draw_prev
  draw_next
  draw_last
  kifu_invert
end

# **********************************************************************
# * selectboxのバインド
# * 引用元 http://gihyo.jp/design/serial/01/jquery-site-production/0012
# **********************************************************************
def init_speed_selectbox
  # 1) a.select(普通) がクリックされたら
  Element[".speed.selectbox a.select"].on :click do |event|
    event.prevent # その要素のイベントをキャンセルする
    event.stop    # stop event propagation 親要素への伝播もキャンセルする
    kifu_number = event.element.parent["data-kifu-number"]
    # 2) pulldownを表示
    Element["#kifu_speed_#{kifu_number} .pulldown"].remove_class("hidden")
  end

  # Element["#kifu_speed_#{kifu_number} .pulldown a"].on :click do |a_tag|
  Element[".speed.selectbox .pulldown a"].on :click do |a_tag|
    a_tag.prevent # その要素のイベントをキャンセルする
    a_tag.stop    # stop event propagation 親要素への伝播もキャンセルする
    kifu_number = a_tag.element.parent.parent["data-kifu-number"]

    # 3) pulldown内の何かがクリックされたら
    #     a.select をpulldown内のクリックされたa_tagで置き換える
    #     value も置き換える
    val = a_tag.element["href"]
    txt = a_tag.element.text()
    Element["#kifu_speed_#{kifu_number} a.select span"].text(txt)
    Element["#current_speed_#{kifu_number}"][:value] = val
    Element["#kifu_speed_#{kifu_number} .pulldown a"].remove_class("selected")
    a_tag.element.add_class("selected")
  end

  # 4) pulldown外(body)がクリックされたら
  #     pulldownを非表示にする
  Element["body"].on :click do
    Element[".speed.selectbox .pulldown"].add_class("hidden")
  end
end

def init_list_selectbox
  # 1) a.select(1.▲５六歩) がクリックされたら
  Element[".list.selectbox a.select"].on :click do |event|
    event.prevent # その要素のイベントをキャンセルする
    event.stop    # stop event propagation 親要素への伝播もキャンセルする
    kifu_number = event.element["data-kifu-number"]
    # 2) pulldownを表示
    Element["#kifu_list_#{kifu_number} .pulldown"].remove_class("hidden")
  end

  Element[".list.selectbox .pulldown a"].on :click do |a_tag|
    a_tag.prevent # その要素のイベントをキャンセルする
    a_tag.stop    # stop event propagation 親要素への伝播もキャンセルする
    kifu_number = a_tag.element.parent.parent["data-kifu-number"]
    kifu_max    = a_tag.element.parent.parent["data-kifu-max"]

    jkfe = Element["#jkfe_#{kifu_number}"].text()
    kifu = eval(jkfe)

    # 3) pulldown内の何かがクリックされたら
    #     a.select をpulldown内のクリックされたa_tagで置き換える
    #     value も置き換える
    val = a_tag.element["href"]
    txt = a_tag.element.text()
    Element["#kifu_list_#{kifu_number} a.select span"].text(txt)

    # このボタンが押される前に表示されていた局面（何手目か？）
    old = Element[".current_te"][:value].to_i
    # このボタンを押すことによって、新たに何手目の局面を表示したいのか？
    now = val.to_i
    Element["#current_te_#{kifu_number}"][:value] = val
    Element["#kifu_list_#{kifu_number} .pulldown a"].remove_class("selected")
    a_tag.element.add_class("selected")

    # コメントを表示する
    Element["#comment_#{kifu_number} .msgbox.selected"].remove_class("selected").add_class("hidden")
    te = Element["#current_te_#{kifu_number}"][:value].to_i
    Element["#comment_#{kifu_number} .msgbox.#{te}"].remove_class("hidden").add_class("selected")

    # 盤面を更新する
    if now > old
      old.upto(now) do |te|
        proceed_phase(kifu, kifu_number, te)
      end
    elsif old > now
      old.downto(now+1) do |te|
        succeed_phase(kifu, kifu_number, te)
      end
    end

    # # ボタンの無効化
    # if now == 0
      Element["#kifu_first_#{kifu_number}"]["disabled"] = true
      Element["#kifu_prev_#{kifu_number}"]["disabled"] = true
    # elsif now == kifu_max
      Element["#kifu_play_#{kifu_number}"]["disabled"] = true
      Element["#kifu_next_#{kifu_number}"]["disabled"] = true
      Element["#kifu_last_#{kifu_number}"]["disabled"] = true
    # else
      Element["#kifu_first_#{kifu_number}"]["disabled"] = false
      Element["#kifu_prev_#{kifu_number}"]["disabled"] = false
      Element["#kifu_play_#{kifu_number}"]["disabled"] = false
      Element["#kifu_next_#{kifu_number}"]["disabled"] = false
      Element["#kifu_last_#{kifu_number}"]["disabled"] = false
    # end
  end

  # 4) pulldown外(body)がクリックされたら
  #     pulldownを非表示にする
  Element["body"].on :click do
    Element[".list.selectbox .pulldown"].add_class("hidden")
  end
end

# 盤面を開始状態の盤面へと書き換える
def draw_first
  Element[".kifu_first"].on :click do |event|
    kifu_number = event.element["data-kifu-number"]
    kifu_max    = event.element["data-kifu-max"].to_i
    current_te = Element["#current_te_#{kifu_number}"][:value].to_i

    # 棋譜リストの更新
    Element["#kifu_list_#{kifu_number} .pulldown a"].remove_class("selected")
    fugo = Element["#kifu_list_#{kifu_number} .pulldown a:first-child"].add_class("selected").text()
    Element["#kifu_list_#{kifu_number} a.select span"].text(fugo)

    # コメントを表示する
    Element["#comment_#{kifu_number} .msgbox.selected"].remove_class("selected").add_class("hidden")
    Element["#comment_#{kifu_number} .msgbox.comment_0"].remove_class("hidden").add_class("selected")

    # 盤面を更新する
    jkfe = Element["#jkfe_#{kifu_number}"].text()
    kifu = eval(jkfe)
    current_te.downto(1) do |te|
      succeed_phase(kifu, kifu_number, te)
    end
    Element["#current_te_#{kifu_number}"][:value] = 0

    # ボタンの無効化
    Element["#kifu_first_#{kifu_number}"]["disabled"] = true
    Element["#kifu_prev_#{kifu_number}"]["disabled"] = true
    Element["#kifu_play_#{kifu_number}"]["disabled"] = false
    Element["#kifu_next_#{kifu_number}"]["disabled"] = false
    Element["#kifu_last_#{kifu_number}"]["disabled"] = false
  end
end

# 盤面を最後の盤面へと書き換える
def draw_last
  Element[".kifu_last"].on :click do |event|
    kifu_number = event.element["data-kifu-number"]
    kifu_max    = event.element["data-kifu-max"].to_i
    current_te = Element["#current_te_#{kifu_number}"][:value].to_i

    # 棋譜リストの更新
    Element["#current_te_#{kifu_number}"][:value] = kifu_max
    Element["#kifu_list_#{kifu_number} .pulldown a"].remove_class("selected")
    fugo = Element["#kifu_list_#{kifu_number} .pulldown a:nth-child(#{kifu_max+1})"].add_class("selected").text()
    Element["#kifu_list_#{kifu_number} a.select span"].text(fugo)

    # コメントを表示する
    Element["#comment_#{kifu_number} .msgbox.selected"].remove_class("selected").add_class("hidden")
    Element["#comment_#{kifu_number} .msgbox.comment_#{kifu_max}"].remove_class("hidden").add_class("selected")

    # 盤面を更新する
    jkfe = Element["#jkfe_#{kifu_number}"].text()
    kifu = eval(jkfe)
    current_te.upto(kifu_max) do |te|
      proceed_phase(kifu, kifu_number, te)
    end

    # ボタンの無効化
    Element["#kifu_first_#{kifu_number}"]["disabled"] = false
    Element["#kifu_prev_#{kifu_number}"]["disabled"] = false
    Element["#kifu_play_#{kifu_number}"]["disabled"] = true
    Element["#kifu_next_#{kifu_number}"]["disabled"] = true
    Element["#kifu_last_#{kifu_number}"]["disabled"] = true
  end
end

def draw_prev
  Element[".kifu_prev"].on :click do |event|
    # 棋譜の準備
    kifu_number = event.element["data-kifu-number"]
    jkfe = Element["#jkfe_#{kifu_number}"].text()
    kifu = eval(jkfe)

    flag          = false
    catch(:exit) do
      Element["#kifu_list_#{kifu_number} .pulldown a"].each_with_index.reverse_each do |a_tag, n|
        if a_tag.has_class?("selected")
          if n != 0
            a_tag.remove_class("selected")
            flag = true
            next
          end
        end
        if flag == true
          a_tag.add_class("selected")
          Element["#kifu_list_#{kifu_number} .select span"].text(a_tag.text())
          Element["#current_te_#{kifu_number}"][:value] = n
          throw :exit
        end
      end
    end

    # コメントを表示する
    Element["#comment_#{kifu_number} .msgbox.selected"].remove_class("selected").add_class("hidden")
    te = Element["#current_te_#{kifu_number}"][:value].to_i
    Element["#comment_#{kifu_number} .msgbox.#{te}"].remove_class("hidden").add_class("selected")

    # 盤面を一手前の盤面へと書き換える
    succeed_phase(kifu, kifu_number, te + 1)
  end
end

def draw_next
  Element[".kifu_next"].on :click do |event|
    puts 'draw_next'

    # 棋譜の準備
    kifu_number = event.element["data-kifu-number"]

    jkfe = Element["#jkfe_#{kifu_number}"].text()
    kifu = eval(jkfe)
    # # このkifuキャッシュに持っておけないのかな？　毎回読むと重いので。
    # 指し手の表示変更
    flag = false # selecedの次を取得するためのworkフラグ
    Element["#kifu_list_#{kifu_number} .pulldown a"].each_with_index do |a_tag, n|
      if a_tag.has_class?("selected")
        if n != kifu.length
          a_tag.remove_class("selected")
          flag = true
          next
        end
      end
      if flag
        a_tag.add_class("selected")
        Element["#kifu_list_#{kifu_number} .select span"].text(a_tag.text())
        Element["#current_te_#{kifu_number}"][:value] = n
        break
      end
    end

    # コメントを表示する
    Element["#comment_#{kifu_number} .msgbox.selected"].remove_class("selected").add_class("hidden")
    te = Element["#current_te_#{kifu_number}"][:value].to_i
    Element["#comment_#{kifu_number} .msgbox.#{te}"].remove_class("hidden").add_class("selected")

    # 盤面を一手先の盤面へと書き換える
    proceed_phase(kifu, kifu_number, te)
  end
end

def succeed_phase(kifu, kifu_number, te)
  if te == kifu.length
    # kifu[te] == nil なので 盤面処理は行わない
    # ボタンの有効化を行う
    Element["#kifu_play_#{kifu_number}"]["disabled"] = false
    Element["#kifu_next_#{kifu_number}"]["disabled"] = false
    Element["#kifu_last_#{kifu_number}"]["disabled"] = false
  else
    # kifu[1] # 一手目
    # => { "from"=>119(0x77), "to"=>118(0x76), "piece"=>17(0x11▲歩),
    #      "promote"=>false, "same"=>false, "relative"=>"", "capture"=>0 }
    to      = kifu[te][:to].to_s(16)
    from    = kifu[te][:from].to_s(16)
    promote = kifu[te][:promote]
    piece   = kifu[te][:piece].to_i
    koma    = code2koma(piece, promote: false) # 行った先で成なら、移動前は生駒なので
    capture = kifu[te][:capture].to_i

    # ７六の駒を７七に戻す
    # 移動先
    Element["#shogiban_#{kifu_number} .b#{to}"].class_name = "b#{to} blk"
    # 移動元（盤上の駒が移動した場合）
    Element["#shogiban_#{kifu_number} .b#{from}"].class_name = "b#{from} #{koma}"

    # 持ち駒の処理
    if from == "0" || capture != 0
      dai = Array.new(2)
      #           # 歩 香 桂 銀 金 角 飛
      # dai[0] = [0, 3, 2, 2, 1, 0, 1, 1]
      # dai[1] = [0, 9, 1, 1, 3, 1, 0, 0]
      mochigoma_0 = Element["#mochigoma_0_#{kifu_number}"].text()
      dai[0]      = mochigoma_0.split(", ").map(&:to_i)
      mochigoma_1 = Element["#mochigoma_1_#{kifu_number}"].text()
      dai[1]      = mochigoma_1.split(", ").map(&:to_i)
      # 取ったとき、取らなかったことになるので、持ち駒を減らす
      if capture != 0
        turn = (piece & 0x30) >> 5
        dai[turn][capture & 0x07] -= 1
        # かつ、その場所にいた相手の駒も盤面に戻してあげる
        # to の場所に captureを表示する (turn == 0 取ったのは後手の駒)
        koma_name = code2koma(capture + (turn == 0 ? 0x20 : 0x10))
        Element["#shogiban_#{kifu_number} .b#{to}"].class_name = "b#{to} #{koma_name}"
        # DOMに書き出す
        str = dai[turn].join(', ')
        Element["#mochigoma_#{turn}_#{kifu_number}"].text(str)
      # 打ったとき、打った駒が持ち駒に戻るので、増やす
      elsif from == "0"
        turn = (piece & 0x30) >> 5
        dai[turn][piece & 0x07] += 1
        # DOM に書き出す
        str = dai[turn].join(', ')
        Element["#mochigoma_#{turn}_#{kifu_number}"].text(str)
      end
      # 持ち駒を表示させる
      draw_mochigoma(dai, kifu_number)
    end

    if te == 1 # 元に戻す処理に加えて、ボタンの無効化も行う
      Element["#kifu_first_#{kifu_number}"]["disabled"] = true # 無効
      Element["#kifu_prev_#{kifu_number}"]["disabled"] = true
    end
  end
end

def proceed_phase(kifu, kifu_number, te)
  if te != kifu.length
    # ボタンを有効化
    Element["#kifu_first_#{kifu_number}"]["disabled"] = false
    Element["#kifu_prev_#{kifu_number}"]["disabled"] = false
    Element["#kifu_play_#{kifu_number}"]["disabled"] = false
    Element["#kifu_next_#{kifu_number}"]["disabled"] = false
    Element["#kifu_last_#{kifu_number}"]["disabled"] = false
    # kifu[1] # 一手目
    # => { "from"=>119(0x77), "to"=>118(0x76), "piece"=>17(0x11▲歩),
    #      "promote"=>false, "same"=>false, "relative"=>"", "capture"=>0 }
    to      = kifu[te][:to].to_s(16)
    from    = kifu[te][:from].to_s(16)
    promote = kifu[te][:promote]
    piece   = kifu[te][:piece].to_i
    koma    = code2koma(piece, promote: promote)
    capture = kifu[te][:capture].to_i

    # 移動先
    # Element["#shogiban_#{kifu_number} .b#{to} img"].class_name = koma
    Element["#shogiban_#{kifu_number} .b#{to}"].class_name = "b#{to} #{koma}"

    # 移動元（盤上の駒が移動した場合）
    # Element["#shogiban_#{kifu_number} .b#{from} img"].class_name = "blk"
    Element["#shogiban_#{kifu_number} .b#{from}"].class_name = "b#{from} blk"
    # 持ち駒の処理
    if from == "0" || capture != 0
      dai = Array.new(2)
      #           # 歩 香 桂 銀 金 角 飛
      # dai[0] = [0, 3, 2, 2, 1, 0, 1, 1]
      # dai[1] = [0, 9, 1, 1, 3, 1, 0, 0]
      mochigoma_0 = Element["#mochigoma_0_#{kifu_number}"].text()
      dai[0]      = mochigoma_0.split(", ").map(&:to_i)
      mochigoma_1 = Element["#mochigoma_1_#{kifu_number}"].text()
      dai[1]      = mochigoma_1.split(", ").map(&:to_i)
      # 取ったとき、持ち駒を増やす
      if capture != 0
        turn = (piece & 0x30) >> 5
        dai[turn][capture & 0x07] += 1
        # DOMに書き出す
        str = dai[turn].join(', ')
        Element["#mochigoma_#{turn}_#{kifu_number}"].text(str)
      # 打ったとき、持ち駒を減らす
      elsif from == "0"
        turn = (piece & 0x30) >> 5
        dai[turn][piece & 0x07] -= 1
        # DOMに書き出す
        str = dai[turn].join(', ')
        Element["#mochigoma_#{turn}_#{kifu_number}"].text(str)
      end
      # 持ち駒を表示させる
      draw_mochigoma(dai, kifu_number)
    end
  else
    # 最終手なら、ボタンを無効化
    Element["#kifu_play_#{kifu_number}"]["disabled"] = true
    Element["#kifu_next_#{kifu_number}"]["disabled"] = true
    Element["#kifu_last_#{kifu_number}"]["disabled"] = true
  end
end

# 駒code(0x11...0x1f/0x21...0x2f)を駒文字(fu...ry)に変換する
def code2koma(code, promote: false)
  # 駒文字(英語)
  koma_en = ["", "fu", "ky", "ke", "gi", "ki", "ka", "hi", "ou",
                 "to", "ny", "nk", "ng", "  ", "um", "ry"]
  # 0x11             "fu"                                            先後"0/1"
  return koma_en[(code & 0x0f) + (promote ? 0x08 : 0x00)].to_s + ((code & 0x30) >> 5).to_s
end

def draw_mochigoma(dai, kifu_number)
  # 初期化しておく
  (1..12).each do |n|
    Element[".komadai img.k0#{'%02d' % n}"].class_name = "blk k0#{'%02d' % n}"
    Element[".komadai img.k1#{'%02d' % n}"].class_name = "blk k1#{'%02d' % n}"
  end
  #           # 歩 香 桂 銀 金 角 飛
  # dai[0] = [0, 3, 2, 2, 1, 0, 1, 1]
  # dai[1] = [0, 9, 1, 1, 3, 1, 0, 0]
  (0..1).each do |turn|
    # if dai[turn].inject(&:+) <= 12
      pos = 1
      # 持ち駒があれば、クラス名をセット
      7.downto(1) do |code|
        if dai[turn][code] > 0
          position  = "k" + turn.to_s + "%02d"%pos
          sente = 0x10; gote = 0x20;
          koma_name = code2koma(code + (turn == 0 ? sente : gote))
          Element["#komadai_#{turn}_#{kifu_number} .#{position}"].class_name = "#{koma_name} #{position}"
          dai[turn][code] -= 1
          pos += 1
          redo
        end
      end
      # pos < 12 のとき、残りの駒台をblkに変更する
      if pos < 12
        pos.upto(12).each do |po|
          position = "k" + turn.to_s + "%02d"%po
          Element["#komadai_#{turn}_#{kifu_number} .#{position}"].class_name = "blk #{position}"
        end
      end
    # else
      # 駒を表示、その横に数を表示する
      # or 幅を狭くして１６枚表示させる
    # end

    # flag false:反転していない true:反転している
    if is_invert(kifu_number) == false # 正転表示なら
      (1..12).each do |pos|
        pos = "%02d"%pos
        Element["#komadai_#{turn}_#{kifu_number} .k0#{pos}"].remove_class("invert")
        Element["#komadai_#{turn}_#{kifu_number} .k1#{pos}"].remove_class("invert")
      end
    else # 反転表示なら
      (1..12).each do |pos|
        pos = "%02d"%pos
        Element["#komadai_#{turn}_#{kifu_number} .k0#{pos}"].add_class("invert")
        Element["#komadai_#{turn}_#{kifu_number} .k1#{pos}"].add_class("invert")
      end
    end
  end # end of turn
end

def kifu_invert
  Element[".kifu_invert"].on :click do |event|
    kifu_number = event.element["data-kifu-number"]

    # flag false:反転していない true:反転している
    invert_flag = is_invert(kifu_number)

    # 筋段の文字　入れ替え
    if invert_flag == false # まだ反転していないなら数字を反転させる
      (1..9).each do |num|
        c_num = 10 - num
        Element["#shogiban_#{kifu_number} .yline#{num}"].text(KAN_SUUJI[c_num])
        Element["#shogiban_#{kifu_number} .xline#{num}"].text(c_num)
      end
    else
      # すでに反転しているなら、元に戻す
      (1..9).each do |num|
        Element["#shogiban_#{kifu_number} .yline#{num}"].text(KAN_SUUJI[num])
        Element["#shogiban_#{kifu_number} .xline#{num}"].text(num)
      end
    end

    # 盤面の対角線右上を入れ替え
    (1..8).each do |dan|
      (1..(9-dan)).each do |suji|
        swap_td(kifu_number, suji, dan)
      end
    end

    # 対角線を入れ替え
    swap_td(kifu_number, 9, 1)
    swap_td(kifu_number, 8, 2)
    swap_td(kifu_number, 7, 3)
    swap_td(kifu_number, 6, 4)
    swap_td(kifu_number, 5, 5)

    # 持ち駒を入れ替える
		# <div class="mochigoma_0">0, 0, 0, 0, 0, 0, 0, 0</div>
    mochigoma_0 = Element["#mochigoma_0_#{kifu_number}"].text()
    mochigoma_1 = Element["#mochigoma_1_#{kifu_number}"].text()
    Element["#mochigoma_0_#{kifu_number}"].text(mochigoma_1)
    Element["#mochigoma_1_#{kifu_number}"].text(mochigoma_0)
    # id を入れ替える
    swap_id("mochigoma_0_#{kifu_number}", "mochigoma_1_#{kifu_number}")

    # 駒台
    (1..12).each do |pos|
      pos = "%02d"%pos
      Element["#komadai_0_#{kifu_number} .k0#{pos}"].class_name = "work_k0#{pos}"
    end
    (1..12).each do |pos|
      pos = "%02d"%pos
      Element["#komadai_1_#{kifu_number} .k1#{pos}"].class_name = "k0#{pos}"
    end
    (1..12).each do |pos|
      pos = "%02d"%pos
      Element["#komadai_0_#{kifu_number} .work_k0#{pos}"].class_name = "k1#{pos}"
    end

    if invert_flag == false # まだ反転していなくて、今回反転したのであれば、
      (1..12).each do |pos|
        pos = "%02d"%pos
        Element["#komadai_0_#{kifu_number} .k0#{pos}"].add_class("invert")
        Element["#komadai_1_#{kifu_number} .k1#{pos}"].add_class("invert")
      end
    else # すでに反転して、今回再反転（＝元に戻した）のであれば
      (1..12).each do |pos|
        pos = "%02d"%pos
        Element["#komadai_0_#{kifu_number} .k0#{pos}"].remove_class("invert")
        Element["#komadai_1_#{kifu_number} .k1#{pos}"].remove_class("invert")
      end
    end
    # 駒台のクラス名を入れ替える
    swap_id("komadai_0_#{kifu_number}", "komadai_1_#{kifu_number}")

    # 駒台の上に再度駒を表示する
    dai = Array.new(2)
    #           # 歩 香 桂 銀 金 角 飛
    # dai[0] = [0, 3, 2, 2, 1, 0, 1, 1]
    # dai[1] = [0, 9, 1, 1, 3, 1, 0, 0]
    mochigoma_0 = Element["#mochigoma_0_#{kifu_number}"].text()
    dai[0]      = mochigoma_0.split(", ").map(&:to_i)
    mochigoma_1 = Element["#mochigoma_1_#{kifu_number}"].text()
    dai[1]      = mochigoma_1.split(", ").map(&:to_i)
    draw_mochigoma(dai, kifu_number)

    # 名前表示を反転させる
    # <div id="player_name0_11" class="player_name0"><p>▲ 太郎</p></div>
    name0 = Element["#player_name_0_#{kifu_number} p"].text()
    name1 = Element["#player_name_1_#{kifu_number} p"].text()
    Element["#player_name_0_#{kifu_number} p"].text(name1)
    Element["#player_name_1_#{kifu_number} p"].text(name0)
  end
end

KAN_SUUJI = ["", "一", "二", "三", "四", "五", "六", "七", "八", "九"]

def is_invert(kifu_number)
  Element["#shogiban_#{kifu_number} .yline1"].text() == "一" ? false : true
end

def swap_id(element_a, element_b)
  Element["#"+element_a].id = "work"
  Element["#"+element_b].id = element_a
  Element["#work"].id       = element_b
end

# 先後を反転する。
# 先手２六歩 後手8四飛 の盤面で、
# 先後を反転すると、
# .b84.hi1 だった升目tdは -> .b26.fu1 に、
# .b26.fu0 だった升目tdは -> .b84.hi0 になる。
def swap_td(kifu_number, suji, dan)
  # 元の升目 先手2六歩
  koma_name = Element["#shogiban_#{kifu_number} .b#{suji}#{dan}"].class_name.split(" ").sort.last
  if koma_name != "blk"
    koma  = koma_name[0..1]
    color = koma_name[2] # "fu0"[2] # => 0
    r_color = 1 - color.to_i
    # fu0 -> fu1に
    koma_r = koma + r_color.to_s
  else
    koma_r = "blk"
  end

  # 反転先の升目 後手8四飛
  c_dan  = 10 - dan
  c_suji = 10 - suji
  c_koma_name = Element["#shogiban_#{kifu_number} .b#{c_suji}#{c_dan}"].class_name.split(" ").sort.last
  if c_koma_name != "blk"
    c_koma  = c_koma_name[0..1]
    c_color = c_koma_name[2] # "hi1"[2] # => 1
    r_color = 1 - c_color.to_i
    # hi1 -> hi0
    c_koma_r = c_koma + r_color.to_s
  else
    c_koma_r = "blk"
  end

  # .b26.fu0 を .b84.hi0に
  # クラス名 .b26 から .w84 に変更
  Element["#shogiban_#{kifu_number} .b#{suji}#{dan}"].class_name = "w#{c_suji}#{c_dan}"

  # .b84.hi1 を .b26.fu1 に変更
  Element["#shogiban_#{kifu_number} .b#{c_suji}#{c_dan}"].class_name = "b#{suji}#{dan} #{koma_r}"

  # .w84 を .b84に変更
  Element["#shogiban_#{kifu_number} .w#{c_suji}#{c_dan}"].class_name = "b#{c_suji}#{c_dan} #{c_koma_r}"
end

# Document.ready? do
  # Element.find("h1").on(:click) do
  #   p_elem = Element.new("p")
  #   p_elem.text("h1 was clicked!")
  #   p_elem.css("color", "red")
  #   Element.find("body").append(p_elem)
  #   Element[".kifu_prev"].on(:click) do
  #   puts "11 opal"
  # end

  # opal-jquery 練習
  # sample_h1 = Element.find("#h100")
  # text      = sample_h1.text()
  # sample_h1.text("テスト成功")
  # Element.find("#h101").text("こんども成功").css({color:"red", fontSize: "100px"})
  # Element['#h101'].text("成功")

  # どの書き方でも良い
  # Element.find("#c999").attr("src", "../assets/ou0.gif")
  # Element["#c999"].attr("src", "../assets/ou0.gif")
  # Element["#c999"]["src"] = "../assets/to0.gif"
  # Element["#c999"][:src] = "../assets/to0.gif"
  # asset_path は使えない
  # Element["#c999"][:src] = <%=asset_path "to0.gif"%>
  # last_movement(0x76)

  # <input type="hidde" name="sample_id" id="sample_id_0" value="">
  # Element["#sample_id_0"].text(999) # NG
  # Element["#sample_id_0"].value = 777 #OK


  # ban[], dai[]を将棋盤tableへ書き出す関数
  # => 指し手ハッシュをもらっているので、そのまま再現すれば良い。ban[]dai[]不要
  # 反転関数
  # 盤のidを反転させて振り直す 筋段の場所はそのまま、数のみ振り直す
  # 一手進める関数
  # 一手戻す関数
  # 任意の手の局面を表示する関数
  # 最終手の背景色を桜色に
# end
