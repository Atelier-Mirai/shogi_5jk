(function(jQuery) {
	jQuery.fn.rating_to_dankyu = function(form_model) {

    // R から 段級位への変換
    var r2d = function(r){
      if (r >= 2300) { return "七段" };
      if (r >= 2200) { return "六段" };
      if (r >= 2100) { return "五段" };
      if (r >= 1950) { return "四段" };
      if (r >= 1800) { return "三段" };
      if (r >= 1650) { return "二段" };
      if (r >= 1500) { return "初段" };
      if (r >= 1425) { return "１級" };
      if (r >= 1350) { return "２級" };
      if (r >= 1300) { return "３級" };
      if (r >= 1250) { return "４級" };
      if (r >= 1200) { return "５級" };
      if (r >= 1150) { return "６級" };
      if (r >= 1100) { return "７級" };
      if (r >= 1050) { return "８級" };
      if (r >= 1000) { return "９級" };
      if (r >= 900)  { return "10級" };
      if (r >= 800)  { return "11級" };
      if (r >= 700)  { return "12級" };
      if (r >= 600)  { return "13級" };
      if (r >= 500)  { return "14級" };
      if (r >= 0)    { return "15級" };
    }

    $("#" + form_model + "_rating").on("change", function(){
      r = this.value;
      dankyu = r2d(r);
      $("#" + form_model + "_dankyu").val(dankyu);
    });
  };

})(jQuery);
