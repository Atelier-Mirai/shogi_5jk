$(() => {
  // セレクトボックス
  $(".searchable").select2({
    width     : 155,  // 横幅
    allowClear: false // Clear x を表示しない
  });
  // datepicker
  $('.datepicker').datepicker();
  // テーブル並び替え
  $('table.sortable').tablesorter();
});
