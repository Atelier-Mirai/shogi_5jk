//初回読み込み、リロード、ページ切り替えで動く。
$(document).on('turbolinks:load', function() {
  // show dropdown on hover
  $('.ui.dropdown').dropdown({
    on: 'hover'
  });

  // flashをxボタンで消せる
  $('.message .close').on('click', function() {
    $(this).closest('.message')
           .transition('fade');
  });

  // サイドバーの開閉処理
  $('#js-sidebar').on('click', function() {
    $('.ui.sidebar').sidebar('toggle');
  });

  // モーダルウィンドウ
  $('.show_about').on('click', function() {
    $('.ui.modal').modal('show');
  });

  // アコーディオン
  $('.ui.accordion').accordion();

  // // テーブル並び替え
  // $('.ui.table').tablesort();

  // タブ
  $('.ui.tabular.menu .item').tab();

  // 新規投稿
  $('.new').on('click', function(){
    const number = $(this).data("post-new");

    if ($(`#post_new_${number}`).hasClass('hidden')) {
      $(`#post_new_${number}`).removeClass('hidden');
    } else {
      $(`#post_new_${number}`).addClass('hidden');
    }
  });

  // 投稿への返信
  // <div class="show reply ui small teal button" data-post-reply="2901">返信</div>
  $('.reply').on('click', function(){
    const number = $(this).data("post-reply");

    if ($(`#post_reply_${number}`).hasClass('hidden')) {
      $(`#post_reply_${number}`).removeClass('hidden');
    } else {
      $(`#post_reply_${number}`).addClass('hidden');
    }
  });

  // fix main menu to page on passing
  // $('.main.menu').visibility({
  //   type: 'fixed'
  // });
  // $('.overlay').visibility({
  //   type: 'fixed',
  //   offset: 80
  // });
  // lazy load images
  // $('.image').visibility({
  //   type: 'image',
  //   transition: 'vertical flip in',
  //   duration: 500
  // });

  // $('.masthead').visibility({
  //   once: false,
  //   onBottomPassed: function() {
  //     $('.fixed.menu').transition('fade in');
  //   },
  //   onBottomPassedReverse: function() {
  //     $('.fixed.menu').transition('fade out');
  //   }
  // });

  // $('.ui.sidebar').sidebar('attach events', '.toc.item');
});
