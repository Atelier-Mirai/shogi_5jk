class AccountActivationController < ApplicationController
  def edit
    @user = User.find_by(email: params[:email])
    if @user &&
       !@user.activated? &&
       Authenticator.new(@user).authenticate(:activation, params[:id])
      # メールアドレスの確認が取れた
      @user.update_columns(activated: true, activated_at: Time.current)
      flash[:success] = "メールアドレスの確認が出来ました"

      redirect_to edit_sign_up_path
    else
      flash[:danger] = "認証トークンが無効です"
      redirect_to root_url
    end
  end
end
