class AccountApprobationController < ApplicationController
  before_action :admin_user

  def approve
    user = User.find_by(id: params[:id])
    if user && !user.approved?
      subject = params[:subject]
      message = params[:message]
      # 次回のためにメッセージを保存
      WelcomeMessage.approbation.update_columns(subject: subject, message: message)
      # 諾否メール送信
      UserMailer.approve_or_reject_message(user, subject, message).deliver_later
      # 入会承認済にDB更新
      user.update_columns(approved: true)

      flash[:success] = "入会を承認しました"
      redirect_to users_path
    else
      flash[:warning] = "会員が見つかりません。または既に入会承認済です。"
      redirect_to users_path
    end
  end

  def reject
    user = User.find_by(id: params[:id])
    if user && !user.approved?
      subject = params[:subject]
      message = params[:message]
      # 次回のためにメッセージを保存
      WelcomeMessage.rejection.update_columns(subject: subject, message: message)
      # 諾否メール送信
      UserMailer.approve_or_reject_message(user, subject, message).deliver_later
      # 入会申請者をDBから削除
      user.destroy

      flash[:danger] = "入会申請を却下しました"
      redirect_to users_path
    else
      flash[:warning] = "会員が見つかりません。または既に入会却下済です。"
      redirect_to users_path
    end
  end
end
