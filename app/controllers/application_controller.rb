class ApplicationController < ActionController::Base
  add_flash_types :success, :info, :warning, :error

  include ReCaptcha::AppHelper # ruby-recaptcha
  include ApplicationHelper
  include SessionsHelper
  include KifuHelper
  include MatchupHelper
  include MarkdownHelper

  # # Prevent CSRF attacks by raising an exception.
  # # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception
  # # protect_from_forgery with: :null_session # CSRF 無効にする
  # after_action :cors

  # httpをhttpsへリダイレクト(対応ブラウザの場合）
  # content_security_policy do |policy|
  #   policy.upgrade_insecure_requests true
  # end

  if Rails.env.production?
    rescue_from Exception, with: :error500
    rescue_from ActiveRecord::RecordNotFound, ActionController::RoutingError, with: :error404
  end

  private

  # ユーザーのログインを確認する
  def logged_in_user
    unless logged_in_user?
      store_location
      flash[:warning] = "ログインして下さい"
      redirect_to login_url
    end
  end

  def admin_user
    if !logged_in_user?
      flash[:warning] = "ログインして下さい"
      redirect_to login_url
    elsif !current_user.admin?
      flash[:danger] = "管理者権限が必要です"
      redirect_to root_url
    end
  end

  def error404(_e)
    render 'error404', status: 404, formats: [:html]
  end

  def error500(_e)
    render 'error500', status: 500, formats: [:html]
  end

  def cors
    response.headers['Access-Control-Allow-Origin']      = '*'
    # response.headers['Access-Control-Allow-Origin']      = 'http://www22.atpages.jp/fiverank/'
    response.headers['Access-Control-Allow-Methods']     = 'POST, GET,PUT, DELETE, OPTIONS, HEAD'
    response.headers['Access-Control-Allow-Credentials'] = 'true'
    response.headers['Access-Control-Allow-Headers']     = 'X-PINGOTHER'
    response.headers['Access-Control-Max-Age']           = '86400' # 24 hours
  end
end
