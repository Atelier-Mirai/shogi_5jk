# BBSController 掲示板トップページ表示を司る
class BbsController < ApplicationController
  def index
    @topics           = Topic.everyone.limit(10)
    @committee_topics = Topic.committee_only if committee_user?
  end
end
