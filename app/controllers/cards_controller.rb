class CardsController < MessagesController
  before_action :set_card

  def edit
  end

  def update
    @card.update(card_params)
    flash[:success] = "案内文を更新しました。"
    redirect_to root_url
  end

  private

  def set_card
    @card = Card.find_by(kisen_id: params[:kisen_id])
  end

  def card_params
    params.require(:card).permit(:title, :comment)
  end
end
