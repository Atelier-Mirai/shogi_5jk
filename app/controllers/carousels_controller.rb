class CarouselsController < MessagesController
  before_action :set_carousel, only: [:edit, :update, :destroy]

  def new
    @carousel = Carousel.new
  end

  def create
    @carousel = Carousel.new(carousel_params)
    @carousel.save
    redirect_to root_path
  end

  def edit
  end

  def update
    @carousel.update(carousel_params)
    redirect_to root_path
  end

  def destroy
    @carousel.destroy
    redirect_to root_path
  end

  private

  def set_carousel
    @carousel = Carousel.find_by(id: params[:id])
  end

  def carousel_params
    params.require(:carousel).permit(:title, :comment)
  end
end
