# 棋戦への参加申し込み
class EntriesController < ApplicationController
  def index
    @kisen   = Kisen.find(params[:kisen_id])
    # @entries = if @kisen.name.include? "交流戦"
    #              @kisen.entries.reorder(team: :asc, rating: :desc)
    #            else
    #              @kisen.entries
    #            end
    # なぜか、@entriesにnilがいっぱい含まれた要素が現れるので修正
    @entries = Entry.where(kisen_id: params[:kisen_id])
  end

  def create
    entry = Entry.new do |t|
      t.kisen_id = params[:kisen_id]
      t.name     = params[:entry][:name]
      user       = User.find_by(name: t.name) || GuestUser.new(t.name)
      t.user_id  = user.id
      role       = (user.role == 'guest' ? 'guest' : 'member')
      t.comment  = params[:entry][:comment]

      ymd            = Time.current.beginning_of_month.strftime('%Y-%m-%d')
      t.number       = Profile.find_by(user_id: t.user_id)&.number || '0'
      t.rating       = Ranking.find_by(name: t.name, ymd: ymd)&.rating || params[:entry][:rating]
      t.dankyu       = rating2dankyu(t.rating)
      t.meijin_class = Profile.find_by(user_id: t.user_id)&.meijin_class
      t.area         = Prefecture.new(Profile.find_by(user_id: t.user_id)&.area).code
      t.entry_category = if t.user_id == Kisen.find(t.kisen_id).former_title_holder&.first&.id
                           'title_holder'        # タイトルホルダーが参加
                         else
                           'general_participant' # 一般参加者
                         end
      t.team = role == 'member' ? '5JK' : params[:entry][:team] && 'U549'
    end

    entry.save
    flash[:success] = "この棋戦に参加表明しました"
    if params[:entry][:prev_path] == 'index'
      redirect_to kisen_entries_path(params[:kisen_id])
    else
      redirect_to kisen_path(params[:kisen_id])
    end
  end

  def edit
    @entry = Entry.find_by(id: params[:id])
    @user  = User.first
  end

  def update
    @entry = Entry.find_by(id: params[:id])
    @entry.update(entry_params)
    @entry.update_columns(dankyu: rating2dankyu(@entry.rating))

    # 諸々更新
    user = User.find_by(name: @entry.name) || GuestUser.new(@entry.name)

    # ユーザーが更新されている
    if user.id != @entry.user_id
      entry_member_changed = true
      new_user_id = user.id
      org_user_id = @entry.user_id

      ymd      = Time.current.beginning_of_month.strftime('%Y-%m-%d')
      rating   = Ranking.find_by(name: user.name, ymd: ymd)&.rating || params[:entry][:rating].to_i
      dankyu   = rating2dankyu(rating)
      profile  = Profile.find_by(user_id: user.id)
      category = if user.id == @entry.kisen.former_title_holder&.first&.id
                   'title_holder' # タイトルホルダーが参加
                 else
                   'general_participant' # 一般参加者
                 end
      # Entry.column_names
      # => ["id", "kisen_id", "user_id", "name", "comment", "created_at", "updated_at", "rating", "dankyu", "number", "meijin_class", "entry_category", "memo", "area", "team"]
      @entry.update_columns(
        user_id:        user.id,
        name:           user.name,
        rating:         rating,
        dankyu:         dankyu,
        number:         profile&.number || '0',
        meijin_class:   profile&.meijin_class,
        entry_category: category,
        area:           Prefecture.new(profile&.area).code,
        team:           user.guest? ? 'U549' : '5JK'
      )
    end

    # 対局表の更新
    if (games = Kisen.find(@entry.kisen_id)&.matchup&.games).present? && entry_member_changed
      games.where(player1_id: org_user_id).update_all(player1_id: new_user_id)
      games.where(player2_id: org_user_id).update_all(player2_id: new_user_id)
    end

    if params[:entry][:prev_path] == 'index'
      redirect_to kisen_entries_path(params[:kisen_id])
    else
      redirect_to kisen_path(params[:kisen_id])
    end
  end

  def destroy
    Entry.find_by(id: params[:id]).destroy
    flash[:success] = 'この棋戦への参加申し込みを取り消しました'
    redirect_to params[:format] == 'index' ? kisen_entries_path(params[:kisen_id]) : kisen_path(params[:kisen_id])
  end

  private

  def entry_params
    params.require(:entry).permit(:name, :comment, :dankyu, :rating, :meijin_class, :area, :team)
  end
end
