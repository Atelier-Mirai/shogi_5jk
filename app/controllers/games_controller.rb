class GamesController < ApplicationController
  before_action :admin_user,     except: [:index, :update]
  before_action :logged_in_user, except: [:index]

  # 対局日程の表示
  def index
    @date       = params[:month] ? Date.parse(params[:month]) : Date.today
    @games      = Game.where(date: @date.all_month).reorder(date: :asc)
    @prev_month = Game.where(date: @date.prev_month.all_month) ? @date.prev_month : 0
    @next_month = Game.where(date: @date.next_month.all_month) ? @date.next_month : 0
  end

  def update
    matchup_id  = params[:matchup_id]
    game_number = params[:game][:game_number]
    date        = params[:game][:date]
    kisen_name  = Matchup.find(matchup_id).kisen.name

    case params[:mode]
    when 'schedule' # 対局日の変更
      Game.find_by(matchup_id: matchup_id, game_number: game_number).update_attributes(date: date)
      # 棋戦最終対局日の更新
      matchup = Matchup.find(matchup_id)
      matchup.kisen.update_columns(finish_date: matchup.last_game_date)
      redirect_to matchup_path(matchup_id)

    when 'result' # 結果報告
      # コメント＆棋譜の処理
      @topic = Topic.find(Matchup.find(matchup_id).topic_result_id)
      @post  = @topic.posts.build(post_params)
      if @post.valid?
        @post.save # 棋譜の作成・保存は、post.rbで行われる

        # 親に設定
        @post.update(roots: :progenitor) # 親に設定

        # トピックの更新日付を更新する
        @topic.update(updated_at: @post.updated_at)
        flash[:success] = "結果報告しました"

        # 対局結果 勝ち負けの処理
        # "game"=>{"winner_id"=>"50", "reason"=>"勝ち", "teai"=>"平手"}
        matchup_id  = params[:matchup_id].to_i
        game_number = params[:game][:game_number]
        winner_id   = params[:game][:winner_id].to_i
        reason      = params[:game][:reason]
        teai        = params[:game][:teai]

        game = Game.find_by(matchup_id: matchup_id, game_number: game_number)
        # どちらが勝者か
        wpm    = winning_point_and_mark(reason, kisen_name)
        winner = game.player1_id.to_i == winner_id.to_i ? 1 : 2
        case winner
        when 1
          point1 = wpm[:point][:winner]
          point2 = wpm[:point][:loser]
          mark1  = wpm[:mark][:winner]
          mark2  = wpm[:mark][:loser]
        when 2
          point2 = wpm[:point][:winner]
          point1 = wpm[:point][:loser]
          mark2  = wpm[:mark][:winner]
          mark1  = wpm[:mark][:loser]
        end

        if teai.in?(['平手', '']) || reason.in?(['不戦勝', '不戦勝_日程不調'])
          uwate_id = 0
        else
          # 駒落ちなら、どちらが上手か判定
          entries  = Matchup.find(params[:matchup_id]).kisen.entries
          p1       = game.player1_id.to_i
          p2       = game.player2_id.to_i
          r1       = entries.find_by(user_id: p1).rating || 0
          r2       = entries.find_by(user_id: p2).rating || 0
          uwate_id = (r1 >= r2 ? p1 : p2)
        end

        teai = "平手" if reason == "勝ち" && teai   == ''
        teai = nil   if reason == "不戦勝" || reason == "不戦勝_日程不調"

        game.update_attributes(winner_id: winner_id,
                               reason:    reason,
                               point1:    point1,
                               point2:    point2,
                               mark1:     mark1,
                               mark2:     mark2,
                               uwate_id:  uwate_id,
                               teai:      teai,
                               kifu_id:   @post.kifu_id)

        # トーナメント用 次局の対局を勝ち抜き者に更新する
        # 次局の対局者
        # +-----+------------+-------------+------------+------------+
        # | id  | matchup_id | game_number | player1_id | player2_id |
        # | 126 | 15         | A21         | A11        | A12        |
        # 二回戦第一局(A21)は A11 対 A12 となっている
        # A11 対局の勝者が 50 と決まったら、A21 の A11を 50 に更新する
        game.matchup.player_update(game_number, winner_id)

        # 敗者復活戦があるかも知れないので、敗者復活戦への出場者を設定する
        # +----+------------+-------------+------------+------------+
        # | id | matchup_id | game_number | player1_id | player2_id |
        # +----+------------+-------------+------------+------------+
        # | 35 | 4          | RA12        | a11        | a12        |
        # A11 の敗者が a11, A12の敗者がa12
        loser_id = game.loser_id
        game.matchup.player_update(game_number.downcase, loser_id)

        # 勝ち抜き決定戦
        # もし第１局で勝利していたら、第２局を行うことなく、
        # タイトル決定戦なので。
        if game_number == 'WA11' && winner == 1
          game.matchup.player_update('WA12', winner_id)
        end

        # リーグ戦で 挑戦者決定戦は A1, A2, B1, B2 と書かれている
        # リーグ戦の対局が全て完了していれば、A1, A2 を
        # A組の順位1位と順位2位のuser_idに更新する
        game_initial = game_number[0]
        if game.matchup.league_finished?(game_initial)
          ids = game.matchup.league_high_rankers(game_initial, 2)
          game.matchup.player_update("#{game_initial}1", ids[0])
          game.matchup.player_update("#{game_initial}2", ids[1])
        end

        # タイトルマッチであれば、優勝者、準優勝者を設定
        # Matchup.find(12).title_match("T")
        # => {:round=>1, :n_max=>3, :held=>true, :winner=>2}
        if game.game_number.start_with?('T')
          th     = [game.player1_id, game.player2_id]
          winner = game.matchup.title_match('T')[:winner]
          th     = th.reverse if winner == 2
          game.matchup.kisen.update_columns(title_holder_id: th)
        end

        # 東西戦、交流戦
        if game.matchup.holding_method[:method] == :team
          game_initial = game.game_number[0...-1] # 末尾は第何局かなので削除
          title = game.matchup.winning_team(game_initial)
          if title
            game.matchup.kisen.update_columns(
              title_holder_id: {
                team_names:   [title[:winner][:team_names],   title[:second_place][:team_names]],
                ids:          [title[:winner][:ids],          title[:second_place][:ids]],
                member_names: [title[:winner][:member_names], title[:second_place][:member_names]] })
            # {:team_names=>["5JK", "U549"], :ids=>[["39", "2", "61"], ["142", "143", "141"]], :member_names=>[["thegul", "WhiteWizard", "bururutti-2"], ["gyane1192", "settlefield", "pukuan"]]}
          end
        end
        redirect_to matchup_path(params[:matchup_id])
      else
        flash[:danger] = "結果報告に失敗しました"
        redirect_to matchup_path(params[:matchup_id])
      end
    end
  end

  private

  # 勝ち不戦勝から、勝ち点を算出
  # 勝ち点は次のとおりです。 ○: 3点 □: 2.9点 ●: 1点 ▲: 1点 ■: 0
  # WINNING_POINT = {
  #   # 勝ち点：○＝３点、□＝２．９点、●＝１点、■＝０点
  #   # 勝ち点：○＝３点、□＝２．９点、●＝１点、▲＝１点 ■＝０点
  #   # 勝ち点：○＝３点、□＝２点、●＝１点、■＝−２点
  #   名人戦: { win: 3, unearned_win: 2.9, lose: 1, unearned_lose: 0 },
  #   棋聖戦: { win: 3, unearned_win: 2.9, lose: 1, lose_failure: 1, unearned_lose: 0 },
  #   王位戦: { win: 3, unearned_win: 2.9, lose: 1, lose_failure: 1, unearned_lose: 0 },
  #   東西戦: { win: 3, unearned_win: 2  , lose: 1, unearned_lose: -2 },
  # }
  # WINNING_POINT_MARK = {
  #   win: "○", unearned_win: "□", lose: "●", lose_failure: "▲", unearned_lose: "■"
  # }
  def winning_point_and_mark(reason, kisen_name)
    case kisen_name
    when "棋聖戦", "王位戦"
      wp = { win: 3, unearned_win: 2.9, lose: 1, lose_failure: 1, unearned_lose: 0 }
    when "東西戦"
      wp = { win: 3, unearned_win: 2,   lose: 1, unearned_lose: -2 }
    else # "名人戦"
      wp = { win: 3, unearned_win: 2.9, lose: 1, unearned_lose: 0 }
    end
    wm = { win: "○", unearned_win: "□", lose: "●", lose_failure: "▲", unearned_lose: "■" }

    points_and_marks = {}
    case reason
    when "勝ち"
      points_and_marks[:point] = { winner: wp[:win], loser: wp[:lose] }
      points_and_marks[:mark]  = { winner: wm[:win], loser: wm[:lose] }
    when "不戦勝"
      points_and_marks[:point] = { winner: wp[:unearned_win], loser: wp[:unearned_lose] }
      points_and_marks[:mark]  = { winner: wm[:unearned_win], loser: wm[:unearned_lose] }
    when "不戦勝_日程不調"
      points_and_marks[:point] = { winner: wp[:unearned_win], loser: wp[:lose_failure] }
      points_and_marks[:mark]  = { winner: wm[:unearned_win], loser: wm[:lose_failure] }
    end
    points_and_marks
  end

  def game_params
    if admin_user?
      params.require(:game).permit(
        :player1_id, :player2_id, :kisen_id,
        :id, :date, :winner_id, :teai, :kifu_id
      )
    else
      params.require(:game).permit(
        :id, :date, :winner_id, :teai, :kifu_id
      )
    end
  end

  def post_params
    params.require(:post).permit(:name, :comment, :owner_id, :picture, :remove_picture, :roots)
  end
end
