class InformationController < MessagesController
  before_action :set_information

  def edit
  end

  def update
    @information.update(information_params)
    flash[:success] = "活動情報を更新しました。"
    redirect_to root_path
  end

  private

  def set_information
    @information = Information.find_by(id: params[:id])
  end

  def information_params
    params.require(:information).permit(:title, :comment, :kisen_id)
  end
end
