# 棋譜を作成する
class KifusController < ApplicationController
  before_action :logged_in_user, only: [:update, :download, :downloads]

  def index
    query     = kifu_params[:query]
    @kifus    = Kifu.search(query).page(params[:page]).per(20)
    # 一括ダウンロードのためにidsを渡しておく
    @kifu_ids = Kifu.search(query).ids

    # @kifus = Kifu.player_search('WhiteWizard').page(params[:page]).per(20)
    # @kifus = Kifu.player_search('((player1:WhiteWizard winner:1) OR (player2:WhiteWizard winner:2)) AND MAT_FIBER').page(params[:page]).per(20)
  end

  def show
    @kifu = Kifu.find_by(id: params[:id])
  end

  def update
    kifu = Kifu.find_by(id: params[:kifu_id]) # hidden_field_tagの値
    kifu_max = kifu.kifu.length

    # コメントの編輯が可能
    comment_hash = {}
    (0..kifu_max).each do |n|
      if (c = params[n.to_s]).present?
        comment_hash[n] = c
      end
    end
    kifu.update_attributes(comment: comment_hash)

    # raw_kifuの更新
    lines = kifu.raw_kifu
    new_raw_kifu = ''
    body  = false
    n     = 0
    lines.split(NEWLINE).each do |line|
      strip_line = line.strip
      if body == false
        body = true if strip_line.include?("手数---")
        new_raw_kifu << line
        new_raw_kifu << NEWLINE
        next
      end

      # 棋譜本体またはコメントの処理
      if strip_line.first == '*'
        next
      elsif strip_line.first =~ /\d/
        n = strip_line.split(' ')[0].to_i
        # コメント差し替え
        if comment_hash[n - 1].present?
          comment_hash[n - 1].split(NEWLINE).each do |comment|
            new_raw_kifu << "*#{comment}#{NEWLINE}"
          end
        end
        new_raw_kifu << line
        new_raw_kifu << NEWLINE
      end
    end

    # 最終手にコメントがあったらそれを書き込む
    if comment_hash[n].present?
      comment_hash[n].split(NEWLINE).each do |comment|
        new_raw_kifu << "*#{comment}#{NEWLINE}"
      end
    end

    # 生棋譜の更新
    kifu.update_attributes(raw_kifu: new_raw_kifu)
    # 投稿日時の更新
    kifu.post.update_attributes(updated_at: Time.current)

    redirect_to request.referer
  end

  # 一つずつダウンロード用
  def download
    require 'browser'
    user_agent = request.env['HTTP_USER_AGENT']
    browser    = Browser.new(user_agent, accept_language: 'ja-JP')

    kifu     = Kifu.find_by(id: params[:id] || params[:kifu_id_for_download])
    kif      = kifu.raw_kifu
    filename = "5JK_#{kifu.id}_#{kifu.player1}_#{kifu.player2}.kif"
    if browser.platform.windows?
      # SJISに変換
      kif = sjis_safe(kif).encode(Encoding::SJIS)
    end
    send_data(kif, filename: filename)
  end

  # 検索した棋譜を一括ダウンロード
  def downloads
    require 'fileutils'
    require 'browser'
    require 'zip'

    user_agent = request.env['HTTP_USER_AGENT']
    browser    = Browser.new(user_agent, accept_language: 'ja-JP')

    # 棋譜ファイルを作業用ディレクトリに書き出す
    FileUtils.mkdir('./public/tmp')
    kifu_ids       = params[:id].split('/')
    kifu_filenames = []
    kifu_ids.each do |id|
      kifu     = Kifu.find_by(id: id)
      kif      = kifu.raw_kifu
      filename = "5JK_#{kifu.id}_#{kifu.player1}_#{kifu.player2}.kif"
      kif      = sjis_safe(kif).encode(Encoding::SJIS) if browser.platform.windows?
      File.open("public/tmp/#{filename}", "w") do |f| # wは書き込み権限
        f.puts kif
        kifu_filenames << f.path
      end
    end

    # 圧縮パス
    zippath = Rails.root.join('public', 'tmp', 'kifu_archive.zip')
    # Zip圧縮
    zippath = compress(kifu_filenames, zippath)
    # ダウンロードする
    send_data(File.read(zippath), filename: '5jk_kifus.zip')
    # send_file(zippath, filename: '5jk_kifus.zip') # send_file使えないみたい

    # 作業用ディレクトリ削除
    FileUtils.rm_rf('./public/tmp')
  end

  # Zip圧縮
  # https://qiita.com/ya-mada/items/c162383eda33dc516c39
  def compress(kifu_filenames, zippath)
    File.delete zippath if File.file?(zippath)
    Zip::File.open(zippath, Zip::File::CREATE) do |z_fp|
      kifu_filenames.each do |filepath|
        z_fp.add(File.basename(filepath), filepath)
      end
    end
    zippath
  end

  # http://qiita.com/akkun_choi/items/a1fbde5d6d065df3fc97
  # pry(main)> sjis_safe("〜−¢£¬−‖")
  # => "～－￠￡￢－∥"
  def sjis_safe(str)
    [
      %w(301C FF5E), # wave-dash
      %w(2212 FF0D), # full-width minus
      %w(00A2 FFE0), # cent as currency
      %w(00A3 FFE1), # lb(pound) as currency
      %w(00AC FFE2), # not in boolean algebra
      %w(2014 2015), # hyphen
      %w(2016 2225), # double vertical lines
    ].inject(str) do |s, (before, after)|
      s.gsub(
        before.to_i(16).chr('UTF-8'),
        after.to_i(16).chr('UTF-8'))
    end
  end

  def json
    kifu = Kifu.find_by(id: params[:id])
    p = KifuPresenter.new(kifu, self)

    # 棋譜リスト用 手のデータ
    te = ["--開始局面--"]
    kifu.kifu[1..-1].each.with_index(1) do |kif, n|
      te << "#{sprintf('%10d', n)}. #{code2te(kif)}"
    end
    te << (te.last.include?("▲") ? "#{sprintf('%3d', te.count)}. △投了" : "#{te.count}. ▲投了")

    render plain: {
      kifu_id:   kifu.id,
      jkfe:      kifu.kifu,
      info:      kifu.info,
      info_html: p.kifu_info,
      teai:      p.teai,
      comment:   kifu.comment,
      raw_kifu:  kifu.raw_kifu,
      te:        te,
      moves:     p.moves
    }.to_json
  end

  private

  # ユーザーのログインを確認する
  def logged_in_user
    unless logged_in_user?
      store_location
      flash[:danger] = "ログインして下さい。"
      redirect_to login_url
    end
  end

  def kifu_params
    params.permit(:query)
  end
end
