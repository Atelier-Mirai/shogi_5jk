# 各棋戦 要項の作成
class KisensController < ApplicationController
  before_action :admin_user?, only: [:new, :create, :edit, :update, :destroy]
  PER = 5

  def index
    # @kisens = %i(now_inviting not_begin closed all).map { |key|
    #   [key, Kisen.page(params[:page]).per(PER).send(key)]
    # }.to_h
    @kisens = Kisen.all
  end

  def show
    @kisen   = Kisen.find(params[:id])
    @entries = @kisen.entries.reorder(created_at: :desc)

    # 新人王戦の要項に、出場資格者を表示
    if @kisen.sym_name == :shinjin
      date              = @kisen.open_date.prev_year.beginning_of_year
      @rookie_candidate = Profile.newcomer(date)
    end
  end

  def new
    @kisen = Kisen.new
    @h     = {}
    %w(棋聖戦 王座戦 東西戦 名人戦 棋王戦 王将戦 王位戦 新人王戦 竜王戦).each do |key|
      @h[key] = Kisen.where('name = ?', key).maximum(:term).succ
    end
    render 'new'
  end

  def edit
    @kisen = Kisen.find(params[:id])
    @h = {}
    %w(棋聖戦 王座戦 東西戦 名人戦 棋王戦 王将戦 王位戦 新人王戦 竜王戦).each do |key|
      value = Kisen.where('name = ?', key).maximum(:term)
      @h.store(key, value.succ)
    end
    render 'edit'
  end

  def create
    @kisen = Kisen.new(kisen_params)
    if @kisen.save
      @kisen.update_columns(finish_date: Date.today.since(3.month))
      title   = "第#{@kisen.term.to_kan}期 #{@kisen.name}"
      Card.create(kisen_id: @kisen.id, title: title)
      flash[:success] = '要項を作成しました'
      redirect_to @kisen
    else
      flash[:danger] = '入力に誤りがあります'
      render 'new'
    end
  end

  def update
    @kisen = Kisen.find_by(id: params[:id])
    if @kisen.update_attributes(kisen_params)
      flash[:success] = '棋戦情報を更新しました'
      redirect_to @kisen
    else
      flash[:danger] = '入力に誤りがあります'
      render 'edit'
    end
  end

  def destroy
    @kisen = Kisen.find(params[:id])
    @kisen.destroy!
    flash[:success] = '棋戦情報を削除しました'
    redirect_to kisens_path
  end

  # 手合割
  def handicap
    return if params[:kisen].blank?
    # h = handicap_params.to_h.transform_values { |v| v.present? ? v : nil }
    h = handicap_params.to_h.transform_values { |v| v.presence }
    p1 = h[:dankyu1] || h[:rating1]
    p2 = h[:dankyu2] || h[:rating2]
    return unless p1
    return unless p2

    # 下位者がplayer2, 上位者がplayer1
    @player2, @player1, @difference, @teai = Dankyu.handicap(p1, p2)
  end

  # 一年間の棋戦の案内
  def information
  end

  # 棋戦共通ルール
  def common_rule
  end

  # 棋戦共通ルール(旧規定)
  def common_rule_old
  end

  private

  def admin_user?
    if current_user.nil? || !current_user.admin?
      flash[:danger] = '管理者権限が必要です'
      redirect_to root_path
    end
  end

  def kisen_params
    params.require(:kisen).permit(:term,
                                  :name,
                                  :open_date,
                                  :begin_date,
                                  :end_date)
  end

  def handicap_params
    params.require(:kisen).permit(:dankyu1,
                                  :rating1,
                                  :dankyu2,
                                  :rating2)
  end

end
