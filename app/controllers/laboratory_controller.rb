class LaboratoryController < ApplicationController
  # 研究会トップページ
  def index
  end

  # コメント付き棋譜
  def comment_kifu
  end

  # 将棋上達法
  def gather
  end

  # 四間飛車講座
  def four_file_rook
    render template: 'laboratory/lecture/four_file_rook'
  end

  # 右玉講座
  def right_king
    render template: 'laboratory/lecture/right_king'
  end

  # 旧研究用掲示板
  def old_bbs
  end

  # 特別IDとは
  def special_id
  end

  # 特別IDの使い方
  def special_id_usage
  end

  # 直通チャット
  def direct_chat
  end

  # 以下、研究会トップページ内よりリンク
  # 指定局面戦
  def particular_positions
  end

  # やまもと流右四間講座
  def migishiken
  end

  # 研究会の概要
  def outline
  end
end
