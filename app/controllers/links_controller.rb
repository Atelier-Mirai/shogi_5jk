class LinksController < ApplicationController
  before_action :set_link, only: [:update, :destroy]
  before_action :logged_in_user?, except: [:index]

  # CSRF対策で、sortのみajax許可
  protect_from_forgery :except => [:sort]

  def index
    @links = {}
    Link.pluck(:type).uniq.each do |kind|
      @links["#{kind}"] = Link.send(kind)
    end
  end

  def create
    # link  = params[:controller].singularize
    # @link = link.classify.constantize.new(link_params)
    @link = Link.new(link_params)
    @link.save
    redirect_back(fallback_location: links_path)
  end

  # 個々のlinkの編輯ではなく、type別の編輯画面にて一括編輯する
  def edit
    @links = Link.send(params[:type])
    @link = Link.new(type: params[:type])
  end

  def update
    @link.update(link_params)
    redirect_back(fallback_location: links_path)
  end

  def destroy
    @link.destroy
    redirect_back(fallback_location: links_path)
  end

  # this action will be called via ajax
  def sort
    link = Link.find(params[:link_id])
    link.update(link_params(link))
    head :ok, content_type: "text/html"
  end

  private

  def logged_in_user?
    if current_user.nil?
      redirect_to login_path
    end
  end

  def set_link
    @link = Link.find(params[:id])
  end

  # this action will be called via ajax
  def link_params(link = nil)
    params.require(:link).permit(:url, :text, :comment, :row_order_position, :type)
  end
end
