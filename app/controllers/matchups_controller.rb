# 各棋戦 対局表の作成
class MatchupsController < ApplicationController
  before_action :admin_user, except: [:index, :show]

  def index
    # 棋戦作成済みで、対局表も作成済みの一覧を返す
    # @kisens = %i(now_open not_open finished all).map { |key|
    #   [key, Kisen.send(key).select { |kisen| kisen.matchup.present? }]
    # }.to_h
    @kisens = Kisen.all.select { |kisen| kisen.matchup.present? }
  end

  def show
    @matchup = Matchup.find(params[:id])
    # 結果報告用
    @topic = Topic.find(@matchup.topic_result_id)
    @post  = @topic.posts.new

    ids = @matchup.games.pluck(:kifu_id).uniq.reject(&:nil?)
    @kifus = Kifu.where(id: ids)

    # 対局表のタブの名前
    @tab_name =
      case @matchup.kisen.to_sym
      when :tozai, :koryu
        %w(対局表 補足説明)
      else
        %w(タイトル戦 予選 補足説明)
      end
  end

  def new
    @matchup = Matchup.new
    render 'form'
  end

  # 対局表matchupにぶら下がる、対局gameを作成する
  def create
    # 同じ棋戦idで、対局表を再作成する際は、既に存在する対局表を削除する
    kisen = Kisen.find_by(id: params[:matchup][:kisen_id])
    kisen&.matchup&.destroy if kisen.present?

    # 対局表新規作成
    @matchup = Matchup.create(matchup_params)
    @matchup.preliminary_group = [] # << で追加できるように既定値設定
    @matchup.final_group       = [] # << で追加できるように既定値設定
    @matchup.save

    # 棋戦の開催方式
    holding_method = @matchup.holding_method
    # 週何回開催するか
    week_frequency = params[:matchup][:week_frequency].to_i

    # タイトルホルダー制の棋戦では、タイトル保持者と一般参加者に分ける
    sort = holding_method[:sort] || { rating: :desc }
    if holding_method[:titleholder_system]
      title_holder_entry = @matchup.title_holder_entry
      entries            = @matchup.general_participant_entries.reorder(sort)
    else
      entries            = @matchup.all_entries.reorder(sort)
    end

    # 各棋戦の開催方式に合わせて、全参加者を、予選グループに振り分ける
    groups = divide_group(entries, holding_method[:member], sort)
    # => {"A"=>[#<Entry id: 76, kisen_id: 48, user_id: 53, name: "iochigen",

    # グループ名を保持 24人参加なら予選はABCの3グループで行われる
    @matchup.update_columns(preliminary_group: ('A'...'A'.next(groups.count)).to_a) # => ["A", "B", "C"]

    # 対局表を生成
    case holding_method[:method]
    when :league
      groups.each do |group_name, entries|
        game_factory_league(
          entries:        entries,
          game_initial:   group_name,
          open_date:      @matchup.open_date + (group_name.ord - 'A'.ord),
          week_frequency: week_frequency)
      end
    when :tournament, :paramus
      groups.each do |group_name, entries|
        # 本戦 生成
        game_factory_tournament(
          entries:        entries,
          game_initial:   group_name,
          open_date:      @matchup.open_date + (group_name.ord - 'A'.ord),
          week_frequency: week_frequency,
          jump_offs:      1,
          method:         holding_method[:method])

        next unless holding_method[:repechage]

        # 敗者復活戦
        game_factory_tournament(
          entries:        entries,
          game_initial:   'R' + group_name, # R は敗者復活戦,
          open_date:      @matchup.games.maximum(:date).days_since(7),
          week_frequency: week_frequency,
          jump_offs:      2,
          method:         :repechage)
        @matchup.update_columns(final_group: @matchup.final_group << 'R')


        # 勝ち抜き決定戦
        # タイトル制の棋戦で、タイトル保持者の参加があった棋戦のみ、勝ち抜き決定戦を行う
        if @matchup.title_holder_entry.present?
          game_factory_tournament(
            entries: [DummyEntry.new(@matchup.maximum_game_number(group_name)),
                      DummyEntry.new(@matchup.maximum_game_number("R#{group_name}"))],
            game_initial: "W#{group_name}",
            open_date: @matchup.games.maximum(:date).days_since(7),
            week_frequency: 1,
            jump_offs: 2)
          @matchup.update_columns(final_group: @matchup.final_group << 'W')
        end
      end

    when :team
      # TODO: ３チーム以上になっている場合にも対応したら良い
      if groups.count == 2
        game_initial = groups.keys.join
        @matchup.update_columns(preliminary_group: [game_initial]) # => ["E1W1"]

        game_factory_team_competition(
          teams:          groups,           # 参加チーム groups = { E1: [members], W1: [members] }
          game_initial:   game_initial,     # E1W1
          week_frequency: week_frequency)   # 週何回開催するか
      end
    end

    # 予選通過者(qualifier)を抽出する
    qualifier = []
    # 名人戦は、A級の優勝者のみが、タイトル挑戦できる

    if @matchup.name == "名人戦"
      qualifier = [DummyEntry.new('A1')]
    elsif @matchup.name == "竜王戦"
      qualifier = [DummyEntry.new(@matchup.games.maximum(:game_number))]
    elsif holding_method[:method] == :tournament
      groups.each do |group_name, _entries|
        # 勝ち抜き決定戦があれば
        w = @matchup.maximum_game_number("W#{group_name}1") ? "W" : ""
        qualifier << DummyEntry.new(@matchup.maximum_game_number("#{w}#{group_name}1"))
      end
    elsif holding_method[:method].in? [:league]
      groups.each do |group_name, _entries|
        (1..holding_method[:qualifier]).each do |n|
          qualifier << DummyEntry.new("#{group_name}#{n}")
        end
      end
    end

    # 予選通過者一名 タイトルホルダー参加なし => そのままタイトル確定
    # 予選通過者一名 タイトルホルダー参加あり => 挑戦者決定戦は行わず、タイトル決定戦開催
    # 予選通過者複数 タイトルホルダー参加なし => そのまま予選通過者で、タイトル決定戦開催
    # 予選通過者複数 タイトルホルダー参加あり => 挑戦者決定戦開催及び、タイトル決定戦開催
    if qualifier.count == 1 && title_holder_entry.blank?
      # 予選通過者一名 タイトルホルダー参加なし => そのままタイトル確定
    elsif qualifier.count == 1 && title_holder_entry.present?
      # 予選通過者一名 タイトルホルダー参加あり => 挑戦者決定戦は行わず、タイトル決定戦開催
      game_factory_tournament(
        entries:      [title_holder_entry, qualifier[0]],
        game_initial: 'T',
        open_date:    @matchup.games.maximum(:date).days_since(7),
        jump_offs:    3)
      @matchup.update_columns(final_group: @matchup.final_group << 'T')
    elsif qualifier.count == 2 && title_holder_entry.blank?
      # 予選通過者複数 タイトルホルダー参加なし => そのまま予選通過者で、タイトル決定戦開催
      game_factory_tournament(
        entries:      qualifier,
        game_initial: 'T',
        open_date:    @matchup.games.maximum(:date).days_since(7),
        jump_offs:    3)
      @matchup.update_columns(final_group: @matchup.final_group << 'T')
    elsif qualifier.count == 2 && title_holder_entry.present?
      # 予選通過者複数 タイトルホルダー参加あり => 挑戦者決定戦開催及び、タイトル決定戦開催
      # 挑戦者決定戦
      game_factory_tournament(
        entries:      qualifier,
        game_initial: 'Q',
        open_date:    @matchup.games.maximum(:date).days_since(7),
        jump_offs:    1)
      @matchup.update_columns(final_group: @matchup.final_group << 'Q')
      # タイトル決定戦
      game_factory_tournament(
        entries:      [title_holder_entry, DummyEntry.new(@matchup.maximum_game_number('Q'))], # Q31
        game_initial: 'T',
        open_date:    @matchup.games.maximum(:date).days_since(7),
        jump_offs:    3)
      @matchup.update_columns(final_group: @matchup.final_group << 'T')
    end

    # 複数グループあると、R, Wが重複しているので
    @matchup.update_columns(final_group: @matchup.final_group&.uniq)

    # 棋戦終了日を更新
    @matchup.kisen.update_columns(finish_date: @matchup.games.last.date)

    # 結果表示しリダイレクト
    flash[:success] = '対局表を作成しました'

    redirect_to matchup_path(@matchup)
  end

  def edit
    @matchup = Matchup.find_by(id: params[:id])
    render 'form'
  end

  def update
    @matchup = Matchup.find_by(id: params[:id])
    if @matchup.update(matchup_params)
      flash[:success] = '対局表を更新しました'
      redirect_to matchup_path(@matchup)
    else
      render 'form'
    end
  end

  def destroy
    @matchup = Matchup.find_by(id: params[:id])
    Topic.find_by(id: @matchup.topic_schedule_id).destroy
    Topic.find_by(id: @matchup.topic_result_id).destroy

    kisen = @matchup.kisen
    @matchup.destroy
    flash[:danger] = '対局表を削除しました'
    redirect_to kisen_path(kisen)
  end

  private

  def matchup_params
    params.require(:matchup).permit(
      :kisen_id,
      :last_game_date1,
      :last_game_date2
    )
  end

  # 総当たり戦
  def game_factory_league(
    entries:        ,           # 参加者
    game_initial:   group_name, # A組なら対局名はAで始まる
    open_date:      ,           # 対局開始日
    week_frequency: )           # 週何回開催するか
    game_dates = game_dates(open_date, total_games(entries.count, :league), times: week_frequency)
    h          = turn_pass_determinant(entries.count) # 日付の順番のために

    entries.each_with_index do |entry, row|
      (row + 1...entries.count).each do |col|
        game_number = "#{game_initial}#{row + 1}#{col + 1}"
        player1_id  = entry.user_id
        player2_id  = entries[col].user_id
        game_date   = game_dates[h["#{row}#{col}"]]
        Game.create(matchup_id:  @matchup.id,
                    game_number: game_number,
                    player1_id:  player1_id,
                    player2_id:  player2_id,
                    date:        game_date)
      end
    end
  end

  # トーナメント戦 作成
  def game_factory_tournament(
      entries:, # 参加者
      game_initial:, # A組なら対局名はAで始まる
      open_date:         @matchup.open_date, # 開幕日を既定値
      week_frequency:    1,     # 週何回開催するか
      jump_offs:         3,     # 決勝戦は、１回勝負、それとも３回勝負
      method:            :tournament) # トーナメント方式

    game_dates = game_dates(open_date, (entries.count - 1) + (jump_offs - 1), times: week_frequency)

    case method
    # 普通のトーナメント
    when :tournament
      sa = seed_algorithm(entries)
      sa.each_value do |v|
        Game.create(matchup_id:  @matchup.id,
                    game_number: "#{game_initial}#{v[:gn]}",
                    player1_id:  v[:p][0].to_s.sub('g', game_initial),
                    player2_id:  v[:p][1].to_s.sub('g', game_initial),
                    seed1:       v[:seed].present? ? v[:seed][0] : nil,
                    seed2:       v[:seed].present? ? v[:seed][1] : nil,
                    date:        game_dates.shift)
      end
      # 通常は一回勝負であるが、最終回のみjump_offsを反映
      # 不足する3回戦第2局 第3局を追加
      if jump_offs >= 2
        last_key  = sa.keys.last
        round_max = last_key.to_s[1]
        (2..jump_offs).each do |j|
          Game.create!(matchup_id: @matchup.id,
                       game_number: "#{game_initial}#{round_max}#{j}",
                       player1_id:  sa[last_key][:p][0].to_s.sub('g', game_initial),
                       player2_id:  sa[last_key][:p][1].to_s.sub('g', game_initial),
                       date:        game_dates.shift)
        end
      end

    # 敗者復活戦
    when :repechage
      sa = seed_algorithm_repechage(entries.count)
      sa.each_value do |v|
        Game.create(matchup_id:  @matchup.id,
                    game_number: "#{game_initial}#{v[:gn]}",
                    player1_id:  v[:p][0].to_s.sub('g', game_initial).sub('a', game_initial[1].downcase),
                    player2_id:  v[:p][1].to_s.sub('g', game_initial).sub('a', game_initial[1].downcase),
                    seed1:       v[:seed].present? ? v[:seed][0] : nil,
                    seed2:       v[:seed].present? ? v[:seed][1] : nil,
                    date:        game_dates.shift)
      end

    # パラマス式トーナメント
    when :paramus
      # entries は rating の降順で渡されてくる。
      # 弱い人の対局が先に作られるように、逆順に並び替えておく
      entries = entries.reverse
      (1...entries.count).each do |r|
        player1_id = entries[r].user_id
        player2_id = (r - 1 == 0 ? player2_id = entries[0].user_id : "#{game_initial}#{r - 1}1")
        Game.create(matchup_id:  @matchup.id,
                    game_number: "#{game_initial}#{r}1",
                    player1_id:  player1_id,
                    player2_id:  player2_id,
                    date:        game_dates.shift)
      end
    end
  end

  # 団体戦（星取り形式）
  # entries 前半は東 後半は西 東西の人数は同じ
  # team_names 未使用
  def game_factory_team_competition(
    teams:, # 参加チーム { team1: [members], team2: [members] } の形式
    game_initial:, # A組なら対局名はAで始まる
    week_frequency: 1) # 週何回開催するか

    open_date    = @matchup.open_date

    team_names   = teams.keys
    team_members = teams.values

    game_dates = game_dates(open_date, [team_members[0].count, team_members[1].count].max, times: week_frequency)

    # rating の合計を求める
    rating_total = [team_members[0].inject(0) { |sum, entry| sum + entry.rating },
                    team_members[1].inject(0) { |sum, entry| sum + entry.rating }]

    # チームメンバー数は一致しているか？
    if team_members[0].count > team_members[1].count
      many = 0
      few  = 1
    else
      few  = 0
      many = 1
    end

    if team_members[0].count != team_members[1].count
      # 人数が少ないチームが二回出場する。
      # R差が小さくなるように、二回出場するメンバーを決める
      diff_adjustment = Array.new(team_members[few].count, 0)
      team_members[few].each_with_index do |member, i|
        diff_adjustment[i] = rating_total[many] - (rating_total[few] + member.rating)
      end
      # index番目の人を加えると、差が一番小さくなる
      index = diff_adjustment.index(diff_adjustment.min)
      team_members[few].insert(index, team_members[few][index])
    end

    # team[0], team[1] 同じ人数になった
    (0...(team_members[0].count)).each do |n|
      Game.create!(matchup_id: @matchup.id,
                   game_number: "#{game_initial}#{n + 1}",
                   player1_id:  team_members[0][n].user_id,
                   player2_id:  team_members[1][n].user_id,
                   date:        game_dates.shift)
    end
  end

  # 対局開始日と、必要な総対局数を基に、対局日を返す
  def game_dates(open_date, total_games, options = {})
    default_options = { times: 2 } # 週二回開催
    options         = default_options.merge(options)

    # 週二回開催で受け取ったのを、何曜日開催に変換する
    # options = { wday: [6, 0] }  # 土日開催
    if options[:wday].present?
      wdays = options[:wday]
    else
      wdays = []
      interval = (7 / options[:times]) % 7
      wdays << open_date.wday
      wdays << (wdays.last + interval) % 7 while wdays.count < options[:times]
    end

    # 棋戦の開催日
    results = []
    results << open_date
    while results.count < total_games
      last_date = results.last
      interval = next_interval(last_date.wday, wdays)
      results << last_date + interval
    end
    results
  end

  # 次の棋戦開催日までの間隔を求める
  # 6(土曜日)なら3(水曜日)までは、4日後
  def next_interval(now_wday, wdays)
    # now_wday # => 2 (火曜日)
    # wdays    # => [2, 5] 週二回、火曜日と金曜日開催

    now_index  = wdays.find_index(now_wday)
    next_index = now_index.succ % wdays.size

    # 次回開催曜日
    next_wday  = wdays[next_index]

    # 次の開催日までの日数を求める
    if (next_wday - now_wday) % 7 == 0
      # (週一回開催なら、7日後を返す)
      7
    else
      # (2 - 5) % 7 => 4 翌週の火曜日は4日後
      (next_wday - now_wday) % 7
    end
  end

  # 参加人数を受け取り、必要な総対局数を返す
  def total_games(member, option = :league)
    case option
    when :league then     member * (member - 1) / 2
    when :tournament then member - 1
    when :paramus then    member - 1
    end
  end

  # シードアルゴリズム
  # matchup#create Game.create 用
  def seed_algorithm(m) # mはEntryクラスのインスタンスの集合
    h = {}
    case m.count
    when 2
      # 参加メンバーが二人の時
      # gn: game_number, p: player
      g11 = { gn: 11, p: [m[0].user_id, m[1].user_id] }
      h   = { g11: g11 }
    when 3
      # g11 = { gn: 11, p: [m[0].user_id, m[3].user_id] }
      g12 = { gn: 12, p: [m[2].user_id, m[1].user_id] }
      g21 = { gn: 21, p: [m[0].user_id, :g12], seed: [1, nil] }
      h   = { g12: g12, g21: g21 }
    when 4
      g11 = { gn: 11, p: [m[0].user_id, m[3].user_id] }
      g12 = { gn: 12, p: [m[2].user_id, m[1].user_id] }
      g21 = { gn: 21, p: [:g11, :g12] }
      h   = { g11: g11, g12: g12, g21: g21 }
    when 5
      # g11 = { gn: 11, p: [m[0].user_id, m[7].user_id] }
      g12 = { gn: 12, p: [m[4].user_id, m[3].user_id] }
      # g13 = { gn: 13, p: [m[2].user_id, m[5].user_id] }
      # g14 = { gn: 14, p: [m[6].user_id, m[1].user_id] }
      g21 = { gn: 21, p: [m[0].user_id, :g12], seed: [1, nil] }
      g22 = { gn: 22, p: [m[2].user_id, m[1].user_id], seed: [1, 1] }
      g31 = { gn: 31, p: [:g21, :g22] }
      h   = { g12: g12, g21: g21, g22: g22, g31: g31 }
    when 6
      # g11 = { gn: 11, p: [m[0].user_id, m[7].user_id] }
      g12 = { gn: 12, p: [m[4].user_id, m[3].user_id] }
      g13 = { gn: 13, p: [m[2].user_id, m[5].user_id] }
      # g14 = { gn: 14, p: [m[6].user_id, m[1].user_id] }
      g21 = { gn: 21, p: [m[0].user_id, :g12], seed: [1, nil] }
      g22 = { gn: 22, p: [:g13, m[1].user_id], seed: [nil, 1] }
      g31 = { gn: 31, p: [:g21, :g22] }
      h   = { g12: g12, g13: g13, g21: g21, g22: g22, g31: g31 }
    when 7
      # g11 = { gn: 11, p: [m[0].user_id, m[7].user_id] }
      g12 = { gn: 12, p: [m[4].user_id, m[3].user_id] }
      g13 = { gn: 13, p: [m[2].user_id, m[5].user_id] }
      g14 = { gn: 14, p: [m[6].user_id, m[1].user_id] }
      g21 = { gn: 21, p: [m[0].user_id, :g12], seed: [1, nil] }
      g22 = { gn: 22, p: [:g13, :g14] }
      g31 = { gn: 31, p: [:g21, :g22] }
      h   = { g12: g12, g13: g13, g14: g14, g21: g21, g22: g22, g31: g31 }
    when 8
      g11 = { gn: 11, p: [m[0].user_id, m[7].user_id] }
      g12 = { gn: 12, p: [m[4].user_id, m[3].user_id] }
      g13 = { gn: 13, p: [m[2].user_id, m[5].user_id] }
      g14 = { gn: 14, p: [m[6].user_id, m[1].user_id] }
      g21 = { gn: 21, p: [:g11, :g12] }
      g22 = { gn: 22, p: [:g13, :g14] }
      g31 = { gn: 31, p: [:g21, :g22] }
      h   = { g11: g11, g12: g12, g13: g13, g14: g14, g21: g21, g22: g22, g31: g31 }
    end
    h
  end

  # シードアルゴリズム敗者復活戦
  # matchup#create Game.create 用
  def seed_algorithm_repechage(member_count)
    h = {}
    case member_count
    when 3
      g11 = { gn: 11, p: [:a21, :a12] }
      h   = { g11: g11 }
    when 4
      g12 = { gn: 12, p: [:a11, :a12] }
      g21 = { gn: 21, p: [:a21, :g12], seed: [1, nil] }
      h   = { g12: g12, g21: g21 }
    when 5
      g11 = { gn: 11, p: [:a21, :a12] }
      g21 = { gn: 21, p: [:g21, :a22], seed: [nil, 1] }
      g31 = { gn: 31, p: [:g31, :a31], seed: [nil, 2] }
      h   = { g11: g11, g21: g21, g31: g31 }
    when 6
      g11 = { gn: 11, p: [:a21, :a12] }
      g12 = { gn: 12, p: [:a13, :a22] }
      g21 = { gn: 21, p: [:g11, :g12] }
      g31 = { gn: 31, p: [:g21, :a31], seed: [nil, 2] }
      h   = { g11: g11, g12: g12, g21: g21, g31: g31 }
    when 7
      g13 = { gn: 13, p: [:a13, :a14] }
      g21 = { gn: 21, p: [:a21, :a12], seed: [1,   1] }
      g22 = { gn: 22, p: [:g13, :a22], seed: [nil, 1] }
      g31 = { gn: 31, p: [:g21, :g22] }
      g41 = { gn: 41, p: [:g31, :a31], seed: [nil, 3] }
      h   = { g13: g13, g21: g21, g22: g22, g31: g31, g41: g41 }
    when 8
      g12 = { gn: 12, p: [:a11, :a12] } # A11の対局の敗者がa11
      g13 = { gn: 13, p: [:a13, :a14] }
      g21 = { gn: 21, p: [:a21, :g12], seed: [1, nil] }
      g22 = { gn: 22, p: [:g13, :a22], seed: [nil, 1] }
      g31 = { gn: 31, p: [:g21, :g22] }
      g41 = { gn: 41, p: [:g31, :a31], seed: [nil, 3] }
      h   = { g12: g12, g13: g13, g21: g21, g22: g22, g31: g31, g41: g41 }
    end
    h
  end

  # 参加者を一グループ8人までに分割する
  def divide_group(entries, member_max, sort)
    member_max = entries.count if member_max == :all

    case sort.keys[0]
    when :rating # レーティング順
      group_max = (entries.count.to_f / member_max).ceil
      groups = {}
      entries.each_with_index do |entry, i|
        letter = 'A'.next(i % group_max) # 均等に振り分ける
        groups[letter] = [] if groups[letter].nil?
        groups[letter] << entry
      end
      groups
    when :meijin_class # 順位戦クラス別
      groups = {}
      entries.each do |entry|
        letter = entry.meijin_class.first # 先頭一文字
        groups[letter] = [] if groups[letter].nil?
        groups[letter] << entry
      end
      groups
    when :area # 都道府県順
      divide_team(entries, member_max)
    when :team
      groups = {}
      entries.each do |entry|
        groups[entry.team] = [] if groups[entry.team].nil?
        groups[entry.team] << entry
      end
      groups
    end
  end

  # 東西戦専用
  # 10人までは2チーム
  # 15人までは3チーム
  # 20人までは4チーム
  def divide_team(entries, member_max)
    team_max = (entries.count / member_max.to_f).ceil == 1 ? 2 : (entries.count / member_max.to_f).ceil

    team = []
    (0...team_max).each { |n| team[n] = [] }

    # 各チームの定員を求める
    fixed_numbers = []
    (0...team_max).each { |n| fixed_numbers[n] = entries.count / team_max }

    # 余りがあれば、各チームの定員を一人ずつ増やす
    remain = entries.count % team_max
    (0...remain).each { |n| fixed_numbers[n] += 1 }
    # fixed_numbers #=> [5, 5, 4, 4]

    # 各チームに参加申込者を割り振る
    # team[0] は 5人 entries[0..5]
    # team[1] は 5人 entries[6..10]
    # team[2] は 4人 entries[11..14]
    # team[3] は 4人 entries[15..18] から成るチームにする
    fixed_numbers.unshift(0) #=> [0, 5, 5, 4, 4]
    (0...team_max).each do |n|
      from = fixed_numbers[0..n].sum
      to   = fixed_numbers[0..n + 1].sum - 1
      # rating の小さい順に並び替えて、格納する
      team[n] = entries[from..to].sort { |a, b| a.rating <=> b.rating }
    end

    # FIXME: Array#group_byなどで綺麗に書けそう
    # メンバーが割り振られたので、
    # 東, 東2, 東3, 西のようにチーム名を付ける
    groups     = {}
    team_count = { east: 0, west: 0 }
    (0...team_max).each do |n|
      # チーム内で各メンバーが東西いずれに属するか、カウント
      count = { east: 0, west: 0 }
      team[n].each do |entry|
        Prefecture.new(entry.area).east? ? count[:east] += 1 : count[:west] += 1
      end
      # 東チームか、西チームか？
      if count[:east] > count[:west] || Prefecture.new(team[n].last.area).east?
        team_count[:east] += 1
        team[n].each { |entry| entry.update_columns(team: "東#{team_count[:east] == 1 ? '' : team_count[:east]}") }
        groups["E#{team_count[:east]}"] = team[n]
      elsif count[:west] < count[:east] || Prefecture.new(team[n].last.area).west?
        team_count[:west] += 1
        team[n].each { |entry| entry.update_columns(team: "西#{team_count[:west] == 1 ? '' : team_count[:west]}") }
        groups["W#{team_count[:west]}"] = team[n]
      end
    end
    groups
  end
end
