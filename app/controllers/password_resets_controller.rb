# パスワードリセット
class PasswordResetsController < ApplicationController
  # before_action :get_user,         only: [:edit, :update]
  # before_action :valid_user,       only: [:edit, :update]
  # before_action :check_expiration, only: [:edit, :update]

  def new
    @password_reset_form = PasswordResetEmailConfirmationForm.new
  end

  def create
    @password_reset_form = PasswordResetEmailConfirmationForm.new(password_reset_params)
    if @password_reset_form.save
      flash[:info] = "パスワード再設定のためのメールを送信しました。"
      redirect_to root_url
    else
      render 'new'
    end
  end

  def edit
    @user = User.find_by(email: params[:email])
    unless @user && @user.activated? &&
      Authenticator.new(@user).authenticate(:reset, params[:id])
      flash[:danger] = "ユーザー認証に失敗しました"
      redirect_to root_url
    end

    @password_reset_form = PasswordResetNewPasswordForm.new(email: params[:email])
  end

  def update
    @user = User.find_by(email: params[:email])
    unless @user &&
      Authenticator.new(@user).authenticate(:reset, params[:id])
      flash[:danger] = "ユーザー認証に失敗しました"
      redirect_to root_url
    end

    @password_reset_form       = PasswordResetNewPasswordForm.new(password_reset_params)
    @password_reset_form.name  = @user.name
    @password_reset_form.email = @user.email
    if @password_reset_form.save
      log_in @user
      flash[:success] = "パスワードを再設定しました。"
      redirect_to root_url
    else
      render 'edit'
    end
  end

  private

  def password_reset_params
    params.require(:password_reset).permit(:name, :email, :password, :password_confirmation)
  end

  # def password_reset_new_password_params
  #   params.require(:password_reset).permit(:password, :password_confirmation)
  # end

  def get_user
    # User.find_by(id: params[:email])
    # updateの際、本番環境では、なぜか params[:email]が、
    # WhiteWizardと、ユーザー名となるので、追加
    # @user = User.find_by(email: params[:email]) || User.find_by(name: params[:email])
    @user = User.find_by(email: params[:email])
  end

  # 正しいユーザーか確認する
  def valid_user
    unless @user && @user.activated? &&
      Authenticator.new(@user).authenticate(:reset, params[:id])
      flash[:danger] = "ユーザー認証に失敗しました"
      redirect_to root_url
    end
  end

  # リセットトークンが期限切れかどうか、確認する
  def check_expiration
    if @user.reset_sent_at < 2.hours.ago
      flash[:danger] = "パスワード再設定の期限を過ぎました。"
      redirect_to new_password_reset_url
    end
  end
end
