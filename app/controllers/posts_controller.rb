# 投稿
class PostsController < ApplicationController
  before_action :restrict_remote_ip, except: [:index]
  before_action :logged_in_user?, only: [:edit]

  def index
  end

  def new
    @topic = Topic.find(params[:topic_id])
  end

  def create
    @topic = Topic.find(params[:topic_id])
    mode = params[:post][:mode]

    if current_user
      params[:post][:owner_id] = current_user.id
      params[:post][:name]     = admin_user? ? "運営" : current_user.name
    else
      params[:post][:owner_id] = 0
    end
    @post  = @topic.posts.build(post_params)

    # if (mode = params[:post][:mode]) != 'reply'
    #   # 画像認証 ruby-recaptcha
    #   if guest_user? && validate_recap(params, @post.errors) == false
    #     flash[:danger] = "画像認証に失敗しました。"
    #     redirect_to request.referer
    #     return
    #   end
    # end

    # 添付ファイル確認
    if params[:upload_file].present?
      filename  = params[:upload_file].original_filename
      extension = File.extname(filename)
      if extension.in? ['.kif']
        # 添付ファイルが棋譜だったら
        require 'kconv'
        raw_kifu = params[:upload_file].read.toutf8
        @post[:comment] << NEWLINE << raw_kifu
        # (文字コードutf-8で「あいうえお」と書かれたファイルがアップされた場合)
        # => "\xE3\x81\x82\xE3\x81\x84\xE3\x81\x86\xE3\x81\x88\xE3\x81\x8A\n"
        # (文字コードshift_jisで「あいうえお」と書かれたファイルがアップされた場合)
        # => "\x82\xA0\x82\xA2\x82\xA4\x82\xA6\x82\xA8\n"
      elsif extension.downcase.in? ['.gif', '.jpeg', '.jpg', '.png']
        # 添付ファイルが画像だったら、
        @post.destroy # 先に生成した@postを破棄
        params[:post][:picture] = params[:upload_file]
        @post = @topic.posts.build(post_params)
      end
    end

    if @post.save
      if mode == 'reply'
        # 返信だったら
        Parentage.create(parent_id: params[:post][:parent_id], child_id: @post.id)
        @post.update(roots: :descendant) # 子に設定
      else
        @post.update(roots: :progenitor) # 親に設定
      end

      # トピックの更新日付を更新する
      @topic.update(updated_at: @post.updated_at)
      flash[:success] = (mode != 'reply' ? "新規記事を投稿しました" : "返信しました")
      redirect_to request.referer
      return
    else
      flash[:danger] = "投稿に失敗しました。"
      redirect_to request.referer
      return
    end
  end

  def edit
    store_location
    @topic = Topic.find(params[:topic_id])
    @post  = Post.find(params[:id])
  end

  def update
    @topic = Topic.find(params[:topic_id])
    @post  = Post.find(params[:id])

    # 添付ファイル確認
    if params[:upload_file].present?
      filename  = params[:upload_file].original_filename
      extension = File.extname(filename)
      if extension.in? ['.kif']
        # 添付ファイルが棋譜だったら
        require 'kconv'
        raw_kifu = params[:upload_file].read.toutf8
        @post[:comment] << NEWLINE << raw_kifu
        # (文字コードutf-8で「あいうえお」と書かれたファイルがアップされた場合)
        # => "\xE3\x81\x82\xE3\x81\x84\xE3\x81\x86\xE3\x81\x88\xE3\x81\x8A\n"
        # (文字コードshift_jisで「あいうえお」と書かれたファイルがアップされた場合)
        # => "\x82\xA0\x82\xA2\x82\xA4\x82\xA6\x82\xA8\n"
      elsif extension.downcase.in? ['.gif', '.jpeg', '.jpg', '.png']
        # 添付ファイルが画像だったら、
        params[:post][:picture] = params[:upload_file]
      end
    end

    # 棋譜の削除
    if params[:remove_kifu].present? && params[:remove_kifu][:value] == '1'
      Kifu.find_by(id: @post.kifu_id).destroy
      @post.update(kifu_id: nil)
    end

    # 保存処理
    if @post.valid?
      @post.update(post_params)
      # トピックの更新日付を更新する
      @topic.update(updated_at: @post.updated_at)
      flash[:success] = "記事を更新しました。"
      redirect_to session[:return_to] || topic_path(@post.topic_id)
    else
      flash[:danger] = "更新に失敗しました。"
      render :edit
    end
  end

  def destroy
    post  = Post.find_by!(id: params[:id])
    topic = post.topic

    post.destroy!
    if Post.where(topic_id: topic.id).count == 0
      flash[:success] = "スレッドと記事を削除しました"
      Topic.delete(topic.id)
      redirect_to request.referer
    else
      flash[:success] = "記事を削除しました"
      last_post = Post.order(updated_at: :desc).find_by(topic_id: topic.id)
      topic.update(updated_at: last_post.updated_at)
      redirect_to request.referer
    end
  end

  private

  def post_params
    params.require(:post).permit(:name, :comment, :owner_id, :picture, :remove_picture, :roots)
  end

  def enough_authority
    unless logged_in_user?
      flash[:danger] = "ログインして下さい"
      redirect_to login_path
    end
    post = Post.find(params[:id])
    unless current_user&.post_owner?(post) || admin_user?
      flash[:danger] = "権限がありません"
      redirect_to root_path
    end
  end

  # ipアドレスからホスト名を取得、
  # 国外からの書き込みを禁止する
  def restrict_remote_ip
    require 'resolv'

    ip_address = request.remote_ip    # => 123.456.789.012
    host = Resolv.getname(ip_address) # => example.co.jp
    if guest_user? && host.exclude?('.jp') && !host.in?(['localhost', 'example.com'])
      flash[:danger] = "スパム対策のため、ゲストの書き込みは禁止しております。"
      redirect_to root_path
    end
  end
end
