# ユーザープロフィール
class ProfilesController < ApplicationController
  before_action :logged_in_user, only: [:new, :create, :edit, :update, :destroy]
  before_action :correct_user,   only: [:new, :create, :edit, :update, :destroy]

  def show
    @user = User.find(params[:user_id])
    @profile = Profile.find_by(user_id: params[:user_id])
  end

  def new
    @user    = User.find(params[:user_id])
    @profile = Profile.new
  end

  def create
    @user    = User.find(params[:user_id])
    @profile = Profile.new(profile_params)

    # 隠しフィールドでViewから送らせる
    # @profile.update_attributes(id: params[:user_id])
    # @profile.update_attributes(user_id: params[:user_id])
    # @profile.update_attributes(joined_dankyu: rating2dankyu(@profile.joined_rating))

    @profile.save
    flash[:success] = "プロフィールを作成しました"
    if admin_user?
      redirect_to users_path
    else
      redirect_to user_profile_path(params[:user_id])
    end
  end

  def edit
    @user    = User.find(params[:user_id])
    @profile = Profile.find_by(user_id: params[:user_id])
  end

  def update
    @profile = Profile.find_by(user_id: params[:user_id])
    if @profile.update_attributes(profile_params)
      flash[:success] = "プロフィールを更新しました"
      if admin_user?
        redirect_to users_path
      else
        redirect_to user_profile_path(params[:user_id])
      end
    end
  end

  def destroy
  end

  private

  def profile_params
    if admin_user?
      params.require(:profile).permit(:number,
                                      :meijin_class,
                                      :joined_date,
                                      :joined_dankyu,
                                      :joined_rating,
                                      :proud_tactics,
                                      :favorite_kishi,
                                      :appeal,
                                      # :main_results,
                                      # :graduation,
                                      # :area,
                                      :condition,
                                      :user_id
                                     )
    else
      params.require(:profile).permit(:proud_tactics,
                                      :favorite_kishi,
                                      :appeal,
                                      :condition,
                                      :user_id)
    end
  end

  def correct_user
    @user = User.find(params[:id] || params[:user_id])
    if !current_user.admin? && !current_user?(@user)
      flash[:danger] = "許可されていない操作です"
      redirect_to root_url
    end
  end
end
