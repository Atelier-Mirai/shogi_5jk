# ランキング
class RankingsController < ApplicationController
  def index
    # ランキング
    if params[:ranking].nil?
      ymd = Ranking.maximum('ymd')
      @ymd = ymd.beginning_of_month
    else
      @ymd = Date.parse(params[:ranking][:ymd]).beginning_of_month
    end
    @rankings = Ranking.where(ymd: @ymd)

    # 今年の履歴
    period = Enumerator.new do |yielder|
      from = start_date(@ymd) # 期間の始め
      to   = @ymd             # 期間の終わり
      while from <= to
        yielder << from
        from = from.next_month
      end
    end
    @monthly_rankers = []
    period.each do |ymd|
      @monthly_rankers << Ranking.monthly_crown(ymd)
    end

    # 年間成績四冠
    @annually_rankers = Ranking.annually_crown(@ymd)

    # 年間三冠
    @annually_count_crown = Ranking.annually_count_crown(@ymd)
  end

  def show
  end

  def new
    @ranking = RankingForm.new
  end

  def create
    @ranking = RankingForm.new(ranking_params)

    # 準備
    ymd = Date.parse(@ranking.ymd).beginning_of_month
    Ranking.where(ymd: ymd).destroy_all # 旧データがあったら削除

    # rating_distribution 段級位とその人数とのハッシュ
    ranks = %i(八段 七段 六段 五段 四段 三段 二段 初段 １級 ２級 ３級 ４級 ５級 ６級 ７級 ８級 ９級 10級 11級 12級 13級 14級 15級 初心)
    ratings = []
    (1..24).each { |n| ratings << @ranking.send("rank#{n}").to_i }
    rating_distribution = [ranks, ratings].transpose.to_h

    r   = RatingDistribution.create(ymd: ymd, data: rating_distribution)
    sss = ShogiStandardScore2.new(ymd)
    r.update_columns(info: sss.values(9999))

    # 添付ファイルよりデータを取得する
    html        = @ranking.upload_file.read.force_encoding('utf-8')

    # スクレイピング
    data       = []
    doc        = Nokogiri::HTML.parse(html, nil, 'utf-8')

    doc.css('#myTable tr').each do |node|
      data << node.css('td')
    end
    data.shift # 先頭のデータはタイトル行。不要なので捨てる

    data.each do |datum|
      user = User.joins(:profile).merge(Profile.active_member).find_by(name: datum[1].text)
      next unless user
      # ["id", "ymd", "rank", "user_id", "name", "rating", "ave", "win", "lose", "game", "best_r", "month_ave", "month_win", "month_lose", "month_game", "month_rating_up", "month_prize_ave", "month_prize_win", "month_prize_game", "year_ave", "year_win", "year_lose", "year_game", "year_rating_up", "year_prize_ave", "year_prize_win", "year_prize_game", "year_prize_rating_up"]
      ranking           = Ranking.new
      ranking.ymd       = ymd
      ranking.user_id   = user.id
      ranking.name      = user.name
      ranking.rating    = datum[3].text
      ranking.ave       = datum[4].text.to_f
      ranking.win       = datum[5].text
      ranking.lose      = datum[6].text
      ranking.game      = datum[8].text
      ranking.best_r    = datum[9].text

      # 24全体順位、上位％、将棋偏差値を取得
      scores = sss.values(ranking.rating)
      ranking.superior_rank  = scores[:superior_rank]
      ranking.percentage     = scores[:percentage].round(2)
      ranking.standard_score = scores[:standard_score].round(2)

      ranking.save!

      # ランキング表の作成
      # 先月のデータ取得
      if (last_month = ranking.last_month_data)
        ranking.month_win       = ranking.win    - last_month.win
        ranking.month_lose      = ranking.lose   - last_month.lose
        ranking.month_game      = ranking.game   - last_month.game
        ranking.month_rating_up = ranking.rating - last_month.rating

        ranking.month_ave = if (ranking.month_win + ranking.month_lose) != 0
                              ranking.month_win.to_f / (ranking.month_win + ranking.month_lose).to_f
                            else
                              0
                            end
      else
        ranking.month_ave       = 0
        ranking.month_win       = 0
        ranking.month_lose      = 0
        ranking.month_game      = 0
        ranking.month_rating_up = 0
      end

      # 前年のデータ取得
      if (last_year = ranking.last_year_data)
        ranking.year_win       = ranking.win    - last_year.win
        ranking.year_lose      = ranking.lose   - last_year.lose
        ranking.year_game      = ranking.game   - last_year.game
        ranking.year_rating_up = ranking.rating - last_year.rating

        ranking.year_ave = if (ranking.year_win + ranking.year_lose) != 0
                             ranking.year_win.to_f / (ranking.year_win + ranking.year_lose).to_f
                           else
                             0
                           end
      else
        ranking.year_ave       = 0
        ranking.year_win       = 0
        ranking.year_lose      = 0
        ranking.year_game      = 0
        ranking.year_rating_up = 0
      end
      ranking.save!
    end

    # 月間三冠算出
    rs = Ranking.where(ymd: ymd)
    rs.each do |r|
      %i(ave win game).each do |crown|
        r["month_prize_#{crown}"] = r.month_prize_rank(crown)
      end
      r.save!
    end

    # 年間四冠算出
    # 年の対局数が２０×月数以上が、年間四冠の対象者となる
    # 入会月により、２０局でも年間四冠の対象者となり得る。
    # そこで、対象者かどうかのフラグフィールドとして
    # rankフィールドを流用する
    rs.each do |r|
      r[:rank] = 1 # 全員仮に対象者とする
      lyd = r.last_year_data # 年初データ
      if lyd.nil? # 今月入会したばかりの人は、年初データはnil。四冠対象外
        r[:rank] = 999 # 対象外
      else
        mc = ((r.ymd - lyd.ymd) / 30.4).round(0) # 月数
        if r.year_game < 20 * mc
          r[:rank] = 999 # 対象外
        end
      end
      r.save!
    end

    rs.each do |r|
      %i(ave win game rating_up).each do |crown|
        r["year_prize_#{crown}"] = r.year_prize_rank(crown)
      end
      r.save!
    end

    # 本来、順位フィールドであるので、順位を算出する
    rs.each do |r|
      r[:rank] = r.rating_rank
      r.save!
    end

    redirect_to rankings_path
  end

  private

  def ranking_params
    ranks = (1..24).map { |n| "rank#{n}".to_sym }
    params.require(:ranking).permit(:ymd, :upload_file, ranks)
  end

end
