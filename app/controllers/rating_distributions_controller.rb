class RatingDistributionsController < ApplicationController
  def index
    @ymd  = RatingDistribution.first.ymd
    @info = RatingDistribution.first.info

    @fd   = RatingDistribution.first.data
    gon.labels = @fd.keys.reverse
    gon.data   = @fd.values.reverse
    gon.index  = 99
  end

  def create
    @rating = rating_params.to_i
    @ymd  = RatingDistribution.first.ymd
    sss = ShogiStandardScore2.new(@ymd)
    @info = sss.values(@rating)

    @fd   = RatingDistribution.first.data
    gon.labels = @fd.keys.reverse
    gon.data   = @fd.values.reverse
    gon.index  = 23 - @info[:index]

    ranks = %i(八段 七段 六段 五段 四段 三段 二段 初段 １級 ２級 ３級 ４級 ５級 ６級 ７級 ８級 ９級 10級 11級 12級 13級 14級 15級 初心)
    @dankyu = ranks[@info[:index]]

    render :index
  end

  private

  def rating_params
    params.require(:rating)
  end
end
