# 入会申請者のメールアドレス確認と、各種情報登録
class RegistrationController < ApplicationController
  before_action :guest_or_admin_user

  # メール確認
  def activation
    user = User.find_by(email: params[:email])
    if user &&
       !user.activated? &&
       Authenticator.new(user).authenticate(:activation, params[:id])
      # メールアドレスの確認が取れた
      user.update_columns(activated: true, activated_at: Time.current)
      @registration_form = RegistrationForm.new(id: user.id, email: user.email)
      render :edit
    else
      flash[:danger] = "認証トークンが無効です"
      redirect_to root_url
    end
  end

  def update
    @registration_form = RegistrationForm.new(registration_params)
    if @registration_form.valid?
      user = User.find_by(email: @registration_form.email)
      password_digest = Authenticator.digest(@registration_form.password)
      user.update_attributes(name: @registration_form.name,
                             password_digest: password_digest,
                             role: :member)
      profile           = Profile.new(profile_params)
      profile.id        = user.id
      profile.user_id   = user.id
      profile.condition = :active
      profile.save
      profile.next_number # 会員番号付与

      flash[:success] = "入会申請しました。会長からの連絡をお待ち下さい。"

      # 運営掲示板への投稿
      Topic.committee_only.posts.build(
        name: '運営',
        comment: "入会申請がありました。\n #{view_context.link_to '会員名簿', users_path} 入会申請中会員 より、「入会承認」または「入会却下」して下さい。",
        owner_id: 0,
        roots: :progenitor).save
      Topic.committee_only.update(updated_at: Time.now)

      redirect_to root_url
    else
      render 'edit'
    end
  end

  private

  def guest_or_admin_user
    unless (guest_user? || admin_user?)
      flash[:warning] = '既にユーザ登録されています'
      redirect_to root_url
    end
  end

  def registration_params
    params.require(:registration).permit(:id,
                                         :name,
                                         :email,
                                         :password,
                                         :password_confirmation,
                                         :joined_date,
                                         :joined_dankyu,
                                         :joined_rating,
                                         :proud_tactics,
                                         :favorite_kishi,
                                         :appeal)
                                         # :graduation,
                                         # :area)
  end

  def user_params
    params.require(:registration).permit(:name,
                                         :email,
                                         :password,
                                         :password_confirmation)
  end

  def profile_params
    params.require(:registration).permit(:joined_date,
                                         :joined_dankyu,
                                         :joined_rating,
                                         :proud_tactics,
                                         :favorite_kishi,
                                         :appeal)
                                         # :graduation,
                                         # :area)
  end
end
