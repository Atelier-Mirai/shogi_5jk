class SessionsController < ApplicationController
  protect_from_forgery except: [:destroy]

  def new
    redirect_to root_path if logged_in_user?
    @form = LoginForm.new
  end

  def create
    @user = User.find_by(name: session_params[:name])

    unless @user && @user.authenticate(session_params[:password])
      flash.now[:danger] = 'ハンドルネームまたはパスワードが異なります。'
      @form = LoginForm.new
      render 'new' and return
    end

    unless @user.activated?
      flash[:warning] = "メールアドレスの確認がなされておりません。"
      redirect_to root_url and return
    end

    unless @user.approved?
      flash[:warning] = "入会審査中です。承認まで暫くお待ち下さい。"
      redirect_to root_url and return
    end

    log_in @user
    flash[:success] = "ようこそ。 #{@user.name} さん!! ログインしました。"
    session_params[:remember_me] == '1' ? remember(@user) : forget(@user)
    redirect_back_or root_path
  end

  def destroy
    log_out if logged_in_user?
    flash[:success] = "またのお越しをお待ちしております!!"
    redirect_to root_url
  end

  private

  def session_params
    params.require(:login_form).permit(:name, :password, :remember_me)
  end

end
