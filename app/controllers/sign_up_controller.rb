# 入会申請(メールアドレスの確認)
class SignUpController < ApplicationController
  before_action :guest_or_admin_user

  def new
    @sign_up_form = SignUpForm.new
  end

  def create
    @sign_up_form = SignUpForm.new(sign_up_params)
    if @sign_up_form.save
      flash[:info] = "メールを送信しました。届いたメールを確認し、入会申請して下さい。"
      redirect_to root_path
    else
      render :new
    end
  end

  def sign_up_params
    params.require(:sign_up).permit(:email)
  end

  private

  def guest_or_admin_user
    unless (guest_user? || admin_user?)
      flash[:warning] = '既にユーザ登録されています'
      redirect_to root_url
    end
  end
end
