# トピック（掲示板スレッド）
class TopicsController < ApplicationController
  include Pagy::Backend

  before_action :enough_authority, only: [:edit, :destroy]
  before_action :logged_in_user,   only: [:new, :create]
  helper_method :enough_authority?

  def index
    @pagy, @topics = pagy(Topic.everyone)

    @enough_authority = enough_authority?
  end

  def show
    # リレー掲示板のみ Basic認証
    # basic_authentication if params[:id].to_i == 26
    @topic = if committee_user?
               Topic.includes(:posts).committee.find(params[:id])
             else
               Topic.includes(:posts).everyone.find(params[:id])
             end

    @pagy, @posts = pagy(@topic.posts.progenitor)
  end

  def new
    @topic = Topic.new
  end

  def create
    # ネストされたモデル
    # http://qiita.com/hmuronaka/items/580b977834d4b3010454
    @topic = Topic.new(topic_params)
    @post  = @topic.posts.build(post_params)
    # 添付ファイル確認
    if params[:upload_file].present?
      filename  = params[:upload_file].original_filename
      extension = File.extname(filename)
      if extension.in? ['.kif']
        # 添付ファイルが棋譜だったら
        require 'kconv'
        raw_kifu = params[:upload_file].read.toutf8
        @post[:comment] << NEWLINE << raw_kifu
        # (文字コードutf-8で「あいうえお」と書かれたファイルがアップされた場合)
        # => "\xE3\x81\x82\xE3\x81\x84\xE3\x81\x86\xE3\x81\x88\xE3\x81\x8A\n"
        # (文字コードshift_jisで「あいうえお」と書かれたファイルがアップされた場合)
        # => "\x82\xA0\x82\xA2\x82\xA4\x82\xA6\x82\xA8\n"
      elsif extension.in? ['.gif', '.jpeg', '.jpg', '.png']
        # 添付ファイルが画像だったら、
        @post.destroy # 先に生成した@postを破棄
        params[:topic][:post][:picture] = params[:upload_file]
        @post = @topic.posts.build(post_params)
      end
    end
    @topic.permission = 'everyone'
    @post.name        = current_user.name
    @post.post_number = 1
    @post.owner_id    = current_user.id
    @post.roots       = 'progenitor'

    if @topic.valid?
      @topic.save
      flash[:success] = "新規スレッドを作成しました"
      redirect_to bbs_path
    else
      render :new
    end
  end

  def edit
    @topic = Topic.find(params[:id])
    store_location
  end

  def update
    @topic = Topic.find(params[:id])
    if @topic.update(topic_params)
      flash[:success] = "更新しました"
      redirect_to session[:return_to] || topics_path
    else
      render :edit
    end
  end

  def destroy
    @topic = Topic.find(params[:id])
    @topic.destroy!
    flash[:success] = "削除しました"
    redirect_to bbs_path
  end

  def enough_authority?
    topic = Topic.find_by(id: params[:id])
    return false if topic.nil?
    logged_in_user? && current_user&.all_posts_owner?(topic) || admin_user?
  end

  private

  def topic_params
    # posts_attributes, post_attributes, どちらでもよい
    params.require(:topic).permit(:title, :permission, posts_attributes: [:id, :topic_id, :comment, :post_number, :owner_id, :picture])
  end

  def post_params
    params[:topic].require(:post).permit(:id, :topic_id, :name, :comment, :post_number, :owner_id, :picture)
  end

  def enough_authority
    unless logged_in_user?
      flash[:danger] = "ログインして下さい"
      redirect_to login_path
    end

    topic = Topic.find(params[:id])
    unless current_user&.all_posts_owner?(topic) || admin_user?
      flash[:danger] = "権限がありません"
      redirect_to root_path
    end
  end

  def basic_authentication
    authenticate_or_request_with_http_basic do |user, pass|
      (user == '@5JKTeamA' && pass == 'JoyfulRelay') || (user == '@5JKTeamB' && pass == 'JoyfulRelay')
    end
  end
end
