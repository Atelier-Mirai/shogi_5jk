# 会員
class UsersController < ApplicationController
  before_action :correct_user,   only: [:edit, :update]
  before_action :admin_user,     only: [:new, :create, :destroy]

  def index
    @users = {}
    %i(active_member sleep_member).each do |kind|
      @users[kind] = User.approved_user.unprivileged_user.joins(:profile).merge(Profile.send(kind))
    end
    # @users[:provisional_member] = User.provisional_user.unprivileged_user if admin_user?
  end

  def new
    @user    = User.new
    @profile = Profile.new
  end

  def create
    @user = User.new(user_params)
    @user.password_digest = Authenticator.digest(@user.password)
    @user.activated       = true
    @user.activated_at    = Time.current
    @user.role            = :member
    @user.approved        = true
    @user.approved_at     = Time.current
    @user.save
    @profile              = Profile.new(profile_params)
    @profile.id           = @user.id
    @profile.user_id      = @user.id
    @profile.save
    @profile.next_number # 会員番号付与
    redirect_to users_path
  end

  def edit
  end

  def update
    if @user.update_attributes(user_params)
      flash[:success] = "会員情報を更新しました"
      redirect_to users_path
    else
      render 'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "会員登録を削除しました"
    redirect_to users_url
  end

  private

  # 正しいユーザーか？
  def correct_user
    @user = User.find(params[:id])
    if !current_user.admin? && !current_user?(@user)
      flash[:danger] = "許可されていない操作です"
      redirect_to root_url
    end
  end

  def user_params
    params.require(:user).permit(:name,
                                 :email,
                                 :password,
                                 :password_confirmation,
                                 profiles_attributes: [
                                   :id,
                                   :number,
                                   :meijin_class,
                                   :joined_date,
                                   :joined_dankyu,
                                   :joined_rating,
                                   :proud_tactics,
                                   :favorite_kishi,
                                   :appeal,
                                   :main_results,
                                   :graduation,
                                   :area,
                                   :condition,
                                   :user_id
                                 ])
  end

  def profile_params
    params[:user].require(:profiles).permit(
      :id,
      :number,
      :meijin_class,
      :joined_date,
      :joined_dankyu,
      :joined_rating,
      :proud_tactics,
      :favorite_kishi,
      :appeal,
      :main_results,
      :graduation,
      :area,
      :condition,
      :user_id
    )
  end
end
