class WelcomeController < ApplicationController
  # トップページ
  def index
    # 最終更新日
    @last_update      = Message.maximum(:updated_at)
    # 告知
    @carousel         = Carousel.first
    # 名局紹介
    @well_played_game = Kifu.first
    # タイトル保持者
    @title_holders    = TitleHolder.current
  end

  # 5JKの歴史
  def circle_history
  end

  # 24リレー成績
  def relay_shogi
  end

  # サークル案内(会則)
  def circle_rule
  end

  # サークル案内(旧会則)
  def circle_rule_old
  end

  # 例会案内
  def regular_meeting
  end

  # よくある質問
  def faq
  end

  # 運営規則
  def administration_rule
  end

  # 旧運営規則
  def administration_rule_old
  end

  # 役員在任期間
  def zainin_kikan
  end

  # # 会員登録 ご案内
  def join_us_information
  end

  # 利用方法
  def usage
    render layout: 'application'
  end
end
