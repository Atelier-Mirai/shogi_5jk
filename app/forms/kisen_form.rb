# http://tech.medpeer.co.jp/entry/2017/05/09/070758
# http://morizyun.github.io/ruby/rails-function-form-object.html
# https://techracho.bpsinc.jp/morimorihoge/2013_07_26/12552
# http://railscasts.com/episodes/416-form-objects

class KisenForm
  include ActiveModel::Model

  attr_accessor :kisen, :term, :name, :open_date, :begin_date, :end_date
  # attr_accessor :kisen
  # delegate :persisted?, to: :kisen

  def initialize(kisen = nil)
    @kisen = kisen
    @kisen ||= Kisen.new
  end
  #
  def save
    return if invalid?
    # @kisen = Kisen.new(kisen_params)
    @kisen = Kisen.new(term: term,
                      name: name,
                      open_date: open_date,
                      begin_date: begin_date,
                      end_date: end_date)
    @kisen.save!
  end

  # def assign_attributes(params = {})
  #   @params = params
  #
  #   kisen.assign_attributes(kisen_params)
  # end

  # private
  #
  # def kisen_params
  #   params.permit(
  #     :term,
  #     :name,
  #     :open_date,
  #     :begin_date,
  #     :end_date
  #   )
  # end
end
