# http://tech.medpeer.co.jp/entry/2017/05/09/070758
# https://qiita.com/quattro_4/items/6636efbf58cca13db02a
# http://bamboo-yujiro.hatenablog.com/entry/2017/07/26/012316
class PasswordResetEmailConfirmationForm
  include ActiveModel::Model

  attr_accessor :email

  # メールアドレスは、newアクションの時にチェックする
  VALID_EMAIL_REGEX = /\A[\w+\-]+(\.[a-z\d\-]+)*+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email,
            presence: true,
            # allow_nil: true,
            length: { maximum: 255 },
            format: { with: VALID_EMAIL_REGEX }

  validate :user_email_exist?, unless: Proc.new { |a| a.email.blank? }
  def user_email_exist?
    unless User.find_by(email: @email.downcase)
      errors.add(:email, :email_not_found)
    end
  end

  # メールアドレスの確認が取れたときに、リセットメールを送信する
  def save
    return false if invalid?
    user   = User.find_by(email: email.downcase)
    token  = Authenticator.new_token
    digest = Authenticator.digest(token)
    user.update_columns(reset_digest:  digest,
                        reset_sent_at: Time.current)
    UserMailer.password_reset(user, token).deliver_later
  end

  # submit時のparamsのkeyに使われる -> params[:user] (無い場合params[:my_form])
  # form_forがURLを生成する時に使われる -> users_path (無い場合my_forms_pathなど)
  def self.model_name
    ActiveModel::Name.new(self, nil, "PasswordReset")
  end

  def attributes
    { email: email }
  end
end
