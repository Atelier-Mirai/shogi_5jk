# http://tech.medpeer.co.jp/entry/2017/05/09/070758
# https://qiita.com/quattro_4/items/6636efbf58cca13db02a
# http://bamboo-yujiro.hatenablog.com/entry/2017/07/26/012316
class PasswordResetNewPasswordForm
  include ActiveModel::Model

  attr_accessor :name, :email # zxcvbn用
  attr_accessor :password, :password_confirmation

  # パスワードは、edit / updateアクションの時にチェックする
  validates :password,
            presence: true,
            allow_nil: false,
            confirmation: true,
            zxcvbn: { score: 1 } # パスワードの強度

  # パスワードを更新する
  def save
    return false if invalid?

    user         = User.find_by(email: email.downcase)
    new_password = self.password
    digest       = Authenticator.digest(new_password)
    user.update_columns(password_digest:  digest)
  end

  # submit時のparamsのkeyに使われる -> params[:user] (無い場合params[:my_form])
  # form_forがURLを生成する時に使われる -> users_path (無い場合my_forms_pathなど)
  def self.model_name
    ActiveModel::Name.new(self, nil, "PasswordReset")
  end

  def attributes
    { name: name,
      email: email,
      password: password,
      password_confirmation: password_confirmation }
  end
end
