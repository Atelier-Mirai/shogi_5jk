# http://tech.medpeer.co.jp/entry/2017/05/09/070758
# http://morizyun.github.io/ruby/rails-function-form-object.html
# https://techracho.bpsinc.jp/morimorihoge/2013_07_26/12552
# http://railscasts.com/episodes/416-form-objects

class RankingForm
  include ActiveModel::Model

  attr_accessor :ymd
  attr_accessor :upload_file

  (1..24).each do |n|
    attr_accessor "rank#{n}".to_sym
  end

  # submit時のparamsのkeyに使われる -> params[:user] (無い場合params[:my_form])
  # form_forがURLを生成する時に使われる -> users_path (無い場合my_forms_pathなど)
  class << self
    def model_name
      ActiveModel::Name.new(self, nil, "ranking")
    end
  end
end
