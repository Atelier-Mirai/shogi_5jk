# http://tech.medpeer.co.jp/entry/2017/05/09/070758
# https://qiita.com/quattro_4/items/6636efbf58cca13db02a
# http://bamboo-yujiro.hatenablog.com/entry/2017/07/26/012316
class RegistrationForm
  include ActiveModel::Model

  # user
  attr_accessor :id, :name, :email, :password, :password_confirmation
  # profile
  attr_accessor :joined_date, :joined_dankyu, :joined_rating,
                :proud_tactics, :favorite_kishi, :appeal
                # :graduation, :area

  validates :name,
            presence: true,
            length: { maximum: 50 }
  validate do
    if User.find_by(name: @name)
      errors.add(:name, :same_name_already_exists)
    end
  end
  # validate do
  #   if @joined_rating.to_i.in?(1550..2099) && @graduation.blank?
  #     errors.add(:graduation, :set_graduation_target)
  #   end
  # end

  validates :password,
            presence: true,
            confirmation: true,
            zxcvbn: { score: 1 } # パスワードの強度

  validates :joined_rating,
            :proud_tactics,
            :favorite_kishi,
            :appeal,
            # :area,
            presence: true

  # submit時のparamsのkeyに使われる -> params[:user] (無い場合params[:my_form])
  # form_forがURLを生成する時に使われる -> users_path (無い場合my_forms_pathなど)
  class << self
    def model_name
      ActiveModel::Name.new(self, nil, "Registration")
    end
  end

  def attributes
    { name: name,
      email: email,
      password: password,
      password_confirmation: password_confirmation,
      joined_date: joined_date,
      joined_dankyu: joined_dankyu,
      joined_rating: joined_rating,
      proud_tactics: proud_tactics,
      favorite_kishi: favorite_kishi,
      appeal: appeal }
  end
end
