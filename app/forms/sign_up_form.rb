# http://tech.medpeer.co.jp/entry/2017/05/09/070758
# https://qiita.com/quattro_4/items/6636efbf58cca13db02a
# http://bamboo-yujiro.hatenablog.com/entry/2017/07/26/012316
class SignUpForm
  include ActiveModel::Model
  attr_accessor :email, :password

  VALID_EMAIL_REGEX = /\A[\w+\-]+(\.[a-z\d\-]+)*+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email,
            presence: true,
            length: { maximum: 255 },
            format: { with: VALID_EMAIL_REGEX }
  validate do
    if User.find_by(email: @email.downcase)
      errors.add(:email, :same_email_already_exists)
    end
  end

  def save
    return false if invalid?
    user = User.new(email: email.downcase, password: 'Happy5JK')
    user.save!
    token  = Authenticator.new_token
    digest = Authenticator.digest(token)
    user.update_columns(activation_digest: digest)
    UserMailer.account_activation(user, token).deliver_later
    true
  end

  # submit時のparamsのkeyに使われる -> params[:user] (無い場合params[:my_form])
  # form_forがURLを生成する時に使われる -> users_path (無い場合my_forms_pathなど)
  def self.model_name
    ActiveModel::Name.new(self, nil, "SignUp")
  end
end
