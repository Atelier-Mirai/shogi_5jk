# ApplicationHelper
# FIXME:
module ApplicationHelper
  # require './app/helpers/expansion'
  include HtmlBuilder
  include Avatar
  include ReCaptcha::ViewHelper # ruby-recaptcha

  # 和暦を返す
  def gengo(time, style = :date_and_time)
    return '' if time.blank?
    time = Time.parse(time) if time.is_a?(String)
    a    = "#{%w(日 月 火 水 木 金 土)[time.wday]}" # day of week

    style = case style
            when :date_and_time
              "%o%E.%m.%d(#{a}) %H:%M"
            when :date2
              "%o%E.%m.%d(#{a})"
            when :date
              if time.in? Time.new(2019, 5, 1)..Time.new(2019, 12, 31)
                "令和元年%m月%d日(#{a})"
              else
                "%O%E年%m月%d日(#{a})"
              end
            when :date3
              "%m月%d日(#{a})"
            when :date_short
              "%m/%d(#{a})"
            else
              style
            end

    markup(:span, class: "date #{time.what_is_today}") do |m|
      m.text time.to_era(style)
    end
  end

  # 年間三冠などにおいて、計算期間の初日を返すメソッド
  # @param [String, Date] ymd: 日付
  # @return [Date] ２月期以降なら当年の２月１日を、１月期なら前年２月１日を返す
  def start_date(ymd)
    ymd = Date.parse(ymd) if ymd.instance_of?(String)
    term_start_date = if ymd.month == 1
                        Date.new(ymd.year - 1,  2,  1)
                      else
                        Date.new(ymd.year,      2,  1)
                      end
  end

  # 勝率等を綺麗に
  def pretty_format(value)
    return '' if value.blank?

    if value.instance_of?(Integer)
      # カンマ区切り
      value.to_s(:delimited)
    elsif value.instance_of?(Float)
      if value <= 1
        # 小数なら、整数部の０は省略
        sprintf('%.3f', value).gsub('0.', ' .')
      else
        sprintf('%5.2f', value)
      end
    else
      value.to_s
    end
  end

  # R から 段級位への変換
  def rating2dankyu(r)
    return '' if r.blank?
    r = r.to_i
    return '八段' if r >= 2900
    return '七段' if r >= 2700
    return '六段' if r >= 2500
    return '五段' if r >= 2300
    return '四段' if r >= 2100
    return '三段' if r >= 1900
    return '二段' if r >= 1700
    return '初段' if r >= 1550
    return '１級' if r >= 1450
    return '２級' if r >= 1350
    return '３級' if r >= 1250
    return '４級' if r >= 1150
    return '５級' if r >= 1050
    return '６級' if r >=  950
    return '７級' if r >=  850
    return '８級' if r >=  750
    return '９級' if r >=  650
    return '10級' if r >=  550
    return '11級' if r >=  450
    return '12級' if r >=  350
    return '13級' if r >=  250
    return '14級' if r >=  150
    return '15級' if r >=   50
    return '初心者' if r >=  0
  end

  # 会員の活動状況
  def condition_sym2kan(condition)
    return '' if condition.nil?

    { active: '活動中', sleep: '休止中' }[condition.to_sym]
  end

  def full_title(page_title = '')
    base_title = '五級位上昇を目指す会'
    page_title.empty? ? base_title : "#{page_title} | #{base_title}"
  end

  def html_br(text)
    # h(text).gsub(/(\r\n?)|(\n)/, '<br>').html_safe
    text.gsub(/(\r\n?)|(\n)/, '<br>').html_safe if text.present?
  end

  def link_to_copyright
    link_to "Copyright &copy; 2004-#{Date.current.year} 五級位上昇を目指す会 All rights reserved.".html_safe, root_url, class: 'text-primary'
  end

  def link_to_common_rule
    link_to '5JK棋戦共通ルール', kisens_common_rule_path
  end

  def link_to_handicap
    link_to '5JK手合割', handicap_path
  end

  def link_to_bbs
    link_to '掲示板', bbs_url
  end

  def link_to_81dojo(options = nil)
    link_to '81道場', 'http://81dojo.com/client/', options
  end

  def link_to_81dojo_circle(options = nil)
    link_to '81道場サークル | ☗ ８１道場で五級位上昇を目指す会 (5JK) ☖', 'https://system.81dojo.com/ja/circles/221', options
  end

  def link_to_guideline(id)
    link_to '要項', kisen_path(id)
  end

  def link_to_matchup(id)
    link_to '対局表', matchup_path(id)
  end

  def mail_to_5JK_url
    link_to '5JK.Shogi＠gmail.com', 'mailto:5JK.Shogi＠gmail.com'
  end

  def safe_invert(original_hash)
    # p safe_invert({'a'=>1, 'b'=>1, 'c'=>3})
    #=> {1=>['a', 'b'], 3=>['c']}
    # 転載：Rubyレシピブック No.120
    result = Hash.new { |h, key| h[key] = [] }
    original_hash.each do |key, value|
      result[value] << key
    end
    result
  end
end
