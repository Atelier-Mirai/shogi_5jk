# KIF形式で渡された棋譜文字列を変換する
module KifParser
  def kif_parser(raw_kifu = RAW_KIF)
    # 将棋盤と駒台の準備
    ban, dai = make_board
    # 棋譜情報
    info = {}
    # 棋譜（指し手）を格納する配列
    # [{ from: 77, to: 76, piece: 0x17, promote: false,
    # time: 120 単位秒, capture: 6(角), same: true, relative: "R"}, {}...]
    kifu    = []
    kifu[0] = {}
    # コメント
    # { 0 => "対局前のコメント", 2 => "2手目へのコメント", 100 => "100手目へのコメント\r\nコメント" }
    # ["", "", "コメント\r\nコメント", "", "コメント"]
    comment = { 0 => '' }

    # 変換前の準備
    previous_to = 0 # 移動元保存("同"で使用)
    n           = 0 # n: 何手目か

    # 変換開始
    raw_kifu.split("\n").each do |line|
      line.strip!
      # 空白行, # 行(kif形式のコメント行) 棋譜開始行 は読み飛ばす
      next if line.blank? || line.first == '#' || line.include?("手数-")
      # >> line
      # => "開始日時：2018/01/27 22:00:19"

      # 棋譜情報行？
      if (i = include_keys(line, KEY_JP))
        info.merge!(i)
        next
      end

      # コメント
      if line[0].in? ['*', '&']
        if comment[n].blank?
          comment[n] = line[1..-1] + NEWLINE
        else
          comment[n] << line[1..-1] + NEWLINE
        end
        next
      # コメントではない行に投了 先手勝ち 中断などと書かれてあれば、棋譜の最後なので終了
      # TODO: 変化手順の時は工夫が必要
      elsif include_keys(line, WINNING_JP) || include_keys(line, RESULT_JP)
        break
      end

      # 棋譜（指し手）部分のみになっているはずなので、正規化して、解析を開始する
      line = move_normalize(line)
      # line => "    2 ８四歩(83)   ( 0:00/00:00:00)"

      time = result = nil # 宣言が必要
      tokens = line.split(' ')
      # => ["2", "８四歩(83)", "(", "0:00/00:00:00)"]

      tokens.each do |str|
        next if str == '' # 空なら読み飛ばす
        next if str =~ /^\d+$/ # 全て数字なら読み飛ばす
        next if str == '(' # 考慮時間の左括弧は読み飛ばす
        if str.include?(':')
          # 考慮時間
          time = time2sec(str)
        else
          # 指し手 (７六角(77),２二角成(88),２一杏(11),５三金打,同　飛(28))
          result = te2code_kif(str, previous_to)
          # 移動先保存
          previous_to = result[:to]
          n += 1 # 手数インクリメント
          piece = if info[:手合割：].nil? || info[:手合割：].strip.tr("　", '') == "平手"
                    # 平手なら、奇数手は先手
                    koma2code(str[2]) + (n.odd? ? SENTE : GOTE)
                  else
                    # 駒落ちなら、奇数手は上手
                    koma2code(str[2]) + (n.odd? ? UWATE : SHITATE)
                  end
          result[:piece] = piece
        end
      end

      # 取った駒があるか、調べるために局面を進める
      te = { from:     result[:from],
             to:       result[:to],
             piece:    result[:piece],
             capture:  'NONE',            # proceedで付与される
             promote:  result[:promote],
             time:     time,
             same:     result[:same],
             relative: result[:relative], # proceed で追記される
           }
      ban, dai, te = proceed(ban, dai, te)
      # 棋譜（指し手）を格納する
      kifu[n] = te
    end

    [kifu, info, comment]
  end

  # KI2形式で渡された棋譜文字列を変換する
  def ki2_parser(raw_kifu = RAW_KI2)
    # TODO: 棋譜とコメントの位置がずれるので、
    # info, kifu, comment それぞれが取れるように書き直す
    # ワンパススキャンが良いかも。

    # 棋譜の練習です。
    # <kifu>
    # 表題：表題（練習対局など）
    # 棋戦：棋戦名（名人戦など）
    # 戦型：居飛車対抗形など戦型
    # 手合割：香落ち
    # 場所：大阪道場T室
    # 開始日時：2016/01/01 12:00
    # 終了日時：2016/01/01 12:34
    # 持ち時間：各15分
    # 消費時間：▲10△24
    # 上手：UWATE_dayo^^
    # 下手：下手です(*^_^*)
    # △１一角
    # * １一角　いない自分の香車を食べないこと
    # ▲７六歩 △８四歩
    # * ８四歩　続けて棋譜が書ける
    # * コメントするときは、改行して先頭に * をつける
    # * 複数行のコメントも書ける
    # ▲３三角不成
    # * ３三角不成　成る成らないの区別が必要
    # * 二行目のコメント
    # △同角
    # ▲５八金右
    # * ５八金右　右左の区別
    # △４五角
    # * ４５角　打つが、４五に行ける角はいないので、「打」は不要
    # ▲４六歩
    # △６七角成
    # * ６七角成　成って馬になる
    # ▲同金
    # * 同金が▲６七金のことと認識される
    # △７七角不成
    # * ７七角不成　相手陣に入っているので、成・不成は必要
    # ▲４九玉
    # △６八角成
    # * ６八角成　成ってみる
    # ▲同金引
    # * 同金引　金の上と引が区別出来ること
    # △８五歩 ▲７七角
    # * ７七角　「打」は不要
    # △８六歩
    # ▲５五角
    # * ５五角　７七の角が５五に移動した
    # △８七歩不成 ▲３三角打
    # * ３三角打　５五の角が移動することも出来るので、「打」が必要
    # △４二玉
    # * ４二玉　利きに向かうのは反則だが、許容する
    # ▲１一角不成
    # * １一角不成　香落ちなので、１一に香車はいないので、持ち駒は増えないはず
    # △３二歩
    # ▲４四角引不成
    # * ４四角引不成　４四に行ける角は二枚ある　上・引で区別する
    # * 相手陣から出たときも、成るか不成かが必要
    # △投了

    # ヘッダー
    # info = raw_kifu2info(raw_kifu, :ki2)

    # コメント用配列
    # ["", "", "コメント\r\nコメント", "", "コメント"]
    comment = ['']

    # コメントを取り出し、棋譜だけの行にする。
    lines = raw_kifu.split("\n")
    kifu_lines = []
    n = 0 # 何手目か？
    # 消費時間に▲△が含まれていると、コメントの位置がずれるので補正する
    if info[:消費時間：].present?
      n -= info[:消費時間：].count("▲")
      n -= info[:消費時間：].count("△")
    end

    lines.each do |line|
      line.strip!
      line.gsub!(/▽/, "△")
      n += line.count("▲")
      n += line.count("△")
      # 空白行, # 行, & 制御行は読み飛ばす
      next if line == '' || line[0] == '#' || line[0] == '&'
      if line[0] == '*'
        if comment[n].nil?
          comment[n]  = line[1..-1] + NEWLINE
        else
          comment[n] += line[1..-1] + NEWLINE
        end
      elsif line.exclude?("：") && (line.include?("▲") || line.include?("△"))
        move_normalize!(line)
        kifu_lines << line.tr("　", ' ')
      end
    end
    # メンバーにnilが含まれていると、面倒なので、空文字に変換
    comment.map! { |e| e.nil? ? '' : e }

    # 指し手配列 生成
    sashite = kifu_lines.join(' ').split(' ').unshift('')
    # =>
    #  ["",
    # "△１一角",
    # "▲７六歩",
    # "△８四歩",
    # "▲３三角ズ",
    # "△同角",
    # "▲５八金右",
    # "△４五角",
    # "▲４六歩",
    # "△６七角成",
    # "▲同金",
    # "△７七角ズ",
    # "▲４九玉",
    # "△６八角成",
    # "▲同金引",
    # "△８五歩",
    # "▲７七角",
    # "△８六歩",
    # "▲５五角",
    # "△８七歩ズ",
    # "▲３三角打",
    # "△４二玉",
    # "▲１一角ズ",
    # "△３二歩",
    # "▲４四角引ズ",
    # "△投了"]

    # 盤と駒台を用意
    # データ形式への変換をお願いする
    # header 情報から、手合割が分かるので、併せて渡す
    # info[:手合割] # => "二枚落ち"
    # "二枚落ち" を :NIMAI に変換して渡す
    teai = TEAI_KAN_2_TEAI_SYM[(info[:手合割：] || "平手").to_sym]
    ban, dai = make_board(teai) # 平手
    ban, dai, kifu = sashite2hash(sashite, ban, dai)

    # return ban, dai, { info: info, kifu: kifu, comment: comment }
    [kifu, info, comment]
  end

  # TODO: 到達地点に移動することによって、「成る」ことが可能な場合、
  # 「成」「不成」を追加記入する
  # sashite 配列を受け取り、kifuハッシュの配列を返す
  def sashite2hash(sashite = SASHITE2, ban = BAN, dai = DAI)
    kifu = []
    kifu[0] = {}
    sashite.shift if sashite.first == ''
    previous_to = 0
    sashite.each do |current_sashite|
      break if current_sashite.include?("投了")
      te = lookup(current_sashite, ban, dai, previous_to)
      previous_to = te[:to]
      # 取った駒があるか、調べるために局面を進める
      ban, dai, te = proceed(ban, dai, te)
      # 棋譜（指し手）を格納する
      kifu << te
    end
    [ban, dai, kifu]
  end

  # 一手一手consoleから譜号を入力して将棋を指せる
  def shogi
    ban, dai = make_board
    previous_to = 0
    puts board_out(ban, dai)
    loop do
      begin
        print '> '
        sashite = gets
        sashite.chomp!
        next if te2code(sashite, previous_to) == 'Invalid Fugo'
        te = lookup(sashite, ban, dai, previous_to)
        previous_to = te[:to]
        # 局面を進める
        ban, dai, te = proceed(ban, dai, te)
        puts board_out(ban, dai)
      rescue
        next
      end
    end
  end

  # 盤の升目の周囲を見渡して、そこに来られる駒があるかどうか、調べる
  # ▲２六銀
  #
  # ３　２　１
  #            五
  # 銀  □     六
  #     銀  銀 七
  #
  # ２六の升目を見渡す。
  # ２六に来られる可能性のある升目は、
  # ３７，２７，１７、３５，１５にいる銀である。
  # (銀の動きを逆にすることによって得られる)
  # そこに銀がいるか調べれば良い。
  # 複数の銀がいるようであれば、relativeを付与する
  # 銀がいないようであれば、「打」で確定
  # 26銀 か 26銀打かは、relativeで判定出来る
  def lookup(fugo = "▲２六銀右", ban = BAN, dai = DAI, previous_to = 0)
    # ▲同銀は、前処理で▲２六銀になっていることが前提
    te_candidate = []
    te = te2code(fugo, previous_to)
    # {:from=>-1, :to=>0x26, :piece=>0x14, :promote=>false, :same=>false, :relative=>"R"}
    # from の -1 を確定する必要がある。
    to      = te[:to]
    piece   = te[:piece]
    r_piece = reverse_color(piece)
    promote = te[:promote]
    same    = te[:same]
    color   = piece2color(piece)
    turn    = piece2turn(piece)

    # 付加情報を、左右動作情報と、成・不成情報、打情報に分離
    str = te[:relative]
    promoted_str = '' # 成・不成
    promoted_str = 'Z' if str.include?('Z')
    promoted_str = 'N' if str.include?('N')
    drop_str = if str.include?('P')
                 'P'
               else
                 '' # "「打」は、盤上から移動可能な駒がない場合には不要"
    end
    str = str.tr('N', '')
    str = str.tr('Z', '')
    fugo_relative = str # 成・不成を除いた付加情報

    # r_piece の移動可能なビットを取得する
    direct = DIRECT[r_piece]
    # 歩き駒
    (0..11).each do |bit|
      if (direct & 0x01) == 1
        # 1 bit 目が立っていたら、RIGHT
        # 右方向に銀があるか、見る
        pos_diff = POS_DIFF[bit]
        from = to + pos_diff
        if ban[from] == piece
          # 候補として加える
          relative = pos_diff2str(pos_diff, color).join
          te_candidate << { from: from, to: to, piece: piece,
                            promote: promote, same: same, relative: relative }
        end
      end
      direct = direct >> 1
    end

    # 走り駒
    direct = (DIRECT[r_piece] >> 12) & 0b0111
    # ▲、△、それぞれ筋段の増加減少の組み合わせで割り振る
    if direct > 0b0000
      case direct
      when 0b0001 # 香車
        direction_hash = {
          下に見ていく: { suji_delta: 0, dan_delta: (color == SENTE ? +1 : -1), str: %w(C C) } }
      when 0b0010 # 角, 馬
        direction_hash = {
          右上に見ていく: { suji_delta: -1, dan_delta: -1, str: %w(RD LU) },
          左上に見ていく: { suji_delta: +1, dan_delta: -1, str: %w(LD RU) },
          右下に見ていく: { suji_delta: -1, dan_delta: +1, str: %w(RU LD) },
          左下に見ていく: { suji_delta: +1, dan_delta: +1, str: %w(LU RD) } }
      when 0b0100 # 飛, 龍
        direction_hash = {
          右に見ていく: { suji_delta: -1, dan_delta: 0, str: %w(RS LS) },
          左に見ていく: { suji_delta: +1, dan_delta: 0, str: %w(LS RS) },
          上に見ていく: { suji_delta: 0, dan_delta: -1, str: %w(D C) },
          下に見ていく: { suji_delta: 0, dan_delta: +1, str: %w(C D) } }
        # TODO: 龍は直は使わないので、後でCをUに変更する
      end
      direction_hash.each do |_key, value|
        suji = (to & 0xf0) >> 4; dan = to & 0x0f
        while suji.between?(1, 9) && dan.between?(1, 9)
          suji += value[:suji_delta]
          dan  += value[:dan_delta]
          here = (suji << 4) + dan
          case ban[here]
          when piece
            # 来ることが可能な角（馬）を発見出来たので、候補に追加する
            te_candidate << { from: here, to: to, piece: piece,
                              promote: promote, same: same, relative: value[:str][turn] }
            break
          when 0x00
            next
          else
            # 他の駒が見つかったのであれば、
            # その先に味方の角（馬）がいても候補たり得ないので、打ち切り
            break
          end
        end # end of while
      end # end of direction_hash
    end # end of if 走り駒終了

    # 歩き駒・走り駒それぞれで判定され、重複しているので
    te_candidate = te_candidate.uniq

    if te_candidate.count == 1
      # もし候補が一つであれば、相対位置などは不要
      te_candidate[0][:relative] = '' + promoted_str
      # 移動する３三角 と ３三角打があるかも
      if drop_str == 'P' && (te = uteru_one(to, piece, ban, dai))
        return te # 打つ手を優先して持ち帰る
      end
      return te_candidate[0]
    elsif te_candidate.count >= 2
      # 左右・動作表現の整形
      # 歩き駒に関しては、U/Cの変換など
      # 龍・馬に関しては、C -> U, LU -> LR などの変換を行う
      te_candidate = karnaugh(te_candidate)
    end

    # もし候補がなければ、打てたら追加する
    # uteru_one は、常にrelative: "P" を返す
    # [{:from=>0, :to=>69, :piece=>38, :promote=>false, :same=>false, :relative=>"P"}]
    if te_candidate.count == 0
      te_candidate << uteru_one(to, piece, ban, dai)
      # count == 0 のときは 「打」は不要なので、消す
      te_candidate[0][:relative] = ''
    end
    # あるいは、角打ちとなっていたら、角を打つ
    if drop_str == 'P'
      te_candidate << uteru_one(to, piece, ban, dai)
      # このときは、「打」は必要なので、消さずにそのままにしておく
      # te_candidate[0][:relative] = ""     # なのでコメントアウトする
    end

    # fugo_relativeと比較して、合っているのを返す
    te_candidate.each do |te|
      if te[:relative] == fugo_relative
        te[:relative] += promoted_str
        return te
      elsif te[:relative] == drop_str
        return te
      end
    end
    # TODO: 棋譜が間違っていて、指せる手がなかったときの処理は？
  end

  # U, D, C, S => 動作
  # R, L => 左右
  #
  # | ・ と ・
  # | と ・ ・    ▲８８と
  # | と と と
  # +---------
  # [{:from=>152, :to=>136, :relative=>"LS"},  => S
  #  {:from=>135, :to=>136, :relative=>"D"},   => D
  #  {:from=>137, :to=>136, :relative=>"C"},   => C(99と/79と がなければU)
  #  {:from=>121, :to=>136, :relative=>"RU"},  => R
  #  {:from=>153, :to=>136, :relative=>"LU"}]  => LU
  #
  #   L N R
  # U 1 1 1   -> 計3
  # S 1       -> 計1 動作のみで確定
  # D   1     -> 計1 動作のみで確定
  #   | | |
  #   v v v
  #   計計計
  #   2 2 1
  #       左右のみで確定
  #
  # まず動作のみで見る　確定すればそれでOK
  # 次に左右を見る　　　確定すればそれでOK
  # 残ったものは両方付記する
  def karnaugh(te_candidate)
    # if te_candidate[0][:to] == 0x55
    #   binding.pry
    # end
    piece = te_candidate[0][:piece]
    # 処理の都合上、一文字目を左右情報に、二文字目を動作になるように整形
    te_candidate.each do |te|
      if te[:relative].length == 1
        te[:relative] = 'N' + te[:relative] # N : 左右どちらでもないニュートラル
      end
    end
    # 処理の都合上、C -> U
    te_candidate.each do |te|
      te[:relative] = te[:relative].tr('C', 'U')
    end

    # カルノー図？に格納する
    karnaugh = {}
    karnaugh[:U] = { L: 0, N: 0, R: 0 }
    karnaugh[:S] = { L: 0, N: 0, R: 0 }
    karnaugh[:D] = { L: 0, N: 0, R: 0 }
    te_candidate.each do |te|
      move  = te[:relative].last.to_sym
      aside = te[:relative].first.to_sym
      karnaugh[move][aside] += 1
    end

    # 縦横に合計してみる
    sum = { U: 0, S: 0, D: 0, L: 0, N: 0, R: 0 }
    karnaugh.each do |key, value|
      sum[key] = karnaugh[key].values.inject(:+) # USDを合計
      value.each { |k, v| sum[k] += v }          # LNRを合計
    end

    sum.each do |key, value|
      next unless value == 1
      # USD, LNRの合計が1 つまりそれしかなかったので確定
      te_candidate.each do |te|
        next unless te[:relative].include?(key.to_s)
        te[:relative] = if te[:relative].include?('N')
                          # N になってしまうと、
                          # もとが U だったのか D だったのか 調べ直さなければならなくなるので
                          te[:relative].tr('N', '')
                        else
                          key.to_s
        end
      end
    end

    # 残っている N があったら削除する
    te_candidate.each do |te|
      te[:relative] = te[:relative].tr('N', '')
    end

    # 歩き駒の時、Uを UまたはCに変換する。
    # LU/RUがあれば、U->C。なければ Uのまま。
    if aruki_goma?(piece)
      flag = false
      te_candidate.each do |te|
        if te[:relative] == 'LU' || te[:relative] == 'RU'
          flag = true
          break
        end
      end
      if flag
        te_candidate.each do |te|
          if te[:relative] == 'U'
            te[:relative] = 'C'
            break
          end
        end
      end
    end

    # 走り駒の時、L, U ではなく、L, Rとして返さなければならない。
    if hashiri_goma?(piece)
      left_or_right_total = 0
      left_or_right       = ''
      up_or_down_total    = 0
      up_or_down          = ''
      te_candidate.each do |te|
        if te[:relative] == 'L' || te[:relative] == 'R'
          left_or_right_total += 1
          left_or_right        = te[:relative]
        end
        if te[:relative] == 'U' || te[:relative] == 'D'
          up_or_down_total += 1
          up_or_down        = te[:relative]
        end
      end
      if left_or_right_total == 1 && up_or_down_total == 1
        # この場合のみ、変換する必要がある。
        other_side = (left_or_right == 'L' ? 'R' : 'L')
        te_candidate.each do |te|
          if te[:relative] == 'U' || te[:relative] == 'D'
            te[:relative] = other_side
            break
          end
        end
      end
    end

    te_candidate
  end

  # ある駒が移動可能な範囲を返す
  def movable(from, koma)
    # FIXME: direction_hash の方法を使うと、簡潔に書ける
    to = []
    # DIRECT[0x11] = 0b0000_0000_0000_0100 # 歩
    movable_bit = DIRECT[koma]

    # 最上段・最下段にいる桂馬の移動先は、盤の外にはみ出すので、例外処理
    if (koma == 0x13 && (from & 0x0f == 1)) || # 一段目にいる先手の桂馬
       (koma == 0x23 && (from & 0x0f == 9))    # 九段目にいる後手の桂馬
      return []
    end

    # 歩き駒の移動範囲求める
    (0..11).each do |n|
      lsb = movable_bit & 0x01 # least significant bit、LSB
      if lsb == 1
        # 動けると言うことなので、動ける位置をもとめる
        candidate = from + POS_DIFF[n]
        # 盤の中なら、移動先として保存する
        suji = (candidate & 0xf0) >> 4
        dan  = candidate & 0x0f
        if (1 <= suji && suji <= 9) &&
           (1 <= dan  && dan  <= 9)
          to.push(candidate) # if suji.between(1, 9) && dan.between(1, 9)
        end
      end
      movable_bit = (movable_bit >> 1)
    end

    # 走り駒なら、さらに利きを追加する
    movable_bit = DIRECT[koma]
    flag        = movable_bit >> 12
    case (flag & 0b0111)
    when 0b0000
    when 0b0001 # 前に真っ直ぐ走れる
      suji = (from & 0xf0) >> 4
      dan  =  from & 0x0f
      sengo = flag & 0b1000
      if sengo == 0b0000
        (1..(dan - 1)).each do |d|
          to.push((suji << 4) + d)
        end
      else
        ((dan + 1)..9).each do |d|
          to.push((suji << 4) + d)
        end
      end
    when 0b0100 # 縦横に走れる
      suji = (from & 0xf0) >> 4
      (1..9).each do |dan|
        to.push((suji << 4) + dan)
      end
      dan = from & 0x0f
      (1..9).each do |suji|
        to.push((suji << 4) + dan)
      end
    when 0b0010 # 斜めに走れる
      suji = (from & 0xf0) >> 4
      dan = from & 0x0f
      # 左下方向へ
      (1..8).each do |diff|
        candidate_suji = suji + diff
        candidate_dan  = dan  + diff
        if candidate_suji <= 9 && candidate_dan <= 9
          to.push((candidate_suji << 4) + candidate_dan)
        else
          break
        end
      end
      # 右上方向へ
      (1..8).each do |diff|
        candidate_suji = suji - diff
        candidate_dan  = dan  - diff
        if 1 <= candidate_suji && 1 <= candidate_dan
          to.push((candidate_suji << 4) + candidate_dan)
        else
          break
        end
      end
      # 右下方向へ
      (1..8).each do |diff|
        candidate_suji = suji - diff
        candidate_dan  = dan + diff
        if 1 <= candidate_suji && candidate_dan <= 9
          to.push((candidate_suji << 4) + candidate_dan)
        else
          break
        end
      end
      # 左上方向へ
      (1..8).each do |diff|
        candidate_suji = suji + diff
        candidate_dan  = dan - diff
        if candidate_suji <= 9 && 1 <= candidate_dan
          to.push((candidate_suji << 4) + candidate_dan)
        else
          break
        end
      end
    end # case文

    to.uniq
  end

  # その持ち駒が打てるか？　打てたらハッシュを、打てなかったらnilを返す
  # 持ち駒を打つ
  # 二歩？
  # 行き所なき駒の禁
  def uteru_one(to, piece, ban = BAN, dai = DAI)
    available     = nil
    nifu          = false
    ikidokoronaki = false
    color         = piece2color(piece)
    turn          = piece2turn(piece) # 先手なら0, 後手なら1になる

    # 持ち駒を打つ
    if ban[to] == 0x00 # 空いている場所なら持ち駒を打てる
      komashu = piece & 0x07
      kazu = dai[turn][komashu]
      if kazu > 0
        # その着手が可能であるか？(歩の場合)
        if piece == color + 0x01 # 歩
          # 二歩でないことを確認
          (1..9).each do |dan|
            koma = ban[(to & 0xf0) + dan]
            if koma == piece
              nifu = true
              return nil
            end
          end
          # 行き所なき駒の禁？
          if color == SENTE && ((to & 0x0f) == 0x01) ||
             color == GOTE  && ((to & 0x0f) == 0x09)
            ikidokoronaki = true
          end
          if (nifu == false) && (ikidokoronaki == false)
            available = { from: 0x00, to: to, piece: piece, promote: false, same: false, relative: 'P' }
          end
        # その着手が可能であるか？(香車の場合)
        elsif piece == color + 0x02 # 香車
          # 行き所なき駒の禁？
          if color == SENTE && ((to & 0x0f) == 0x01) ||
             color == GOTE  && ((to & 0x0f) == 0x09)
            ikidokoronaki = true
          end
          if ikidokoronaki == false
            available = { from: 0x00, to: to, piece: piece, promote: false, same: false, relative: 'P' }
          end
        # その着手が可能であるか？(桂馬の場合)
        elsif piece == color + 0x03 # 桂馬
          # 行き所なき駒の禁？
          if color == SENTE && ((to & 0x0f) == 0x01) ||
             color == SENTE && ((to & 0x0f) == 0x02) ||
             color == GOTE  && ((to & 0x0f) == 0x09) ||
             color == GOTE  && ((to & 0x0f) == 0x08)
            ikidokoronaki = true
          end
          if ikidokoronaki == false
            available = { from: 0x00, to: to, piece: piece, promote: false, same: false, relative: 'P' }
          end
        else
          available = { from: 0x00, to: to, piece: piece, promote: false, same: false, relative: 'P' }
        end
      end
    end
    available
  end

  # 持ち駒が打てるか、手の全セットを返す
  def uteru_all(to, color = SENTE, ban = BAN, dai = DAI)
    available = []
    color &= 0xf0 # 誤って0x11などを渡したときにも動作するように
    turn = piece2turn(color) # 先手なら0, 後手なら1
    # 持ち駒を打つ
    dai[turn].each_with_index do |kazu, komashu|
      next unless kazu > 0
      piece = color + komashu
      if temp = uteru_one(to, piece, ban, dai)
        available << temp
      end
    end
    available
  end

  # 手を受け取り、局面を一つ進める
  def proceed(ban, dai, te)
    # => {:from=>51, :to=>119, :piece=>38, :promote=>false, :same=>false, :relative=>"Z"}

    # 先手番か、後手番か取得
    color   = (te[:piece]   & 0xf0) == SENTE ? 0 : 1
    # 取った駒の種類(ない場合は0)
    # 成り駒を取ったときは、成り駒のままで持つ（一手前に戻す処理のため）
    capture = (ban[te[:to]] & 0x0f)

    # 駒を取ったときに、駒台の駒を増やす
    te[:capture] = capture
    if capture != 0
      dai[color][capture & 0x07] += 1 # 成り駒なら生駒の数を増やす
    end

    # 駒を打ったときに、駒台の駒を減らす
    if te[:relative].include?('P')
      koma = te[:piece] & 0x0f
      dai[color][koma] -= 1
    end

    # 盤上の駒を動かしたのであれば、移動元を空升にする
    ban[te[:from]] = 0 if te[:from] != 0
    ban[te[:from]] = 0
    ban[te[:to]] = te[:piece] + (te[:promote] ? 0x08 : 0x00)
    [ban, dai, te]
  end
end
