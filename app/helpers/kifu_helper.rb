module KifuHelper
  include KifParser

  def koma_tag(koma, pos_number = '')
    # image_tag('blank.png', class: "#{koma} #{pos_number}", alt: koma)
    tag.div class: "#{koma} #{pos_number}"
  end

  def s_body(dan, koma_dan)
    tag.tr do
      tags = (0..10).to_a.reverse.map do |suji|
        case suji
        when 10
          tag.th class: 'l_fuchi'
        when 1..9
          # tag.td koma_tag(koma_dan[suji]), class: "b#{suji}#{dan}"
          tag.td class: "b#{suji}#{dan} #{koma_dan[suji]}"
        when 0
          tag.th KAN_SUUJI[dan], class: "r_fuchi yline#{dan}"
        end
      end
      safe_join tags
    end
  end

  # http://qiita.com/mnishiguchi/items/d7a225e89806666e48fc
  # Rails - tagでビューヘルパーを自作する
  def shogiban_tag(kifu_number, teai = :平手)
    teai = teai.to_sym if teai.is_a?(String)
    # 開始局面
    phase = TEAI_KAN[teai] || HIRATE # シンボルから、初期局面文字列へ変換する

    # ban[dan][suji] 9*9 で良いが、10*10で確保。(0は未使用)
    ban = Array.new(10).map { |e| e = Array.new(10, 'blk') }
    lines = phase.split("\n")
    lines.each.with_index(1) do |line, dan|
      # 3文字ずつに区切る。逆順にする。
      # http://qiita.com/seinosuke/items/960b35c8c9437dfa3f5a

      # 先頭に一文字追加。三文字ずつに区切りやすくなる。要素数と、筋が揃う
      line.insert(0, 'P')
      pieces = line.each_char.each_slice(3).map(&:join).reverse!
      pieces.each.with_index(1) do |piece, suji|
        next if piece[0] == 'P' # 読み飛ばす
        if piece.include?('*')
          koma = :blk # 空
        else
          color = (piece[0] == '+' ? '0' : '1') # 先手は黒、後手は白なので、color
          koma  = ((piece[1..2]).downcase + color).to_sym
        end
        ban[dan][suji] = koma
      end
    end

    tbody = tag.tbody do
      tags = ''
      (1..9).each do |dan|
        tags << s_body(dan, ban[dan])
      end
      raw(tags)
    end

    thead = tag.thead do
      tag.tr class: 'u_fuchi' do
        tags = (0..10).to_a.reverse.map do |suji|
          case suji
          when 10
            tag.th '', class: 'l_fuchi'
          when 1..9
            tag.th suji, class: "xline#{suji}"
          when 0
            tag.th '', class: 'r_fuchi'
          end
        end
        safe_join tags
      end
    end
    tfoot = tag.tfoot do
      tag.tr class: 'd_fuchi' do
        tags = (0..10).to_a.reverse.map do |suji|
          case suji
          when 0
            tag.th '', class: 'r_fuchi'
          when 1..9
            tag.th '', class: ''
          when 10
            tag.th '', class: 'l_fuchi'
          end
        end
        safe_join tags
      end
    end
    tag.table(id: "shogiban_#{kifu_number}", class: "shogiban") { thead + tbody + tfoot }
  end

  # 初期状態の将棋盤と駒台配列を生成し、返す
  def make_board(teai = :HIRATE)
    # 平手初期配置は、"PI"とする。駒落ちは、"PI"に続き、落とす駒の位置と種類を必要なだけ記述する。
    # 例:二枚落ちPI82HI22KA

    # 将棋盤
    # ban[上位4bit x:筋 + 下位4bit y:段]で構成する
    # 筋は0筋-10筋まである。
    ban = Array.new(0xB0, WALL) # 最初は全て壁で初期化しておく

    # 初期局面をセット
    phase = TEAI_SYM[teai] # シンボルから、初期局面文字列へ変換する
    # TEAI_SYM[:HIRATE] =>
    # HIRATE = <<~EOS
    #     P1-KY-KE-GI-KI-OU-KI-GI-KE-KY
    #     P2 * -HI *  *  *  *  * -KA *
    #     P3-FU-FU-FU-FU-FU-FU-FU-FU-FU
    #     P4 *  *  *  *  *  *  *  *  *
    #     P5 *  *  *  *  *  *  *  *  *
    #     P6 *  *  *  *  *  *  *  *  *
    #     P7+FU+FU+FU+FU+FU+FU+FU+FU+FU
    #     P8 * +KA *  *  *  *  * +HI *
    #     P9+KY+KE+GI+KI+OU+KI+GI+KE+KY
    #   EOS

    lines = phase.split("\n")
    lines.each.with_index(1) do |line, dan|
      # 3文字ずつに区切る。逆順にする。
      # http://qiita.com/seinosuke/items/960b35c8c9437dfa3f5a

      # 先頭に一文字追加。要素数と、筋が揃うので、三文字ずつに区切りやすくなる
      line.insert(0, 'P')
      pieces = line.each_char.each_slice(3).map(&:join).reverse!
      pieces.each.with_index(1) do |piece, suji|
        next if piece[0] == 'P'   # 読み飛ばす
        if piece.include?('*')
          koma = 0                # 空
        else
          color = (piece[0] == '+' ? SENTE : GOTE) # 先手は黒、後手は白なので、color
          koma  = KOMA_EN.index(piece[1..2]) + color
        end
        ban[(suji << 4) + dan] = koma
      end
    end
    dai = Array.new(2) # 駒台
    dai[0] = Array.new(8, 0)
    dai[1] = Array.new(8, 0)
    [ban, dai]
  end

  # bod形式を受け取り、将棋盤と駒台配列を返す
  def read_board(bod = BOD)
    # 後手：羽生善治
    # 後手の持駒：飛　角　金　銀　桂　香　歩四　
    #   ９ ８ ７ ６ ５ ４ ３ ２ １
    # +---------------------------+
    # |v香v桂 ・ ・ ・ ・ ・ ・ ・|一
    # | ・ ・ ・ 馬 ・ ・ 龍 ・ ・|二
    # | ・ ・v玉 ・v歩 ・ ・ ・ ・|三
    # |v歩 ・ ・ ・v金 ・ ・ ・ ・|四
    # | ・ ・v銀 ・ ・ ・v歩 ・ ・|五
    # | ・ ・ ・ ・ 玉 ・ ・ ・ ・|六
    # | 歩 歩 ・ 歩 歩v歩 歩 ・ 歩|七
    # | ・ ・ ・ ・ ・ ・ ・ ・ ・|八
    # | 香 桂v金 ・v金 ・ ・ 桂 香|九
    # +---------------------------+
    # 先手：谷川浩司
    # 先手の持駒：銀二　歩四　
    # 手数＝171  ▲６二角成  まで
    # *第44期王位戦第4局
    #
    # 後手番

    # 将棋盤
    # ban[上位4bit x:筋 + 下位4bit y:段]で構成する
    # 筋は0筋-10筋まである。
    ban    = Array.new(0xB0, WALL) # 最初は全て壁で初期化しておく
    dai    = Array.new(2) # 駒台
    dai[0] = Array.new(8, 0)
    dai[1] = Array.new(8, 0)
    # dai[1][2] = 3 #=> 後手の持ち駒、香車3枚

    body = false
    dan = 1
    bod.split("\n").each do |line|
      if line.include?("後手の持駒：") || line.include?("先手の持駒：")
        sengo = line.include?("先手") ? 0 : 1
        komas = line.split("：")[1].split("　")
        # => ["飛", "角", "金", "銀", "桂", "香", "歩四"]
        # => ["なし"]
        komas.each do |koma|
          if koma == "なし"
            next # 何もしない
          end
          # KOMA_JP.index("飛") # => 7
          # KAN_SUUJI.index("二") # => 2
          shubetsu = KOMA_JP.index(koma[0])
          maisuu = if koma.length == 1
                     1
                   else
                     KAN_SUUJI.index(koma[1])
          end
          dai[sengo][shubetsu] = maisuu
        end
        next
      end
      next unless line[0] == '|'
      line = line[1...-2]
      # 2文字ずつ区切る 逆順にしている
      komas = line.each_char.each_slice(2).map(&:join).reverse!
      # 九段目の出力例
      # [" 香", " 桂", " ・", " ・", "v金", " ・", "v金", " 桂", " 香"]
      # KOMA_JP = ["", "歩", "香", "桂", "銀", "金", "角", "飛", "玉",
      #               "と", "杏", "圭", "全", "  ", "馬", "龍"].map(&:freeze).freeze
      # KOMA_JP.index("玉")
      # => 8 なので、玉は駒番号8と分かる
      # SENTE 0x10 GOTE 0x20 を加算して、ban配列に保存すれば良い
      komas.each.with_index(1) do |koma, suji|
        next if koma == " ・"
        sengo = koma[0] != 'v' ? SENTE : GOTE
        ban[(suji << 4) + dan] = sengo + KOMA_JP.index(koma[1])
      end
      dan += 1
    end
    [ban, dai]
  end

  # ban, dai を受け取り、ki2形式で局面を表示する
  def board_out(ban = BAN, dai = DAI)
    #   出力結果
    #   持駒：なし
    #     ９ ８ ７ ６ ５ ４ ３ ２ １
    #   +---------------------------+
    #   |v香v桂v銀v金v玉v金v銀v桂v香|一
    #   | ・v飛 ・ ・ ・ ・ ・v角 ・|二
    #   |v歩v歩v歩v歩v歩v歩v歩v歩v歩|三
    #   | ・ ・ ・ ・ ・ ・ ・ ・ ・|四
    #   | ・ ・ ・ ・ ・ ・ ・ ・ ・|五
    #   | ・ ・ ・ ・ ・ ・ ・ ・ ・|六
    #   | 歩 歩 歩 歩 歩 歩 歩 歩 歩|七
    #   | ・ 角 ・ ・ ・ ・ ・ 飛 ・|八
    #   | 香 桂 銀 金 玉 金 銀 桂 香|九
    #   +---------------------------+
    #   持駒：なし

    # 持ち駒文字列の生成
    mochigoma = mochigoma2str(dai)

    # 盤 文字列の生成
    board_str = "後手の持駒：#{mochigoma[1]}\n"
    board_str += "  ９ ８ ７ ６ ５ ４ ３ ２ １\n+---------------------------+\n"

    (1..9).each do |dan|
      dan_str = '|'
      9.step(1, -1).each do |suji|
        koma    = ban[(suji << 4) + dan]
        color   = koma & 0xf0
        # そのまま使うと、KOMA_JP内のオブジェクトが返ってくる。
        # KOMA_JPとそのメンバーはfreezeしてある。
        # そこで、文字列オブジェクトを新規作成する。
        komamei = String.new(KOMA_JP[koma & 0x0f])
        if komamei == ''
          komamei = " ・"
        else
          komamei.insert(0, color == SENTE ? ' ' : 'v')
        end
        dan_str += komamei
      end
      dan_str += "|#{KAN_SUUJI[dan]}\n"
      board_str += dan_str
    end
    board_str += "+---------------------------+\n"
    board_str += "先手の持駒：#{mochigoma[0]}\n"
    board_str
  end

  # dai 配列 -> 持ち駒文字列
  def mochigoma2str(dai)
    mochigoma_str = Array.new(2, '')
    dai.each_with_index do |d, color|
      mochigoma = ''
      d.each_with_index do |e, i|
        next unless e >= 1
        komamei = KOMA_JP[i]
        str = komamei
        str += KAN_SUUJI[e] if e >= 2
        str += "　"
        mochigoma.insert(0, str)
      end
      mochigoma = "なし" if mochigoma == ''
      mochigoma_str[color] = mochigoma
    end
    mochigoma_str
  end

  # # 持ち駒文字列 -> dai 配列
  # # 持駒：飛二　角二　歩十八　
  # # 持駒：なし
  # def str2mochigoma(str_array)
  #   # str[0] 先手の持ち駒文字列
  #   # str[1] 後手の持ち駒文字列
  #   binding.pry
  #   str_array.each do |str|
  #     str = str.split("：")[1]
  #   end
  #   return str_array
  # end

  # ki2形式で局面を受け取り、ban, dai を出力する
  def board_in(kyokumen_str = KYOKUMEN)
    #   持駒：なし
    #     ９ ８ ７ ６ ５ ４ ３ ２ １
    #   +---------------------------+
    #   |v香v桂v銀v金v玉v金v銀v桂v香|一
    #   | ・v飛 ・ ・ ・ ・ ・v角 ・|二
    #   |v歩v歩v歩v歩v歩v歩v歩v歩v歩|三
    #   | ・ ・ ・ ・ ・ ・ ・ ・ ・|四
    #   | ・ ・ ・ ・ ・ ・ ・ ・ ・|五
    #   | ・ ・ ・ ・ ・ ・ ・ ・ ・|六
    #   | 歩 歩 歩 歩 歩 歩 歩 歩 歩|七
    #   | ・ 角 ・ ・ ・ ・ ・ 飛 ・|八
    #   | 香 桂 銀 金 玉 金 銀 桂 香|九
    #   +---------------------------+
    #   持駒：なし
    # 持駒情報と、盤面情報に分離
    mochigoma_str = []
    banmen_str    = []
    kyokumen_str.each_line do |line|
      if line.include?("持駒：")
        # 最初に取り出した持ち駒行は、後手の持ち駒なので、
        # unshiftメソッドで追加する
        mochigoma_str.unshift(line.chomp.split("持駒：").last)
      elsif line.include?('|')
        banmen_str << line.strip.chomp.split('|').second
      end
    end

    # 持ち駒情報をdai配列に格納
    dai = str2mochigoma(mochigoma_str)

    # 盤面情報をban配列に格納
    ban = Array.new(0xB0, WALL)
    banmen_str.each.with_index(1) do |str, dan|
      # 二文字ずつに区切る
      pieces = str.each_char.each_slice(2).map(&:join).reverse!
      pieces.each.with_index(1) do |piece, suji|
        if piece.include?("・")
          ban[(suji << 4) + dan] = 0x00
          next
        end
        # 駒種類の判定
        color = (piece[0] == ' ' ? SENTE : GOTE)
        koma  = KOMA_JP.index(piece[1])
        ban[(suji << 4) + dan] = color + koma
      end
    end

    [ban, dai]
  end

  # 持ち駒文字列 -> dai 配列
  def str2mochigoma(mochigoma_str)
    dai    = Array.new(2)
    dai[0] = Array.new(8, 0)
    dai[1] = Array.new(8, 0)
    # 飛二　香四　
    mochigoma_str.each_with_index do |str, color|
      str = str.split("：")[1] if str.include?("：")
      next if str == "なし"
      str.tr("　", ' ').split(' ').each do |s|
        # s => "飛二"
        s = s.strip
        koma = KOMA_JP.index(s[0])
        kazu = KAN_SUUJI.index(s[1..-1])
        dai[color][koma] = kazu
      end
    end
    dai
  end

  # {:from=>119, :to=>118, :piece=>17, :promote=>false, :same=>false, :relative=>"", :capture=>0},
  # から ▲７六歩 を返す
  def code2te(te)
    str = (te[:piece] & 0x30) == SENTE ? "▲" : "△"
    if te[:same] == false
      suji = (te[:to] & 0xf0) >> 4
      dan  = (te[:to] & 0x0f)
      suji = suji.to_s.tr('1-9', "１-９")
      dan  = KAN_SUUJI[dan]
      str += suji + dan
    else
      str += "同　"
    end
    str += code2koma(te[:piece], 2)
    rel = te[:relative]
    rel = rel.tr('R', "右")
    rel = rel.tr('L', "左")
    rel = rel.tr('U', "上")
    rel = rel.tr('C', "直")
    rel = rel.tr('D', "引")
    rel = rel.tr('S', "寄")
    rel = rel.sub('Z', "不成")
    rel = rel.tr('N', "成")
    rel = rel.tr('P', "打")
    str += rel
    str
  end

  # 一手の指し手を文字列からデータ形式へ変換する
  # ７六角(77),２二角成(88),２一杏(11),
  # ５三金打,同飛(28),同飛成(28), 同飛右成(28), ２二と右上
  # 1	 2	3	 4	5	 6
  # ５	２	銀	右	上	成
  # 1･･･到達地点の筋　2･･･到達地点の段　3･･･駒の種類
  # 4･･･駒の相対位置（複数ある場合）
  # 右＝指す側から見て右側の駒を動かした場合
  # 左＝指す側から見て左側の駒を動かした場合
  # 5･･･駒の動作（複数ある場合）
  # 上＝1段以上、上に動く (金銀が横に2枚以上並んでいる場合のみ「直」)
  # 寄＝1マス以上、横に動く
  # 引＝1段以上、下に動く
  # 6･･･成・不成・打
  def te2code(str, previous_to = 0)
    promote  = false
    same     = false
    relative = ''

    # 「同」を、筋段に戻す
    if str.include?("同")
      same = true
      suji = (previous_to & 0xf0) >> 4
      dan  = (previous_to & 0x0f)
      suji.to_s.tr!('0-9', "０-９")
      dan = String.new(KAN_SUUJI[dan])
      str.sub!("同", "#{suji}#{dan}")
    end
    # 移動元
    # kif形式なら(43)から求められる。
    # 打以外の場合は、後ほど処理することにして、仮に-1にしておく。
    # ５	二	銀	右	上	成(43)
    # from = str.match(/\d{2}/)[0]              # => "43"
    # from = (from[0].to_i << 4) + from[1].to_i # => 0x43
    from = str.include?("打") ? 0 : -1
    # 移動先
    to = (zen2num(str[1]) << 4) + kan2num(str[2])
    # 駒の種類
    piece = KOMA_JP.index(str[3]) + (str.include?("▲") ? SENTE : GOTE)
    # 成、不成
    promote = true if str.exclude?("不成") && str.include?("成")

    # TODO: この付加情報追記は不完全
    relative += 'L' if str.include?("左")
    relative += 'C' if str.include?("直")
    relative += 'R' if str.include?("右")
    relative += 'U' if str.include?("上")
    relative += 'S' if str.include?("寄")
    relative += 'D' if str.include?("引")
    relative += 'P' if str.include?("打")
    relative += 'N' if str.exclude?("不成") && str.include?("成") # Nari
    relative += 'Z' if str.include?("不成") || str.include?("ズ") # naraZu
    # ５	２	銀	右	上	成
    return { from: from, to: to, piece: piece, promote: promote, same: same, relative: relative }

  rescue
    return 'Invalid Fugo'
  end

  def pos_diff2str(pos_diff, color)
    # if str.include?("左") then relative += "L" end
    # if str.include?("直") then relative += "C" end
    # if str.include?("右") then relative += "R" end
    # if str.include?("上") then relative += "U" end
    # if str.include?("寄") then relative += "S" end
    # if str.include?("引") then relative += "D" end
    # POS_DIFF = [
    #   RIGHT              = 0x02 - 0x12, # 1bit目が立っていたら右へ動ける
    #   LEFT               = 0x22 - 0x12, # 2bit目
    #   UP                 = 0x11 - 0x12, # 3bit目
    #   DOWN               = 0x13 - 0x12, # 4bit目
    #   UPPER_RIGHT        = 0x01 - 0x12, # 5bit目
    #   UPPER_LEFT         = 0x21 - 0x12, # 6bit目
    #   LOWER_RIGHT        = 0x03 - 0x12, # 7bit目
    #   LOWER_LEFT         = 0x23 - 0x12, # 8bit目
    #   UPPER_RIGHT_KNIGHT = 0x00 - 0x12, # 9bit目
    #   UPPER_LEFT_KNIGHT  = 0x20 - 0x12, # 10bit目
    #   LOWER_RIGHT_KNIGHT = 0x04 - 0x12, # 11bit目
    #   LOWER_LEFT_KNIGHT  = 0x24 - 0x12, # 12bit目
    # ]

    # 駒に聞いたときに、上に行ったと返事が来たと言うことは、
    # 来られた盤の升目から見たら、自分の升目の下側にあった駒が来たということである。
    if color == SENTE
      case pos_diff
      when RIGHT              ; %w(R S)
      when LEFT               ; %w(L S)
      when UP                 ; %w(D)
      when DOWN               ; %w(C)
      when UPPER_RIGHT        ; %w(R D)
      when UPPER_LEFT         ; %w(L D)
      when LOWER_RIGHT        ; %w(R U)
      when LOWER_LEFT         ; %w(L U)
      when LOWER_RIGHT_KNIGHT ; %w(R)
      when LOWER_LEFT_KNIGHT  ; %w(L)
      end
    else
      case pos_diff
      when RIGHT              ; %w(L S)
      when LEFT               ; %w(R S)
      when UP                 ; %w(C)
      when DOWN               ; %w(D)
      when UPPER_RIGHT        ; %w(L U)
      when UPPER_LEFT         ; %w(R U)
      when LOWER_RIGHT        ; %w(L D)
      when LOWER_LEFT         ; %w(R D)
      when UPPER_RIGHT_KNIGHT ; %w(L)
      when UPPER_LEFT_KNIGHT  ; %w(R)
      end
    end
  end

  # 一手の差し手を文字列からデータ形式へ変換する
  # ７六角(77),２二角成(88),２一杏(11),
  # ５三金打,同飛(28),同飛成(28), 同飛右成(28), ２二と右上
  def te2code_kif(str, previous_to = 0)
    # 1	 2	3	 4	5	 6
    # ５	２	銀	右	上	成
    # 1･･･到達地点の筋　2･･･到達地点の段　3･･･駒の種類
    # 4･･･駒の相対位置（複数ある場合）
    # 右＝指す側から見て右側の駒を動かした場合
    # 左＝指す側から見て左側の駒を動かした場合
    # 5･･･駒の動作（複数ある場合）
    # 上＝1段以上、上に動く (金銀が横に2枚以上並んでいる場合のみ「直」)
    # 寄＝1マス以上、横に動く
    # 引＝1段以上、下に動く
    # 6･･･成・不成・打
    from    = 0
    to      = 0
    promote = false
    same    = false
    if str.include?("同")
      same = true
      # 「同」を、筋段に戻す
      suji = (previous_to & 0xf0) >> 4
      dan  = (previous_to & 0x0f)
      suji.to_s.tr!('0-9', "０-９")
      dan = String.new(KAN_SUUJI[dan])
      str.sub!("同", "#{suji}#{dan}")
    end

    to = (zen2num(str[0]) << 4) + kan2num(str[1])
    if str.include?("打")
      from = 0
    else
      # ５	二	銀	右	上	成(43)
      from = str.match(/\d{2}/)[0]              # => "43"
      from = (from[0].to_i << 4) + from[1].to_i # => 0x43
    end
    promote = true if str.exclude?("不成") && str.include?("成")

    # relative 以下の文字列を連結したもの
    # 左, 直, 右: それぞれL, C, R(Left, Center/Choku, Right)
    # 上, 寄, 引: それぞれU, S, D(Up, Side, Down)
    # 打: P(Put)
    relative = ''
    relative += 'L' if str.include?("左")
    relative += 'C' if str.include?("直")
    relative += 'R' if str.include?("右")
    relative += 'U' if str.include?("上")
    relative += 'S' if str.include?("寄")
    relative += 'D' if str.include?("引")
    relative += 'P' if str.include?("打")
    # Nari
    relative += 'N' if str.exclude?("不成") && str.include?("成")
    # naraZu
    relative += 'Z' if str.include?("不成")
    # ５	２	銀	右	上	成
    { from: from, to: to, promote: promote, same: same, relative: relative }
  end

  # line に keysが含まれていたら、
  # key: value のハッシュを返す
  # 含まれていなかったら、nilを返す
  def include_keys(line, keys)
    keys.each do |key|
      next unless line.include?(key)
      value = line[(key.length)..-1]
      return { key.to_sym => value.strip }
    end
    nil
  end

  # 0x22が先手の駒か、後手の駒かを返す
  def piece2color(piece)
    (piece & 0xf0) == SENTE ? SENTE : GOTE
  end

  # 先手なら0, 後手なら1になる
  def piece2turn(piece)
    (piece & 0x20) >> 5
  end

  # 逆の動きの駒を返す
  def reverse_color(piece)
    if piece >= 0x20
      piece -= 0x10
    else
      piece += 0x10
    end
  end

  # 走り駒なら true
  def hashiri_goma?(piece)
    direct = (DIRECT[piece] >> 12) & 0b0111
    direct > 0 ? true : false
  end

  # 歩き駒なら true
  def aruki_goma?(piece)
    !hashiri_goma?(piece)
  end

  # 駒文字(FU...RY/歩...龍)を駒番号(1...15)に変換する
  def koma2code(str)
    if str.bytesize <= 2 # 英語
      return KOMA_EN.index(str) || 0
    else # 日本語
      return KOMA_JP.index(str) || 0
    end
  end

  # 駒番号(17...31/33...47)を駒文字(FU...RY/歩...龍)に変換する
  # jp = 0:英語 1:日本語(1文字) 2:日本語
  def code2koma(code, jp = 1)
    code &= 0x0f # 下位4ビット
    case jp
    when 0
      return KOMA_EN[code]
    when 1
      return KOMA_JP[code]
    when 2
      return KOMA_JP2[code]
    end
  end

  # 全角数字(１...９)を数字(1...9)に変換する
  # 1...9も許容する(そのまま返す)
  def zen2num(str)
    str.tr("０-９", '0-9').to_i
  end

  # 漢数字(一...十八)を数字(1...18)に変換する
  # 全角数字(１...９)を数字(1...9)に変換する
  # 1...9も許容する(そのまま返す)
  def kan2num(str)
    (KAN_SUUJI.index(str) || zen2num(str)).to_i
  end

  # 手の正規化
  def move_normalize(move)
    move.gsub("成香", "杏")
        .gsub("成桂", "圭")
        .gsub("成銀", "全")
        .tr("王",   "玉")
        .tr("竜",   "龍")
        .gsub("不成", "ズ")
        .tr("行",   "直")
        .tr("下",   "引")
        .gsub("同　", "同")
  end

  # 秒を時間表示へ変換する
  def sec2hms(sec)
    sec = sec.to_i
    fmt = sec < 3600 ? '%M:%S' : '%H:%M:%S'
    (Time.parse('1/1') + sec).strftime(fmt)
  end

  # 考慮時間
  # 139 １八金(28)   ( 0:00/00:00:00)
  # の
  # ( 0:00/00:00:00)
  # が渡されている
  def time2sec(str)
    str.tr('(','').tr(')','')
    min, sec = str.split('/')[0].split(':')
    min.to_i * 60 + sec.to_i
  end

  # 棋譜情報を抽出、記事の投稿部分と、棋譜部分に分ける
  # PostsController & GamesController で使用している
  def extract_kifu(article_and_kifu)
    hash = { article: '', kifu: '' }
    case kifu_format = kifu_format(article_and_kifu)
    when :kif
      mode = :article
      article_and_kifu.each_line do |line|
        # 棋譜開始の判定条件(kif形式)
        # 開始行以降は hash[:kifu]に格納される
        # mode = :kifu if include_keys(line, KEY_JP) || line.include?('# ---')
        mode = :kifu if line.include?('開始日時：') || line.include?('# ---')
        hash[mode] << line
      end
      [hash[:article], hash[:kifu], kifu_format]
    when :no_kifu
      [hash[:article] = article_and_kifu, hash[:kifu], kifu_format]
    end
  end

  # textがkif形式を判定する
  def kifu_format(text)
    text.split("\r\n").each do |line|
      return :kif if line.include?("手数----指手---------消費時間--")
    end
    :no_kifu
  end
end
