module MatchupHelper
  # 優勝者、準優勝者の表彰
  def testimonial_tag(matchup)
    if matchup.sym_name.in?([:tozai, :koryu])
      # 団体戦
      if title = matchup.kisen.title_holder_id
        # => {:team_names=>["5JK", "U549"], :ids=>[["39", "2", "61"], ["142", "143", "141"]], :member_names=>[["thegul", "WhiteWizard", "bururutti-2"], ["gyane1192", "settlefield", "pukuan"]]}
        # 優勝チーム確定時
        tag.ul class: 'winner' do
          tags = []
          (0..1).each do |i|
            tags[i] = tag.li do
              safe_join [
                tag.span { "#{%w(優勝 準優勝)[i]}　" },
                tag.span {
                  safe_join [tag.span { title[:team_names][i] },
                             tag.small { "　(" + title[:member_names][i].join("さん, ") << "さん" } + ')']
                },
                tag.span { "　チーム" }
              ]
            end
          end
          safe_join tags
        end
      else
        # 団体戦 対局途中
        tag.ul class: 'winner' do
          tags = []
          (0..1).each do |i|
            tags[i] = tag.li do
              safe_join [
                tag.span { "#{%w(優勝 準優勝)[i]}　" },
                tag.span { (matchup.title_holder[i]&.name).to_s },
                tag.span { "　チーム" }
              ]
            end
          end
          safe_join tags
        end
      end
    else
      # 通常棋戦
      tag.ul class: 'winner' do
        tags = []
        last = matchup.kisen.entries.count >= 2 ? 1 : 0
        (0..last).each do |i|
          tags[i] = tag.li do
            safe_join [
              tag.span { "５ＪＫ第#{matchup.term.to_kan}代 #{['', "準"][i]}#{matchup.title_name}" },
              tag.span { (matchup.title_holder[i]&.name).to_s },
              tag.span { "さん" }
            ]
          end
        end
        safe_join tags
      end
    end
  end

  def player_tag(game, player_number)
    # player_number: 1 or 2
    # +----+------------+-------------+------------+------------+
    # | id | matchup_id | game_number | player1_id | player2_id |
    # +----+------------+-------------+------------+------------+
    # | 41 | 5          | A11         | 50         | 48         |
    # +----+------------+-------------+------------+------------+
    r = game.r
    n = game.n

    # seed を求める
    seed     = game.send("seed#{player_number}")
    seed     = "seed#{seed}" unless seed.nil?
    # n を求める
    # 3回戦第1局でシードなら 1 と 5 のどちらかである
    # seedable_pair(3,1) # => [1, 5]
    player_n = seedable_pair(r, n)[player_number - 1]

    # 名前を求める
    player_name = game.send("player#{player_number}_name")

    tag.ul class: 'player' do
      tag.li class: "n#{player_n} #{seed}" do
        tag.span { player_name }
      end
    end
  end

  def teai_or_reason_tag(game)
    return '' if game.reason.blank?
    r = game.r
    n = game.n
    if game.teai.present? && game.teai != "平手"
      teai_or_reason = game.teai
      n = game.uwate == 1 ? 2 * n - 1 : 2 * n # 上側に表示させるか、下側に表示させるか
    end
    if game.reason.include?("不戦勝")
      teai_or_reason = "不戦勝"
      n = game.winner == 1 ? 2 * n - 1 : 2 * n # 上側に表示させるか、下側に表示させるか
    end
    tag.ul class: 'teai_or_reason' do
      tag.li class: "r#{r} n#{n}" do
        tag.span { teai_or_reason }
      end
    end
  end

  def tournament_tag(matchup, game_initial)
    # A組の対局 (game_initialがAの場合)
    games        = matchup.games_group(game_initial)
    title_match  = matchup.title_match(game_initial)

    # トーナメントdiv
    ceil_member  = 2**title_match[:round]
    tag.div class: 'tournament', style: "height: #{51 * ceil_member}px;" do
      ul_dates           = ''
      ul_teais_or_reason = ''
      div_lines          = ''
      ul_players         = ''
      ul_title_matches   = ''

      games.each do |game|
        r = game.r # r 回戦
        n = game.n # 第 n 局

        # 日付
        if !title_match[:held] || r != title_match[:round]
          # ul_dates << tag.ul class: 'date' do
          #   tag.li class: "r#{r} n#{n}") do
          #     game_date_tag(game)
          #   end
          # end
          ul_dates << tag.ul(class: 'date') do
            tag.li(class: "r#{r} n#{n}") do
              game_date_tag(game)
            end
          end
          # 手合い、勝ち理由
          ul_teais_or_reason << teai_or_reason_tag(game)
        end

        # 線を引く
        div_lines << tag.div(class: 'line') do
          lines = []
          winner = if title_match[:held] && r == title_match[:round]
                      title_match[:winner]
                   else
                     game.winner
                   end
          unless title_match[:held] && r == title_match[:round] && n >= 2
            # 上の回への右への横線
            lines << tag.div(class: "r#{r+1} n#{n} #{['', 'win', 'win'][winner]} h")
            # 自分の回の上下の縦線
            lines << tag.div(class: "r#{r} n#{2*n-1} #{['', 'win', ''][winner]} v")
            lines << tag.div(class: "r#{r} n#{2*n}   #{['', '', 'win'][winner]} v")
          end

          # シード特例の処理
          [1, 2].each do |player_number|
            next unless game.send("seed#{player_number}?")
            # 対局者名
            ul_players << player_tag(game, player_number)
            # player表示用ul.player li span まで線を引く
            line_n = if player_number == 1
                       2 * n - 1
                     else
                       2 * n
                     end
            r.downto(1).each do |r|
              win = game.played? ? 'win' : ''
              lines << tag.div(class: "r#{r} n#{line_n} h seed#{game.send("seed#{player_number}")} #{win}")
              line_n = 2 * line_n - 1
            end
          end

          if r == 1 # 一回戦の特例
            unless title_match[:held] && title_match[:round] == 1 && n >= 2
              # 対局者名 (player1, player2 のマスを生成する)
              ul_players << player_tag(game, 1)
              ul_players << player_tag(game, 2)
              # player表示用ul.player li span まで線を引く
              winner = title_match[:held] && r == title_match[:round] ? title_match[:winner] : game.winner
              lines << tag.div(class: "r1 n#{2 * n - 1} #{['', 'win', ''][winner]} h")
              lines << tag.div(class: "r1 n#{2 * n}   #{['', '', 'win'][winner]} h")
            end
          end
          safe_join lines
        end
      end

      # タイトルマッチ
      if title_match[:held]
        ul_title_matches = tag.ul(class: 'title_match') do
          li_title_matches = []
          1.upto(title_match[:n_max]).each do |n|
            game = matchup.games.find_by(game_number: "#{game_initial}#{title_match[:round]}#{n}")
            li_title_matches << tag.li(class: "date r#{title_match[:round]} n#{n}") { game_date_tag(game) }
            next unless game.played?

            hoshi = { up: game.mark1, down: game.mark2 }
            hoshi.each do |key, value|
              li_title_matches << tag.li(class: "#{key} hoshi r#{title_match[:round]} n#{n}") do
                tag.span { value }
              end
            end
            next unless game.komaochi? # 駒落ち対局であれば
            li_title_matches << tag.li(class: "#{['', 'up', 'down'][game.uwate]} teai r#{title_match[:round]} n#{n}") do
              tag.span { game.teai }
            end
          end
          safe_join li_title_matches
        end
      end

      [ul_dates, ul_teais_or_reason, ul_players, div_lines, ul_title_matches].join.html_safe
    end
  end

  def paramus_tag(matchup, game_initial)
    # A組の対局 (game_initialがAの場合)
    games = matchup.games_group(game_initial)

    # member の名前は、A11 の player2 と A21-A51 の player1 で求めることが出来る
    entries   = []
    member    = matchup.maximum_game_number(game_initial)[1].to_i # A51 -> 5
    player_id = games.find_by(game_number: "#{game_initial}11").player2_id
    entries << games.first.matchup.kisen.entries.find_by(user_id: player_id)
    (1..member).each do |r|
      player_id = games.find_by(game_number: "#{game_initial}#{r}1").player1_id
      entries << games.first.matchup.kisen.entries.find_by(user_id: player_id)
    end

    div = tag.div(class: 'paramus', style: "height: #{entries.count * 54}px;") do
      ul_players = tag.ul class: 'player' do
        li_players = []
        entries.each.with_index(0) do |_entry, i|
          li_players << tag.li(class: "n#{i + 1}") do
            tag.span do
              entries[i].star_name_with_rating
            end
          end
        end
        safe_join li_players
      end

      ul_dates = tag.ul class: 'date' do
        li_dates = []
        games.each do |game|
          r = game.r
          li_dates << tag.li(class: "n#{r}") { game_date_tag(game) }
        end
        safe_join li_dates
      end

      ul_reason = tag.ul class: 'reason' do
        li_reason = []
        games.each do |game|
          break unless game.played?
          next unless game.reason.include?("不戦勝")
          r = game.r
          li_reason << tag.li(class: "n#{r} #{[nil, :down, :up][game.winner]}") do
            tag.span { "不戦勝" }
          end
        end
        safe_join li_reason
      end

      div_lines = tag.div(class: 'line') do
        lines = []
        games.each do |game|
          r = game.r
          %w(h v).each do |hv|
            %w(up down).each do |ud|
              # 勝者を判定して、赤くする
              win = 'win' if game.winner == 1 && ud == 'down'
              win = 'win' if game.winner == 2 && ud == 'up'
              if r == 1
                lines << tag.div(class: "r#{r} #{hv} #{ud} #{win}") {}
                win = 'win' if game.played?
                lines << tag.div(class: "r#{r + 1} h up #{win}") {} # 次の回の横線は必ず赤なので
              else
                unless hv == 'h' && ud == 'up'
                  lines << tag.div(class: "r#{r} #{hv} #{ud} #{win}") {}
                  win = 'win' if game.played?
                  lines << tag.div(class: "r#{r + 1} h up #{win}") {} # 次の回の横線は必ず赤なので
                end
              end
            end
          end
        end
        safe_join lines
      end
      ul_players + ul_dates + ul_reason + div_lines
    end
    div
  end

  # 行列式を通過する順番を返す
  def turn_pass_determinant(dimension)
    combinatorial_number = dimension * (dimension - 1) / 2
    numbers = (0..(combinatorial_number - 1)).to_a
    keys = (0..dimension).to_a.permutation(2).map(&:join)
    h = {}

    (0..(dimension - 1)).each do |line|
      # 初期値
      r     = 0    # row
      c     = line # column
      count = 0    # 探索した升目の数

      while count < dimension
        if r == c.abs
          # 何もしない
        else
          # 21 と 12
          key           = "#{r}#{c.abs}"
          conjugate_key = "#{c.abs}#{r}"

          if h[key].nil? || h[conjugate_key].nil?
            number           = numbers.shift
            h[key]           = number
            h[conjugate_key] = number
          end
        end
        count += 1
        # 行列式の要領で、右斜め下に探索する

        if c > 0
          r += 1
        elsif c == 0
          r = dimension - 1
          c -= line
        else
          r -= 1
        end
        c -= 1
      end
    end

    return h
  end

  def league_tag(matchup, game_initial)
    kisen_name = matchup.kisen.name
    # 何人のリーグ戦であるか取得する
    games  = matchup.games_group(game_initial) # リーグ戦の総数を求める
    member = ((1 + Math.sqrt(4 * 2 * games.count + 1)) / 2).to_i # 解の公式より

    # member の名前は、A12 の player1 と A12-A18 の player2 で求めることが出来る
    member_ids = []
    member_ids << games.find_by(game_number: "#{game_initial}12").player1_id
    (2..member).each do |n|
      member_ids << games.find_by(game_number: "#{game_initial}1#{n}").player2_id
    end
    # このリーグへの参加者を取得
    entries = []
    member_ids.each do |id|
      entries << games.first.matchup.kisen.entries.find_by(user_id: id)
    end

    thead = tag.thead do
      tag.tr do
        tags = (1..(8 + entries.count)).to_a.map do |i|
          case i
          when 1
            if kisen_name == "名人戦"
              tag.th "#{game_initial}級".html_safe, class: 'group'
            else
              tag.th "#{game_initial}組".html_safe, class: 'group'
            end
          when 2 then tag.th "参加者名", class: 'handle_name'
          when 3 then tag.th "R<br>段級".html_safe, class: 'rating'
          when 4
            if kisen_name == "名人戦"
              tag.th "級内<br>順位".html_safe, class: 'group_ranking'
            else
              tag.th "組内<br>順位".html_safe, class: 'group_ranking'
            end
          when 5 then tag.th "勝敗", class: 'win_and_lose'
          when 6 then tag.th "勝ち点", class: 'point'
          when 7..(6 + entries.count)
            if kisen_name == "名人戦"
              tag.th entries[i - 7].meijin_class, class: 'schedule'
            else
              tag.th "#{game_initial}-#{i - 6}", class: 'schedule'
            end
          when 8 + entries.count then tag.th "備考", class: 'note'
          end
        end
        safe_join tags
      end
    end

    # 組内順位、勝敗、勝ち点を計算する
    group_rank_win_lose = matchup.group_rank_win_lose(game_initial)

    trs = ''
    entries.each.with_index(1) do |entry, row|
      tr = tag.tr do
        tags = (1..(8 + entries.count)).to_a.map do |column|
          case column
          when 1
            if kisen_name == "名人戦"
              tag.th entry.meijin_class.to_s, class: 'group'
            else
              tag.th "#{game_initial}-#{row}", class: 'group'
            end
          when 2
            tag.td "#{entry.number}<br>#{entry.star_name}".html_safe, class: 'handle_name'
          when 3
            tag.td "#{entry.rating}<br>#{rating2dankyu(entry.rating)}".html_safe, class: 'rating'
          when 4
            tag.td group_rank_win_lose[entries[row - 1].user_id][:rank], class: 'group_ranking'
          when 5
            tag.td "#{group_rank_win_lose[entries[row - 1].user_id][:win]}-#{group_rank_win_lose[entries[row - 1].user_id][:lose]}", class: 'win_and_lose'
          when 6
            tag.td group_rank_win_lose[entries[row - 1].user_id][:win_point], class: 'point'
          when 7..(6 + entries.count)
            col = column - 6
            if col == row
              tag.td "※", class: 'schedule'
            else
              tag.td '', class: 'schedule' do
                spans = []
                game_number = row < col ? "#{game_initial}#{row}#{col}" : "#{game_initial}#{col}#{row}"
                game = matchup.games.find_by(game_number: game_number)
                spans << game_date_tag(game)
                mark = row < col ? game.mark1 : game.mark2
                spans << tag.span(mark || "　")

                spans << case game.reason
                         when "勝ち"
                           tag.span(game.teai)
                         when "不戦勝"
                           tag.span("不戦勝")
                         when nil? # 未対局
                           tag.span("　")
                         else
                           tag.span mark2word(mark)
                         end
                spans[0] + '<br>'.html_safe + spans[1] + '<br>'.html_safe + spans[2]
              end
            end
          when 8 + entries.count then tag.td '', class: 'note'
          end
        end
        safe_join tags
      end
      trs += tr
    end

    tbody = tag.tbody do
      raw(trs)
    end

    tag.table(class: "sortable league #{game_initial.downcase}_group") { thead + tbody }
  end

  def team_competition_tag(matchup, game_initial, show_area: true)
    # 各チーム何名の東西戦であるかを取得する
    games  = matchup.games_group(game_initial)
    member = games.count

    # 参加者のidを取得
    ids = [[], []]
    games.each do |game|
      ids[0] << game.player1_id
      ids[1] << game.player2_id
    end

    # 各チームの参加者を取得
    team_members = [
      games.first.matchup.kisen.entries.where(user_id: ids[0]).reorder(rating: :asc),
      games.first.matchup.kisen.entries.where(user_id: ids[1]).reorder(rating: :asc)
    ]

    # チーム名を取得
    team_names = [team_members[0].first.team, team_members[1].first.team]

    # ratingの合計を求める
    rating_total = team_members.map { |members| members.pluck(:rating).inject(:+) }

    # 何勝何敗か ＆ 勝ち点を求める
    result = [{ win: 0, lose: 0, point: 0 }, { win: 0, lose: 0, point: 0 }]
    games.each do |game|
      if game.winner == 1
        result[0][:win]  += 1
        result[1][:lose] += 1
      elsif game.winner == 2
        result[1][:win]  += 1
        result[0][:lose] += 1
      end
      result[0][:point] += game.point1.to_i
      result[1][:point] += game.point2.to_i
    end

    thead = tag.thead do
      tag.tr do
        [ tag.th(class: 'team_name'),
          (1..member).to_a.map { |col|
            tag.th(general_names(member)[col], class: 'handle_name')
          }.join,
          tag.th("備考", class: 'note'),
        ].join.html_safe
      end
    end

    trs = []
    (0..1).each do |i|
      trs[i] = tag.tr class: [:east, :west][i] do
        tags = (0..member + 1).to_a.map do |col|
          case col
          when 0 then tag.th team_names[i], class: 'team_name'
          when 1..member
            str = "#{team_members[i][col - 1].star_name_with_rating}<br>"
            # 都道府県
            str << Prefecture.new(team_members[i][col - 1].area).name if show_area
            tag.td str.html_safe, class: 'handle_name'
          when member + 1
            str = "合計#{rating_total[i]}<br>"
            str << "#{result[i][:win]}勝#{result[i][:lose]}敗"
            tag.td str.html_safe, class: 'note'
          end
        end
        safe_join tags
      end
    end

    tr_result = tag.tr class: 'result' do
      tags = (0..member + 1).to_a.map do |col|
        case col
        when 0
          tag.th('')
        when 1..member
          game = games[col - 1]
          tags = ''
          tags << tag.span(game.uwate == 1 ? game.teai : "　", class: 'teai')
          tags << tag.span(game.mark1.presence || "　")
          tags << tag.span(game_date_tag(game), class: 'schedule')
          tags << tag.span(game.mark2.presence || "　")
          tags << tag.span(game.uwate == 1 ? "　" : game.teai, class: 'teai')
          tag.td tags.html_safe
        when member + 1
          tag.td ''
        end
      end
      safe_join tags
    end

    tbody = tag.tbody do
      raw(trs[0] + tr_result + trs[1])
    end

    tag.table(class: 'team_competition') { thead + tbody }
  end

  # 対局日用
  def game_date_tag(game)
    data_target = if game.played? && game&.kifu_id
                    '#matchup_show_kifu'
                  elsif game.played? && !game&.kifu_id
                    'matchup_show_no_kifu'
                  else
                    '#matchup_schedule_and_result'
                  end

    tag.a(class: "game_date #{game.date.what_is_today}",
          'data-toggle':       'modal',
          'data-target':       data_target,                    'data-game_number':  game.game_number,
          'data-player1_id':   game.player1_id,
          'data-player2_id':   game.player2_id,
          'data-player1_name': game.player1_name,
          'data-player2_name': game.player2_name,
          'data-game_date':    game.date,
          'data-kifu_id':      game.kifu_id || 0) do
      tag.span { gengo(game.date, :date_short) }
    end
  end

  # リーグ戦表示用
  def mark2word(mark)
    {
      "○": "勝ち",
      "□": "不戦勝",
      "●": "負け",
      "▲": "不戦敗",
      "■": "不戦敗"
    }[mark]
  end

  # 団体戦の際、用いる
  def general_names(member_count)
    case member_count
    when 1 then ['', "大将"]
    when 2 then ['', "先鋒", "大将"]
    when 3 then ['', "先鋒", "中堅", "大将"]
    when 4 then ['', "先鋒", "次鋒", "副将", "大将"]
    when 5 then ['', "先鋒", "次鋒", "中堅", "副将", "大将"]
    end
  end

  # r回戦 第n局から最下層のnはいくつといくつか？
  def seedable_pair(r, n)
    pair_step = 2**(r - 1)
    interval  = 2**r * (n - 1)
    [1 + interval, 1 + pair_step + interval]
  end
end
