module SessionsHelper
  # 渡されたユーザーでログインする
  def log_in(user)
    session[:user_id] = user.id
  end

  def current_user
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      if (user = User.find_by(id: user_id)) && Authenticator.new(user).authenticate(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end

  # Returns true if the given user is the current user
  def current_user?(user)
    user == current_user
  end

  # ユーザーがログインしていればtrue, その他ならば、falseを返す
  def logged_in_user?
    !current_user.nil?
  end

  def guest_user?
    current_user.nil?
  end

  def admin_user?
    !!current_user&.admin?
  end

  def committee_user?
    !!current_user&.committee?
  end

  # ユーザーを永続的セッションに記憶する
  def remember(user)
    token = Authenticator.new_token
    user.update_attributes(remember_digest: Authenticator.digest(token))
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = token
  end

  # 永続的セッションを破棄する
  def forget(user)
    user.update_attributes(remember_digest: nil)
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

  # 現在のユーザーをログアウトする
  def log_out
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end

  # Redirects to stored location (or to the default).
  # for friendly forwarding
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  def store_location
    session[:forwarding_url] = request.url if request.get?
    session[:return_to]      = request.referer
  end
end
