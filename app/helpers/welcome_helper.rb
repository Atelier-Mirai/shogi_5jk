module WelcomeHelper
  def topic_title_post_count(topic, classname = '')
    if topic.updated_at >= Time.current.days_ago(7)
      link_to("#{topic.title}(#{topic.posts.count})#{image_tag('new-1.gif')}  ".html_safe, topic_path(topic), class: classname)
    else
      link_to "#{topic.title}(#{topic.posts.count}) ", topic, class: classname
    end
  end
end
