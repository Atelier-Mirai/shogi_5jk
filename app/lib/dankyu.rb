# 段級
class Dankyu
  attr_accessor :rating, :dankyu, :rank

  # RANK_HASH = { 2900 => ['八段', -7],
  #               2700 => ['七段', -6],
  #               2500 => ['六段', -5],
  #               2300 => ['五段', -4],
  #               2100 => ['四段', -3],
  #               1900 => ['三段', -2],
  #               1700 => ['二段', -1],
  #               1550 => ['初段',  0],
  #               1450 => ['１級',  1],
  #               1350 => ['２級',  2],
  #               1250 => ['３級',  3],
  #               1150 => ['４級',  4],
  #               1050 => ['５級',  5],
  #                950 => ['６級',  6],
  #                850 => ['７級',  7],
  #                750 => ['８級',  8],
  #                650 => ['９級',  9],
  #                550 => ['10級', 10],
  #                450 => ['11級', 11],
  #                350 => ['12級', 12],
  #                250 => ['13級', 13],
  #                150 => ['14級', 14],
  #                 50 => ['15級', 15],
  #                  0 => ['初心者', 16] }

  RANK_HASH = {
                2300 => ['七段', -6],
                2200 => ['六段', -5],
                2100 => ['五段', -4],
                1950 => ['四段', -3],
                1800 => ['三段', -2],
                1650 => ['二段', -1],
                1500 => ['初段',  0],
                1425 => ['１級',  1],
                1350 => ['２級',  2],
                1300 => ['３級',  3],
                1250 => ['４級',  4],
                1200 => ['５級',  5],
                1150 => ['６級',  6],
                1100 => ['７級',  7],
                1050 => ['８級',  8],
                1000 => ['９級',  9],
                 900 => ['10級', 10],
                 800 => ['11級', 11],
                 700 => ['12級', 12],
                 600 => ['13級', 13],
                 500 => ['14級', 14],
                   0 => ['15級', 15] }

  def initialize(rating_or_dankyu)
    if rating_or_dankyu.numeric?
      @rating = rating_or_dankyu.to_i
      RANK_HASH.each do |key, value|
        next unless @rating >= key
        @dankyu = value[0]
        @rank   = value[1]
        break
      end
    else
      @dankyu = rating_or_dankyu
      RANK_HASH.each do |key, value|
        next unless @dankyu == value[0]
        @rating = key
        @rank   = value[1]
        break
      end
    end
  end

  class << self
    def handicap(score1, score2)
      player1 = Dankyu.new(score1)
      player2 = Dankyu.new(score2)

      # player1が上位者
      if player1.rating < player2.rating
        player1, player2 = player2, player1
      end

      deff = defference(player1, player2)
      teai = if    deff >=  9 then '四枚落'
             elsif deff >=  7 then '二枚落'
             elsif deff >=  6 then '飛香落'
             elsif deff >=  5 then '飛落'
             elsif deff >=  4 then '角落'
             elsif deff >=  3 then '香落'
             else                  '平手'
             end
      [player2, player1, deff, teai]
    end

    private

    def defference(player1, player2)
      d = player2.rank - player1.rank

      d -= 1 if player1.rank <= 0 # 有段者特別ルール
      d = 0  if d < 0
      d
    end
  end
end
