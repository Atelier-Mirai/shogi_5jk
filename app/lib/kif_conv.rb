module KifConv
  class << self
    # kif形式をsfen形式に変換する
    def kif_to_sfen(kifu)
      return :unexpected_kifu_format if check_format(kifu) != :kif

      body = false
      moves = []
      # ７六歩 ３四歩 # => 76P 34P
      kifu = kan_to_num(kifu)
      kifu.split("\n").each do |line|
        # コメントは読み飛ばす
        next if line.include?('*') || line.include?('#')
        if body == false
          body = true if line.include?("手数----指手---------消費時間--")
          next
        end
        if body == true
          # puts line #=> "10 94歩(93)   ( 0:00/00:00:00)"
          moves << line.split(' ')[1]
        end
      end

      # 初期配置
      sfen = 'lnsgkgsnl/1r5b1/ppppppppp/9/9/9/9/9/LNSGKGSNL'
      sfen << ' b - 1'
      sfen << ' moves'
      # sfen 形式に変換
      # 94歩(93)
      # => 9394
      # => 9c9d
      # ５二金打
      # => G*5b
      previous_to = ''
      moves.each do |move|
        if move.include?("打")
          m = /(?<to>\d{2})(?<piece>[A-Z])/.match(move)
          sfen << ' ' + m[:piece] + '*' + num_to_alpha(m[:to])
          previous_to = num_to_alpha(m[:to])
        elsif move.include?("同")
          m = /(?<from>\d{2})/.match(move)
          sfen << ' ' + num_to_alpha(m[:from]) + previous_to
          sfen << '+' if move.include?("成") && !move.include?("不成")
        else
          m = /(?<to>\d{2})(.+)(?<from>\d{2})/.match(move)
          sfen << ' ' + num_to_alpha(m[:from]) + num_to_alpha(m[:to])
          sfen << '+' if move.include?("成") && !move.include?("不成")
          previous_to = num_to_alpha(m[:to])
        end
      end
      sfen
    end

    # 棋譜形式を判定する
    def check_format(kifu)
      regex_csa  = Regexp.new('[+|-]\\d{4}[A-Z]{2}')
      regex_sfen = Regexp.new(('[A-Za-z0-9]+/' * 9).chop)

      return :kif     if kifu.include?("手数----指手---------消費時間--")
      return :bod     if kifu.include?('+---------------------------+')
      # csa形式 +2726FU
      return :csa     if kifu.match(regex_csa)
      # sfen形式 lnsgkgsn1/1r5b1/ppppppppp/9/9/9/9/9/LNSGKGSNL
      return :sfen    if kifu.match(regex_sfen)
      :no_kifu
    end

    # ７六歩 ３四歩 # => 76歩 34歩
    def kan_to_num(str)
      [ %w(１ 1),
        %w(２ 2),
        %w(３ 3),
        %w(４ 4),
        %w(５ 5),
        %w(６ 6),
        %w(７ 7),
        %w(８ 8),
        %w(９ 9),
        %w(一 1),
        %w(二 2),
        %w(三 3),
        %w(四 4),
        %w(五 5),
        %w(六 6),
        %w(七 7),
        %w(八 8),
        %w(九 9),
        %w(歩 P),
        %w(香 L),
        %w(桂 N),
        %w(銀 S),
        %w(金 G),
        %w(角 B),
        %w(飛 R)
      ].inject(str) do |s, (before, after)|
        s.gsub(before, after)
      end
    end

    # "11" => "1a", "99" => "9i"
    def num_to_alpha(num)
      "#{num[0]}#{('`'.ord + num[1].to_i).chr}"
    end
  end
end
