module Poker
  # ディーラー dealer カードを配る人
  # 手札 ホールカードholecard。handとも
  # 山札 stock
  # 捨て札 discard
  # rank カードの順位 AKQJ1098765432
  # Aは 5432A のストレート場合に限り1として扱われ、2よりも下の扱い
  # deck デッキ カード一組 deck
  # (スート suit の強い順位は、♠・♥・♦・♣) spade heart diamond club
  # hand ハンド（役）
  # ストレートフラッシュ Straight Flush
  # フォーカード Four of a Kind
  # フルハウス Full House
  # フラッシュ Flush
  # ストレート Straight
  # スリーカード Three of a Kind
  # ツーペア Two Pair
  # ワンペア One Pair
  # ハイカード High Cards

  # player
  # opponent

  # カード形式
  # 0b1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111
  # 0xf_ffff_ffff_ffff

  CLUB         = 0b00
  DIAMOND      = 0b01
  HEART        = 0b10
  SPADE        = 0b11

  STRAIGHT_FLUSH  = 0b1000
  FOUR_OF_A_KIND  = 0b0111
  FULL_HOUSE      = 0b0110
  FLUSH           = 0b0101
  STRAIGHT        = 0b0100
  THREE_OF_A_KIND = 0b0011
  TWO_PAIR        = 0b0010
  ONE_PAIR        = 0b0001
  HIGH_CARDS      = 0b0000

  class << self
    # 山札からそれぞれのプレーヤーに5枚ずつカードを配る
    # @param  [nil]
    # @return [Integer[2]] card形式(52bit)の配列
    def dealer
      # 山札
      stock  = 0xf_ffff_ffff_ffff
      # それぞれのプレーヤーの手札
      holecard = [0, 0]
      count    = [0, 0]
      loop do
        # 0 - 51 までの乱数生成
        n = rand(52)
        # n ビット目が０でない (= 山札にそのカードがある)
        if (stock & (1 << n)) != 0
          stock &= ~(1 << n) # 山札を 0 に
          # どちらのプレーヤーにカードに割り振るか
          i = rand(10) % 2
          if count[i] < 5
            holecard[i] |= (1 << n)
            count[i]    += 1
          end
        end
        break if count[0] == 5 && count[1] == 5
      end
      [*holecard, stock]
    end


    # それぞれのプレーヤーの不要なカードを交換する
    # @param  [card, change_card, stock] 手札、何番目のカードを交換するか、残りの山札
    # @return [card, stock]
    def change(card, change_card, stock)
      # change_card 10000 一枚目のカードを交換する
      # change_card 00011 四枚目と五枚目のカードを交換する
      mask = 0b10000
      arr = []
      (0..4).each do |i|
        if (change_card & mask) >> 4 == 1
          arr << i
        end
        mask >>= 1
      end

      change_times = bit_count8(change_card)
      loop do
        # 0 - 51 までの乱数生成
        n = rand(52)
        # n ビット目が０でない (= 山札にそのカードがある)
        if (stock & (1 << n)) != 0
          stock &= ~(1 << n) # 山札を 0 に
          # TODO ちゃんとその人のカードの何番目かみて修正すること
          holecard[i] |= (1 << n)
          change_times -= 1
        end
        break if change_times == 0
      end

      52.times do |i|
      end
    end

    # 手札を見やすい形で表示する
    # straight_wheel は 5♡ 4♢ 3♣ 2♣ A♠ と並び替えて表示する
    # @param  [Integer] card カードフォーマット 0b0001_0001_0001_0001_0001_0000_0000_0000_0000_0000_0000_0000_0000
    # @return [String] 例) "A♠ K♠ Q♠ J♠ 10♠ "
    def prity(card)
      card_org = card
      str = ''
      count = 0
      %w(A K Q J 10 9 8 7 6 5 4 3 2).each do |rank|
        %w(♠ ♡ ♢ ♣).each do |suit|
          if card >> 51 == 1 # 最上位ビットが1
            # 全角には、最後のスペースが必要らしい"
            str << "#{rank}#{suit} "
            count += 1
            if count == 5
              # straight_wheel の時に A を最後に
              if straight(card_org) == 5
                ace = str.slice!(0..2)
                return str << ace
              else
                return str
              end
            end
          end
          card = (card << 1) & 0xf_ffff_ffff_ffff
        end
      end
    end

    # 役の判定を行う
    # @param  [Integer] card カードフォーマット 0b1000000000000_1000000000000_1000000000000_1100000000000
    # @return [Integer] ストレートフラッシュ〜ハイカードまで役の種類
    def hand(card)
      hd = case
           when h = straight(card) && flush(card)
             STRAIGHT_FLUSH
           when h = four_of_a_kind(card)
             FOUR_OF_A_KIND
           when h = full_house_or_three_of_a_kind(card)
           when h = flush(card)
             FLUSH
           when h = straight(card)
             STRAIGHT
           when h = two_or_one_pair(card)
             h
           else
             HIGH_CARDS
           end
    end

    # フラッシュを判定
    # @param  [Integer] card カードフォーマット     0b0001_0001_0001_0001_0001_0000_0000_0000_0000_0000_0000_0000_0000
    # @return [Integer] SPADE 0b11 / HEART 0b10 / DIAMOND 0b01 / CLUB 0b00
    # @return [nil] フラッシュでなかった場合 nil
    def flush(card)
      return Poker::CLUB    if bit_count64(card & 0x1_1111_1111_1111) == 5
      return Poker::DIAMOND if bit_count64(card & 0x2_2222_2222_2222) == 5
      return Poker::HEART   if bit_count64(card & 0x4_4444_4444_4444) == 5
      return Poker::SPADE   if bit_count64(card & 0x8_8888_8888_8888) == 5
      return nil
    end

    # @param  [Integer] card カードフォーマット 0b0001_0001_0001_0001_0001_0000_0000_0000_0000_0000_0000_0000_0000
    # @return [Integer] ハイカードのランク(A:14 - 5:5)
    # @return [nil] ストレートでなかった場合 nil
    def straight(card)
      # 圧縮する 52bit -> 13bit に
      scard = 0
      13.times do
        i = (card & 0xf_0000_0000_0000) >> 48
        if (i >> 3) == 1 || (i >> 2) == 1 || (i >> 1) == 1 || (i >> 0) == 1
          scard |= 1
        end
        scard <<= 1
        card  = (card << 4) & 0xf_ffff_ffff_ffff
      end
      scard >>= 1

      mask = 0b0000011111111
      %w(A K Q J 10 9 8 7 6).each_with_index do |rank, i|
        if scard & mask == 0
          return 14 - i
        else
          mask >>= 1
          mask |= (0b1 << 13)
        end
      end
      # A5432もストレートなので
      return 5 if scard == 0b1000000001111
      return nil
    end

    def four_high(card)
      card = sort(card)
      # flag MSB フォーカード
      #          スリーカード
      #          ツーペア
      #      LSB ワンペア
      flag = 0b0000
      rank = 0
      pair = 0x00 # 上位4ビット 上位のスリーカード／ペア 下位4ビット 下位のペア
      high = 0
      mask = 0xf_0000_0000_0000
      %w(A K Q J 10 9 8 7 6 5 4 5 3 2).each_with_index do |r, i|
        # カードの強さ A:14 〜 2:2
        rank = 14 - i
        case bit_count4((card & mask) >> 48)
        when 4
          # フォーカード
          flag = 0b1000
          pair == 0 ? pair = rank << 4 : pair |= rank
          return flag, pair, high
        when 3
          # フルハウス / スリーカード
          flag |= 0b0100
          pair == 0 ? pair = rank << 4 : pair |= rank
        when 2
          # ツーペア / ワンペア
          flag += 0b0001
          pair == 0 ? pair = rank << 4 : pair |= rank
        when 1
          # ハイカード
          high = rank if high == 0
        end

        card = (card << 4) & 0x0f_ffff_ffff_ffff
        break if card == 0
      end
      return flag, pair, high
    end

    # http://www.mwsoft.jp/programming/java/java_lang_integer_bit_count.html
    # 立っているbitの数
    def bit_count4(i)
      i = i - ((i >> 1) & 0x5)
      i = (i & 0x3) + ((i >> 2) & 0x3)
    end

    def bit_count8(i)
      i = i - ((i >> 1) & 0x55)
      i = (i & 0x33) + ((i >> 2) & 0x33)
      i = (i + (i >> 4)) & 0x0f
      i & 0x3f
    end

    def bit_count16(i)
      i = i - ((i >> 1) & 0x5555)
      i = (i & 0x3333) + ((i >> 2) & 0x3333)
      i = (i + (i >> 4)) & 0x0f0f
      i = i + (i >> 8)
      i & 0x3f
    end

    def bit_count32(i)
      i = i - ((i >> 1) & 0x55555555)
      i = (i & 0x33333333) + ((i >> 2) & 0x33333333)
      i = (i + (i >> 4)) & 0x0f0f0f0f
      i = i + (i >> 8)
      i = i + (i >> 16)
      i & 0x3f
    end

    def bit_count64(i)
      i = i - ((i >> 1) & 0x5555_5555_5555_5555)
      i = (i & 0x3333_3333_3333_3333) + ((i >> 2) & 0x3333_3333_3333_3333)
      i = (i + (i >> 4)) & 0x0f0f_0f0f_0f0f_0f0f
      i = i + (i >> 8)
      i = i + (i >> 16)
      i = i + (i >> 32)
      i & 0x3f
    end
  end
end
