module Relay
  MEMBERS = [
             { koutan21: 2736 },
             # { YUGEN1262: 2219 },
             # { kannju: 1873 },
             { WhiteWizard: 1680 },
             # { shigeo3: 1623 },
             { thegul: 1567 },
             { ms1985: 1394 },
             { 'ko-tan': 1064 },
             { rarapoto: 1064 },
             # { 'bururutti-2': 680 },
            ]

  class << self
    # 組み合わせを求める
    def combi(top = 3, n = 4)
      raw = Relay::MEMBERS.combination(n).select do |elm|
        total = elm.inject(0){ |sum, e| sum + e.values[0] }
        elm.prepend(total) if total <= 1600 * n
        # elm.unshift(total) if total <= 1600 * n
      end
      # 降順に並び替え
      raw.sort { |a, b| b[0] <=> a[0] }.map { |r| prity(r) }[0...top]
    end

    def prity(members)
      str = ""
      members.each do |member|
        if member.kind_of?(Integer)
          str << "計:#{member}, "
        else
          str << "#{member.keys[0]}(#{member.values[0]}), "
        end
      end
      str.slice(0..-3)
    end
  end
end
