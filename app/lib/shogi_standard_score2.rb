# 年月日別のＲ分布をもとに、偏差値を返す

# 使い方
# sss = ShogiStandardScore2.new(ymd)
# scores = sss.values(ranking.rating)
# ranking.superior_rank  = scores[:superior_rank]
class ShogiStandardScore2
  attr_reader :fd

  def initialize(ymd)
    # 度数分布 frequency_distribution
    @@fd = RatingDistribution.find_by(ymd: ymd).data.values.prepend(0)
  end

  def values(rating)
    rating = 3099 if rating >= 3100
    rating =    0 if rating <     0

    # 対象者人数
    n = @@fd.sum

    # 上からの順位
    superior_rank = superior_rank(rating)

    # 上からの割合
    percentage = percentage(rating)

    # 中央値
    median = self.median

    # 算術平均
    mean   = self.mean

    # 偏差平方和
    sum_of_squares = 0
    @@fd.each.with_index do |fd, index|
      sum_of_squares += ((lower_bound(index) + range(index) / 2) - mean)**2 * fd
    end

    # 分散
    variance = sum_of_squares / n

    # 標準偏差
    standard_deviation = Math.sqrt(variance)

    # 偏差値
    standard_score = (10 * (rating - mean)) / standard_deviation + 50

    { n: n, superior_rank: superior_rank, percentage: percentage, median: median, mean: mean, standard_deviation: standard_deviation, standard_score: standard_score, index: index(rating)-1 }
  end

  # 上からの順位
  def superior_rank(rating)
    index       = index(rating)
    range       = range(index)
    lower_bound = lower_bound(index)

    @@fd[0...index].sum + (@@fd[index]*(range-(rating-lower_bound))/range.to_f).round
  end

  def percentage(rating)
    ((superior_rank(rating) / @@fd.sum.to_f) * 100).round(2)
  end

  # 中央値を求める
  def median
    (0..).each do |rating|
      return rating if percentage(rating) <= 50
    end
  end

  # 平均値を求める
  def mean
    # 対象者人数
    n = @@fd.sum

    # 算術平均
    sum = 0
    @@fd.each.with_index do |fd, index|
      sum += fd * (lower_bound(index) + range(index) / 2)
    end
    mean = sum / n.to_f
  end

  # 下限 lower bound
  # R 2800の場合
  # 3100 - 2900 @fd[0]
  # 2900 - 2700 @fd[1]
  # @fd[index] そのratingの属する人数

  def index(rating)
    case
    when rating >= 1700
      1 + (3099 - rating) / 200
    when rating >= 1550
      8
    else
      9 + (1549 - rating) / 100
    end
  end

  # その階級に含まれるＲの幅
  def range(index)
    case index
    when 0
      0
    when 1..7
      200
    when 8
      150
    when 9..23
      100
    else 24
      50
    end
  end

  # その階級の下限のＲ
  def lower_bound(index)
    case index
    when 0
      3100
    when 1..7
      -200 * index + 3100
    when 8
      1550
    when 9..23
      -100*(index-8) + 1550
    when 24
      0
    end
  end
end
