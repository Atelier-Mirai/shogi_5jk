class ApplicationMailer < ActionMailer::Base
  default from: '五級位上昇を目指す会 <5JK.Shogi@gmail.com>'
  layout 'mailer'
end
