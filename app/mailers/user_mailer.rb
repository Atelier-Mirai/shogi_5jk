class UserMailer < ApplicationMailer
  def account_activation(user, token)
    @user  = user
    @token = token
    mail to: @user.email, subject: "五級位上昇を目指す会 入会登録のご案内"
  end

  def approve_or_reject_message(user, subject, message)
    @user    = user
    @subject = subject
    @message = message
    email_with_name = "#{@user.name}さん <#{@user.email}>"
    mail to: email_with_name, subject: @subject
  end

  def password_reset(user, token)
    @user  = user
    @token = token
    email_with_name = "#{@user.name}さん <#{@user.email}>"
    mail to: email_with_name, subject: "五級位上昇を目指す会 パスワード再設定のご案内"
  end
end
