class Entry < ApplicationRecord
  belongs_to :kisen

  default_scope               -> { order(rating: :desc, number: :asc) }
  scope :title_holder,        -> { where('entry_category = ?', 'title_holder') }
  scope :general_participant, -> { where('entry_category = ?', 'general_participant') }

  validates :comment, length: { maximum: 300 }, allow_blank: true
  validates :name, presence: true

  serialize :memo

  # 上級会員なら、★付きの名前を返す
  def star_name
    # self&.number&.start_with? 'J' ? "★#{name}" : self&.name
    number&.start_with?('J') ? "★#{name}" : name
  end

  def star_name_with_rating
    # self&.rating.present? ? "#{star_name} (#{rating})" : self&.star_name
    rating ? "#{star_name} (#{rating})" : star_name
  end
end
