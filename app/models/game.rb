class Game < ApplicationRecord
  belongs_to :matchup

  default_scope -> { order(game_number: :asc) }
  scope :player, ->(id) { where('player1_id = ? or player2_id = ?', id.to_s, id.to_s).reorder(date: :asc) }
  scope :next_plays, -> { where('date >= ?', Time.current).where(reason: nil).reorder(date: :asc) }

  validates :player1_id,  presence: true
  validates :player2_id,  presence: true
  validates :date,        presence: true

  def player1
    User.find(player1_id)
  end

  def player2
    User.find(player2_id)
  end

  # 対局済みか否か？
  def played?
    reason.present? && reason != 'finish'
  end

  # 対局済みか否か？ (タイトル戦・勝ち抜き決定戦で、対局不要の場合を含む)
  def finished?
    reason.present? || reason == 'finish'
  end

  # 対局結果をリセット (デバッグ用/管理者用)
  def reset
    update_columns(winner_id: nil, reason: nil, point1: nil, point2: nil,
                   mark1: nil, mark2: nil, uwate_id: nil, teai: nil, kifu_id: nil)
  end

  # 対局結果を全件リセット (デバッグ用/管理者用)
  def self.reset_all
    Game.all.find_each(&:reset)
  end

  # player1とplayer2 どちらが勝者だったのか？
  def winner
    return 0 if winner_id.nil?
    player1_id.to_i == winner_id.to_i ? 1 : 2
  end

  # player1とplayer2 どちらが敗者だったのか？
  def loser
    return nil if winner_id.nil?
    player1_id.to_i == winner_id.to_i ? 2 : 1
  end

  def loser_id
    send("player#{self.loser}_id").to_i
  end

  def komaochi?
    uwate_id != 0
  end

  # player1とplayer2 どちらが上手だったのか？
  def uwate
    return nil if uwate_id.nil? || uwate_id == 0
    player1_id.to_i == uwate_id.to_i ? 1 : 2
  end

  # player1とplayer2 どちらが下手だったのか？
  def shitate
    return nil if uwate_id.nil? || uwate_id == 0
    player1_id.to_i == uwate_id.to_i ? 2 : 1
  end

  # r 回戦
  def r
    if repechage_match? || winning_deciding_match?
      game_number[2].to_i
    else
      game_number[1].to_i
    end
  end

  # 第 n 局
  def n
    if repechage_match? || winning_deciding_match?
      game_number[3].to_i
    else
      game_number[2].to_i
    end
  end

  # 敗者復活戦か？
  def repechage_match?
    game_number[0] == 'R'
  end

  # 勝ち抜き決定戦か？
  def winning_deciding_match?
    game_number[0] == 'W'
  end

  # A組 B組
  def group_name
    if winning_deciding_match? || repechage_match?
      game_number[1]
    else
      game_number[0]
    end
  end

  def player1_name
    player_name(1)
  end

  def player2_name
    player_name(2)
  end

  def winner_name
    User.find_by(id: winner_id)&.name || "対局者未定"
  end

  # 対局名
  # a21  => A組2回戦第1局の敗者
  # RA21 => 復活戦A組2回戦第1局 勝者
  # A2   => A組2位
  def player_name(args)
    case args
    when 1 then player_id = player1_id
    when 2 then player_id = player2_id
    end

    if player_id.numeric?
      return matchup.kisen.entries.find_by(user_id: player_id)&.star_name_with_rating
    end

    # self.r では、game_number（この対局）が何回戦の第何局なのかになる。
    # ここで求めたいのは、当該プレーヤーは何回戦何局の勝者／敗者なのかということ。
    repechage_match        = (player_id[0] == 'R')
    winning_deciding_match = (player_id[0] == 'W')
    player_id              = player_id[1..-1] if repechage_match || winning_deciding_match
    k                      = player_id[0].upcase
    r                      = player_id[1]
    n                      = player_id[2]
    wl                     = player_id[0].capital_letter? ? '勝者' : '敗者'

    # 基本的には、player_idを参照して a21 なら 二回戦第1局敗者と表示すれば良いが、
    # タイトル決定戦の時に、二回戦第1局勝者と表示されても嫌なので、
    # game_numberに応じて適宜変更する

    # トーナメント戦の対局
    if n.present?
      # タイトル決定戦
      if group_name == 'T'
        if matchup.preliminary_group == ['A']
          if matchup.title_holder_entry.present?
            # 一組での開催＆タイトルホルダーがいる
            "挑戦者"
          else
            repechage_match ? "復活戦 勝者" : "本　戦 勝者"
          end
        else
          if matchup.title_holder_entry.present?
            # 一組での開催＆タイトルホルダーがいる
            "挑戦者"
          else
            # 他にもあるのであれば、A組勝者
            "#{k}組 勝者"
          end
        end
      # 挑戦者決定戦
      elsif group_name == 'Q'
        "#{k}組 勝者"
      # 勝ち抜き決定戦
      elsif winning_deciding_match?
        repechage_match ? "復活戦 勝者" : "本　戦 勝者"
      else
        "#{r}回戦 第#{n}局 #{wl}"
      end
    # リーグ戦は、A1 と書いて、A組1位を表すこととするので、
    else
      "#{k}組 #{r}位" # A-P 16組出場可能
    end
  end

  def seed1?
    !!seed1
  end

  def seed2?
    !!seed2
  end

  def seed?
    seed1? || seed2?
  end

  # TODO: 日程欄にタイトル決定戦第何局などと表示出来るように
  def game_name
    k = group_name
    r = self.r
    n = self.n
    case game_number.first
    when 'A'..'P' then "本戦"
    when 'R'      then "敗者復活戦"
    when 'W'      then "勝抜決定戦"
    when 'Q'      then "挑戦者決定戦"
    when 'T'      then "タイトル決定戦"
    end
  end
end
