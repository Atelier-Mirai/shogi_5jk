# ゲストユーザー作成
class GuestUser
  attr_accessor :id, :name, :role

  def initialize(name = 'guest')
    @name  = name
    # 1000人ものUser登録はない前提
    @id    = Digest::MD5.new.update(@name).to_s[-3..-1].hex + 1000
    @role  = 'guest'
  end
end
