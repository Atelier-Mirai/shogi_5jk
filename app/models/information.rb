class Information < Message
  # has_one :kifu, class_name: 'Kifu', foreign_key: 'kisen_id'

  def kifu
    Kifu.find_by(id: kisen_id)
  end
end
