class Kifu < ApplicationRecord
  include SearchCop

  belongs_to :post
  default_scope -> { order(updated_at: :desc) }

  serialize :kifu
  serialize :info
  serialize :comment
  serialize :raw_kifu

  search_scope :search do
    attributes :player1, :sengata1, :player2, :sengata2, :teai, :kisen, :date
  end
end
