class Kisen < ApplicationRecord
  has_many :entries, dependent: :destroy
  has_one  :matchup, dependent: :destroy
  has_one  :message, dependent: :destroy

  default_scope -> { order(term: :desc, created_at: :desc) }

  # 参加者募集中
  scope :now_inviting, -> { where(['begin_date <= ? and ? <= end_date', Date.today, Date.today]) }
  # 募集開始前
  scope :not_begin,    -> { where(['begin_date > ?', Time.current]) }
  # 募集終了
  scope :closed,       -> { where(['end_date   < ?', Time.current.tomorrow]) }

  # 開幕中
  scope :now_open,     -> { where('open_date <= ? and finish_date > ?', Date.today, Date.today) }
  # 開幕前
  scope :not_open,     -> { where('open_date > ?', Date.today) }
  # 棋戦終了
  scope :finished,     -> { where('finish_date < ?', Date.today) }

  # メニューへの 要項・対局表 表示用
  scope :for_menu,     -> { where('begin_date <= ? and finish_date > ?', Date.today, Date.today.days_ago(10)) }

  validates :term, presence: true
  validates :name, length: { maximum: 50 }, presence: true
  validates :open_date, presence: true
  validates :begin_date, presence: true
  validates :end_date, presence: true
  validate  :begin_date_should_be_before_end_date
  validate  :open_date_should_be_after_end_date

  serialize :title_holder_id

  def to_s
    self.name
  end

  def to_sym
    h = {
      棋聖戦:         :kisei,
      王座戦:         :ohza,
      東西戦:         :tozai,
      名人戦:         :meijin,
      棋王戦:         :kioh,
      王将戦:         :ohsho,
      王位戦:         :ohi,
      新人王戦:       :shinjin,
      竜王戦:         :ryuoh
    }
    if name.include? "交流戦"
      :koryu
    else
      h[name.to_sym]
    end
  end

  def sym_name
    self.to_sym
  end

  def full_name
    if name.include?("交流戦")
      "第#{term.to_kan}回 #{name}"
    else
      "第#{term.to_kan}期 #{name}"
    end
  end

  def title_name
    self.name.sub('東西戦', '東西杯').tr('戦', '')
  end

  def holding_method
    # ActiveHash を使う方法もあるみたい
    # http://labs.timedia.co.jp/2014/01/rails-enum-activehash.html
    h = {
      棋聖戦: { time: "15分", teai: "手合割", method_name: "リーグ戦",
             member: :all, method: :league, qualifier: 1, titleholder_system: true },
      王座戦: { time: "早2",  teai: "平手", method_name: "トーナメント戦",
             member: 8, method: :tournament, repechage: true, qualifier: 1, titleholder_system: true },
      東西戦: { time: "15分", teai: "手合割", method_name: "団体戦",
             member: 5, sort: { area: :asc, rating: :asc }, method: :team, titleholder_system: false },
      名人戦: { time: "15分", teai: "平手", method_name: "階級別リーグ戦",
             member: :class_all, sort: { meijin_class: :asc }, method: :league, qualifier: 1, titleholder_system: true },
      リレー将棋大会: { time: "15分", teai: "平手", method_name: "30手ごとに交代" },
      棋王戦: { time: "15分", teai: "手合割", method_name: "トーナメント戦",
             member: 8, method: :tournament, repechage: true, qualifier: 1, titleholder_system: true },
      王将戦: { time: "早2", teai: "手合割", method_name: "リーグ戦",
             member: 8, method: :league, qualifier: 1, titleholder_system: true },
      王位戦: { time: "15分", teai: "手合割", method_name: "予選リーグ方式 本戦トーナメント戦",
             member: 8, method: :league, repechage: true, qualifier: 2, titleholder_system: true },
      新人王戦: { time: "15分", teai: "手合割", method_name: "トーナメント戦",
              member: 8, method: :tournament, repechage: true, titleholder_system: false },
      竜王戦: { time: "15分", teai: "平手",   method_name: "トーナメント戦",
             member: 8, method: :paramus, qualifier: 1, titleholder_system: true },
      交流戦: { time: "15分", teai: "手合割", method_name: "団体戦",
             member: 5, sort: { team: :asc, rating: :asc }, method: :team, titleholder_system: false }
    }
    name.include?("交流戦") ? h[:交流戦] : h[name.to_sym]
  end

  def pretty_holding_method
    h = holding_method
    "#{h[:time]} #{h[:teai]} #{h[:method_name]}"
  end

  # { ○: 3, □: 2.9 ... }
  def winning_point
    wp = {
      # 勝ち点：○＝３点、□＝２．９点、●＝１点、■＝０点
      # 勝ち点：○＝３点、□＝２．９点、●＝１点、▲＝１点 ■＝０点
      # 勝ち点：○＝３点、□＝２点、●＝１点、■＝−２点
      棋聖戦: { win: 3, unearned_win: 2.9, lose: 1, lose_failure: 1, unearned_lose:  0 },
      王位戦: { win: 3, unearned_win: 2.9, lose: 1, lose_failure: 1, unearned_lose:  0 },
      王将戦: { win: 3, unearned_win: 2.9, lose: 1, lose_failure: 1, unearned_lose:  0 },
      東西戦: { win: 3, unearned_win: 2, lose: 1, unearned_lose: -2 },
      名人戦: { win: 3, unearned_win: 2.9, lose: 1, unearned_lose:  0 }
    }
    wpm = {
      win: "○", unearned_win: "□", lose: "●", lose_failure: "▲", unearned_lose: "■"
    }

    h = {}
    wp[name.to_sym].each_key do |k|
      key    = wpm[k]
      value  = wp[name.to_sym][k]
      h[key] = value
    end
    h
  end

  # 棋戦 募集中か？
  def now_inviting_participants?
    Time.current.in? begin_date.beginning_of_day..end_date.end_of_day
  end

  private

  def begin_date_should_be_before_end_date
    return unless begin_date && end_date
    errors.add(:begin_date, 'は募集終了日より前に設定して下さい') if begin_date >= end_date
  end

  def open_date_should_be_after_end_date
    return unless open_date && end_date
    errors.add(:open_date, 'は募集終了日より後に設定して下さい') if open_date <= end_date
  end
end
