class Matchup < ApplicationRecord
  after_create   :schedule_and_result_topic_create
  before_destroy :schedule_and_result_topic_destroy

  belongs_to :kisen
  has_many   :games, dependent: :destroy

  serialize :preliminary_group
  serialize :final_group

  delegate :player, to: :games
  delegate :name, :full_name, :sym_name, :title_name, :term, :open_date, :title_holder, :holding_method, :pretty_holding_method, to: :kisen

  # 結果報告＆日程調整用トピックも一緒に削除
  def destroy
    Topic.find_by(id: topic_schedule_id)&.destroy
    Topic.find_by(id: topic_result_id)&.destroy
    super
  end

  # 対局表の最終更新日
  def last_updated
    games.maximum(:updated_at)
  end

  # 対局者を仮にa11としているので、
  # 対局者確定時に、その対局者のuser_idで更新する
  # +----+------------+-------------+------------+------------+
  # | id | matchup_id | game_number | player1_id | player2_id |
  # +----+------------+-------------+------------+------------+
  # | 35 | 4          | RA12        | a11        | a12        |
  def player_update(tentative_player_id, definitive_player_id)
    games.where(player1_id: tentative_player_id).update_all(player1_id: definitive_player_id)
    games.where(player2_id: tentative_player_id).update_all(player2_id: definitive_player_id)
  end

  def games_group(group = 'A')
    games.where('game_number like ?', "#{group}%")
  end

  # 対局表の最後の対局日を返す
  def last_game_date
    games.reorder(date: :desc).first.date
  end

  # リーグ戦の最終局はA45, トーナメント表の最終局はA31などを返す
  def maximum_game_number(group)
    games.where('game_number like ?', "#{group}%").maximum(:game_number)
  end

  # 対局者の勝敗、勝ち点を取得　順位も付与
  def group_rank_win_lose(group = 'A')
    result = {}
    temp   = []

    # 勝敗、勝ち点の集計
    h = {}
    games_group(group).each do |game|
      if h[game.player1_id.to_i].nil?
        h[game.player1_id.to_i] = { win: 0, lose: 0, win_point: 0 }
      end
      if h[game.player2_id.to_i].nil?
        h[game.player2_id.to_i] = { win: 0, lose: 0, win_point: 0 }
      end
      next if game.winner_id.nil?

      # winner = (game.player1_id.to_i == game.winner_id.to_i ? :player1 : :player2)
      winner = game.winner
      if winner == 1 # player1が勝者であれば
        h[game.player1_id.to_i][:win]       += 1
        h[game.player1_id.to_i][:win_point] += game.point1.to_f
        h[game.player2_id.to_i][:lose]      += 1
        h[game.player2_id.to_i][:win_point] += game.point2.to_f
      elsif winner == 2 # player2が勝者であれば
        h[game.player2_id.to_i][:win]       += 1
        h[game.player2_id.to_i][:win_point] += game.point2.to_f
        h[game.player1_id.to_i][:lose]      += 1
        h[game.player1_id.to_i][:win_point] += game.point1.to_f
      end
    end

    # 順位付け
    points = []
    h.each do |_k, v|
      points << v[:win_point]
    end
    ranks = points.rank
    if ranks.present?
      i = 0
      h.each do |_k, v|
        v[:rank] = ranks[i]
        i += 1
      end

      # idと順位も持ち帰る
      # :ranks=>{50=>1, 60=>2, 56=>5, 46=>3, 62=>3}}
      alist = [h.keys, ranks].transpose
      h[:ranks] = Hash[alist].safe_invert
    end
    h
  end

  # リーグ戦全て終了していたら、trueを返す
  def league_finished?(game_initial)
    games_group(game_initial).each do |game|
      return false if game.played? == false
    end
    true
  end

  # 東西戦・交流戦が全て終了していたら、勝利チームを返す
  # 全終了でなければ、falseを返す
  def winning_team(game_initial)
    games = games_group(game_initial)

    # 全局終了？
    games.each do |game|
      return false if game.played? == false
    end

    # どちらのチームが勝った？
    point = Array.new(3, 0)
    games.each do |game|
      point[1] += game.point1
      point[2] += game.point2
    end

    # TODO: 引き分けの処理
    if point[1] > point[2]
      winner       = 0
      second_place = 1
    else
      winner       = 1
      second_place = 0
    end

    # チームメンバーのid
    ids = [games.pluck(:player1_id), games.pluck(:player2_id)]

    # チーム名
    team_names = [games.first.matchup.kisen.entries.where(user_id: ids[0].first).pluck(:team)[0],
                  games.first.matchup.kisen.entries.where(user_id: ids[1].first).pluck(:team)[0]]

    # メンバー名
    member_names = [games.first.matchup.kisen.entries.where(user_id: ids[0]).pluck(:name),
                    games.first.matchup.kisen.entries.where(user_id: ids[1]).pluck(:name)]

    { winner:       { team_names:   team_names[winner],
                      ids:          ids[winner],
                      member_names: member_names[winner] },
      second_place: { team_names:   team_names[second_place],
                      ids:          ids[second_place],
                      member_names: member_names[second_place] } }
  end

  # 各リーグの上位者のuser_idを返す
  # 挑戦者決定戦への進出用
  def league_high_rankers(game_initial, rankers = 2)
    r_ids = []
    # ranks = {1=>[50], 2=>[60], 5=>[56], 3=>[46, 62]}
    ranks = group_rank_win_lose(game_initial)[:ranks]
    temp = []
    ranks.keys.sort.each do |key|
      ranks[key].each do |player_id|
        temp << player_id
        break if temp.count == rankers # TODO: 二位が2人いたときの処理など入れた方が良い
      end
      if temp.count == rankers
        r_ids << temp
        break
      end
    end
    r_ids.flatten!
    # return self.kisen.entries.where(user_id: r_ids) # Entryのid順に帰るのでコメントアウト
  end

  # タイトルマッチ開催の有無held 開催round 開催回数n 勝者winner=0or1or2 を返す
  def title_match(game_initial)
    maximum_game_number = games_group(game_initial).maximum(:game_number)

    return if maximum_game_number.blank?

    if maximum_game_number[0] == 'R' || maximum_game_number[0] == 'W' # RA41
      round = maximum_game_number[2].to_i
      n_max = maximum_game_number[3].to_i
    else
      round = maximum_game_number[1].to_i
      n_max = maximum_game_number[2].to_i
    end

    # タイトルマッチの有無
    title_match = (n_max >= 2 ? true : false)

    game = []
    1.upto(n_max).each do |n|
      game[n] = games.find_by(game_number: "#{game_initial}#{round}#{n}")
    end

    case n_max
    when 1
      winner = game[1].winner
    when 2
      # タイトルマッチ(敗者のみ二連勝)
      winner = if game[1].winner == 1 || game[2].winner == 1
                 # 第一局でplayer1が勝利 または 第二局で勝利
                 1
               elsif game[1].winner_id == game[2].winner_id && game[1].winner == 2
                 # 敗者復活戦勝ち上がり者が二連勝していたら
                 2
               else
                 0
      end
    when 3
      # タイトルマッチ(3回勝負)
      # どちらが多く勝ったのか？
      win_experience = [0, 0, 0]
      (1..3).each { |n| win_experience[game[n].winner.to_i] += 1 }
      winner = if win_experience[1] >= 2
                 1
               elsif win_experience[2] >= 2
                 2
               else
                 0
      end
    end
    { round: round, n_max: n_max, held: title_match, winner: winner }
  end

  def schedule_and_result_topic_create
    # 日程調整スレッド新規作成
    topic = Topic.create(title: "#{full_name} 日程調整", permission: 'everyone')
    topic.save
    topic.posts.build(name: "運営", comment: "#{full_name} の日程調整は ここでどうぞ。", owner_id: 0, roots: :progenitor).save
    update_columns(topic_schedule_id: topic.id)

    # 結果報告スレッド新規作成
    # topic = Topic.create(title: "棋戦", permission: "everyone")
    topic = Topic.create(title: "#{full_name} 対局結果", permission: 'everyone')
    topic.save
    topic.posts.build(name: "運営", comment: "#{full_name} の対局結果です。（なお結果報告は、対局表より行えます）", owner_id: 0, roots: :progenitor).save
    update_columns(topic_result_id: topic.id)
  end

  def schedule_and_result_topic_destroy
    Topic.find_by(id: topic_schedule_id)&.destroy
    Topic.find_by(id: topic_result_id)&.destroy
  end

  # 全参加者を返す
  def all_entries
    kisen.entries
  end

  # 参加者の中からタイトルホルダーの申し込みを返す
  def title_holder_entry
    title_holder_entry = kisen.entries.title_holder[0]
    update_columns(title_holder_entry: title_holder_entry&.user_id)
    title_holder_entry
  end

  # 参加者の中から一般参加者の申し込みを返す
  def general_participant_entries
    kisen.entries.general_participant
  end

  # メタプログラミング 動的にメソッドを定義
  # TODO: delegateすればよいのでは？
  # %w(name full_name sym_name title_name term open_date title_holder holding_method pretty_holding_method).each do |method|
  #   define_method method.to_s do
  #     kisen.send(method)
  #   end
  # end
end
