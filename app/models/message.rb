class Message < ApplicationRecord
  has_one :kifu, class_name: 'Kifu', foreign_key: :kisen_id
end
