class Parentage < ApplicationRecord
  belongs_to :parent, foreign_key: :parent_id, class_name: :Post
  belongs_to :child,  foreign_key: :child_id,  class_name: :Post
end
