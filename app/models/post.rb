class Post < ApplicationRecord
  after_initialize :set_default_roots, if: :new_record?

  include KifuHelper

  # before_save  :han_to_zen
  after_create :set_post_number # update時にはpost_numberはそのまま
  after_save   :kifu_create

  # 親はTopic
  belongs_to :topic, inverse_of: :posts
  validates_presence_of :topic
  validates :name,    presence: true
  validates :comment, length: { maximum: 30_000 }, allow_blank: false

  # 始祖、子孫？
  validates :roots, presence: true

  # 棋譜
  has_one :kifu, dependent:  :destroy

  # 画像添付のために
  mount_uploader :picture, PictureUploader
  validate :picture_size
  validate :contain_hiragana

  # 新しいものから順に表示
  default_scope -> { order(updated_at: :desc) }

  # 投稿に、親子関係を持たせる。（親の投稿に対する返信が子）
  # http://0x00.hateblo.jp/entry/2015/01/02/221420
  has_many :parents_association, foreign_key: :child_id, class_name: :Parentage, dependent: :delete_all
  has_many :parents, through: :parents_association, source: :parent
  has_many :children_association, foreign_key: :parent_id, class_name: :Parentage, dependent: :delete_all
  has_many :children, through: :children_association, source: :child
  scope :progenitor, -> { where(roots: :progenitor) } # 始祖（＝親を持たない投稿）
  scope :descendant, -> { where(roots: :descendant) } # 子孫（＝親へ返信された投稿）

  # 更新を許可するカラムを定義
  def self.updatable_attributes
    %w(id topic_id name comment created_at updated_at post_number owner_id)
  end

  def kifu?
    kifu_id.present?
  end

  # 子供がいないか、あるいは、全て自分の子ならtrue
  def all_my_children?
    children.blank? ||
      (children_owner_ids = children.pluck(:owner_id).uniq).count == 1 && children_owner_ids.first == owner_id
  end

  private

  def set_default_roots
    self.roots ||= 'progenitor'
  end

  def set_post_number
    number = Post.where('topic_id = ?', topic_id).maximum(:post_number) || 0
    update_columns(post_number: number + 1)
  end

  # アップロード画像のサイズを検証する
  def picture_size
    errors.add(:picture, "5MBより小さい画像をアップして下さい") if picture.size > 5.megabytes
  end

  # ひらがなを含むこと
  def contain_hiragana
    if comment.present? && comment !~ /[あ-ん]/
      errors.add(:comment, 'Invalid post!!')
    end
  end

  # 半角カタカナを全角に変換
  def han_to_zen
    require 'moji'
    self.comment.tr!("　", "🌃")
    self.comment = Moji.normalize_zen_han(self.comment.encode('utf-8'))
    self.comment.tr!("🌃", "　")
    # self.comment = Moji.han_to_zen(self.comment.encode('utf-8'))
  end

  # 棋譜部分があるようなら、コメントと棋譜を分離して、コメントと棋譜を作成・保存する
  # 棋譜がなければ、そのままコメントを保存する
  def kifu_create
    return if invalid?

    # コメントから棋譜部分の抽出
    article, raw_kifu, kifu_format = extract_kifu(comment)
    update_columns(comment: article)
    return unless kifu_format == :kif

    kifu, info, kifu_comment = kif_parser(raw_kifu)
    regexp = /(?<name>[a-zA-Z０-９\-\_\ \d\p{Hiragana}\p{Katakana}\p{Han}\p{Latin}]+)(?<rating>\(\d+\))*/

    # 棋譜の先頭部分(info情報)の解析
    player1 = if (player1 = (info[:先手：] || info[:下手：])).present?
                # WhiteWizard(1600)のように後ろにRがあれば、名前だけにする
                player1.match(regexp)[:name]
              else
                "名無しさん"
              end
    player2 = if (player2 = (info[:後手：] || info[:上手：])).present?
                player2.match(regexp)[:name]
              else
                "名無しさん"
              end
    teai    = if info[:手合割：].present?
                info[:手合割：].tr('：', '').tr('　', '')
              else
                '平手'
              end
    moves   = kifu.length - 1
    winner  = ((teai == '平手') && (moves.odd?)) ? 1 : 2
    kisen   = info[:棋戦：]&.tr('：', '') || '' # infoの記述が初期値

    # 棋戦名 トピックタイトルに棋戦名があるならinfo内の棋戦：を上書きする
    title = self.topic.title
    %w(名人戦 竜王戦 王位戦 王座戦 棋王戦 王将戦 棋聖戦 新人王戦 東西戦).each do |name|
      if title&.include?(name)
        kisen = name
        break
      end
    end
    date = DateTime.parse(info[:開始日時：].present? ? "#{info[:開始日時：]} JST" : kifu.created_at.to_s)
    # 棋譜の作成・保存
    kifu_ar = Kifu.create(post_id:  id,
                          kifu:     kifu,
                          info:     info,
                          comment:  kifu_comment,
                          raw_kifu: raw_kifu,
                          player1:  player1,
                          player2:  player2,
                          winner:   winner,
                          moves:    moves,
                          teai:     teai,
                          kisen:    kisen,
                          date:     date)
    # postの更新
    update_columns(kifu_id: kifu_ar.id)
  end
end
