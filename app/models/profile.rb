class Profile < ApplicationRecord
  belongs_to :user, inverse_of: :profile
  validates  :user_id, presence: true, allow_nil: false

  default_scope              -> { order(joined_date: :asc) }
  scope :active_member,      -> { where(condition: 'active') }
  scope :sleep_member,       -> { where(condition: 'sleep') }

  def next_number
    num = Profile.where("number not like 'J%'").pluck(:number).map(&:to_i).max + 1
    update_columns(number: num)
  end

  # メタプログラミング 動的にメソッドを定義
  %w(active sleep).each do |kind|
    define_method "#{kind}_member?" do
      condition == kind
    end
  end

  def types_of_members
    return '活動中会員' if active_member?
    return '休止中会員'
  end
end
