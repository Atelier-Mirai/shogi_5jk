class Ranking < ApplicationRecord
  default_scope -> { order(rank: :asc, game: :desc) }
  scope :ymd_between, lambda { |from, to|
    if from.present? && to.present?
      where(ymd: from..to)
    elsif from.present?
      where('ymd >= ?', from)
    elsif to.present?
      where('ymd <= ?', to)
    end
  }

  class << self
    # 月間成績三冠
    def monthly_crown(ymd)
      monthly = { ymd: ymd }
      %i(ave win game).each do |crown|
        ranking = Ranking.where(ymd: ymd).where("month_prize_#{crown} <= 3").select("name, month_#{crown}, month_prize_#{crown}")
        monthly[crown] = ranking.group_by(&:"month_prize_#{crown}")
      end
      return monthly
    end

    # 年間成績四冠
    def annually_crown(ymd)
      annually = { ymd: ymd }
      %i(ave win game rating_up).each do |crown|
        ranking = Ranking.where(ymd: ymd).where("year_prize_#{crown} <= 3").select("name, year_#{crown}, year_prize_#{crown}")
        annually[crown] = ranking.group_by(&:"year_prize_#{crown}")
      end
      return annually
    end

    def annually_count_crown(ymd)
      annually_count = { ymd: ymd }
      [:ave, :win, :game].each do |crown|
        start_date = if ymd.month == 1
                        Date.new(ymd.year - 1,  2,  1)
                      else
                        Date.new(ymd.year,      2,  1)
                      end
        data = Ranking.unscoped.ymd_between(start_date, ymd).where("month_prize_#{crown} = ?", 1).group('name').count("month_prize_#{crown}")
        # => {"malio1119"=>1, "WhiteWizard"=>2, "tatsu98"=>1, "yoshinory"=>1, "Stuki Jiro"=>1, "sunny-light"=>1}

        # safe_invert
        # p safe_invert({'a'=>1, 'b'=>1, 'c'=>3})
        #=> {1=>['a', 'b'], 3=>['c']}
        # 転載：Rubyレシピブック No.120
        result = Hash.new { |h, key| h[key] = [] }
        data.each { |key, value| result[value] << key }
        annually_count[crown] = result.sort.reverse.to_h
        # {1=>["malio1119", "tatsu98", "yoshinory", "Stuki Jiro", "sunny-light"], 2=>["WhiteWizard"]}
      end
      annually_count
      # {:ymd=>Fri, 01 Dec 2017, :ave=>{4=>["WhiteWizard"], 2=>["pika_pika707"], 1=>["malio1119", "saiiti", "taniyandayo", "tatsu98", "thegul"]}, :win=>{4=>["WhiteWizard"], 3=>["thegul"], 2=>["Stuki Jiro", "Tethys0925"]}, :game=>{3=>["WhiteWizard"], 2=>["Stuki Jiro", "Tethys0925", "mitter", "thegul"], 1=>["kitazawa kazumi"]}}
    end
  end

  # 先月のデータを返すメソッド
  def last_month_data
    Ranking.find_by(ymd: ymd.months_ago(1), user_id: user_id)
  end

  # １月ならば、前年の１月１日のデータを返す
  # なければ 前年の２月１日〜１２月１日までの間の最も昔のデータを返す
  # ２月〜１２月なら、今年の１月１日のデータを返す
  # なければ 今年の２月１日以降の最も昔のデータを返す
  def last_year_data
    if ymd.month == 1
      12.downto(1) do |n|
        r = Ranking.find_by(ymd: ymd.months_ago(n), user_id: user_id)
        return r until r.nil?
      end
    else
      (ymd.month - 1).downto(1) do |n|
        r = Ranking.find_by(ymd: ymd.months_ago(n), user_id: user_id)
        return r until r.nil?
      end
    end
    nil # downto の 繰返し回数が戻らないように
  end

  def rating_rank
    # 自分自身より上のレーティングの件数を数えて、＋１する
    # （一位が二人いたときに、次の人が三位になるようにするため）
    Ranking.where(ymd: ymd).where('rating > ?', rating).count + 1
  end

  def month_prize_rank(kind)
    # 月の対局数が２０局以上が、月間三冠の対象
    return 999 if month_game < 20

    # > で判定している自分自身超過のスコアの数を数えている。
    # 勝率の判定のために、小数点６位以下を切り上げている。（自分自身を含まないために）
    # 順位は１位から始まるので、＋１している。
    score = BigDecimal(send("month_#{kind}").to_s).ceil(6).to_f
    Ranking.where('ymd = ?', ymd).where('month_game >= ?', 20).where("month_#{kind} > ?", score).count + 1
  end

  # 年間四冠算出用。月間で１位になった回数を数える。
  # なので、where("rank = 1") に固定
  def year_prize_rank(kind)
    return 999 if rank == 999

    score = BigDecimal(send("year_#{kind}").to_s).ceil(6).to_f
    Ranking.where('ymd = ?', ymd).where('rank = 1').where("year_#{kind} > ?", score).count + 1
  end
end
