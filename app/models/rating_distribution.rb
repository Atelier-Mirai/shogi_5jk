class RatingDistribution < ApplicationRecord
  default_scope -> { order(updated_at: :desc) }

  serialize :data
  serialize :info
end
