class TitleHolder < ApplicationRecord
  serialize :winners
  serialize :members

  class << self
    def current
      [ {:name=>"名人戦", :title_holder=>"ko-tan" },
        {:name=>"竜王戦", :title_holder=>"z-air-6" },
        {:name=>"王座戦", :title_holder=>"kokolemon" },
        {:name=>"棋王戦", :title_holder=>"thegul" },
        {:name=>"王将戦", :title_holder=>"WhiteWizard" },
        {:name=>"棋聖戦", :title_holder=>"bururutti-2" } ]
    end

    # 現在のタイトル保持者を抽出する
    def title_holders
      results =
      # %w(名人戦 竜王戦 王位戦 王座戦 棋王戦 王将戦 棋聖戦 新人王戦).map do |name|
      %w(名人戦 竜王戦 王座戦 棋王戦 王将戦 棋聖戦).map do |name|
        kisen = Kisen.find_by(name: name)
        term = kisen.term
        term.downto(1).each do |t|
          kisen = Kisen.find_by(name: name, term: t)
          if kisen.title_holder != [nil, nil]
            term = t
            break
          end
        end
        { name:         name,
          title_holder: kisen.title_holder.first&.name,
          matchup_id:   kisen.matchup&.id }
      end

      # kisen = Kisen.find_by(name: '東西戦')
      # term = kisen.term
      # term.downto(1).each do |t|
      #   kisen = Kisen.find_by(name: '東西戦', term: t)
      #   if kisen.title_holder_id.present?
      #     term = t
      #     break
      #   end
      # end
      # results << { name: '東西戦',
      #              title_holder: "#{kisen.title_holder_id[:team_names].first}軍<br>#{kisen.title_holder_id[:member_names].first.join('<br>')}",
      #              matchup_id: kisen.matchup&.id }
    end

    # タイトルホルダーと準タイトルホルダーを返す
    def title_holder
      return [nil, nil] if title_holder_id.nil?
      title_holder_id.map { |id| User.find_by(id: id) }
    end
  end
end
