# Topic （板、スレッド）
class Topic < ApplicationRecord
  # before_save :han_to_zen
  has_many    :posts, dependent: :destroy, inverse_of: :topic

  # 子も同時にnew, create可能に。
  accepts_nested_attributes_for :posts, allow_destroy: true

  default_scope          -> { order(updated_at: :desc) }
  # permission は、administrator / committee / everyone の三種類がある
  scope :committee,      -> { where(permission: %i(committee everyone)) }
  scope :committee_only, -> { find_by(permission: %i(committee)) }
  scope :everyone,       -> { where(permission: :everyone) }

  validates :title, length: { maximum: 50 }, presence: true

  # 画像投稿時に「ファイルを選択」ボタンの#idを一意に設定する
  def uniq_post_picture_id
    count = Post.where(topic_id: id).maximum(:post_number) || 0
    "picture_#{id}_#{count}".to_sym
  end

  # 更新を許可するカラムを定義
  def self.updatable_attributes
    %w(id title created_at updated_at)
  end

  private

  # 半角を全角に変換
  def han_to_zen
    require 'moji'
    self.title = Moji.normalize_zen_han(self.title.encode('utf-8'))
  end
end
