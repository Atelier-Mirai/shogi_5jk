class User < ApplicationRecord
  has_secure_password
  attr_accessor :remember_token,
                :reset_token

  has_many :created_events, class_name: 'Event', foreign_key: :owner_id

  has_one :profile, dependent: :destroy, inverse_of: :user
  accepts_nested_attributes_for :profile, allow_destroy: true # 子も同時にnew
  has_many :created_topics, class_name: 'Topic', foreign_key: :owner_id
  has_many :created_posts,  class_name: 'Post',  foreign_key: :owner_id

  scope :activated_user,    -> { where(activated: true) }  # メール確認済
  scope :approved_user,     -> { where(approved:  true) }  # 入会承認済
  scope :provisional_user,  -> { where(approved:  false) } # 承認待ち
  scope :privileged_user,   -> { where(role: :admin) }
  scope :unprivileged_user, -> { where(role: %i(committee member)) }
  scope :search_by_name,    ->(name) { where('name like ?', "%#{name}%") }


  # for BBS
  def topic_owner?(topic)
    id == topic.owner_id
  end

  def post_owner?(post)
    id == post.owner_id
  end

  def all_posts_owner?(topic)
    Post.where(topic_id: topic.id).count ==
      Post.where(topic_id: topic.id, owner_id: id).count
  end

  # 利用者種別
  def admin?
    role == 'admin'
  end

  def committee?
    role.in? %w(committee admin)
  end

  def member?
    role == 'member'
  end

  def guest?
    role == 'guest'
  end
end
