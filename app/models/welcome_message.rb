class WelcomeMessage < ApplicationRecord
  self.inheritance_column = nil

  scope :approbation, -> { find_by(type: :approbation) }  # 入会承認
  scope :rejection,   -> { find_by(type: :rejection) }    # 入会却下
end
