class FormPresenter
  include HtmlBuilder
  include Avatar
  include SessionsHelper

  attr_reader :form_builder, :view_context
  delegate :label, :text_field, :email_field, :password_field, :check_box, :radio_button, :text_area, :object, to: :form_builder
  delegate :raw, :link_to, :image_tag, to: :view_context

  def initialize(form_builder, view_context)
    @form_builder = form_builder
    @view_context = view_context
  end

  def upload_file_block(uploaded_file_name: nil, accept: :'kifu-or-image', name: 'upload_file')
    markup(:div, class: 'input-group mb-3') do |m|
      m.div(class: 'custom-file') do
        format = case accept.to_sym
                 # when :pdf
                 #   'application/pdf'
                 when :image
                   'image/jpeg, image/gif, image/png'
                 when :'kifu-or-image'
                   '*'
                 # when :excel
                 #   'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                 end
        m.input(type: 'file', name: name, class: 'custom-file-input', accept: format)
        m.label(class: "custom-file-label custom-file-label-#{accept} text-left", title: uploaded_file_name) do
          m.text uploaded_file_name
        end
      end
    end
  end

  def poster_block
    current_user = view_context.current_user
    markup(:div, class: 'form-inline') do |m|
      unless current_user
        m << avatar_link_to(current_user, avatar_only: true)
        m << text_field(:name, placeholder: 'ハンドルネーム', skip_label: true)
      end
    end
  end

  def submit_button(button_text = '投稿する')
    markup do |m|
      m.input(type: 'submit', value: button_text, class: 'ui green button')
    end
  end

  def comment_field_block(name = 'post[comment]')
    markup do |m|
      m.textarea(name: name, placeholder: 'コメントをどうぞ＼(^_^)／', class: 'form-control')
    end
  end

  def text_field_block(name, label_text, options = {})
    markup do |m|
      # m << label(name, label_text)
      m << text_field(name, options)
      # m << error_messages_for(name)
    end
  end

  def password_field_block(name, label_text, options = {})
    markup(:div, class: 'input-block') do |m|
      m << decorated_label(name, label_text, options)
      m << password_field(name, options)
      m << error_messages_for(name)
    end
  end

  def date_field_block(name, label_text, options = {})
    markup(:div, class: 'input-block') do |m|
      m << decorated_label(name, label_text, options)
      if options[:class].kind_of?(String)
        classes = options[:class].strip.split + ['datepicker']
        options[:class] = classes.uniq.join(' ')
      else
        options[:class] = 'datepicker'
      end
      m << text_field(name, options)
      m << error_messages_for(name)
    end
  end

  def drop_down_list_block(name, label_text, choices, options = {})
    markup(:div, class: 'input-block') do |m|
      m << decorated_label(name, label_text, options)
      m << form_builder.select(name, choices, { include_blank: true }, options)
      m << error_messages_for(name)
    end
  end

  def error_messages_for(name)
    markup do |m|
      object.errors.full_messages_for(name).each do |message|
        m.div(class: 'error-message') do |m|
          m.text message
        end
      end
    end
  end

  def update_button(text: '更新', size: :small, color: :color)
    markup do |m|
      m.input(type: 'submit', value: text, class: "ui #{size} #{color} button")
    end
  end

  def append_button(text: '追加', size: :sm, color: :primary)
    markup do |m|
      m.input(type: 'submit', value: text, class: "ui #{size} #{color} button")
    end
  end

  def link_field_block
    link = @form_builder.object
    markup(:div, class: 'form-group') do |m|
      m.div(class: 'col-12 form-inline') do
        m.div(class: 'col-4') do
          m.input(type: 'text', name: 'link[text]', class: 'form-control', value: link.text, placeholder: 'リンク名', style: 'width: 100%; margin: 5px 0;')
          m.input(type: 'text', name: 'link[url]', class: 'form-control', value: link.url, placeholder: 'URL', style: 'width: 100%; margin: 5px 0;')
        end
        m.div(class: 'col-7') do
          m.div(class: 'col-12') do
            m.textarea(name: 'link[comment]', class: 'form-control', style: 'width: 100%; height: 100%;', rows: 3, placeholder: 'リンク先の紹介文') do
              m << link.comment
            end
          end
        end
        m.div(class: 'col-1') do
          m << update_button
          m << '<br>'
          m << '<br>'
          m << delete_link_button(link)
        end
      end
    end
  end

  def append_link_field_block
    link = @form_builder.object
    f = @form_builder
    markup(:div, class: 'form-group') do |m|
      m << f.hidden_field(:type, value: link.type)
      m.div(class: 'col-12 form-inline') do
        m.div(class: 'col-4') do
          m.input(type: 'text', name: 'link[text]', class: 'form-control', placeholder: 'リンク名', style: 'width: 100%; margin: 5px 0;')
          m.input(type: 'text', name: 'link[url]', class: 'form-control', placeholder: 'URL', style: 'width: 100%; margin: 5px 0;')
        end
        m.div(class: 'col-7') do
          m.div(class: 'col-12') do
            m.textarea(name: 'link[comment]', class: 'form-control', style: 'width: 100%; height: 100%;', rows: 3, placeholder: 'リンク先の紹介文')
          end
        end
        m.div(class: 'col-1') do
          m << append_button(text: '追加')
        end
      end
    end
  end

  # Link type: member_site / shogi_site / web_creator の編輯に用いる
  def edit_button(type, size: :tiny, color: :yellow)
    f = @form_builder
    markup(:div, class: 'mt-3 text-right') do |m|
      # m.input(type: 'hidden', name: 'type', value: 'member')
      m << f.hidden_field(:type, value: type)
      m.input(type: 'submit', value: '編輯', class: "ui #{size} #{color} button")
    end
  end

  def delete_button(link, size: :tiny, color: :color)
    view_context.link_to("削除", view_context.link_path(link), method: :delete, data: { confirm: '本当に削除しますか' }, class: "ui #{size} #{color} button")
  end

  private

  def decorated_label(name, label_text, options = {})
    label(name, label_text, class: options[:required] ? :required : nil)
  end
end
