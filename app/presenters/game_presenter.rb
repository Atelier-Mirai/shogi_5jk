class GamePresenter < ModelPresenter
  delegate :id,
           :matchup_id,
           :game_number,
           :player1_id,
           :player2_id,
           :date,
           :winner_id,
           :reason,
           :point1,
           :point2,
           :mark1,
           :mark2,
           :uwate_id,
           :teai,
           :kifu_id,
           :seed1,
           :seed2,
           :matchup,
           :game_name,
           to: :object
  delegate :matchup_path, to: :view_context

  # @object には @topicが渡されている
  def header(enough_authority = false, contribution_button = false)
    markup(:h3) do |m|
      # スレッドタイトルの表示
      m << "[#{id}] "
      m << decorated_title

      # 投稿ボタンの表示
      m << link_to('投稿', "#collapse_form_#{id}", class: 'btn btn-success', role: 'button', 'data-toggle' => 'collapse') if contribution_button

      # 編輯、削除ボタンの表示
      if enough_authority
        m << edit_link_button
        m << delete_link_button
      end
    end
  end

  def decorated_title(contribution_count = false)
    count = contribution_count ? "(#{@object.posts.count})" : nil
    if updated_at > 7.day.ago
      link_to "#{title}#{count} #{image_tag('new-1.gif')}".html_safe, view_context.topic_path(id)
    else
      link_to "#{title}#{count}", view_context.topic_path(id)
    end
  end

  # current_user != nil ならトップページで、今後の対局 表示用
  # current_user == nil なら対局日程 一覧用
  def table_row(current_user = nil)
    markup(:tr) do |m|
      m.td(class: 'text-center') { m << kisen_name }
      if current_user.present?
        m.td { m << avatar_link_to(opponent(current_user), size: 20) }
      else
        m.td { m << avatar_link_to(player1, size: 20) }
        m.td { m << avatar_link_to(player2, size: 20) }
      end
      m.td { m << gengo(date, :date_short) }
      m.td { m << game_name }
    end
  end

  # 棋戦名
  def kisen_name
    link_to matchup.kisen.name, matchup_path(matchup)
  end

  # 対局相手
  def opponent(current_user)
    opponent = current_user.id == player1_id.to_i ? player2_id.to_i : player1_id.to_i
    User.find_by(id: opponent) || GuestUser.new('対局者未定')
  end

  # 対局相手
  def player1
    User.find_by(id: player1_id.to_i) || GuestUser.new('対局者未定')
  end

  def player2
    User.find_by(id: player2_id.to_i) || GuestUser.new('対局者未定')
  end
end
