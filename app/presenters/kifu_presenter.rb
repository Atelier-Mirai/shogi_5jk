class KifuPresenter < ModelPresenter
  delegate :id,
           :post_id,
           :kifu,
           :info,
           :comment,
           :raw_kifu,
           :created_at,
           :updated_at,
           :player1,
           :player2,
           :sengata1,
           :sengata2,
           :winner,
           :moves,
           :teai,
           :kisen,
           :date,
           :sfen,
           :post,
           to: :object
  delegate :kifu_path,
           :download_kifu_path,
           to: :view_context

  def table_row
    kifu        = @object
    sente = User.find_by(name: player1) || GuestUser.new(player1)
    gote  = User.find_by(name: player2) || GuestUser.new(player2)

    markup(:tr) do |m|
      m.td class: 'text-center' do
        m << gengo(date, :date2)
        m << '<br>'.html_safe
        m << kisen
        m << shogiban_link_button
      end
      m.td class: winner == 1 ? 'gold' : '' do
        m << avatar_link_to(sente, size: 20)
      end
      m.td class: winner == 2 ? 'gold' : '' do
        m << avatar_link_to(gote, size: 20)
      end
      m.td class: 'text-center' do |m|
        m << moves.to_s
        m << '<br>'.html_safe
        m << teai.to_s[0..1] if teai != '平手'
      end
      m.td post&.comment
    end
  end

  def kifu_info
    hash = { 対局ID：:   'taikyokuid',
             表題：:     'hyodai',
             # 棋戦：:     'kisen',
             戦型：:     'sengata',
             # 手合割：:   'teai',
             場所：:     'basho',
             開始日時：: 'kaishi',
             終了日時：: 'shuryo',
             持ち時間：: 'mochijikan',
             消費時間：: 'shohijikan' }
    markup do |m|
      m.span class: 'kisen' do |m|
         m << "棋戦：#{kisen}"
       end
      m << '<br>'
      m.span class: 'teai' do |m|
        m << "手合割：#{teai}"
      end
      m << '<br>'

      hash.each do |key, value|
        if info[key]
          m.span class: value do
            m.text "#{key}#{info[key]}"
          end
          m << '<br>'
        end
      end
    end
  end

  def winner_mark
    winner == 1 ? '▲' : '△'
  end

  def loser_mark
    winner == 1 ? '△' : '▲'
  end

  def shogiban_link_button
    link_to "将棋盤", kifu_path(id), class: "ui button"
  end

  def kifu_download_button
    link_to "棋譜保存", download_kifu_path(id), class: "ui basic brown button download_kifu"
  end
end
