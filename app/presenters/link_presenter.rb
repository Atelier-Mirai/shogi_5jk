class LinkPresenter < ModelPresenter
  include MarkdownHelper
  delegate :url, :text, :comment, to: :object

  def table_row
    link    = @object
    markup(:tr) do |m|
      m.td do
        m << view_context.link_to(text, url)
      end
      m.td do
        m << markdown(comment)
      end
    end
  end
end
