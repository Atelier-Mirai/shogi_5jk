class MessagePresenter < ModelPresenter
  include MarkdownHelper
  delegate :title, :comment, to: :object

  def carousel_slide(enough_authority: false)
    carousel = @object

    markup(:div, class: 'ui segment carousel') do |m|
      m.div(class: 'ui text container') do
        m.div(class: 'ui center aligned header') do
          if carousel.blank? && enough_authority
            m << new_carousel_button
          end
          m << markdown(carousel&.title)
        end

        m.div(class: 'ui center aligned') do
          if enough_authority
            m << new_carousel_button
            m << edit_carousel_button(carousel)
            m << delete_carousel_button(carousel)
          end
          m << markdown(carousel&.comment)
        end
      end
    end
  end

  def new_carousel_button
    link_to "新規", view_context.new_carousel_path, class: 'ui primary button'
  end

  def edit_carousel_button(carousel)
    link_to "編輯", [:edit, carousel], class: 'ui yellow button'
  end

  def delete_carousel_button(carousel)
    link_to "削除", carousel, 'data-confirm': '本当に削除しますか', rel: 'nofollow', 'data-method': :delete, class: 'ui negative button'
  end

  def card_body(enough_authority: false)
    kisen = @object
    markup(:div, class: 'd-flex p2') do |m|
      m.div(class: 'card') do
        m.div(class: 'card-body') do
          m.h3(class: 'card-title text-info') do
            m << markdown(kisen.message&.title)
            m << edit_card_button if enough_authority
          end
          m.h4 do
            m << view_context.link_to("募集要項", view_context.kisen_path(kisen.id)) + " "
            if kisen&.matchup
              m << view_context.link_to("対局表", view_context.matchup_path(kisen.matchup)) + " "
            end
            m << image_tag('new-1.gif', alt: 'new')
          end
          m.h5 do
            method = kisen.holding_method
            m << "大阪道場Ｔ室／#{method[:time]}／#{method[:teai]}／22時-"
          end
          m << markdown(kisen.message&.comment)
        end
      end
    end
  end

  def edit_card_button
    link_to "編輯", [:edit, @object, :card], class: 'ui yellow button'
  end

  def info_body(enough_authority: false)
    info = @object
    markup(:div) do |m|
      m.h3(class: 'ui left aligned header') do
        m << markdown(info&.title)
        m << edit_info_button if enough_authority
      end
      m.p do
        m << markdown(info&.comment)
      end
    end
  end

  def edit_info_button
    link_to "編輯", [:edit, @object], class: 'ui yellow button'
  end
end
