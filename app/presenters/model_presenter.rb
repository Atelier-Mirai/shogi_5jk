# https://www.oiax.jp/rails/tips/decorators_and_presenters.html
class ModelPresenter
  include HtmlBuilder
  include ApplicationHelper
  include MarkdownHelper
  include Avatar

  attr_reader :object, :view_context
  delegate :created_at, :updated_at, to: :object
  delegate :raw, :link_to, :image_tag, :sanitize, :label, to: :view_context

  def initialize(object, view_context)
    @object       = object
    @view_context = view_context
  end

  # build メソッドの定義
  class << self
    def build(objects, view_context)
      objects&.each { |object| yield new(object, view_context) }
    end
  end

  def index_button(text: nil, size: :tiny)
    link_to text || "一覧", { controller: @object.class.name.pluralize.downcase, action: :index }, class: "ui #{size} blue button"
  end

  def new_button(text: nil, size: :tiny)
    link_to text || "作成", { controller: @object.class.name.pluralize.downcase, action: :new }, class: "ui #{size} green button"
  end

  def edit_button(text: nil, size: :tiny)
    link_to text || "編輯", [:edit, @object], class: "ui #{size} yellow button"
  end

  def delete_button(text: nil, size: :tiny)
    link_to text || "削除", @object, 'data-confirm': '本当に削除しますか', rel: 'nofollow', 'data-method': :delete, class: "ui #{size} red button"
  end

  def logged_in_user?
    view_context.current_user.present?
  end

  def authorized?
    current_user = view_context.current_user
    current_user.present? && current_user.admin?
  end
end
