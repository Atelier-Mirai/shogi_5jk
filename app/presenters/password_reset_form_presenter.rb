class PasswordResetFormPresenter < FormPresenter
  def email_field_block
    markup(:div, class: 'row user_information') do |m|
      m.div(class: 'field') do
        m << email_field(:email, placeholder: '登録済みのメールアドレス')
      end
    end
  end

  def password_field_block
    markup(:div, class: 'row user_information') do |m|
      m.div(class: 'col-12 form-inline') do
        m << password_field(:password, placeholder: '新しいパスワード')
      end
      m.div(class: 'col-12 form-inline') do
        m << password_field(:password_confirmation, placeholder: 'タイプミス防止の為、再入力')
      end
    end
  end
end
