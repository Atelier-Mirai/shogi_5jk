class PostPresenter < ModelPresenter
  delegate :id, :topic_id, :name, :comment, :post_number, :owner_id, :picture, :kifu_id, :roots, to: :object

  def header
    markup(:header, class: 'header') do |m|
      m.h3 do |m|
        # 投稿連番
        m.span "[#{post_number}]", class: 'number'

        # 投稿者名
        user = User.find_by(id: owner_id) || GuestUser.new(name)
        m << avatar_link_to(user, size: 36)

        # 投稿日時
        m << markup(:div, class: 'time') do |m|
          m.time datetime: updated_at do |m| m << gengo(updated_at, :date_and_time) end
          m << "#{image_tag('new-1.gif')} ".html_safe if updated_at > 7.day.ago
        end

        # 返信ボタン
        if logged_in_user?
          m << markup(:div, class: 'buttons') do |m|
            m.div "返信", class: "ui tiny teal reply button", 'data-post-reply': self.id
            # 編輯、削除ボタンの表示
            if authorized?
              m << edit_button
              m << delete_button
            end
          end
        end
      end
    end
  end

  def edit_button
    link_to "編輯", [:edit, Topic.find(@object.topic_id), @object], class: 'ui tiny yellow button'
  end

  def delete_button
    link_to "削除", [Topic.find(@object.topic_id), @object], 'data-confirm': 'この投稿を本当に削除しますか', rel: 'nofollow', 'data-method': :delete, class: 'ui tiny red button'
  end

  # ModelPresenterを上書き
  def authorized?
    current_user = view_context.current_user
    current_user.present? && (current_user.admin? || current_user.id == owner_id)
  end
end
