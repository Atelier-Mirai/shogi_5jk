class ProfilePresenter < ModelPresenter
  delegate :id,
           :proud_tactics,
           :favorite_kishi,
           :appeal,
           :number,
           :joined_date,
           :joined_dankyu,
           :joined_rating,
           :condition,
           :types_of_members,
           to: :object

  def show_user_profile
    markup(:div, class: 'ui stackable selectable striped sortable table') do |m|
      m.tr(class: '') do
        m << "会員番号"
      end
      m.div(class: 'col-10') do
        m << number
      end
      m.div(class: 'col-2 text-center') do
        m << label('', '入会日', class: 'control-label')
      end
      m.div(class: 'col-10') do
        m << gengo(joined_date, :date2)
      end
      m.div(class: 'col-2 text-center') do
        m << label('', '入会時段級位', class: 'control-label')
      end
      m.div(class: 'col-10') do
        m << rating2dankyu(joined_rating)
      end
      m.div(class: 'col-2 text-center') do
        m << label('', '入会時Ｒ', class: 'control-label')
      end
      m.div(class: 'col-10') do
        m << joined_rating.to_s
      end
      m.div(class: 'col-2 text-center') do
        m << label('', '得意な戦法', class: 'control-label')
      end
      m.div(class: 'col-10') do
        m << markdown(proud_tactics)
      end
      m.div(class: 'col-2 text-center') do
        m << label('', '好きな棋士', class: 'control-label')
      end
      m.div(class: 'col-10') do
        m << markdown(favorite_kishi)
      end
      m.div(class: 'col-2 text-center') do
        m << label('', '一言ＰＲ', class: 'control-label')
      end
      m.div(class: 'col-10') do
        m << markdown(appeal)
      end
      m.div(class: 'col-2 text-center') do
        m << label('', '活動区分', class: 'control-label')
      end
      m.div(class: 'col-10') do
        m << types_of_members
      end

      if authorized?
        m << edit_user_profile_button
      end
    end
  end

  def edit_user_profile_button
    link_to "編輯", [:edit, :user, @object], class: 'ui yellow tiny button'
  end

  def authorized?
    current_user = view_context.current_user
    current_user.present? && (current_user.admin? || current_user.id == id)
  end
end
