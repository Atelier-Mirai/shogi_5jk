class RankingPresenter < ModelPresenter
  include ApplicationHelper

  delegate :id,
           :ymd,
           :rank,
           :user_id,
           :name,
           :rating,
           :ave,
           :win,
           :lose,
           :game,
           :best_r,
           :month_ave,
           :month_win,
           :month_lose,
           :month_game,
           :month_rating_up,
           :month_prize_ave,
           :month_prize_win,
           :month_prize_game,
           :year_ave,
           :year_win,
           :year_lose,
           :year_game,
           :year_rating_up,
           :year_prize_ave,
           :year_prize_win,
           :year_prize_game,
           :year_prize_rating_up,
           :superior_rank,
           :percentage,
           :standard_score,
           to: :object

  def table_row
    ranking = @object
    user    = User.find_by(id: ranking.user_id)
    return unless user

    profile = user.profile
    prize   = if ranking.month_prize_ave.between?(1, 3) ||
                 ranking.month_prize_win.between?(1, 3) ||
                 ranking.month_prize_game.between?(1, 3)
                'prize'
              else
                'non_prize'
              end

    markup(:tr, class: "#{prize}") do |m|
      # 基本情報
      m.td { m.div rank, class: 'text-right' }
      m.td { m.div profile.number, class: 'text-right' }
      m.td { m << avatar_link_to(user, size: 20) }
      m.td { m.div profile.joined_dankyu, class: 'text-center' }
      m.td { m.div rating, class: 'text-right' }
      m.td { m.div rating2dankyu(rating), class: 'text-center' }
      m.td(class: 'vl text-right') { m << pretty_format(ave) }
      m.td { m.div pretty_format(win), class: 'text-right' }
      m.td { m.div pretty_format(game), class: 'text-right' }
      m.td { m.div best_r, class: 'text-right' }

      # 月間
      h = { 1 => 'gold', 2 => 'silver', 3 => 'bronze' }
      prize = h[month_prize_ave]
      m.td class: 'vl' do
        m.div class: "text-right #{prize}" do
          m << pretty_format(month_ave)
        end
      end
      prize = h[month_prize_win]
      m.td { m.div pretty_format(month_win), class: "text-right #{prize}" }
      prize = h[month_prize_game]
      m.td { m.div pretty_format(month_game), class: "text-right #{prize}" }
      m.td { m.div month_rating_up, class: "text-right" }

      # 年間
      prize = h[year_prize_ave]
      m.td class: 'vl' do
        m.div class: "text-right #{prize}" do
          m << pretty_format(year_ave)
        end
      end
      prize = h[year_prize_win]
      m.td { m.div pretty_format(year_win), class: "text-right #{prize}" }
      prize = h[year_prize_game]
      m.td { m.div pretty_format(year_game), class: "text-right #{prize}" }
      prize = h[year_prize_rating_up]
      m.td { m.div year_rating_up, class: "text-right #{prize}" }

      if superior_rank.present?
        # 偏差値等
        m.td(class: 'vl text-right') { m.div pretty_format(superior_rank),  class: "text-right non_prize" }
        m.td { m.div pretty_format(percentage),     class: "text-right non_prize" }
        m.td { m.div pretty_format(standard_score), class: "text-right non_prize" }
      end
    end
  end

  def period
    monthly_ranker = @object
    ymd            = monthly_ranker[:ymd]
    markup do |m|
      m.span(class: 'small') do
        m << " #{ymd.month.to_kan}月期 "
      end
      m.span(class: 'small text-right text-secondary', style: 'font-size: small;') do
        m << gengo(ymd.months_ago(1), :date)
        m << ' 〜 '
        m << gengo(ymd.months_ago(1).end_of_month, :date3)
      end
    end
  end

  def annually_period
    annually_ranker = @object
    ymd            = annually_ranker[:ymd]
    markup do |m|
      # m.span(class: 'small') do
      #   m << " #{ymd.month.to_kan}月期 "
      # end
      m.span(class: 'small text-right text-secondary', style: 'font-size: small;') do
        m << " " + gengo(ymd.years_ago(1), :date)
        m << ' 〜 '
        # m << gengo(ymd.months_ago(1).end_of_month, :date)
        m << "令和元年12月31日(火)"
      end
    end
  end



  def monthly_crown(display_period: true)
    monthly = @object
    # monthly
    # {:ymd=>Sat, 01 Jul 2017, :ave=>{1=>[#<Ranking id: nil, name: "thegul", month_ave: 0.558441558441558, month_prize_ave: 1>], 3=>[#<Ranking id: nil, name: "WhiteWizard", month_ave: 0.506944444444444, month_prize_ave: 3>], 2=>[#<Ranking id: nil, name: "bururutti-2", month_ave: 0.555555555555556, month_prize_ave: 2>]}, :win=>{3=>[#<Ranking id: nil, name: "thegul", month_win: 43, month_prize_win: 3>], 1=>[#<Ranking id: nil, name: "WhiteWizard", month_win: 73, month_prize_win: 1>], 2=>[#<Ranking id: nil, name: "kitazawa kazumi", month_win: 61, month_prize_win: 2>]}, :game=>{3=>[#<Ranking id: nil, name: "thegul", month_game: 77, month_prize_game: 3>], 1=>[#<Ranking id: nil, name: "WhiteWizard", month_game: 144, month_prize_game: 1>, #<Ranking id: nil, name: "kitazawa kazumi", month_game: 144, month_prize_game: 1>]}}
    crown_title = { ave: "勝 率 王", win: "勝ち星王", game: "対局数王" }
    markup do |m|
      [:ave, :win, :game].each do |crown|
        m.tr do
          if display_period && crown == :ave
            m.td(class: 'text-center bl', rowspan: "3") do
              m << "#{gengo(monthly[:ymd].months_ago(1), :date_short)}<br>〜<br>#{gengo(monthly[:ymd].days_ago(1), :date_short)}"
            end
          end
          m.th(class: "text-center #{'bl' if crown == :ave}") { m << crown_title[crown] }
          m.td(class: "#{'bl' if crown == :ave}") { m << monthly[crown][1]&.map { |ranker| ranker[:name] }&.join('<br>') }
          m.td(class: "text-right pr-4 #{'bl' if crown == :ave}") do
            m.text pretty_format(monthly[crown][1]&.map { |ranker| ranker["month_#{crown}".to_sym] }&.first)
          end
          m.td(class: "#{'bl' if crown == :ave}") { m << monthly[crown][2]&.map { |ranker| ranker[:name] }&.join('<br>') }
          m.td(class: "text-right pr-4 #{'bl' if crown == :ave}") do
            m.text pretty_format(monthly[crown][2]&.map { |ranker| ranker["month_#{crown}".to_sym] }&.first)
          end
          m.td(class: "#{'bl' if crown == :ave}") { m << monthly[crown][3]&.map { |ranker| ranker[:name] }&.join('<br>') }
          m.td(class: "text-right pr-4 #{'bl' if crown == :ave}") do
            m.text pretty_format(monthly[crown][3]&.map { |ranker| ranker["month_#{crown}".to_sym] }&.first)
          end
        end
      end
    end
  end

  def annually_crown(display_period: true)
    annually = @object
    crown_title = { ave: "勝 率 王", win: "勝ち星王", game: "対局数王", rating_up:  "Ｒ上昇王" }
    markup do |m|
      %i(ave win game rating_up).each do |crown|
        m.tr do
          if display_period && crown == :ave
            m.td(class: 'text-center bl', rowspan: "4") do
              new_years_day = annually[:ymd].beginning_of_year
              new_years_day = new_years_day.prev_year if annually[:ymd].month == 1
              m << "#{gengo(new_years_day, :date_short)}<br>〜<br>#{gengo(annually[:ymd].days_ago(1), :date_short)}"
            end
          end

          m.th(class: "text-center #{'bl' if crown == :ave}") do
            m << crown_title[crown]
          end
          m.td(class: "#{'bl' if crown == :ave}") do
            m << annually[crown][1]&.map { |ranker| ranker[:name] }&.join('<br>')
          end
          m.td(class: "text-right pr-4 #{'bl' if crown == :ave}") do
            m.text pretty_format(annually[crown][1]&.map { |ranker| ranker["year_#{crown}".to_sym] }&.first)
          end
          m.td(class: "#{'bl' if crown == :ave}") do
            m << annually[crown][2]&.map { |ranker| ranker[:name] }&.join('<br>')
          end
          m.td(class: "text-right pr-4 #{'bl' if crown == :ave}") do
            m.text pretty_format(annually[crown][2]&.map { |ranker| ranker["year_#{crown}".to_sym] }&.first)
          end
          m.td(class: "#{'bl' if crown == :ave}") do
            m << annually[crown][3]&.map { |ranker| ranker[:name] }&.join('<br>')
          end
          m.td(class: "text-right pr-4 #{'bl' if crown == :ave}") do
            m.text pretty_format(annually[crown][3]&.map { |ranker| ranker["year_#{crown}".to_sym] }&.first)
          end
        end
      end
    end
  end

  def annually_count_crown
    annually = @object
    # {:ymd=>Fri, 01 Dec 2017, :ave=>{4=>["WhiteWizard"], 2=>["pika_pika707"], 1=>["malio1119", "saiiti", "taniyandayo", "tatsu98", "thegul"]}, :win=>{4=>["WhiteWizard"], 3=>["thegul"], 2=>["Stuki Jiro", "Tethys0925"]}, :game=>{3=>["WhiteWizard"], 2=>["Stuki Jiro", "Tethys0925", "mitter", "thegul"], 1=>["kitazawa kazumi"]}}
    crown_title = { ave: "勝 率 王", win: "勝ち星王", game: "対局数王" }
    markup do |m|
      %i(ave win game).each do |crown|
        m.tr do
          if crown == :ave
            m.td(class: 'text-center bl', rowspan: "3") do
              m << "平成 #{annually[:ymd].year - 1988} 年"
            end
          end
          m.th(class: "text-center vm #{'bl' if crown == :ave}") do
            m << crown_title[crown]
          end
          annually[crown].each do |key, value|
            m.td(class: "#{'bl' if crown == :ave}") do
              m << value.join('<br>')
            end
            m.td(class: "#{'bl' if crown == :ave}") do
              m << "#{key} 回"
            end
          end
        end
      end
    end
  end
end
