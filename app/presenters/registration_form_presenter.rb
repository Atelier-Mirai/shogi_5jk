class RegistrationFormPresenter < FormPresenter

  PREFECTURES = [[:北海道, :hokkaido],
                 [:青森, :aomori],
                 [:岩手, :iwate],
                 [:宮城, :miyagi],
                 [:秋田, :akita],
                 [:山形, :yamagata],
                 [:福島, :fukushima],
                 [:茨城, :ibaraki],
                 [:栃木, :tochigi],
                 [:群馬, :gunma],
                 [:埼玉, :saitama],
                 [:千葉, :chiba],
                 [:東京, :tokyo],
                 [:神奈川, :kanagawa],
                 [:新潟, :niigata],
                 [:富山, :toyama],
                 [:石川, :ishikawa],
                 [:福井, :fukui],
                 [:山梨, :yamanashi],
                 [:長野, :nagano],
                 [:岐阜, :gifu],
                 [:静岡, :shizuoka],
                 [:愛知, :aichi],
                 [:三重, :mie],
                 [:滋賀, :shiga],
                 [:京都, :kyoto],
                 [:大阪, :osaka],
                 [:兵庫, :hyogo],
                 [:奈良, :nara],
                 [:和歌山, :wakayama],
                 [:鳥取, :tottori],
                 [:島根, :shimane],
                 [:岡山, :okayama],
                 [:広島, :hiroshima],
                 [:山口, :yamaguchi],
                 [:徳島, :tokushima],
                 [:香川, :kagawa],
                 [:愛媛, :ehime],
                 [:高知, :kochi],
                 [:福岡, :fukuoka],
                 [:佐賀, :saga],
                 [:長崎, :nagasaki],
                 [:熊本, :kumamoto],
                 [:大分, :oita],
                 [:宮崎, :miyazaki],
                 [:鹿児島, :kagoshima],
                 [:沖縄, :okinawa],
                 [:海外, :oversea]].freeze

  def user_information_block
    markup(:div, class: 'user_information') do |m|
      m.div(class: 'col-12 form-inline') do
        # instance_variable_get レシーバが持っているインスタンス変数の値を取得するメソッド
        user = view_context.instance_variable_get(:@user)

        m << label('name', class: 'col-sm-2 col-form-label')
        m.div(class: 'col-sm-10') do
          m << text_field(:name, placeholder: '81道場に登録のハンドルネーム', class: "form-control")
        end
      end

      m.div(class: 'col-12 form-inline') do
        m << label('email', class: 'col-sm-2 col-form-label')
        m.div(class: 'col-sm-10') do
          m << email_field(:email, readonly: true, class: 'form-control-plaintext')
        end
      end

      m.div(class: 'col-12 form-inline') do
        m << label('password', class: 'col-sm-2 col-form-label')
        m.div(class: 'col-sm-10') do
          m << password_field(:password, placeholder: '当会の活動で用いるパスワード', class: 'form-control')
        end
      end

      m.div(class: 'col-12 form-inline') do
        m << label('password_confirmation', class: 'col-sm-2 col-form-label')
        m.div(class: 'col-sm-10') do
          m << password_field(:password_confirmation, placeholder: 'タイプミス防止の為、再入力', class: 'form-control')
        end
      end
    end
  end

  def profile_information_block
    markup(:div, class: 'profile_information') do |m|
      m.div(class: 'col-12 form-inline') do
        m << label('joined_date', class: 'col-sm-2 col-form-label')
        m.div(class: 'col-sm-10') do
          m << text_field(:joined_date, readonly: true, class: "datepicker form-control-plaintext", value: Date.today.strftime("%Y-%m-%d"))
        end
      end

      m.div(class: 'col-12 form-inline') do
        m << label('joined_dankyu', class: 'col-sm-2 col-form-label')
        m.div(class: 'col-sm-10') do
          m << text_field(:joined_dankyu, readonly: true, class: 'form-control-plaintext')
        end
      end

      m.div(class: 'col-12 form-inline') do
        m << label('joined_rating', class: 'col-sm-2 col-form-label')
        m.div(class: 'col-sm-10') do
          m << text_field(:joined_rating, placeholder: '現時点でのR', class: "form-control")
        end
      end

      m.div(class: 'col-12 form-inline') do
        m << label('proud_tactics', class: 'col-sm-2 col-form-label')
        m.div(class: 'col-sm-10') do
          m << text_area(:proud_tactics, class: 'form-control', placeholder: "居飛車、振り飛車など得意戦法")
        end
      end

      m.div(class: 'col-12 form-inline') do
        m << label('favorite_kishi', class: 'col-sm-2 col-form-label')
        m.div(class: 'col-sm-10') do
          m << text_area(:favorite_kishi, class: 'form-control', placeholder: "プロアマ問わず二人")
        end
      end

      m.div(class: 'col-12 form-inline') do
        m << label('appeal', class: 'col-sm-2 col-form-label')
        m.div(class: 'col-sm-10') do
          m << text_area(:appeal, class: "form-control", placeholder: "入会に当たって一言ＰＲ")
        end
      end

      # m.div(class: 'col-12 form-inline') do
      #   m << label('graduation', class: 'col-sm-2 col-form-label')
      #   m.div(class: 'col-sm-10') do
      #     m << text_field(:graduation, class: "form-control", placeholder: "初段-三段 要入力")
      #   end
      # end

      # m.div(class: 'col-12 form-inline selectbox') do
      #   m << label('area', class: 'col-sm-2 col-form-label')
      #   m << form_builder.select(:area,
      #                            PREFECTURES,
      #                            { label: '都道府県', prompt: '--選択して下さい--'},
      #                            { selected: @profile&.area,
      #                              class: 'searchable form-control'})
      # end
    end
  end
end
