class SignUpFormPresenter < FormPresenter
  def sign_up_email_block
    markup(:div, class: '') do |m|
      m << label('email')
      m << email_field(:email, placeholder: 'フリーメール可')
    end
  end
end
