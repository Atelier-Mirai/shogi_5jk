class StaffEventPresenter < ModelPresenter
  delegate :staff, :description, :occurred_at, to: :object

  def table_row
    markup(:tr) do |m|
      unless view_context.instance_variable_get(:@staff)
        m.td do
          m << link_to("#{staff.family_name} #{staff.given_name}", [:admin, staff, :staff_events])
        end
      end
      m.td description
      m.td(class: :date) do
        m.text occurred_at.strftime("%Y-%m-%d %H:%M:%S")
      end
    end
  end
end
