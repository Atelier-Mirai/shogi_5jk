class TitleHolderPresenter < ModelPresenter
  def title_holders_table
    data = @object
    markup(:table, class: "ui celled table") do |m|
      # 称号名
      m.thead do
        m.tr(class: 'center aligned') do
          data.each do |datum|
            m.th do
              # m << link_to(datum[:name], view_context.matchup_path(datum[:matchup_id]))
              m << datum[:name]
            end
          end
        end
      end
      # 称号保持者
      m.tbody do
        m.tr(class: 'center aligned') do
          data.each do |datum|
            m.td do
              m << datum[:title_holder]
            end
          end
        end
      end
    end
  end

  def title_holders_list
    data = @object
    markup(:ul, style: "list-style: none;") do |m|
      data.each do |datum|
        m.li do
          m << link_to(datum[:name], view_context.matchup_path(datum[:matchup_id]))
          m << " "
          m << datum[:title_holder]
        end
      end
    end
  end
end
