class TopicPresenter < ModelPresenter
  delegate :id, :title, :permission, :posts, to: :object
  delegate :new_topic_path, :topics_path, to: :view_context

  def header
    markup(:header, class: 'header') do |m|
      m.h2 do |m|
        # 投稿連番
        m.span "[#{id}]", class: 'number'

        m.span(class: :title) do |m| m << decorated_title end

        # 投稿ボタン
        m << markup(:div, class: 'buttons') do |m|
          m.div "投稿", class: "new ui tiny green button", 'data-post-new': self.id
          # 編輯、削除ボタンの表示
          if authorized?
            m << edit_button
            m << delete_button
          end
        end
      end
    end
  end

  # 新着スレッド 10件表示用
  def decorated_title(count: false)
    count = count ? "(#{@object.posts.count})" : nil
    if updated_at > 7.day.ago
      link_to "#{title}#{count} #{image_tag('new-1.gif')}".html_safe, view_context.topic_path(id)
    else
      link_to "#{title}#{count}", view_context.topic_path(id)
    end
  end

  # スレッド一覧
  def table_row
    markup(:tr) do |m|
      m.td do
        m << "[#{sprintf("%3d", id)}] "
        m << decorated_title
      end
      m.td class: 'right aligned' do
        m << posts.count.to_s
      end
      m.td do
        m << gengo(updated_at, :date_and_time)
      end
      if view_context.admin_user?
        m.td do
          m << edit_button
          m << delete_button
        end
      end
    end
  end
end
