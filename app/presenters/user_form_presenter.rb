class UserFormPresenter < FormPresenter
  delegate :proud_tactics,
           :favorite_kishi,
           :appeal,
           :number,
           # :meijin_class,
           :joined_date,
           :joined_dankyu,
           :joined_rating,
           # :main_results,
           # :graduation,
           # :area,
           :condition,
           # :types_of_members,
           to: :profile

  # PREFECTURES = [[:北海道, :hokkaido],
  #                [:青森, :aomori],
  #                [:岩手, :iwate],
  #                [:宮城, :miyagi],
  #                [:秋田, :akita],
  #                [:山形, :yamagata],
  #                [:福島, :fukushima],
  #                [:茨城, :ibaraki],
  #                [:栃木, :tochigi],
  #                [:群馬, :gunma],
  #                [:埼玉, :saitama],
  #                [:千葉, :chiba],
  #                [:東京, :tokyo],
  #                [:神奈川, :kanagawa],
  #                [:新潟, :niigata],
  #                [:富山, :toyama],
  #                [:石川, :ishikawa],
  #                [:福井, :fukui],
  #                [:山梨, :yamanashi],
  #                [:長野, :nagano],
  #                [:岐阜, :gifu],
  #                [:静岡, :shizuoka],
  #                [:愛知, :aichi],
  #                [:三重, :mie],
  #                [:滋賀, :shiga],
  #                [:京都, :kyoto],
  #                [:大阪, :osaka],
  #                [:兵庫, :hyogo],
  #                [:奈良, :nara],
  #                [:和歌山, :wakayama],
  #                [:鳥取, :tottori],
  #                [:島根, :shimane],
  #                [:岡山, :okayama],
  #                [:広島, :hiroshima],
  #                [:山口, :yamaguchi],
  #                [:徳島, :tokushima],
  #                [:香川, :kagawa],
  #                [:愛媛, :ehime],
  #                [:高知, :kochi],
  #                [:福岡, :fukuoka],
  #                [:佐賀, :saga],
  #                [:長崎, :nagasaki],
  #                [:熊本, :kumamoto],
  #                [:大分, :oita],
  #                [:宮崎, :miyazaki],
  #                [:鹿児島, :kagoshima],
  #                [:沖縄, :okinawa],
  #                [:海外, :oversea]]

  def user_information_block(enough_authority: false)
    markup(:div, class: 'ui form') do |m|
      m.div(class: 'field') do
        # instance_variable_get レシーバが持っているインスタンス変数の値を取得するメソッド
        user = view_context.instance_variable_get(:@user)

        readonly = (enough_authority || !user&.persisted?) ? false : true
        m << label('name')
        m << text_field(:name, readonly: readonly, placeholder: '81道場に登録のハンドルネーム')
      end

      m.div(class: 'field') do
        m << label('email')
        m << email_field(:email, placeholder: 'メールアドレス(フリーメール可)')
      end

      m.div(class: 'field') do
        m << label('password')
        m << password_field(:password, placeholder: '当会の活動で用いるパスワード')
      end

      m.div(class: 'field') do
        m << label('password_confirmation')
        m << password_field(:password_confirmation, placeholder: 'タイプミス防止の為、再入力')
      end
    end
  end

  def profile_information_block(enough_authority: false)
    markup(:div, class: 'ui form') do |m|
      readonly = enough_authority ? false : true

      # instance_variable_get レシーバが持っているインスタンス変数の値を取得するメソッド
      profile = view_context.instance_variable_get(:@profile)
      date = (profile&.persisted? ? profile&.joined_date : Date.today).strftime("%Y-%m-%d")

      m.div(class: 'field') do
        m << label('joined_date')
        m << text_field(:joined_date, readonly: readonly, class: "datepicker form-control#{readonly ? '-plaintext' : ''}", value: date)
      end

      m.div(class: 'field') do
        m << label('joined_dankyu')
        m << text_field(:joined_dankyu, readonly: true)
      end

      m.div(class: 'field') do
        m << label('joined_rating')
        m << text_field(:joined_rating, placeholder: '現時点でのR', readonly: readonly)
      end

      m.div(class: 'field') do
        m << label('proud_tactics')
        m << text_area(:proud_tactics, placeholder: "居飛車、振り飛車など得意戦法")
      end

      m.div(class: 'field') do
        m << label(:favorite_kishi)
        m << text_area(:favorite_kishi, placeholder: "プロアマ問わず二人")
      end

      m.div(class: 'field') do
        m << label('appeal')
        m << text_area(:appeal, class: "form-control", placeholder: "入会に当たって一言ＰＲ")
      end

      if enough_authority
        m.div(class: 'field') do
          m << label('condition')
          m << form_builder.select(:condition,
                                    { 活動中: :active, 休止中: :sleep },
                                    { label: "活動・休止中"},
                                    { selected: @profile&.condition,
                                      class: "ui selection dropdown"})
        end
      end
    end
  end

  def password_field_block(name, label_text, options = {})
    if object.new_record?
      super(name, label_text, options)
    else
      markup(:div, class: 'input-block') do |m|
        m << decorated_label(name, label_text, options.merge(required: false))
        m << password_field(name, options.merge(disabled: true))
        m.button('変更する',   type: 'button', id: 'enable-password-field', class: 'btn btn-outline-primary btn-sm')
        m.button('変更しない', type: 'button', id: 'disable-password-field', style: 'display: none;', class: 'btn btn-outline-primary btn-sm')
        m << error_messages_for(name)
      end
    end
  end

  def full_name_block(name1, name2, label_text, options = {})
    markup(:div, class: 'input-block') do |m|
      m << decorated_label(name1, label_text, options)
      m << text_field(name1, options)
      m << text_field(name2, options)
      m << error_messages_for(name1)
      m << error_messages_for(name2)
    end
  end
end
