class UserPresenter < ModelPresenter
  delegate :id, :name, :email, :role, to: :object

  # @object には @topicが渡されている
  def header(enough_authority = false, contribution_button = false)
    markup(:h3) do |m|
      # スレッドタイトルの表示
      m << "[#{id}] "
      m << decorated_title

      # 投稿ボタンの表示
      m << link_to('投稿', "#collapse_form_#{id}", class: 'ui green small button', role: 'button', 'data-toggle' => 'collapse') if contribution_button

      # 編輯、削除ボタンの表示
      if enough_authority
        m << edit_button
        m << delete_button
      end
    end
  end

  def decorated_title(contribution_count = false)
    count = contribution_count ? "(#{@object.posts.count})" : nil
    if updated_at > 7.day.ago
      link_to "#{title}#{count} #{image_tag('new-1.gif')}".html_safe, view_context.topic_path(id)
    else
      link_to "#{title}#{count}", view_context.topic_path(id)
    end
  end

  def table_row
    user    = @object
    profile = user.profile
    markup(:tr) do |m|
      m.td class: 'center' do
        m << avatar_link_to(user, size: 52)
        m << '<br>'
        m << profile&.number
        # m << '<br>'
        # m << profile&.meijin_class
      end
      m.td class: 'center'  do
        m << gengo(profile&.joined_date, :date2)
        m << '<br>'
        m << profile&.joined_dankyu
        m << '<br>'
        m << profile&.joined_rating.to_s
      end
      m.td do m << html_br(profile&.proud_tactics) end
      m.td do m << html_br(profile&.favorite_kishi) end
      m.td do m << html_br(profile&.appeal) end
      # m.td class: 'left'   do m << html_br(profile&.main_results) end
      # m.td class: 'center' do
      #   m << Prefecture.new(profile&.area).name
      #   m << '<br>'
      #   m << html_br(profile&.graduation)
      # end
      m.td class: 'center' do
        m.p class: 'date text-center text-nowrap small' do
          m << gengo(profile.updated_at, :date2)
        end
        if view_context.current_user?(user) || view_context.admin_user?
          m << edit_user_button
          m << edit_user_profile_button
        end
        if view_context.admin_user?
          if !user.approved?
            m << approbation_button
            m << rejection_button
          else
            m << delete_button('会員削除')
          end
        end
      end
    end
  end

  def edit_user_button
    user = @object
    link_to "情報編輯", [:edit, user], class: 'ui yellow small button'
  end

  def edit_user_profile_button
    user = @object
    link_to "紹介編輯", [:edit, user, :profile], class: 'ui yellow small button'
  end

  def approbation_button
    user = @object
    link_to "入会承認", view_context.approbation_path(user), class: 'ui green small button', data: { toggle: 'modal', target: "#approbation_#{user.id}" }
  end

  def rejection_button
    user = @object
    link_to "入会却下", view_context.rejection_path(user), class: 'ui red small button', data: { toggle: 'modal', target: "#rejection_#{user.id}" }
  end

  def approbation_modal
    user = @object
    markup(:div, class: 'modal fade', id: "approbation_#{user.id}", role: 'dialog', 'area-hidden' => true) do |m|
      m.div(class: 'modal-dialog', role: 'document') do
        m.div(class: 'modal-content') do
          m.form(action: view_context.approbation_path(user), 'accept-charset' => 'UTF-8', method: :get) do
            m.div(class: 'modal-header') do
              m.h4(class: 'modal-title text-success') { m.text "#{user.name}さん 入会承認" }
              m.button(type: 'button', class: 'close', 'data-dismiss' => 'modal', 'area-label' => 'close') do
                m.span('area-hidden' => true) { m.text "×" }
              end
            end

            m.div(class: 'modal-body') do
              m << '<label>件名</label>'
              approbation = WelcomeMessage.approbation
              subject = approbation.subject || "五級位上昇を目指す会 入会承認のご連絡"
              m << "<input type='text' name='subject' class='form-control' value='#{subject}'>"
              m << '<label>本文</label>'
              msg = <<~EOS
                ようこそ5JKへ！
                入会手続きが完了しましたので、
                ハンドルネームが<a href='https://shogi-5jk.herokuapp.com/users'>名簿</a>にあることをご確認下さい。
                一緒に卒業目指してがんばりましょう！

                五級位上昇を目指す会 会長 z-air-6
              EOS
              message = approbation.message || msg
              m << "<textarea name='message' class='form-control' >#{message}</textarea>"
            end

            m.div(class: 'modal-footer') do
              m.button(type: 'button', class: 'btn btn-light', 'data-dismiss' => 'modal') { m.text "キャンセル" }
              m.input(type: 'submit', value: '承認する', class: "btn btn-success", data: { disable_with: '送信中...' })
            end
          end
        end
      end
    end
  end

  def rejection_modal
    user = @object
    markup(:div, class: 'modal fade', id: "rejection_#{user.id}", role: 'dialog', 'area-hidden' => true) do |m|
      m.div(class: 'modal-dialog', role: 'document') do
        m.div(class: 'modal-content') do
          m.form(action: view_context.rejection_path(user), 'accept-charset' => 'UTF-8', method: :get) do
            m.div(class: 'modal-header') do
              m.h4(class: 'modal-title text-danger') { m.text "#{user.name}さん 入会却下" }
              m.button(type: 'button', class: 'close', 'data-dismiss' => 'modal', 'area-label' => 'close') do
                m.span('area-hidden' => true) { m.text "×" }
              end
            end

            m.div(class: 'modal-body') do
              m << '<label>件名</label>'
              rejection = WelcomeMessage.rejection
              subject = rejection.subject || "五級位上昇を目指す会 入会却下のご連絡"
              m << "<input type='text' name='subject' class='form-control' value='#{subject}'>"
              m << '<label>本文</label>'
              msg = <<~EOS
                5JKへの入会申し込みありがとうございます。
                せっかくお申し込み頂きましたが、
                規程の対局数（100局）に達していないため、
                また改めてお申し込み頂けますでしょうか？
                一緒に集える日を楽しみに致しております。

                五級位上昇を目指す会 会長 z-air-6
              EOS
              message = rejection.message || msg
              m << "<textarea name='message' class='form-control' >#{message}</textarea>"
              m.div(class: 'text-right') do
                m.small << '(※却下とともに入会希望者からの申請データも削除します)'
              end
            end

            m.div(class: 'modal-footer') do
              m.button(type: 'button', class: 'ui small button', 'data-dismiss' => 'modal') { m.text "キャンセル" }
              m.input(type: 'submit', value: '却下する', class: "ui red small button", 'data-confirm' => '本当に却下しますか', data: { disable_with: '送信中...' })
            end
          end
        end
      end
    end
  end
end
