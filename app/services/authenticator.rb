class Authenticator
  def initialize(user)
    @user = user
  end

  class << self
    # ランダムなトークンを返す
    def new_token
     SecureRandom.urlsafe_base64
    end

    # 与えられた文字列のハッシュ値を返す
    def digest(string)
     cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST
                                                 : BCrypt::Engine.cost
     BCrypt::Password.create(string, cost: cost)
    end
  end

  # 渡されたトークンが、ダイジェストと一致したら、trueを返す
  # attribute: password_digest, remember_digest, activation_digest, reset_digest
  def authenticate(attribute, token)
    @user &&
    (digest = @user.send("#{attribute}_digest")) &&
    BCrypt::Password.new(digest) == token
  end
end
