# https://qiita.com/matsumos/items/5563519a8e7f2dbc427a
require 'zxcvbn'

class ZxcvbnValidator < ActiveModel::EachValidator
  attr_reader :record, :attribute, :value

  SCORE_RANGE = (0..4).freeze

  def initialize(options)
    unless SCORE_RANGE.include? options[:score]
      fail ArgumentError, ":range must be a #{SCORE_RANGE.to_s.gsub('..', ' to ')}"
    end

    super
  end

  def validate_each(record, attribute, value)
    @record    = record
    @attribute = attribute
    @value     = value

    @score = options[:score]
    add_error unless valid?
  end

  private

  def valid?
    @messages_options = {}
    week_keywords     = []

    @record.attributes.each do |key, val|
      next if %i(password password_confirmation encrypted_password).include?(key)
      next unless val.is_a?(String)
      week_keywords.push val, *val.split(/[@._]/) if val
    end

    result = Zxcvbn.test(value, week_keywords)
    @messages_options[:crack_time] = display_time result.crack_time
    result.score >= @score
  end

  def add_error
    if message = options[:message]
      record.errors[attribute] << message
    else
      record.errors.add attribute, :week_password, @messages_options
    end
  end

  def display_time(seconds)
    minute  = 60
    hour    = minute *  60
    day     = hour   *  24
    month   = day    *  30.4
    year    = month  *  12
    century = year   * 100

    case
    when seconds < minute
      unit = 'x_seconds'
      count = 1 + seconds.ceil
    when seconds < hour
      unit = 'x_minutes'
      count = 1 + (seconds / minute).ceil
    when seconds < day
      unit = 'about_x_hours'
      count = 1 + (seconds / hour).ceil
    when seconds < month
      unit = 'x_days'
      count = 1 + (seconds / day).ceil
    when seconds < year
      unit = 'x_months'
      count = 1 + (seconds / month).ceil
    when seconds < century
      unit = 'x_years'
      count = 1 + (seconds / year).ceil
    end

    I18n.t "#{unit}.other", scope: [:datetime, :distance_in_words], count: count
  end
end
