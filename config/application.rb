require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
# require "active_job/railtie"
require "active_record/railtie"
require "active_storage/engine"
require "action_controller/railtie"
require "action_mailer/railtie"
# require "action_mailbox/engine"
# require "action_text/engine"
require "action_view/railtie"
# require "action_cable/engine"
require "sprockets/railtie"
require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Shogi5JK
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    # ロケールを日本に設定
    config.i18n.default_locale = :ja
    # 日本中央標準時で、時刻を表示
    config.time_zone = 'Asia/Tokyo'
    # DBにローカル時刻で保存する
    config.active_record.default_timezone = :local

    # Compiler options for opal
    config.opal.method_missing           = true
    config.opal.optimized_operators      = true
    config.opal.arity_check              = false
    config.opal.const_missing            = true
    config.opal.dynamic_require_severity = :ignore

    # add custom validators path
    config.autoload_paths += Dir["#{config.root}/app/validators"]

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config/localesにディレクトリを作成して階層化したいなら、以下を設定する。
    # config.i18n.load_path += Dir[Rails.root.join('locales', '**', '*.{rb,yml}').to_s]
  end
end
