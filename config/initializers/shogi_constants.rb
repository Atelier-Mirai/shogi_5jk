PawnScore             =  100 * 9 / 10
LanceScore            =  350 * 9 / 10
KnightScore           =  450 * 9 / 10
SilverScore           =  550 * 9 / 10
GoldScore             =  600 * 9 / 10
BishopScore           =  950 * 9 / 10
RookScore             = 1100 * 9 / 10
ProPawnScore          =  600 * 9 / 10
ProLanceScore         =  600 * 9 / 10
ProKnightScore        =  600 * 9 / 10
ProSilverScore        =  600 * 9 / 10
HorseScore            = 1050 * 9 / 10
DragonScore           = 1550 * 9 / 10

KingScore             = 15_000

CapturePawnScore      = PawnScore      * 2
CaptureLanceScore     = LanceScore     * 2
CaptureKnightScore    = KnightScore    * 2
CaptureSilverScore    = SilverScore    * 2
CaptureGoldScore      = GoldScore      * 2
CaptureBishopScore    = BishopScore    * 2
CaptureRookScore      = RookScore      * 2
CaptureProPawnScore   = ProPawnScore   + PawnScore
CaptureProLanceScore  = ProLanceScore  + LanceScore
CaptureProKnightScore = ProKnightScore + KnightScore
CaptureProSilverScore = ProSilverScore + SilverScore
CaptureHorseScore     = HorseScore     + BishopScore
CaptureDragonScore    = DragonScore    + RookScore
CaptureKingScore      = KingScore * 2

PromotePawnScore      = ProPawnScore   - PawnScore
PromoteLanceScore     = ProLanceScore  - LanceScore
PromoteKnightScore    = ProKnightScore - KnightScore
PromoteSilverScore    = ProSilverScore - SilverScore
PromoteBishopScore    = HorseScore     - BishopScore
PromoteRookScore      = DragonScore    - RookScore

ScoreKnownWin         = KingScore

# 初期配置
#                    ９九                                                                                  １一
w_pawn_bitboard   = 0b000000100_000000100_000000100_000000100_000000100_000000100_000000100_000000100_000000100
w_lance_bitboard  = 0b000000001_000000000_000000000_000000000_000000000_000000000_000000000_000000000_000000001
w_knight_bitboard = 0b000000000_000000001_000000000_000000000_000000000_000000000_000000000_000000001_000000000
w_silver_bitboard = 0b000000000_000000000_000000001_000000000_000000000_000000000_000000001_000000000_000000000
w_gold_bitboard   = 0b000000000_000000000_000000000_000000001_000000000_000000001_000000000_000000000_000000000
w_bishop_bitboard = 0b000000000_000000000_000000000_000000000_000000000_000000000_000000000_000000010_000000000
w_rook_bitboard   = 0b000000000_000000010_000000000_000000000_000000000_000000000_000000000_000000000_000000000
w_king_bitboard   = 0b000000000_000000000_000000000_000000000_000000001_000000000_000000000_000000000_000000000

b_pawn_bitboard   = 0b001000000_001000000_001000000_001000000_001000000_001000000_001000000_001000000_001000000
b_lance_bitboard  = 0b100000000_000000000_000000000_000000000_000000000_000000000_000000000_000000000_100000000
b_knight_bitboard = 0b000000000_100000000_000000000_000000000_000000000_000000000_000000000_100000000_000000000
b_silver_bitboard = 0b000000000_000000000_100000000_000000000_000000000_000000000_100000000_000000000_000000000
b_gold_bitboard   = 0b000000000_000000000_000000000_100000000_000000000_100000000_000000000_000000000_000000000
b_bishop_bitboard = 0b000000000_010000000_000000000_000000000_000000000_000000000_000000000_000000000_000000000
b_rook_bitboard   = 0b000000000_000000000_000000000_000000000_000000000_000000000_000000000_010000000_000000000
b_king_bitboard   = 0b000000000_000000000_000000000_000000000_100000000_000000000_000000000_000000000_000000000

w_occupied_bitboard = w_pawn_bitboard | w_lance_bitboard | w_knight_bitboard | w_silver_bitboard | w_gold_bitboard | w_bishop_bitboard | w_rook_bitboard | w_king_bitboard
b_occupied_bitboard = b_pawn_bitboard | b_lance_bitboard | b_knight_bitboard | b_silver_bitboard | b_gold_bitboard | b_bishop_bitboard | b_rook_bitboard | b_king_bitboard
occupied_bitboard   = w_occupied_bitboard | b_occupied_bitboard

# extern PieceScore[PieceNone]
# extern CapturePieceScore[PieceNone]
# extern PromotePieceScore[7]
