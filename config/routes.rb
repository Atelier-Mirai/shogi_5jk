Rails.application.routes.draw do
  get 'title_holders/index'
  get 'title_holders/show'
  get 'title_holders/new'
  get 'title_holders/edit'
  # サークル案内メニュー
  root 'welcome#index'

  resources :rating_distributions

  get 'circle_history'      => 'welcome#circle_history'
  get 'relay_shogi'         => 'welcome#relay_shogi'
  get 'circle_rule'         => 'welcome#circle_rule'
  get 'regular_meeting'     => 'welcome#regular_meeting'
  get 'faq'                 => 'welcome#faq'
  get 'faq_old'             => 'welcome#faq_old'
  get 'administration_rule' => 'welcome#administration_rule'
  get 'zainin_kikan'        => 'welcome#zainin_kikan'
  get 'join_us_information' => 'welcome#join_us_information'

  get 'usage'               => 'welcome#usage' # 利用方法

  # 会員名簿
  resources :users do
    resource :profile, only: [:new, :create, :show, :edit, :update]
  end
  # 順位
  resources :rankings

  # 研究会メニュー
  get 'laboratory'                      => 'laboratory#index'
  get 'laboratory/comment_kifu'         => 'laboratory#comment_kifu'
  get 'laboratory/gather'               => 'laboratory#gather'
  get 'laboratory/four_file_rook'       => 'laboratory#four_file_rook'
  get 'laboratory/right_king'           => 'laboratory#right_king'
  get 'laboratory/old_bbs'              => 'laboratory#old_bbs'
  get 'laboratory/particular_positions' => 'laboratory#particular_positions'
  get 'laboratory/migishiken'           => 'laboratory#migishiken'
  get 'laboratory/outline'              => 'laboratory#outline'

  resources :kifus do
    member do
      get 'json'
      get 'download'
      get 'downloads'
    end
  end

  # 棋戦メニュー
  get 'kisens/information'  => 'kisens#information'
  get 'kisens/common_rule' => 'kisens#common_rule'
  get 'kisens/handicap'    => 'kisens#handicap', as: :handicap
  resources :kisens do
    # resources :entries, only: [:index, :new, :create, :edit, :update, :destroy]
    # resource :card
  end
  resources :matchups do
    # resources :games, only: [:update]
  end
  # get 'game_dates'      => 'games#index'

  # 案内文
  resources :carousels
  resources :information, only: [:edit, :update]

  # 掲示板トップ
  get 'bbs' => 'bbs#index'
  resources :topics do
    resources :posts
  end

  # リンクメニュー
  resources :links, only: [:index, :new, :create, :update, :destroy]
  get   'links/edit'          => 'links#edit', as: :edit_links
  patch 'links/:link_id/sort' => 'links#sort', as: :link_sort

  # 会員登録・ログイン・ログアウトなど
  resources :sessions, only: [:new, :create, :destroy]
  get    'login'  => 'sessions#new'
  post   'login'  => 'sessions#create'
  delete 'logout' => 'sessions#destroy'

  # password resets
  resources :password_resets, only: [:new, :create, :edit, :update]

  # 会員登録
  get  'sign_up'      => 'sign_up#new'
  post 'sign_up'      => 'sign_up#create'
  get  'registration' => 'registration#edit'
  post 'registration' => 'registration#update'

  # メールアドレスの確認
  get  'activation/:id' => 'registration#activation',     as: :activation

  # 入会承認/却下
  get 'approbation/:id' => 'account_approbation#approve', as: :approbation
  get 'rejection/:id'   => 'account_approbation#reject',  as: :rejection

  # 称号保持者
  resources :title_holders

  # letter_opener_web
  mount LetterOpenerWeb::Engine, at: '/letter_opener' if Rails.env.development?
end
