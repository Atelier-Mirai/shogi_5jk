class CreateTopics < ActiveRecord::Migration[4.2]
  def change
    create_table :topics do |t|
      t.string :title, null: false

      t.timestamps
    end

    add_index :topics, :updated_at
  end
end
