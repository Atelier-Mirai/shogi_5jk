class CreatePosts < ActiveRecord::Migration[4.2]
  def change
    create_table :posts do |t|
      t.references :topic, null: false
      t.string :name
      t.string :comment

      t.timestamps
    end

    add_index :posts, :updated_at
  end
end
