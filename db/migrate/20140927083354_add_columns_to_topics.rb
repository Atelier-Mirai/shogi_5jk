class AddColumnsToTopics < ActiveRecord::Migration[4.2]
  def change
    add_column :topics, :name, :string
    add_column :topics, :comment, :string
  end
end
