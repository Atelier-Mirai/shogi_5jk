class AddColumnPostNumberToPost < ActiveRecord::Migration[4.2]
  def change
    add_column :posts, :post_number, :integer
  end
end
