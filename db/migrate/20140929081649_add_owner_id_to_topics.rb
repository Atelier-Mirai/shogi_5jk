class AddOwnerIdToTopics < ActiveRecord::Migration[4.2]
  def change
    add_column :topics, :owner_id, :integer
  end
end
