class AddOwnerIdToPosts < ActiveRecord::Migration[4.2]
  def change
    add_column :posts, :owner_id, :integer
  end
end
