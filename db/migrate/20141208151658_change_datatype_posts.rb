class ChangeDatatypePosts < ActiveRecord::Migration[4.2]
  def change
    change_column :posts, :comment, :text, limit: 3000
  end
end
