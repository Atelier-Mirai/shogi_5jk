class RenameColumnToUsers < ActiveRecord::Migration[4.2]
  def change
    # [形式] rename_column(テーブル名, 変更前のカラム名, 変更後のカラム名)
    rename_column :users, :remember_token, :remember_digest
  end
end
