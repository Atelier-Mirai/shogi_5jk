class RenameImageToPictureToPosts < ActiveRecord::Migration[4.2]
  def change
    # [形式] rename_column(テーブル名, 変更前のカラム名, 変更後のカラム名)
    rename_column :posts, :image, :picture
  end
end
