class RemoveNameCommentFromTopic < ActiveRecord::Migration[4.2]
  def change
    remove_column :topics, :name,     :string
    remove_column :topics, :comment,  :string
    remove_column :topics, :owner_id, :integer
  end
end
