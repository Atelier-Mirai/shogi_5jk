class CreateKifus < ActiveRecord::Migration[4.2]
  def change
    create_table :kifus do |t|
      t.references :post, null: false

      t.text :kifu     # jkfa形式に変換済みの棋譜
      t.text :info     # 対局情報
      t.text :comment  # 指し手へのコメント
      t.text :raw_kifu # 投稿された生の棋譜
      t.timestamps null: false
    end
  end
end
