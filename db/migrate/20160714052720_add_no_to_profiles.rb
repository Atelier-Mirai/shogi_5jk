class AddNoToProfiles < ActiveRecord::Migration[5.0]
  def change
    # postgres で、enumがうまく扱えないので、stringで書き直す
    # if Rails.env.production?
    #   execute <<-SQL
    #     CREATE TYPE area AS ENUM (
    #       'hokkaido',
    #       'aomori',
    #       'iwate',
    #       'miyagi',
    #       'akita',
    #       'yamagata',
    #       'fukushima',
    #       'ibaraki',
    #       'tochigi',
    #       'gunma',
    #       'saitama',
    #       'chiba',
    #       'tokyo',
    #       'kanagawa',
    #       'niigata',
    #       'toyama',
    #       'ishikawa',
    #       'fukui',
    #       'yamanashi',
    #       'nagano',
    #       'gifu',
    #       'shizuoka',
    #       'aichi',
    #       'mie',
    #       'shiga',
    #       'kyoto',
    #       'osaka',
    #       'hyogo',
    #       'nara',
    #       'wakayama',
    #       'tottori',
    #       'shimane',
    #       'okayama',
    #       'hiroshima',
    #       'yamaguchi',
    #       'tokushima',
    #       'kagawa',
    #       'ehime',
    #       'kochi',
    #       'fukuoka',
    #       'saga',
    #       'nagasaki',
    #       'kumamoto',
    #       'oita',
    #       'miyazaki',
    #       'kagoshima',
    #       'okinawa',
    #       'oversea'
    #     );
    #   SQL
    #   execute <<-SQL
    #     CREATE TYPE condition AS ENUM ('active', 'sleep');
    #   SQL
    # end

    add_column :profiles, :number,        :string   # 会員番号
    add_column :profiles, :meijin_class,  :string   # 名人戦・順位戦のクラス
    add_column :profiles, :joined_date,   :date     # 加入日
    add_column :profiles, :joined_dankyu, :string   # 加入時の段級位
    add_column :profiles, :joined_rating, :integer  # 加入時のR
    add_column :profiles, :main_results,  :text     # 主な戦績、役割
    add_column :profiles, :graduation,    :string   # 卒業要件
    add_column :profiles, :area,          :string   # 地域
    add_column :profiles, :condition,     :string   # 活動中／休止中
  end
end
