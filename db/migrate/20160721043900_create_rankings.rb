class CreateRankings < ActiveRecord::Migration[5.0]
  def change
    create_table :rankings do |t|
      t.date    :ymd
      t.integer :rank
      t.integer :user_id
      t.string  :user_name
      t.integer :rating
      t.float   :ave
      t.integer :win
      t.integer :lose
      t.integer :game
      t.integer :best_r

      t.float   :month_ave
      t.integer :month_win
      t.integer :month_lose
      t.integer :month_game
      t.integer :month_rating_up

      t.integer :month_prize_ave
      t.integer :month_prize_win
      t.integer :month_prize_game

      t.float   :year_ave
      t.integer :year_win
      t.integer :year_lose
      t.integer :year_game
      t.integer :year_rating_up

      t.integer :year_prize_ave
      t.integer :year_prize_win
      t.integer :year_prize_game
      t.integer :year_prize_rating_up
    end
  end
end
