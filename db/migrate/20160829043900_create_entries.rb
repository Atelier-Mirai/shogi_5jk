class CreateEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :entries do |t|
      t.integer  :kisen_id, null: false
      t.integer  :user_id, null: false
      t.string   :name, null: false
      t.string   :comment

      t.timestamps
    end
    add_index :entries, [:kisen_id], name: :index_entries_on_kisen_id
  end
end
