class CreateKisens < ActiveRecord::Migration[5.0]
  def change
    create_table :kisens do |t|
      t.integer  :term,         null: false
      t.string   :name,         null: false
      t.date     :open_date,    null: false
      t.date     :begin_date,   null: false
      t.date     :end_date,     null: false
      t.text     :special_note

      t.timestamps
    end
  end
end
