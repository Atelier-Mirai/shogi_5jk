class AddColumnToEntry < ActiveRecord::Migration[5.0]
  def change
    add_column :entries, :rating,       :integer  # 申し込み時のランキング表記載のR
    add_column :entries, :dankyu,       :string   # 段級
    add_column :entries, :number,       :string   # 会員番号
    add_column :entries, :meijin_class, :string   # 名人戦クラス
  end
end
