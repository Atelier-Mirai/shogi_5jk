class CreateMatchups < ActiveRecord::Migration[5.0]
  def change
    create_table :matchups do |t|
      t.integer :kisen_id # 第12期王将戦など、
      t.integer :topic_schedule_id
      t.integer :topic_result_id
      t.date    :last_game_date1
      t.date    :last_game_date2

      t.timestamps
    end
  end
end
