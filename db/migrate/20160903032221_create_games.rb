class CreateGames < ActiveRecord::Migration[5.0]
  def change
    create_table :games do |t|
      t.integer :matchup_id     # 第12期王将戦など、
      t.string  :game_number    # 対局番号 A12 なら A組1番と2番の参加者の対局
      t.integer :player1_id     # 対局者1
      t.integer :player2_id     # 対局者2
      t.date    :date           # 対局日
      t.integer :winner_id      # 勝者
      t.string  :reason         # 理由（勝ち、不戦勝など）
      t.decimal :point1, precision: 3, scale: 1 # player1の勝ち点(全体で3桁, 小数点以下1桁)
      t.decimal :point2, precision: 3, scale: 1
      t.string  :mark1          # player1のマーク（○●など）
      t.string  :mark2          # player2のマーク（●○など）
      t.integer :uwate_id       # 手合割の場合、どちらが上手か
      # (ランキング表よりRを参照して設定する。平手なら0)
      t.string  :teai           # 手合割
      t.integer :kifu_id        # 対局棋譜
      # 未対局: nil, 不戦勝(敗): 0, 対局済: 1, 2, 3, etc

      t.timestamps
    end
    add_index :games, :player1_id
    add_index :games, :player2_id
    add_index :games, :date
    add_index :games, :game_number
  end
end
