class AddColumnsToKisen < ActiveRecord::Migration[5.0]
  def change
    add_column :kisens, :finish_date,     :date
    add_column :kisens, :title_holder_id, :text
  end
end
