class AddKindToEntry < ActiveRecord::Migration[5.0]
  def change
    add_column :entries, :entry_category, :string # 参加者の種類
    # タイトルホルダー
    # または、一般参加者
    add_column :entries, :memo,           :text # 一回戦シード、
    # 敗者復活戦シード、
    # また備考欄への記載などのハッシュ
  end
end
