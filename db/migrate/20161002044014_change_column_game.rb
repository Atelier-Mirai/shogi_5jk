class ChangeColumnGame < ActiveRecord::Migration[5.0]
  def change
    change_column(:games, :player1_id, :string)
    change_column(:games, :player2_id, :string)
  end
end
