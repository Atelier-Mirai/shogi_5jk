class AddColumnToMatchup < ActiveRecord::Migration[5.0]
  def change
    add_column :matchups, :preliminary_group, :text
    add_column :matchups, :final_group, :text
    add_column :matchups, :title_holder_entry, :integer
  end
end
