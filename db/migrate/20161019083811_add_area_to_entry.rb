class AddAreaToEntry < ActiveRecord::Migration[5.0]
  def change
    add_column :entries, :area, :integer
  end
end
