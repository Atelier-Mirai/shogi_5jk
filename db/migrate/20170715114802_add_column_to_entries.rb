class AddColumnToEntries < ActiveRecord::Migration[5.0]
  def change
    add_column :entries, :team, :string
  end
end
