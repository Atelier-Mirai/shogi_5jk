class RenameNameColumToRankings < ActiveRecord::Migration[5.2]
  def change
    rename_column :rankings, :user_name, :name
  end
end
