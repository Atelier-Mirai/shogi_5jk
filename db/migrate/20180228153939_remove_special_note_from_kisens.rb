class RemoveSpecialNoteFromKisens < ActiveRecord::Migration[5.2]
  def up
    remove_column :kisens, :special_note
  end

  def down
    add_column :kisens, :special_note, :string
  end
end
