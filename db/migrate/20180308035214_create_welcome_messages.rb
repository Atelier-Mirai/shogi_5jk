class CreateWelcomeMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :welcome_messages do |t|
      t.string :subject             # 件名
      t.text   :message             # メッセージ本文
      t.string :type,   null: false # approbation or rejection
    end
  end
end
