class CreateMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :messages do |t|
      t.integer :kisen_id            # 外部キー(card専用)
      t.string  :type, null: false   # carousel or card
      t.string  :title               # タイトル
      t.string  :comment             # 本文
    end
  end
end
