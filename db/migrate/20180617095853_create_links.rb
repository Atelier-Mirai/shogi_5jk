class CreateLinks < ActiveRecord::Migration[5.2]
  def change
    create_table :links do |t|
      t.string  :url
      t.string  :text
      t.string  :comment
      t.integer :row_order
      t.string  :type

      t.timestamps
    end
  end
end
