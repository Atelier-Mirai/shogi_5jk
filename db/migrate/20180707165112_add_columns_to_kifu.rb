class AddColumnsToKifu < ActiveRecord::Migration[5.2]
  def change
    add_column :kifus, :player1,  :string
    add_column :kifus, :sengata1, :string
    add_column :kifus, :player2,  :string
    add_column :kifus, :sengata2, :string
    add_column :kifus, :winner,   :integer
    add_column :kifus, :moves,    :integer
    add_column :kifus, :teai,     :string
    add_column :kifus, :kisen,    :string
    add_column :kifus, :date,     :datetime

    add_column :kifus, :sfen,     :text
  end
end
