class AddColumnToRanking < ActiveRecord::Migration[5.2]
  def change
    add_column :rankings, :superior_rank,  :integer
    add_column :rankings, :percentage,     :float
    add_column :rankings, :standard_score, :float
  end
end
