class CreateRatingDistributions < ActiveRecord::Migration[5.2]
  def change
    create_table :rating_distributions do |t|
      t.date   :ymd,  null: false
      t.string :data, null: false
      t.string :info

      t.timestamps
    end
  end
end
