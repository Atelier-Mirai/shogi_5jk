class CreateTitleHolders < ActiveRecord::Migration[6.0]
  def change
    create_table :title_holders do |t|
      t.string  :kisen, null: false
      t.integer :term, null: false
      t.string  :url
      t.string  :winners
      t.string  :members

      t.timestamps
    end
  end
end
