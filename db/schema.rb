# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_11_14_090610) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "cards", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "carousels", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "entries", id: :serial, force: :cascade do |t|
    t.integer "kisen_id", null: false
    t.integer "user_id", null: false
    t.string "name", null: false
    t.string "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "rating"
    t.string "dankyu"
    t.string "number"
    t.string "meijin_class"
    t.string "entry_category"
    t.text "memo"
    t.integer "area"
    t.string "team"
    t.index ["kisen_id"], name: "index_entries_on_kisen_id"
  end

  create_table "games", id: :serial, force: :cascade do |t|
    t.integer "matchup_id"
    t.string "game_number"
    t.string "player1_id"
    t.string "player2_id"
    t.date "date"
    t.integer "winner_id"
    t.string "reason"
    t.decimal "point1", precision: 3, scale: 1
    t.decimal "point2", precision: 3, scale: 1
    t.string "mark1"
    t.string "mark2"
    t.integer "uwate_id"
    t.string "teai"
    t.integer "kifu_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "seed1"
    t.integer "seed2"
    t.index ["date"], name: "index_games_on_date"
    t.index ["game_number"], name: "index_games_on_game_number"
    t.index ["player1_id"], name: "index_games_on_player1_id"
    t.index ["player2_id"], name: "index_games_on_player2_id"
  end

  create_table "information", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "kifus", id: :serial, force: :cascade do |t|
    t.integer "post_id", null: false
    t.text "kifu"
    t.text "info"
    t.text "comment"
    t.text "raw_kifu"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "player1"
    t.string "sengata1"
    t.string "player2"
    t.string "sengata2"
    t.integer "winner"
    t.integer "moves"
    t.string "teai"
    t.string "kisen"
    t.datetime "date"
    t.text "sfen"
  end

  create_table "kisens", id: :serial, force: :cascade do |t|
    t.integer "term", null: false
    t.string "name", null: false
    t.date "open_date", null: false
    t.date "begin_date", null: false
    t.date "end_date", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "finish_date"
    t.text "title_holder_id"
    t.text "message_id"
  end

  create_table "links", force: :cascade do |t|
    t.string "url"
    t.string "text"
    t.string "comment"
    t.integer "row_order"
    t.string "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "matchups", id: :serial, force: :cascade do |t|
    t.integer "kisen_id"
    t.integer "topic_schedule_id"
    t.integer "topic_result_id"
    t.date "last_game_date1"
    t.date "last_game_date2"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "preliminary_group"
    t.text "final_group"
    t.integer "title_holder_entry"
  end

  create_table "messages", force: :cascade do |t|
    t.integer "kisen_id"
    t.string "type", null: false
    t.string "title"
    t.string "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "parentages", id: :serial, force: :cascade do |t|
    t.integer "parent_id"
    t.integer "child_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "posts", id: :serial, force: :cascade do |t|
    t.integer "topic_id", null: false
    t.string "name", limit: 255
    t.text "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "post_number"
    t.integer "owner_id"
    t.string "picture", limit: 255
    t.integer "kifu_id"
    t.string "roots"
    t.index ["updated_at"], name: "index_posts_on_updated_at"
  end

  create_table "profiles", id: :serial, force: :cascade do |t|
    t.string "proud_tactics"
    t.string "favorite_kishi"
    t.text "appeal"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "number"
    t.string "meijin_class"
    t.date "joined_date"
    t.string "joined_dankyu"
    t.integer "joined_rating"
    t.text "main_results"
    t.string "graduation"
    t.string "area"
    t.string "condition"
    t.index ["user_id"], name: "index_profiles_on_user_id"
  end

  create_table "rankings", id: :serial, force: :cascade do |t|
    t.date "ymd"
    t.integer "rank"
    t.integer "user_id"
    t.string "name"
    t.integer "rating"
    t.float "ave"
    t.integer "win"
    t.integer "lose"
    t.integer "game"
    t.integer "best_r"
    t.float "month_ave"
    t.integer "month_win"
    t.integer "month_lose"
    t.integer "month_game"
    t.integer "month_rating_up"
    t.integer "month_prize_ave"
    t.integer "month_prize_win"
    t.integer "month_prize_game"
    t.float "year_ave"
    t.integer "year_win"
    t.integer "year_lose"
    t.integer "year_game"
    t.integer "year_rating_up"
    t.integer "year_prize_ave"
    t.integer "year_prize_win"
    t.integer "year_prize_game"
    t.integer "year_prize_rating_up"
    t.integer "superior_rank"
    t.float "percentage"
    t.float "standard_score"
  end

  create_table "rating_distributions", force: :cascade do |t|
    t.date "ymd", null: false
    t.string "data", null: false
    t.string "info"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "simple_captcha_data", id: :serial, force: :cascade do |t|
    t.string "key", limit: 40
    t.string "value", limit: 6
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["key"], name: "idx_key"
  end

  create_table "title_holders", force: :cascade do |t|
    t.string "kisen", null: false
    t.integer "term", null: false
    t.string "url"
    t.string "winners"
    t.string "members"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "topics", id: :serial, force: :cascade do |t|
    t.string "title", limit: 255, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "permission"
    t.index ["updated_at"], name: "index_topics_on_updated_at"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255
    t.string "email", limit: 255
    t.string "password_digest", limit: 255
    t.string "remember_digest", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "activation_digest"
    t.boolean "activated", default: false
    t.datetime "activated_at"
    t.string "reset_digest"
    t.datetime "reset_sent_at"
    t.string "role"
    t.boolean "approved", default: false
    t.datetime "approved_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["remember_digest"], name: "index_users_on_remember_digest"
  end

  create_table "welcome_messages", force: :cascade do |t|
    t.string "subject"
    t.text "message"
    t.string "type", null: false
  end

  add_foreign_key "profiles", "users"
end
