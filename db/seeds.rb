unless Rails.env.production?
  connection = ActiveRecord::Base.connection
  tables = %w(kifus posts profiles rankings topics users)

  # connection.execute("SET FOREIGN_KEY_CHECKS=0;")
  # tables.each do |table|
  # connection.execute("TRUNCATE #{table}") unless table == "schema_migrations"
  # end
  # connection.execute("SET FOREIGN_KEY_CHECKS=1;")

  # - IMPORTANT: SEED DATA ONLY
  # - DO NOT EXPORT TABLE STRUCTURES
  # - DO NOT EXPORT DATA FROM `schema_migrations`
  Dir.glob('db/seeds.sql').each do |sql_file|
    sql = File.read(sql_file)
    statements = sql.split(/;$/)
    statements.pop  # the last empty statement

    ActiveRecord::Base.transaction do
      statements.each do |statement|
        connection.execute(statement)
      end
    end
  end
  Dir.glob('db/ranking.sql').each do |sql_file|
    sql = File.read(sql_file)
    statements = sql.split(/;$/)
    statements.pop  # the last empty statement

    ActiveRecord::Base.transaction do
      statements.each do |statement|
        connection.execute(statement)
      end
    end
  end
end
