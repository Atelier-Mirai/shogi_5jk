# kisen
Kisen.create!(term:                  11,
              name:                  "王位戦",
              open_date:             '2015-10-01',
              begin_date:            '2015-09-01',
              end_date:              '2015-09-24',
              finish_date:           '2015-12-31',
              title_holder_id:       [58, 54])
Kisen.create!(term:                  12,
              name:                  "王位戦",
              open_date:             '2016-10-01',
              begin_date:            '2016-09-01',
              end_date:              '2016-09-24',
              finish_date:           '2016-12-31')
# Matchup.create!(kisen_id: 1)
# Matchup.create!(kisen_id: 2)

Kisen.create!(term:                  12,
              name:                  "新人王戦",
              open_date:             '2016-11-01',
              begin_date:            '2016-10-01',
              end_date:              '2016-10-24',
              finish_date:           '2016-12-31')
Kisen.create!(term:                  12,
              name:                  "竜王戦",
              open_date:             '2016-11-01',
              begin_date:            '2016-10-01',
              end_date:              '2016-10-24',
              finish_date:           '2016-12-31')

Kisen.create!(term:                  13,
              name:                  "棋聖戦",
              open_date:             '2017-01-10',
              begin_date:            '2016-12-01',
              end_date:              '2016-12-24',
              finish_date:           '2017-03-31')
Kisen.create!(term:                  13,
              name:                  "王座戦",
              open_date:             '2017-03-01',
              begin_date:            '2017-02-01',
              end_date:              '2017-02-24',
              finish_date:           '2017-05-31')
Kisen.create!(term:                  13,
              name:                  "東西戦",
              open_date:             '2017-04-01',
              begin_date:            '2017-03-01',
              end_date:              '2017-03-24',
              finish_date:           '2017-05-31')
Kisen.create!(term:                  13,
              name:                  "名人戦",
              open_date:             '2017-05-01',
              begin_date:            '2017-04-01',
              end_date:              '2017-04-24',
              finish_date:           '2017-07-31')
Kisen.create!(term:                  13,
              name:                  "棋王戦・王将戦",
              open_date:             '2017-09-01',
              begin_date:            '2017-08-01',
              end_date:              '2017-08-24',
              finish_date:           '2017-10-31')

# Entry
# R から 段級位への変換
def rating2dankyu(r)
  return '' if r.nil?

  r = r.to_i
  return "八段" if r >= 2900
  return "七段" if r >= 2700
  return "六段" if r >= 2500
  return "五段" if r >= 2300
  return "四段" if r >= 2100
  return "三段" if r >= 1900
  return "二段" if r >= 1700
  return "初段" if r >= 1550
  return "１級" if r >= 1450
  return "２級" if r >= 1350
  return "３級" if r >= 1250
  return "４級" if r >= 1150
  return "５級" if r >= 1050
  return "６級" if r >= 950
  return "７級" if r >= 850
  return "８級" if r >= 750
  return "９級" if r >= 650
  return "10級" if r >= 550
  return "11級" if r >= 450
  return "12級" if r >= 350
  return "13級" if r >= 250
  return "14級" if r >= 150
  return "15級" if r >= 50
  return "初心者" if r >= 0
end

18.times do |n| # 18人が申し込む
  user_id        = 2 * n + 44
  user_name      = User.find_by(id: user_id).name
  comment        = "愉しみます"
  number         = Profile.find_by(id: user_id).number
  rating         = Ranking.where(ymd: '2016-02-01').where(user_name: user_name).pluck(:rating)[0]
  if rating.blank?
    rating = if number =~ /^[+-]?[0-9]+$/
               number.to_i * 2
             else
               number[1..-1].to_i * 2
    end
  end
  dankyu         = rating2dankyu(rating)
  meijin_class   = Profile.find_by(id: user_id).meijin_class
  area           = Prefecture.new(Profile.find_by(id: user_id).area).code
  9.times do |i| # 8種類の各棋戦への参加申し込み
    entry_category = (user_id == 58 && i == 1) ? 'title_holder' : 'general_participant' # 一般参加者
    Entry.create!(kisen_id:      i + 1,
                  user_id:        user_id,
                  name:           user_name,
                  comment:        comment,
                  rating:         rating,
                  dankyu:         dankyu,
                  number:         number,
                  meijin_class:   meijin_class,
                  entry_category: entry_category,
                  area:           area)
  end
end
