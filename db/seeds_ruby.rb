# administrator
User.create!(name:                  'Admin',
             email:                 'example@railstutorial.org',
             password:              'ke927dzx',
             password_confirmation: 'ke927dzx',
             admin:                 true,
             activated:             true,
             activated_at:          Time.current)

# user
User.create!(name:                  'User',
             email:                 'example-1@railstutorial.org',
             password:              'ke927dzx',
             password_confirmation: 'ke927dzx',
             admin:                 false,
             activated:             true,
             activated_at:          Time.current)

10.times do |n|
  name = Faker::Name.name
  email = "example-#{n + 2}@railstutorial.org"
  password = 'ke927dzx'
  User.create!(name:                  name,
               email:                 email,
               password:              password,
               password_confirmation: password,
               activated:             true,
               activated_at:          Time.current)
end

name = 'no profile'
email = 'no_profile@railstutorial.org'
password = 'ke927dzx'
User.create!(name:                  name,
             email:                 email,
             password:              password,
             password_confirmation: password,
             activated:             true,
             activated_at:          Time.current)

# topic
users    = User.order(:created_at).take(12)
titles   = %w(
  右玉について
  ゴキゲン中飛車
  穴熊最強
  棒銀
  四間飛車の基本
  石田流のすすめ
  日々詰め将棋
  居飛車党です
  今日の快勝譜
  棋戦開催についてのお知らせ
  例会
  日々のいろいろを綴る
)
comments = %w(
  右玉はいいですね
  中飛車も素敵です
  穴熊は固くていいですよね
  棒銀は初心者が最初に覚える戦法です
  振り飛車の基本は四間飛車だ
  石田流は必勝戦法かも知れません
  上達するためには詰め将棋が何よりです
  頑固に居飛車党
  ほとんど全部の駒をもらうことが出来ました
  名人戦を開催します
  今週の土曜日は例会です
  紅葉が綺麗な時期ですね
)
# Topic.create(title: "foo", posts_attributes: [{ name: "guest", comment: "こめんとです", post_number: 1, owner_id: 0 }])

12.times do |n|
  Topic.create!(
    title: titles[n],
    posts_attributes: [
      {
        name:  users[n].name,
        comment: comments[n],
        owner_id: users[n].id,
        post_number: 1
      }
    ]
  )
end

migi_comments = %w(
  広くて逃げやすいです
  ぬるぬる入玉を目指しましょう
  指しこなせば無敵です
  相手は慣れないので、苦労しますね。
  玉飛接近すべからず、という格言もありますが、右玉は強いです
  バランスを取って指すことが大切です。
  薄いので自分から攻めると言うよりは、カウンターを狙う感じが良いかも知れません
  ともあれ、指してみると楽しいですね
  やっぱり右玉最高ですね
  名前が似ている左玉という戦法もあります
  風車も右玉の親戚かな　途中までよく似ています
  アマチュアにも人気の戦法ですね
)
12.times do |n|
  Post.create!(
    topic_id: 1,
    name: users[1].name,
    comment: migi_comments[n],
    post_number: 2 + n,
    owner_id: 1
  )
end
Post.create!(
  topic_id: 1,
  name: "Mr. 初心者",
  comment: "僕も指してみますね",
  post_number: 14,
  owner_id: 0, # ゲストの投稿
  kifu_id: 1
)

# profile
tactics = %w(
  右玉
  ゴキゲン中飛車
  穴熊
  棒銀
  四間飛車
  石田流
)
kishi = %w( 羽生先生 渡辺先生 加藤先生 中原先生 大山先生 谷川先生 米長先生 森内先生 )
6.times do |n|
  Profile.create!(
    proud_tactics: tactics[n],
    favorite_kishi: kishi.sample(2).join("\r\n"),
    appeal: titles[n],
    user_id: n + 1
  )
end

# kifu
kifu = [
  {}, # 0 手目として入れておく
  { from: 119, to: 118, piece: 17, promote: false, same: false, relative: '', capture: 0 },
  { from: 131, to: 132, piece: 33, promote: false, same: false, relative: '', capture: 0 },
  { from: 136, to: 51, piece: 22, promote: false, same: false, relative: '', capture: 1 },
  { from: 34, to: 51, piece: 38, promote: false, same: true, relative: '', capture: 6 },
  { from: 73, to: 88, piece: 21, promote: false, same: false, relative: 'R', capture: 0 },
  { from: 0, to: 69, piece: 38, promote: false, same: false, relative: 'P', capture: 0 },
  { from: 71, to: 70, piece: 17, promote: false, same: false, relative: '', capture: 0 },
  { from: 69, to: 103, piece: 38, promote: true, same: false, relative: 'N', capture: 1 },
  { from: 88, to: 103, piece: 21, promote: false, same: true, relative: '', capture: 14 },
  { from: 51, to: 119, piece: 38, promote: false, same: false, relative: 'Z', capture: 0 },
  { from: 89, to: 73, piece: 24, promote: false, same: false, relative: '', capture: 0 },
  { from: 119, to: 104, piece: 38, promote: true, same: false, relative: 'N', capture: 0 },
  { from: 103, to: 104, piece: 21, promote: false, same: true, relative: 'D', capture: 14 },
  { from: 132, to: 133, piece: 33, promote: false, same: false, relative: '', capture: 0 },
  { from: 0, to: 119, piece: 22, promote: false, same: false, relative: 'P', capture: 0 },
  { from: 133, to: 134, piece: 33, promote: false, same: false, relative: '', capture: 0 },
  { from: 119, to: 85, piece: 22, promote: false, same: false, relative: '', capture: 0 },
  { from: 134, to: 135, piece: 33, promote: false, same: false, relative: 'Z', capture: 1 },
  { from: 0, to: 51, piece: 22, promote: false, same: false, relative: 'P', capture: 0 }]
info = { "開始日時：" => '2016/01/01 10:00', "終了日時：" => '2016/01/01 20:00',
         "棋戦：" => "テスト用の練習", "持ち時間：" => "15分",
         "消費時間：" => "▲1時間00分△3時間15分", "場所：" => "大阪道場特別対局室",
         "先手：" => 'Haruka', "後手：" => 'Mirai' }
comment = [
  "楽しく対局出来た一局",
  "初手は７六歩と角道を開けました。",
  '',
  "いきなりの王手です",
  "取られちゃいました(T-T)(T-T)"
]
raw_kifu = ['', "▲７六歩", "△８四歩", "▲３三角", "△同角", "▲５八金右", "△４五角",
            "▲４六歩", "△６七角成", "▲同金", "△７七角不成", "▲４九玉", "△６八角成",
            "▲同金引", "△８五歩", "▲７七角", "△８六歩", "▲５五角", "△８七歩不成",
            "▲３三角打"]

Kifu.create!(
  post_id:  25,
  kifu:     kifu,
  info:     info,
  comment:  comment,
  raw_kifu: raw_kifu
)
