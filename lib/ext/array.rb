# **********************************************************
# Ruby script to rank array items, considering same ranks.
# http://www.mk-mode.com/octopress/2015/07/24/ruby-ranking-simultaneous-arrival-by-array-class/
# **********************************************************
class Array
  def rank
    # 以下の場合は例外スロー
    # - 自身配列が空
    # - 自身配列に数値以外の値を含む
    # raise "Self array is nil!" if self.size == 0
    return nil if size == 0

    each do |v|
      fail 'Items except numerical values exist!' unless v.to_s =~ /[\d\.]+/
    end

    # ランク付け(降順)
    map { |v| count { |a| a > v } + 1 }
  end
end
