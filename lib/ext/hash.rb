class Hash
  # https://jugyo.wordpress.com/2007/09/23/ruby-の-hash-に-array-みたいな-join-メソッド/
  def join(str1, str2)
    to_a.map { |array| array.join(str1) }.join(str2)
  end

  # 値が重複していたときに備えて、変換後の値を配列として保持する
  def safe_invert
    result = Hash.new { |h, key| h[key] = [] }
    each do |key, value|
      result[value] << key
    end
    result
  end
  # p safe_invert({"a"=>1, "b"=>1, "c"=>3})
  #=> {1=>["a", "b"], 3=>["c"]}
  # 転載：Rubyレシピブック No.120
end
