class Integer
  def to_h
    to_s(16)
  end

  def to_b
    to_s(2).reverse.gsub(/(\d{4})(?=\d)/, '\1_').reverse
  end

  def to_n
    to_s(2).reverse.gsub(/(\d{9})(?=\d)/, '\1_').reverse
  end

  # 二桁の算用数字を漢数字に変換
  def to_kan
    case self
    when 0..9
      num2kan1(self)
    when 10
      "十"
    when 11..19
      "十#{num2kan1(self % 10)}"
    when 20..99
      "#{num2kan1(self / 10)}十#{num2kan1(self % 10)}"
    end
  end

  # 一桁の算用数字を漢数字に変換
  def num2kan1(num)
    ['', "一", "二", "三", "四", "五", "六", "七", "八", "九"][num]
  end
end
