# A組 <=> 1組
class String
  # value文字先の文字を返す
  def next(value = 1)
    (ord + value).chr
  end

  # アルファベットか？
  def alpha?
    !match(/[^A-Za-z]/)
  end

  # 先頭文字が大文字か？
  def capital_letter?
    !!self[0].match(/[A-Z]/)
  end

  # 先頭文字が小文字か？
  def small_letter?
    !!self[0].match(/[a-z]/)
  end

  # 先頭文字が数字か？
  def numeric_character?
    !!self[0].match(/[0-9]/)
  end

  # 整数か否か
  def numeric?
    !!match(/^[+-]?[0-9]+$/)
  end

  # 引用の為に '>' を付ける
  def to_reply
    str = ''
    each_line do |line|
      str << "> #{line}"
    end
    str << "\n\n"
  end
end
