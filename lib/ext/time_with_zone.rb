class ActiveSupport::TimeWithZone
  def holiday?
    HolidayJp.holiday?(self).nil? ? false : true
  end

  def what_is_today
    return :holiday  if holiday?
    return :weekday  if on_weekday?
    return :saturday if saturday?
    return :sunday   if sunday?
  end
end
