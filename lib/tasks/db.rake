# # Rails3.xでseeds.rbを追加投入したい
# # http://d.hatena.ne.jp/yuunachan/20120921/1348183289
# namespace :db do
#   desc 'Load the seed data from SEED_FILENAME'
#   task :seed_from_file => 'db:abort_if_pending_migrations' do
#     seed_file = File.join(Rails.root, 'db', ENV['SEED_FILENAME'])
#     load(seed_file) if File.exist?(seed_file)
#   end
# end
#
# # rake db:seed_from_file SEED_FILENAME='hogehoge.rb'
# # とすると追加データーが入るようになりました。
#
# # namespace :db do
# #   desc 'Load the seed data from seeds_kisen.rb'
# #   task seed_kisen: 'db:abort_if_pending_migrations' do
# #     seed_file = File.join(Rails.root, 'db', 'seeds_kisen.rb')
# #     load(seed_file) if File.exist?(seed_file)
# #   end
# # end
