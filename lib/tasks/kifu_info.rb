# # http://keruuweb.com/rails-テーブルにレコードを追加する/
# # 次のコードを作成して、/lib/tasks/kifu_info.rb として保存します。
#
# # Kifu.column_names
# # => ["id", "post_id", "kifu", "info", "comment", "raw_kifu", "created_at", "updated_at", "player1", "sengata1", "player2", "sengata2", "teai", "kisen", "date", "sfen"]
#
# class KifuInfo
#   def save
#     # kifu.info
#     # [:開始日時：, :棋戦：, :手合割：, :先手：, :後手：,
#     # :終了日時：, :戦型：, :上手：, :下手：, :持ち時間：,
#     # :対局ID：, :記録ID：, :表題：, :消費時間：, :場所：,
#     # :振り駒：, :計測時方式：, :秒読み：, :先手消費時間加算：,
#     # :後手消費時間加算：, :先手省略名：, :後手省略名：]
#     regexp = /(?<name>[a-zA-Z０-９\-\_\ \d\p{Hiragana}\p{Katakana}\p{Han}\p{Latin}]+)(?<rating>\(\d+\))*/
#
#     Kifu.all.each do |kifu|
#     # kifu = Kifu.find(417)
#       info = kifu.info
#       if (player1 = (info[:先手：] || info[:下手：])).present?
#         # WhiteWizard(1600) のように後ろにRが付いているときは、名前だけにする
#         player1 = player1.match(regexp)[:name]
#       else
#         player1 = "名無しさん"
#       end
#       if (player2 = (info[:後手：] || info[:上手：])).present?
#         player2 = player2.match(regexp)[:name]
#       else
#         player2 = "名無しさん"
#       end
#
#       teai = info[:手合割：].present? ? info[:手合割：].tr('：', '').tr('　', '') : '平手'
#       moves = kifu.kifu.length - 1
#       winner = ((teai == '平手') && (moves.odd?)) ? 1 : 2
#
#       kisen = info[:棋戦：]&.tr('：', '') || '' # infoの記述が初期値
#       # 棋戦名をたどれるなら上書き
#       title = kifu&.post&.topic&.title
#       %w(名人戦 竜王戦 王位戦 王座戦 棋王戦 王将戦 棋聖戦 新人王戦 東西戦).each do |name|
#         if title&.include? name
#           kisen = name
#           break
#         end
#       end
#
#       date = DateTime.parse(info[:開始日時：].present? ? "#{info[:開始日時：]} JST" : kifu.created_at.to_s)
#       kifu.update_columns(player1: player1,
#                           player2: player2,
#                           winner:  winner,
#                           moves:   moves,
#                           teai:    teai,
#                           kisen:   kisen,
#                           date:    date)
#     end
#   end
# end
#
# KifuInfo.new.save
# puts "done"
#
# # 実行します。
# # $ rails runner lib/tasks/kifu_info.rb
# # "done"
# # これでカラムに追加されたはずです。
