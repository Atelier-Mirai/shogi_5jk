# # http://keruuweb.com/rails-テーブルにレコードを追加する/
# # 次のコードを作成して、/lib/tasks/save_record.rb として保存します。
#
# class SaveRecord
#  def save
#   Link.create(url:'http://kouki3110.at.webry.info/', text:'kouki3110の将棋', comment:'kouki3110の将棋', type:'member_site')
#   Link.create(url:'http://human-soul.seesaa.net/', text:'の〜んびり行きましょう！', comment:'HUMAN SOULさん', type:'member_site')
#   Link.create(url:'http://www.ne.jp/asahi/kurikuri750/ram.ten.yumira/', text:'kurikuri750’room', comment:'kurikuri750さん', type:'member_site')
#   Link.create(url:'http://theair6.blog109.fc2.com/', text:'The Air sicks', comment:'z-air-6さん', type:'member_site')
#   Link.create(url:'http://t-minato-syougi.at.webry.info/', text:'t-minatoの独り言・・。', comment:'t-minatoさん', type:'member_site')
#   Link.create(url:'http://miyukisyan.blog.shinobi.jp', text:'はらやん放浪記', comment:'harayan-goさん', type:'member_site')
#
#   Link.create(url:'http://www.shogitown.com/', text:'将棋タウン', comment:'（手筋等）isodaさん', type:'shogi_site')
#   Link.create(url:'http://shodan-navi.com/', text:'初段NAVI', comment:'（手筋等）ABBEY ROADERさん', type:'shogi_site')
#   Link.create(url:'http://www005.upp.so-net.ne.jp/tsumepara/contents/3problem/ted/ted.htm', text:'詰パラ', comment:'（詰将棋）易しい３〜７手詰めを毎日更新。4000問以上の詰め将棋があります。', type:'shogi_site')
#   Link.create(url:'http://www.geocities.co.jp/Playtown-Bishop/2518/problem2/indexf.html', text:'実戦の詰み', comment:'（詰将棋）実戦に現れた詰みのある局面を、詰将棋盤で紹介。全てプロ棋士の実戦譜からです。ブラウザ上で駒を動かすことができます。', type:'shogi_site')
#   Link.create(url:'http://www.ne.jp/asahi/hp/komaochi/tume/startEasy/start3.html', text:'やさしい詰め将棋', comment:'（詰将棋）初心者向けの易しい詰め将棋。テーマ毎に分類され学習も容易です。ブラウザ上で駒を動かすことができます。', type:'shogi_site')
#   Link.create(url:'http://park6.wakwak.com/~k-oohasi/shougi/', text:'詰将棋博物館', comment:'（詰将棋）江戸時代の古典詰将棋の宝庫。将棋図巧に収録されている伊藤看寿作「煙詰」など名作揃いです。是非、ご鑑賞下さい。', type:'shogi_site')
#   Link.create(url:'http://rocky-and-hopper.sakura.ne.jp/Kisho-Michelin/index.htm', text:'棋書ミシュラン！', comment:'（棋書紹介）将棋の本を詳しく紹介し、ランク付けしているサイトです。', type:'shogi_site')
#   Link.create(url:'https://shogidb2.com', text:'将棋DB2', comment:'（棋譜）プロ棋士の実践棋譜のデータベースです。棋譜並べにどうぞ。', type:'shogi_site')
#   Link.create(url:'https://ameblo.jp/shogi-strategy', text:'将棋・序盤のStrategy', comment:'（戦法）オールラウンダーを目指す序盤研究ブログです。様々な戦法を詳しく解説しています。', type:'shogi_site')
#
#   Link.create(url:'http://www.digitalstage.jp/bind/', text:'BiND for WebLiFE 10', comment:"誰でもプ口顔負けの洗練されたデザインや機能を備えたウェブサイトを、ワープロ感覚で簡単に作れるソフトウェア。\n もちろん iPhone など、レスポンシブデザインにも対応。一押しです!!", type:'web_creator')
#   Link.create(url:'https://atom.io', text:'atom', comment:"21世紀の標準エディタ。Webサイト作成はもちろんですが、プログラミングに威力を発揮します。フリーで軽く機能も豊富。このサイトも愛用しています。", type:'web_creator')
#   Link.create(url:'http://www.htmq.com/html5/index.shtml', text:'HTMLクイックリファレンス', comment:"エディタで作成したい方のためのHTML講座。基本は押さえておきたいものです。", type:'web_creator')
#   Link.create(url:'http://www.colordic.org/w/', text:'和色大辞典', comment:"美しい日本の伝統色の紹介がなされています。サイトの色使いの参考に。", type:'web_creator')
#   Link.create(url:'https://www.ruby-lang.org/ja/', text:'Ruby', comment:"松江発のプログラミング言語で、シンプルさと高い生産性を備えています。エレガントな文法を持ち、自然に読み書きができます。一押しです!!", type:'web_creator')
#   Link.create(url:'https://railstutorial.jp', text:'Ruby on Rails チュートリアル', comment:"Webフレームワークとして名高い Ruby on Rails の初心者向けの学習サイト。Twitter風の課題作成と楽しみながら、Webアプリの基本が身に付きます。", type:'web_creator')
#   Link.create(url:'https://getbootstrap.com', text:'Bootstrap', comment:"Webアプリケーションフレームワークの定番。レスポンシブデザインに対応したWebサイトを簡単に作成出来るよう、様々なコンポーネントが用意されています。", type:'web_creator')
#   Link.create(url:'https://dotinstall.com/lessons', text:'ドットインストール', comment:"プログラムを始めたい初心者向けに、3分間の短い動画で解説しています。\n基本を身に着けるには良いと思います。", type:'web_creator')
#   Link.create(url:'https://dotinstall.com/lessons', text:'e-typing', comment:"インターネットでタイピング練習 ができます。プログラミングのみならず、オフィスワーク等でも役立つ必須スキルです。\n簡単に習得出来ますので、毎日コツコツ練習して、身に着けましょう。", type:'web_creator')
#   Link.create(url:'http://www.fe-siken.com', text:'基本情報技術者試験ドットコム', comment:"IT技術者ならば、取得しておきたい[基本情報技術者](https://www.jitec.ipa.go.jp/1_11seido/fe.html)のサイトです。\nITリテラシの基礎を身に着けたい社会人には、[ITパスポート試験](https://www3.jitec.ipa.go.jp/JitesCbt/index.html)もあります。", type:'web_creator')
#   Link.create(url:'https://binarynights.com/forklift/', text:'ForkList 3', comment:"Mac環境では定番のFTP転送ソフト。2画面構成で使いやすく、機能も豊富でおすすめです。\n 体験版もあります。また ForkList 2.6.6 は無料ですのでこちらを利用するのも良いかと思います。", type:'web_creator')
#   Link.create(url:'http://www2.biglobe.ne.jp/~sota/ffftp.html', text:'FFFTPソフト', comment:"Windows環境の定番FTP転送ソフト。フリーです。最新版は[こちら](https://ja.osdn.net/projects/ffftp/)から入手可能です。", type:'web_creator')
#  end
# end
#
# SaveRecord.new.save
# puts "done"
#
# # 実行します。
# # $ rails runner lib/tasks/save_record.rb
# # "done"
# # これでレコードが追加されたはずです。
