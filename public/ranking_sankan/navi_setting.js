$(function(){
	//ナビ設定
	$("div.panel > div:not("+$("ul.tab li a.selected").attr("href")+")").hide();	//selected 以外を隠しておく
	//tab がクリックされたときは
	$("nav ul.tab li a").click(function(){
		$("nav ul.tab li a").removeClass("selected");
		$(this).addClass("selected");
		$("div.panel > div").slideUp("fast");
		$($(this).attr("href")).slideDown("fast");
		return false;
	});

	//クラスをセット
	$("[id^='area']").addClass('area');
});
