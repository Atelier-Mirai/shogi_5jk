require 'test_helper'

class RatingDistributionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @rating_distribution = rating_distributions(:one)
  end

  test "should get index" do
    get rating_distributions_url
    assert_response :success
  end

  test "should get new" do
    get new_rating_distribution_url
    assert_response :success
  end

  test "should create rating_distribution" do
    assert_difference('RatingDistribution.count') do
      post rating_distributions_url, params: { rating_distribution: {  } }
    end

    assert_redirected_to rating_distribution_url(RatingDistribution.last)
  end

  test "should show rating_distribution" do
    get rating_distribution_url(@rating_distribution)
    assert_response :success
  end

  test "should get edit" do
    get edit_rating_distribution_url(@rating_distribution)
    assert_response :success
  end

  test "should update rating_distribution" do
    patch rating_distribution_url(@rating_distribution), params: { rating_distribution: {  } }
    assert_redirected_to rating_distribution_url(@rating_distribution)
  end

  test "should destroy rating_distribution" do
    assert_difference('RatingDistribution.count', -1) do
      delete rating_distribution_url(@rating_distribution)
    end

    assert_redirected_to rating_distributions_url
  end
end
