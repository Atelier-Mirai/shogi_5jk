require 'test_helper'

class TitleHoldersControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get title_holders_index_url
    assert_response :success
  end

  test "should get show" do
    get title_holders_show_url
    assert_response :success
  end

  test "should get new" do
    get title_holders_new_url
    assert_response :success
  end

  test "should get edit" do
    get title_holders_edit_url
    assert_response :success
  end

end
