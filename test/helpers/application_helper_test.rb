require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test 'full title helper' do
    assert_equal full_title, "メイン掲示板"
    assert_equal full_title("スレッド一覧"), "スレッド一覧 | メイン掲示板"
  end
end
