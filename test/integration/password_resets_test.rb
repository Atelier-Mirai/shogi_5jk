require 'test_helper'

class PasswordResetsTest < ActionDispatch::IntegrationTest
  def setup
    ActionMailer::Base.deliveries.clear
    @user = users(:taro)
  end

  test "password resets" do
    get new_password_reset_path
    assert_template 'password_resets/new'
    # メールアドレスが無効
    post password_resets_path, params: { password_reset: { email: "" } }
    assert_not flash.empty?
    assert_template 'password_resets/new'
    # メールアドレスが有効
    post password_resets_path,
         params: { password_reset: { email: @user.email } }
    assert_not_equal @user.reset_digest, @user.reload.reset_digest
    assert_equal 1, ActionMailer::Base.deliveries.size
    assert_not flash.empty?
    assert_redirected_to root_url
    # パスワード再設定フォームのテスト
    user = assigns(:user)
    # メールアドレスが無効
    get edit_password_reset_path(user.reset_token, email: "")
    assert_redirected_to root_url
    # 無効なユーザー
    user.toggle!(:activated)
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_redirected_to root_url
    user.toggle!(:activated)
    # メールアドレスが有効で、トークンが無効
    get edit_password_reset_path('wrong token', email: user.email)
    assert_redirected_to root_url
    # メールアドレスもトークンも有効
    get edit_password_reset_path(user.reset_token, email: @user.email)
    assert_template 'password_resets/edit'
    # assert_select "input[name=email][type=hidden][value=?]", @user.email
    assert_select 'h1'
    # <input type="hidden" name="email" id="email" value="taro@example.com">

    # 無効なパスワードとパスワード確認
    patch password_reset_path(user.reset_token),
          params: { email: user.email,
                    user: { password:              "foobaz",
                            password_confirmation: "barquux" } }
    # assert_select 'div#error_explanation'
    # パスワードが空
    patch password_reset_path(user.reset_token),
          params: { email: user.email,
                    user: { password:              "",
                            password_confirmation: "" } }
    # assert_select 'div#error_explanation'
    # 有効なパスワードとパスワード確認
    patch password_reset_path(user.reset_token),
          params: { email: user.email,
                    user: { password:              "foobaz",
                            password_confirmation: "foobaz" } }
    assert is_logged_in_user?
    assert_not flash.empty?
    assert_redirected_to user
  end

#   test "password resets org" do
#     # http://localhost:3000/password_resets/
#     # x3veXb9dCH0MTJCfb6cDMg/edit?email=example%40railstutorial.org
#     get new_password_reset_path
#     # gem 'rails-controller-testing' が必要
#     assert_template 'password_resets/new'
#     # メールアドレスが無効
#     post password_resets_path, params: { password_reset: { email: "" } }
#     assert_not flash.empty?
#     assert_template 'password_resets/new'
#     # メールアドレスが有効
#     post password_resets_path, params: { password_reset: { email: @user.email } }
#     assert_not_equal @user.reset_digest, @user.reload.reset_digest
#     assert_equal 1, ActionMailer::Base.deliveries.size
#     assert_not flash.empty?
#     assert_redirected_to root_url
#     # パスワードリセット用フォーム
#     user = assigns(:user)
#     # メールアドレスが無効
#     get edit_password_reset_path(user.reset_token, email: "")
#     assert_redirected_to root_url
#     # 無効なユーザー
#     user.toggle!(:activated)
#     get edit_password_reset_path(user.reset_token, email: user.email)
#     assert_redirected_to root_url
#     user.toggle!(:activated)
#     # メールアドレスが有効、トークンが無効
#     get edit_password_reset_path('wrong token', email: user.email)
#     assert_redirected_to root_url
#     # メールアドレスもトークンも有効
#     get edit_password_reset_path(user.reset_token, email: user.email)
#     assert_template 'password_resets/edit'
#     assert_select 'input[name=email][type=hidden][value=?]', user.email
# #     "input[name=email][type=hidden][value="taro@example.com"]", found 0..
# #
# #     <form class="edit_user" id="edit_user_201862782" role="form" action="/password_resets/lh_i3xLQjJyyq_OEbJeP-A" method="post">
# #     <input type="hidden" name="_method" value="patch">
# #     <input type="hidden" name="authenticity_token" value="HIm2JRUt7GgQT3BR50mQC43yKnkTc8kxHoTj36+JV0d1/6xSfY/EmXH63BL1WnyUQ+DjrvGW4HFMvuxAS4YRfw==">
# #     <input type="hidden" name="email" id="email" value="taro@example.com">
# #
# # <div class="form-group"><label class="control-label required" for="user_password">パスワード</label><input class="form-control" type="password" name="user[password]" id="user_password"></div><div class="form-group"><label class="control-label" for="user_password_confirmation">パスワード(確認)</label><input class="form-control" type="password" name="user[password_confirmation]" id="user_password_confirmation"></div><input type="submit" name="commit" value="更新する" class="btn btn-primary" data-disable-with="更新する"></form>
#
#     # 無効なパスワードを確認
#     patch password_reset_path(user.reset_token), email: user.email,
#                 user: { password: "ke927dzx",
#                         password_confirmation: "hogehoge" }
#     assert_select 'div.alert-danger'
#     # パスワードが空
#     patch password_reset_path(user.reset_token), email: user.email,
#                 user: { password: "",
#                         password_confirmation: "" }
#     assert_select 'div.alert-danger'
#     # 有効なパスワードと確認
#     patch password_reset_path(user.reset_token), email: user.email,
#                 user: { password: "ke927dzx",
#                         password_confirmation: "ke927dzx" }
#     assert is_logged_in_user?
#     assert_not flash.empty?
#     assert_redirected_to root_url
#   end

  test "expired token" do
    get new_password_reset_path
    post password_resets_path, password_reset: { email: @user.email }

    @user = assigns(:user)
    @user.update_attribute(:reset_sent_at, 3.hours.ago)
    patch password_reset_path(@user.reset_token), email: @user.email,
                user: { password: "password",
                        password_confirmation: "password" }
    assert_response :redirect
    follow_redirect!
    assert_match /パスワード再設定の期限を過ぎました。/i, response.body
  end
end
