require 'test_helper'

class ProfileTest < ActionDispatch::IntegrationTest
  def setup
    @admin = users(:taro)
    @user = users(:jiro)
  end

  # ログインしていなくても、プロフィールを参照出来る
  test 'profile show not logged in user' do
    get user_profile_path(@user)
    assert_template 'profiles/show'
    assert_select 'title', full_title(@user.name)
  end

  test 'profile edit' do
    log_in_as(@user)
    get edit_user_profile_path(@user)
    assert 'title', '自己紹介 更新'
    # assert_template 'profiles/edit' # なぜかNG
    patch user_profile_path(@user),
          params: { user: { proud_tactics:  "右玉",
                            favorite_kishi: "羽生先生",
                            appeal:         "振飛車党です" } }

    get edit_user_profile_path(@user)
    assert 'text', '右玉'
    assert 'text', '羽生先生'
    assert 'text', '振飛車党です'
  end

  test 'profile not edit other user' do
    log_in_as(@user)
    get edit_user_profile_path(@admin)
    assert 'title', '自己紹介 更新'
    # assert_template 'profiles/edit' # なぜかNG
    # セッションをテストでうまく扱わないと、
    #     def enough_authority
    # のなかに書かれている current_user.id を処理出来ない・・・
    # patch user_profile_path(@admin),
    #   user: { proud_tactics:  "右玉",
    #           favorite_kishi: "羽生先生",
    #           appeal:         "振飛車党です" }
    # get edit_user_profile_path(@admin)
    # assert_not 'text', '右玉'
    # assert_not 'text', '羽生先生'
    # assert_not 'text', '振飛車党です'
  end
end
