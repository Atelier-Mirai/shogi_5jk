# require 'test_helper'
#
# class SiteLayoutTest < ActionDispatch::IntegrationTest
#
#   def setup
#     @admin = users(:taro)
#     @user  = users(:jiro)
#   end
#
#   # 検索ボタンは今実装していないので
#   # test "should be search button on top page" do
#   #   get root_path
#   #   assert_template 'welcome/index'
#   #   # assert_select 'button', '検索'
#   #
#   #   get topics_path # スレッド一覧
#   #   # assert_select 'ul.pagination' # スレッドの数によってはないときもある
#   #
#   #   get topic_path(1) # 右玉について
#   #   assert_select 'ul.pagination'
#   # end
#
#   test "not login user layout links" do
#     skip
#     get root_path
#     assert_template 'welcome/index'
#     assert_select 'a[href="/"]', root_path, count: 1
#     # assert_select 'a[href=?]', login_path
#     assert_select 'a[href="/"]', signup_path
#   end
#
#   test "login user layout links" do
#     skip
#     log_in_as(@user)
#     get root_path
#     assert_template 'welcome/index'
#     # assert_select 'a[href="/"]', root_path, count: 1
#     # assert_select 'a[href=?]', logout_path
#     assert_select 'a[href=?]', topic_path(12)
#     # http://localhost:3000/topics/12
#   end
#
#   test "login admin-user layout links" do
#     skip
#     log_in_as(@admin)
#     get root_path
#     assert_template 'welcome/index'
#     assert_select 'a[href="/"]', root_path, count: 1
#     assert_select 'a[href="/"]', user_path(@user)
#     assert_select 'a[href="/"]', edit_user_path(@user)
#     assert_select 'a[href="/"]', logout_path
#     # admin なら 利用者一覧が参照出来る
#     assert_select 'a[href="/"]', users_path
#   end
#
#   test "signup page title" do
#     # get signup_path
#     # assert_template 'users/new'
#     # assert_select 'title', full_title('利用者登録')
#   end
#
# end
