require 'test_helper'

class TopicsTest < ActionDispatch::IntegrationTest

  def setup
    @admin = users(:taro)
    @user = users(:taro)
    @new_topic = topics(:guest_ita)
  end

  test "successful create new topic" do
    log_in_as(@user)
    get new_topic_path
    assert_difference 'Topic.count', 1 do
      post topics_path, topic: { title: "new_title",
        name: @user.name, owner_id: @user.id }
    end
  end

  test "successful destroy topic" do
    log_in_as(@admin)
    assert_difference 'Topic.count', -1 do
      delete topic_path(1)
    end
  end

  # test "successful edit" do
  #   log_in_as(@user)
  #   get edit_topic_path(2)
  #   # assert_template 'users/edit'
  #   patch topic_path(2),
  #     topic: { title: "foobar" }
  #   assert_template 'topics/edit'
  # end

#   test "should follow a user the standard way" do
#     assert_difference '@user.following.count', 1 do
#       post relationships_path, followed_id: @other.id
#     end
#   end
#
# content = "This micropost really ties the room together"
# picture = fixture_file_upload('test/fixtures/rails.png', 'image/png')
# assert_difference 'Micropost.count', 1 do
#  post microposts_path, micropost: { content: content, picture: FILL_IN }
# end

  # test "unsuccessful edit" do
  #   log_in_as(@user)
  #   get edit_topic_path(1)
  #   # assert_template 'users/edit'
  #   patch topic_path(1),
  #     topic: { title: "foobar" }
  #   assert_template 'topics/edit'
  # end

  # test "successful edit with friendly forwarding" do
  #   get edit_user_path(@user)
  #   # フレンドリーフォワーディングの確認　
  #   assert_equal edit_user_url(@user), session[:forwarding_url]
  #   log_in_as(@user)
  #   assert_redirected_to edit_user_path(@user)
  #   name = "Foo Bar"
  #   email = "foo@bar.com"
  #   patch user_path(@user),
  #     user: { name: name,
  #             email: email,
  #             password: "",
  #             password_confirmation: "" }
  #   assert_not flash.empty?
  #   assert_redirected_to @user
  #   @user.reload
  #   assert_equal name, @user.name
  #   assert_equal email, @user.email
  # end
end
