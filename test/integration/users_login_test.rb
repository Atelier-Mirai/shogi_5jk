require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:taro) # test/fixtures/users.yml の users
  end

  test 'login with invalid information' do
    get login_path
    assert_template 'sessions/new'
    post login_path, params: { session: { email: '', password: '' } }
    assert_template 'sessions/new'
    assert_not flash.empty?

    get root_path
    assert flash.empty?
  end

  test "login with valid information followed by logout" do
    skip
    # ログイン用のパスを開く
    # セッション用パスに有効な情報をpostする
    # ログイン用リンクが表示されなくなったことを確認する
    # ログアウト用リンクが表示されていることを確認する
    # プロフィール用リンクが表示されていることを確認する
    get login_path
    post login_path, params: { session: { name: @user.name, password: 'ke927dzx' } }
    assert is_logged_in_user?
    assert_redirected_to @user
    follow_redirect!
    assert_template 'users/show'
    assert_select "a[href=?]", login_path,       count: 0
    assert_select "a[href=?]", logout_path,      count: 1
    assert_select "a[href=?]", user_path(@user)
    # ログアウト
    delete logout_path
    assert_not is_logged_in_user?
    assert_redirected_to root_url
    # 二番目のウィンドウでログアウトするユーザーのシミュレート
    delete logout_path
    follow_redirect!
    assert_select "a[href=?]", login_path,       count: 1
    assert_select "a[href=?]", logout_path,      count: 0
    assert_select "a[href=?]", user_path(@user), count: 0
  end

  test "login with remembering" do
    skip
    log_in_as(@user, remember_me: '1')
    assert_not_nil cookies['remember_token']
    assert_equal cookies['remember_token'], assigns(:user).remember_token
  end

  test 'login without remembering' do
    log_in_as(@user, remember_me: '0')
    assert_nil cookies['remember_token']
  end
end
