require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  include ApplicationHelper

  def setup
    @user = users(:taro)
  end

  test 'profile display' do
    get user_profile_path(@user)
    # assert_template 'users/show'
    assert_select 'title', full_title(@user.name)
    # <h1>
    #   <a class="avatar-link" href="/users/47/profile">
    #     <img alt="zenmaru" class="gravatar with_margins" src="https://secure.gravatar.com/avatar/e95959730e80f5219e9031c59593ed24?s=48" width="48" height="48">
    #     zenmaru
    #   </a>
    #   自己紹介
    # </h1>
    # assert_select 'a', text: @user.name
    assert_match  @user.name, response.body
  end
end
