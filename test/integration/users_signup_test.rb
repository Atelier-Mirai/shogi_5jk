# require 'test_helper'
#
# class UsersSignupTest < ActionDispatch::IntegrationTest
#
#   def setup
#     ActionMailer::Base.deliveries.clear
#   end
#
#   test "invalid signup information" do
#     skip
#     get signup_path # 無効化しているので
#     assert_no_difference 'User.count' do
#       post users_path, user: { name: "",
#                                email: "user@invalid",
#                                password: "ke927dzx",
#                                password_confirmation: "bar" }
#     end
#     assert_template 'users/new'
#     assert_select 'div.alert-danger'
#   end
#
#   test "valid signup information with account activation" do
#     skip
#     get signup_path
#     assert_difference 'User.count', 1 do
#       post users_path, user: { name: "Example User",
#                                email: "user@example.com",
#                                password: "ke927dzx",
#                                password_confirmation: "ke927dzx" }
#     end
#     assert_equal 1, ActionMailer::Base.deliveries.size
#     user = assigns(:user)
#     assert_not user.activated?
#     # 有効化していない状態でログインしてみる
#     log_in_as(user)
#     assert_not is_logged_in_user?
#     # 有効化トークンが不正な場合
#     get edit_account_activation_path("invalid token")
#     assert_not is_logged_in_user?
#     # トークンは正しいが、メールアドレスが不正な場合
#     get edit_account_activation_path(user.activation_token, email: 'wrong')
#     assert_not is_logged_in_user?
#     # 有効化トークンが正しい場合
#     get edit_account_activation_path(user.activation_token, email: user.email)
#     assert user.reload.activated?
#     follow_redirect!
#     assert_template 'users/show'
#     assert is_logged_in_user?
#     assert_select 'div.alert-success'
#   end
# end
