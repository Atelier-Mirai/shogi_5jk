require 'test_helper'

# メール本文に日本語が入っていると、テストがうまくいかない
class UserMailerTest < ActionMailer::TestCase
  test 'account_activation' do
    user = users(:taro)
    user.activation_token = User.new_token
    mail = UserMailer.account_activation(user)
    # assert_equal "利用者登録 有効化のご案内", mail.subject
    # assert_equal 'ISO-2022-JP', mail.charset
    assert_equal 'UTF-8', mail.charset
    # assert_equal "利用者登録 有効化のご案内", mail.subject
    assert_equal [user.email],                mail.to
    assert_equal ['5JK.Shogi@gmail.com'],     mail.from
    # assert_match user.name,                   mail.body.encoded
    # assert_match user.activation_token,       mail.body.encoded
    # assert_match CGI::escape(user.email),     mail.body.encoded
  end

  test 'password_reset' do
    user = users(:taro)
    user.reset_token = User.new_token
    mail = UserMailer.password_reset(user)
    # assert_equal "パスワード再設定のご案内", mail.subject
    assert_equal [user.email], mail.to
    assert_equal ['5JK.Shogi@gmail.com'], mail.from
    # assert_match user.reset_token, mail.body.encoded
    # assert_match CGI::escape(user.email), mail.body.encoded
  end
end
