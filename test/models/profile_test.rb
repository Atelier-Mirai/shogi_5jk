require 'test_helper'

class ProfilesTest < ActiveSupport::TestCase
  def setup
    @user    = users(:taro)
    @profile = Profile.create(proud_tactics: "右玉",
                              favorite_kishi: "大山先生",
                              appeal: "右玉好きです",
                              user_id: @user.id)
  end

  test 'should be profile valid' do
    assert @profile.valid?
  end

  test 'should be profile invalid user_id = nil' do
    @profile.user_id = nil
    assert_not @profile.valid?
  end
end
