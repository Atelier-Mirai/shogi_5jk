require 'test_helper'

class PokerTest < ActiveSupport::TestCase
  def setup
    @CLUB         = 0b00
    @DIAMOND      = 0b01
    @HEART        = 0b10
    @SPADE        = 0b11

    @straight_flush  = 0b1000_1000_1000_1000_1000_0000_0000_0000_0000_0000_0000_0000_0000
    @four_of_a_kind  = 0b1111_1000_0000_0000_0000_0000_0000_0000_0000_0000_0000_0000_0000
    @full_house      = 0b0000_1110_0000_0000_0000_1100_0000_0000_0000_0000_0000_0000_0000
    @flush           = 0b0000_0100_0000_0100_0100_0100_0000_0100_0000_0000_0000_0000_0000
    @straight        = 0b0000_0000_0000_1000_0100_0001_0001_0001_0000_0000_0000_0000_0000
    @straight_wheel  = 0b1000_0000_0000_0000_0000_0000_0000_0000_0000_0100_0010_0001_0001
    @three_of_a_kind = 0b0000_1011_0000_0000_0100_0100_0000_0000_0000_0000_0000_0000_0000
    @two_pair        = 0b0000_0000_1000_0000_0000_0000_0000_0000_0000_1001_0000_1010_0000
    @one_pair        = 0b0000_0000_0000_0000_0000_0000_0000_0000_1000_1000_0100_0000_0011
    @high_cards      = 0b1000_0000_1000_0000_0100_0010_0001_0000_0000_0000_0000_0000_0000
  end

  test "prity(card)" do
    assert_equal "A♠ K♠ Q♠ J♠ 10♠ ", Poker.prity(@straight_flush)
    assert_equal "A♠ A♡ A♢ A♣ K♠ ",  Poker.prity(@four_of_a_kind)
    assert_equal "K♠ K♡ K♢ 9♠ 9♡ ",  Poker.prity(@full_house)
    assert_equal "K♡ J♡ 10♡ 9♡ 7♡ ", Poker.prity(@flush)
    assert_equal "J♠ 10♡ 9♣ 8♣ 7♣ ", Poker.prity(@straight)
    assert_equal "5♡ 4♢ 3♣ 2♣ A♠ ",  Poker.prity(@straight_wheel)
    assert_equal "K♠ K♢ K♣ 10♡ 9♡ ", Poker.prity(@three_of_a_kind)
    assert_equal "Q♠ 5♠ 5♣ 3♠ 3♢ ",  Poker.prity(@two_pair)
    assert_equal "6♠ 5♠ 4♡ 2♢ 2♣ ",  Poker.prity(@one_pair)
    assert_equal "A♠ Q♠ 10♡ 9♢ 8♣ ", Poker.prity(@high_cards)
  end

  # test "Flush" do
  #   assert_equal @SPADE, Poker.flush(@flush)
  #   assert_nil           Poker.flush(@one_pair)
  # end
  #
  # test "Straight" do
  #   assert_equal 'A', Poker.straight(@straight)
  #   assert_equal 'A', Poker.straight(@straight_flush)
  #   assert_equal '5', Poker.straight(@straight_wheel)
  #   assert_nil        Poker.straight(@high_cards)
  #   assert_nil        Poker.straight(@one_pair)
  # end
  #
  # test "Four of a kind" do
  #   assert_equal 'A', Poker.four_of_a_kind(@four_of_a_kind)
  #   assert_nil        Poker.four_of_a_kind(@three_of_a_kind)
  # end
  #
  # test "Full house or Three of a kind" do
  #   assert_equal 'AK', Poker.full_house_or_three_of_a_kind(@full_house)
  #   assert_equal '2',  Poker.full_house_or_three_of_a_kind(@three_of_a_kind)
  #   assert_nil         Poker.full_house_or_three_of_a_kind(@four_of_a_kind)
  # end
  #
  # test "One Pair / Two Pair" do
  #   assert_equal 'AK', Poker.two_or_one_pair(@two_pair)
  #   assert_equal 'A',  Poker.two_or_one_pair(@one_pair)
  #   assert_nil         Poker.two_or_one_pair(@three_of_a_kind)
  # end
end
