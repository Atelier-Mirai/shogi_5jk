require "application_system_test_case"

class RatingDistributionsTest < ApplicationSystemTestCase
  setup do
    @rating_distribution = rating_distributions(:one)
  end

  test "visiting the index" do
    visit rating_distributions_url
    assert_selector "h1", text: "Rating Distributions"
  end

  test "creating a Rating distribution" do
    visit rating_distributions_url
    click_on "New Rating Distribution"

    click_on "Create Rating distribution"

    assert_text "Rating distribution was successfully created"
    click_on "Back"
  end

  test "updating a Rating distribution" do
    visit rating_distributions_url
    click_on "Edit", match: :first

    click_on "Update Rating distribution"

    assert_text "Rating distribution was successfully updated"
    click_on "Back"
  end

  test "destroying a Rating distribution" do
    visit rating_distributions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Rating distribution was successfully destroyed"
  end
end
