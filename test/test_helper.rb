ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'
require 'minitest/reporters'
Minitest::Reporters.use!

class ActiveSupport::TestCase
  # Run tests in parallel with specified workers
  # parallelize(workers: :number_of_processors)
  parallelize(workers: 1)

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  include ApplicationHelper

  # テストユーザーがログインしていればtrueを返す
  def is_logged_in_user?
    !session[:user_id].nil?
  end

  # テストユーザーとしてログインする
  def log_in_as(user, options = {})
    password    = options[:password] || 'ke927dzx'
    remember_me = options[:remember_me] || '1'
    if integration_test?
      post login_path, params: { session: { email: user.email,
                                            password: password,
                                            remember_me: remember_me } }
    else
      session[:user_id] = user.id
    end
  end

  private

  # 統合テスト内ではtrueを返す
  def integration_test?
    defined?(post_via_redirect)
  end
end

# https://y-yagi.tumblr.com/post/165708828990/rubyのwarningから不要なwarningを除外する
module Warning
  def warn(str)
    return if str.match?("gems")

    super
  end
end
$VERBOSE = true

# module LoginHelper
#   def login_as(user)
#     post login_url(email: user.email, password: 'pass')
#   end
# end
#
# class ActionDispatch::IntegrationTest
#   include LoginHelper
# end
